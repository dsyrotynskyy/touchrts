using UnityEngine;
using System.Collections;


/* !!! - NOMES VARIAVEIS SHADERS BUILTIN - !!!
 * 
 *  "_MainTex"
 *  "_BumpMap"
 *  "_Cube"
 *  
 *  - O nome est� la em cima no shader (Properties), a esquerda do nome que aparece no Inspector. 
 *    Ex: ---> _MainTex <--- ("Base (RGB)", 2D) = "white" {}
*/


#region UVScrollerData

/// <summary>
/// 
/// </summary>
[System.Serializable]
public class UVScrollerUVData
{
    /// <summary>
    /// 
    /// </summary>
    public string propertyName = "_MainTex";
    public Vector2 direction;

    /// <summary>
    /// 
    /// </summary>
    private Vector2 _currentOffset;
    public Vector2 currentOffset
    {
        get { return _currentOffset; }
        set { _currentOffset = value; }
    }
}

#endregion


/// <summary>
/// 
/// </summary>
public class UVScroller : MonoBehaviour
{
    #region Inspector Data

    public UVScrollerUVData[] uv;

    #endregion

    #region Private Data

    private Material _thisMaterial;

    #endregion

    /// <summary>
    /// 
    /// </summary>
	void Start () 
    {
        _thisMaterial = transform.renderer.material;

        for (int i = 0; i < uv.Length; i++)
        {
            uv[i].currentOffset += _thisMaterial.GetTextureOffset(uv[i].propertyName);
        }
	}
	
    /// <summary>
    /// 
    /// </summary>
	void Update () 
    {
        for (int i = 0; i < uv.Length; i++)
        {
            uv[i].currentOffset += uv[i].direction * Time.deltaTime;
            _thisMaterial.SetTextureOffset(uv[i].propertyName, uv[i].currentOffset);
        }
	}
}
