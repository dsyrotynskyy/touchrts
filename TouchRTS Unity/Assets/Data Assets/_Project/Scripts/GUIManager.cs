using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

    public UIPanel intro, mainMenu;

    public GameObject games;

    public UICamera uiCamera;

    public enum UIScreens
    { 
        Intro, MainMenu, Games, Academy, Papers    
    }

    public UIScreens currentScreen;

    public TweenColor fadeInMainMenu;

    UIWidget[] mWidgets;

    float currentAlpha;

    private UIButton gameBt;

    void Start()
    {
        SetAlpha(0);

        fadeInMainMenu.Toggle();

        uiCamera.useMouse = false;

        InvokeRepeating("FadeIn", 0, 0.01f);
    }

    /// <summary>
    /// Set the alpha of all widgets.
    /// </summary>
    void SetAlpha(float val)
    {
        mWidgets = mainMenu.GetComponentsInChildren<UIWidget>();
        for (int i = 0, imax = mWidgets.Length; i < imax; ++i)
        {
            UIWidget w = mWidgets[i];
            Color c = w.color;
            c.a = val;
            w.color = c;
        }
    }
	
	// Update is called once per frame
	void Update () 
	{
     
	}

    public void GameScreen(GameObject p_button)
    {
        fadeInMainMenu.duration = 1;
        fadeInMainMenu.Toggle();
          
        gameBt = p_button.GetComponent<UIButton>();
        gameBt.enabled = false;

        Debug.Log("GameScreen");

        uiCamera.useMouse = false;

        InvokeRepeating("FadeOut", 0, 0.01f);
        
        mainMenu.animation.Play("MainMenuFadeOut");
        StartCoroutine(DisableScreen(mainMenu));
    }

    public void MainMenuScreen(GameObject p_button)
    {
        fadeInMainMenu.duration = 2;
        fadeInMainMenu.gameObject.SetActiveRecursively(true);
        fadeInMainMenu.Toggle();

        gameBt.enabled = true;
        games.gameObject.SetActiveRecursively(false);
        currentScreen = UIScreens.MainMenu;
        mainMenu.gameObject.SetActiveRecursively(true);

        uiCamera.useMouse = false;
        InvokeRepeating("FadeIn", 0, 0.01f);
        mainMenu.animation.Play("MainMenuFadeIn");
    }

    private IEnumerator DisableScreen(UIPanel p_panel)
    {
        yield return new WaitForSeconds(1);

        
        games.gameObject.SetActiveRecursively(true);
        currentScreen = UIScreens.Games;
        p_panel.gameObject.SetActiveRecursively(false);

        yield return new WaitForSeconds(0.1f);

        fadeInMainMenu.gameObject.SetActiveRecursively(false);
    }


    public void FadeIn()
    {
        if (currentAlpha > 1)
        {
            CancelInvoke("FadeIn");
            uiCamera.useMouse = true;
            return;
        }
        currentAlpha += Time.deltaTime;
        SetAlpha(currentAlpha);
    }

    public void FadeOut()
    {
        if (currentAlpha <= 0)
        {
            CancelInvoke("FadeOut");
            uiCamera.useMouse = true;
            return;
        }
        currentAlpha -= Time.deltaTime;
        SetAlpha(currentAlpha);
    }


    public void SoundToggle(GameObject p_button)
    {
        AudioListener.volume = AudioListener.volume * -1;
        UIButton bt = p_button.GetComponent<UIButton>();
        UISprite sprite = bt.tweenTarget.GetComponent<UISprite>();

        if (AudioListener.volume == 1)
            sprite.sprite = sprite.atlas.GetSprite("sound");
        else
            sprite.sprite = sprite.atlas.GetSprite("soundOff");
    }

}
