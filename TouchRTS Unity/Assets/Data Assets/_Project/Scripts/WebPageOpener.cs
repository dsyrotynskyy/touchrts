using UnityEngine;
using System.Collections;

public class WebPageOpener : MonoBehaviour
{

    public string pageLink;

    void OnClick() { if (enabled) Send(); }

    void Send()
    {
        if (string.IsNullOrEmpty(pageLink)) return;

        //Application.OpenURL(pageLink);
        Application.ExternalEval("window.open('" + pageLink + " ','_blank')");
    }

}
