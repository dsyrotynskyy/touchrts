using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Draw a dialog box to edit import settings
/// </summary>
public class AudioImportDialog : EditorWindow
{
    public static AudioImporterFormat audioFormat = AudioImporterFormat.Compressed;
    public static bool threeD;
    public static bool forceMono;
    //public static AudioImporterLoadType loadType;
    public static bool hardwareDecoding;

    public static bool changeCompression;
    public static int compression = 156;

    static List<Object> modelList;

    static int modelCount;

    [MenuItem("Assets/Importer/Import Audios")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        AudioImportDialog window = (AudioImportDialog)EditorWindow.GetWindow(typeof(AudioImportDialog));

        modelList = SelectionHelper.GetAssetsInSelection(UnityType.AUDIO_CLIP, typeof(AudioClip));

        modelCount = modelList.Count;

        window.Show();

        SelectionHelper.Deselect();
    }

    void OnGUI()
    {
        GUILayout.Label("Choose import settings:", EditorStyles.boldLabel);

        GUI.BeginGroup(new Rect(25, 5, 500, 300));

        // Format   
        audioFormat = (AudioImporterFormat)EditorGUILayout.EnumPopup("Audio Format:", audioFormat, GUILayout.ExpandWidth(false), GUILayout.Width(200));

        //3D sound
        threeD = GUILayout.Toggle(threeD, "3D Sound");

        //Force to be Mono
        forceMono = GUILayout.Toggle(forceMono, "Force to mono");

        // Decompress Sound on Load
        //loadType = (AudioImporterLoadType)EditorGUILayout.EnumPopup("Load Type", loadType);

        // Hardware Decoding
        hardwareDecoding = GUILayout.Toggle(hardwareDecoding, "Hardware Decoding");

        // Compression Level
        changeCompression = EditorGUILayout.BeginToggleGroup("Change Compression?", changeCompression);
        compression = EditorGUILayout.IntSlider("Compression", compression, 45, 500, GUILayout.ExpandWidth(false), GUILayout.Width(300));

        EditorGUILayout.EndToggleGroup();

        GUI.EndGroup();


        GUI.BeginGroup(new Rect(50, 20, 200, 300));
        try
        {
            if (modelList == null || modelList.Count <= 0)
                GUILayout.Label("No sounds selected!!!", EditorStyles.boldLabel);
            else
            {
                if (GUILayout.Button("Import", GUILayout.Width(100), GUILayout.Height(40)))
                {
                    if (AudioBatchImporter.ImportAudios(modelList))
                    {
                        EditorUtility.DisplayDialog("Notify", "Audio(s) successfully imported", "Close");
                        this.Close();
                    }
                }
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex);
            GUILayout.Label("No sounds selected!!!", EditorStyles.boldLabel);
        }


        GUILayout.Label("Selected audios: " + modelCount, EditorStyles.boldLabel);


        GUI.EndGroup();
    }

 
}
