using UnityEngine;
using UnityEditor;
using System.Collections.Generic;


/// <summary>
/// Import Audios with a specific configuration
/// </summary>
public class AudioBatchImporter
{
    /// <summary>
    /// Import AudioClip from selected Folder(s)
    /// </summary>
    /// <returns></returns>
    public static bool ImportAudios(List<Object> audioList)
    {
        string path;

        AudioImporter audioImporter;

        foreach (AudioClip audioClip in audioList)
        {
            // Get file path
            path = AssetDatabase.GetAssetPath(audioClip.GetInstanceID());
            // Get the AudioImporter
            audioImporter = AssetImporter.GetAtPath(path) as AudioImporter;

            //Set new import configurations                
            SetImportCfg(audioImporter);
                            
            //Force import
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }
        return true;
    }


    /// <summary>
    /// Set new import configuration
    /// </summary>
    /// <param name="audioImporter">The audio to be imported</param> 
    private static void SetImportCfg(AudioImporter audioImporter)
    {
        //Audio Importer Settings        
        audioImporter.format = AudioImportDialog.audioFormat;
        audioImporter.threeD = AudioImportDialog.threeD;
        audioImporter.forceToMono = AudioImportDialog.forceMono;
        //audioImporter.loadType = AudioImportDialog.loadType;
        audioImporter.compressionBitrate = AudioImportDialog.compression * 1000;

    }

}
