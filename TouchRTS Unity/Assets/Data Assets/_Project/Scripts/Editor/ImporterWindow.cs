﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class ImporterWindow : EditorWindow
{


    protected string GetAssetFolder(Object p_asset)
    {
        string s = AssetDatabase.GetAssetPath(p_asset);
        string[] spt = s.Split('/');
        return string.Join("/", spt, 0, spt.Length - 1) + "/";
    }

    protected void GetAssetFileData(Object p_asset,ref string p_name,ref string p_extension)
    {
        string s = AssetDatabase.GetAssetPath(p_asset);
        string[] spt = s.Split('/');        
        p_name = spt[spt.Length - 1];
        spt = p_name.Split('.');
        p_name = spt[0];
        p_extension = spt[spt.Length - 1];
    }

    protected string GetAssetFileName(Object p_asset)
    {
        string n="";
        string e="";
        GetAssetFileData(p_asset, ref n, ref e);
        return n + "." + e;
    }

    protected string GetUnityAssetPath(string p_system_path)
    {        
        string[] spt;
        int i;
        spt = p_system_path.Split('/');
        for (i = 0; i < spt.Length; i++) if (spt[i].IndexOf("Assets") >= 0) break;
        if (i >= spt.Length) return "";
        return string.Join("/", spt, i, spt.Length - i);
    }

    protected string GetFullPath(Object p_asset)
    {
        string app_path = Application.dataPath + "/";
        string s = AssetDatabase.GetAssetPath(p_asset);
        string[] spt = s.Split('/');

        return app_path + string.Join("/", spt, 1, spt.Length - 1);
    }
    

}

