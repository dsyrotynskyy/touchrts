﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Copy each property and Field from one component to another
/// </summary>
public class CopyInspector : Editor
{
    static System.Type m_OriginalType;
    static Dictionary<PropertyInfo, object> m_Values;
    static Dictionary<FieldInfo, object> m_FieldValues;

    /// <summary>
    /// Get component properties filtering ordinary GO components (Renderer, gameobject, AudioSource)
    /// </summary>
    /// <param name="component">The Component to get properties</param>
    /// <returns>All component Properties</returns>
    private List<PropertyInfo> GetProperties(Component component)
    {
        List<string> ignoredProperties;
        List<PropertyInfo> properties;

        properties = new List<PropertyInfo>();
        ignoredProperties = new List<string>();

        //Ignore properties of Component Type
        foreach (PropertyInfo propertyInfo in typeof(Component).GetProperties())
        {            
            ignoredProperties.Add(propertyInfo.Name);
        }

        foreach (PropertyInfo propertyInfo in component.GetType().GetProperties())
        {
            if (ignoredProperties.Contains(propertyInfo.Name))
            {
                continue;
            }
            properties.Add(propertyInfo);
        }

        return properties;
    }

    /// <summary>
    /// Get component fields or variables
    /// </summary>
    /// <param name="component"></param>
    /// <returns></returns>
    public static List<FieldInfo> GetFields(Component component)
    {
        List<FieldInfo> fields = new List<FieldInfo>();
        
        foreach (FieldInfo field in component.GetType().GetFields())
        {
            ////Debug.Log(field.Name);
            fields.Add(field);
        }

        return fields;
    }

    /// <summary>
    /// Get component fields or variables
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static List<FieldInfo> GetFields(object obj)
    {
        List<FieldInfo> fields = new List<FieldInfo>();

        foreach (FieldInfo field in obj.GetType().GetFields())
        {
            ////Debug.Log(field.Name);
            fields.Add(field);
        }

        return fields;
    }

    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        OnCopyInspectorGUI();
    }

    public void OnCopyInspectorGUI()
    {
        bool enabled;
        
        Component component;
        
        component = target as Component;

        if (component == null)
        {
            return;
        }

        GUILayout.Space(10.0f);

        Color backgroundColor = GUI.backgroundColor;

        GUI.backgroundColor = new Color(0.1f, 0.8f, 0.8f);

        GUILayout.BeginVertical(EditorStyles.toolbarPopup);

        GUI.backgroundColor = backgroundColor;

        GUILayout.BeginHorizontal();

        GUILayout.Space(10.0f);

        GUILayout.Label("Copied: " + (m_OriginalType != null ? m_OriginalType.Name : "Nothing"), EditorStyles.miniLabel);

        GUILayout.FlexibleSpace();

        if (GUILayout.Button(new GUIContent("Copy", "Copy component values"), EditorStyles.miniButton))
        {
            CopyComponent(component);   
        }

        enabled = GUI.enabled;
        GUI.enabled = target.GetType() == m_OriginalType;

        GUILayout.Space(10.0f);

        if (GUILayout.Button(new GUIContent("Paste", "Paste component values"), EditorStyles.miniButton))
        {
            PasteComponent(component);            
        }

        GUILayout.Space(10.0f);

        GUI.enabled = enabled;

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.Space(-2.0f);
    }

    /// <summary>
    /// Paste entire script variables
    /// </summary>
    /// <param name="component"></param>
    private void PasteComponent(Component component)
    {
        List<PropertyInfo> properties;
        List<FieldInfo> fields;

        properties = GetProperties(component);
        foreach (PropertyInfo property in properties)
        {
            if (!property.CanWrite)
            {
                continue;
            }

            if (property.Name == "parent")
            {
                continue;
            }

            property.SetValue(component, m_Values[property], null);
        }

        fields = GetFields(component);
        foreach (FieldInfo field in fields)
        {            
            field.SetValue(component, m_FieldValues[field]);
        }
    }

    /// <summary>
    /// Copy component script fields and properties
    /// </summary>
    /// <param name="component"></param>
    private void CopyComponent(Component component)
    {
        List<PropertyInfo> properties;
        List<FieldInfo> fields;

        m_OriginalType = target.GetType();

        properties = GetProperties(component);
        fields = GetFields(component);

        m_Values = new Dictionary<PropertyInfo, object>();
        foreach (PropertyInfo property in properties)
        {
            m_Values[property] = property.GetValue(component, null);
        }

        m_FieldValues = new Dictionary<FieldInfo, object>();
        foreach (FieldInfo field in fields)
        {
            m_FieldValues[field] = field.GetValue(component);
        }
    }
}