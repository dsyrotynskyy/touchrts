using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;


/// <summary>
/// Retrieve selection objects filtering by a type
/// </summary>
public class SelectionHelper
{
    
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    static public List<T> GetAssetsInSelection<T>()
    { 
        List<T> objList = new List<T>();


        foreach (UnityEngine.Object obj in Selection.objects)
        {
            if (obj is T)
            {
                objList.Add(((T)(object)obj));
            }
        }
        return objList;
    }




    /// <summary>
    /// Search in the inspector active selection for a list of a specific Type
    /// It could be a folder, a single file or multiples files
    /// </summary>
    /// <param name="p_type">Type name</param>
    /// <returns>The Object List of one Type only</returns>
    static public List<Object> GetAssetsInSelection(string p_type, System.Type type)
    {
        string assetType;
        UnityEngine.Object obj;
        List<UnityEngine.Object> objList = new List<Object>();

        for (int i = 0; i < Selection.objects.Length; i++)
        {
            obj = Selection.objects[i];

            //Check asset type
            assetType = obj.GetType().ToString();                

            if (string.Compare(assetType, p_type) == 0)
            {
                if (objList.Contains(obj))
                    continue;
                objList.Add(obj);
            }
            else if (string.Compare(assetType, UnityType.OBJECT) == 0)
            {                    
                Object[] auxObjList = Selection.GetFiltered(type, SelectionMode.DeepAssets);

                foreach (Object o in auxObjList)
                    objList.Add(o);
            }
        }

        return objList;
    }

    /// <summary>
    /// Remove selection in the inspector
    /// </summary>
    public static void Deselect()
    {
        Selection.activeObject = null;
    }
}


/// <summary>
/// Class used to store Type names
/// </summary>
public class UnityType
{		
    public const string TEXTURE2D = "UnityEngine.Texture2D";
    public const string AUDIO_CLIP = "UnityEngine.AudioClip";
    public const string FONT = "UnityEngine.Font";
    public const string OBJECT = "UnityEngine.Object";
    public static string GAMEOBJECT = "UnityEngine.GameObject";
	public static string MODEL = "UnityEngine.ModelImporter";
}

