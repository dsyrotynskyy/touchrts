using UnityEngine;
using System.Collections;

public class NavigationBar : MonoBehaviour
{

    public UIDraggablePanel scrollview;
    public UISlicedSprite mask;
    public UIButton arrowButton;

    public AudioClip inAudioClip, offAudioClip;

    private float arrowCurrentAlpha;
    private TweenPosition tweenPosition;
    private bool visible;

    void Awake()
    {
        tweenPosition = GetComponent<TweenPosition>();
        transform.localPosition = new Vector3(transform.localPosition.x, 105, transform.localPosition.z);

        arrowCurrentAlpha = 1;
    }

    void Update()
    {
        if (visible)
        {
            if (Input.mousePosition.y < Screen.height - Screen.height * 0.172f)
            {
                mask.enabled = visible = false;
                InvokeRepeating("FadeIn", 0, 0.01f);
                //arrowButton.gameObject.SetActiveRecursively(true);
                tweenPosition.Toggle();
                NGUITools.PlaySound(offAudioClip, 0.2f);
            }
        }
        else
        {
            if (Input.mousePosition.y > Screen.height - Screen.height * 0.072f)
            {
                mask.enabled = visible = true;
                InvokeRepeating("FadeOut", 0, 0.01f);
                //arrowButton.gameObject.SetActiveRecursively(false);
                tweenPosition.Toggle();
                NGUITools.PlaySound(inAudioClip, 0.2f);
            }
        }
    }

    public void RightArrow(GameObject p_button)
    {
        scrollview.Scroll(scrollview.scrollWheelFactor);
    }

    public void LeftArrow(GameObject p_button)
    {
        scrollview.Scroll(scrollview.scrollWheelFactor * -1);
    }

    public void ShowTooltip(GameObject p_button)
    {
        ProjectData projectData = p_button.GetComponent<ProjectData>();
	
    //    UITooltip.ShowImage(projectData.projectName, p_button.GetComponent<UIButton>().tweenTarget.GetComponent<UISlicedSprite>());
    }

    public void HideTooltip(GameObject p_button)
    {
        UITooltip.ShowText(null);
    }


    /// <summary>
    /// Set the alpha of all widgets.
    /// </summary>
    void SetAlpha(UIWidget[] widgets, float val)
    {
        for (int i = 0, imax = widgets.Length; i < imax; ++i)
        {
            UIWidget w = widgets[i];
            Color c = w.color;
            c.a = val;
            w.color = c;
        }
    }

    public void FadeIn()
    {
        if (arrowCurrentAlpha > 1)
        {
            CancelInvoke("FadeIn");
            return;
        }
        arrowCurrentAlpha += Time.deltaTime;
        SetAlpha(arrowButton.GetComponentsInChildren<UIWidget>(), arrowCurrentAlpha);
    }

    public void FadeOut()
    {
        if (arrowCurrentAlpha <= 0)
        {
            CancelInvoke("FadeOut");
            return;
        }
        arrowCurrentAlpha -= Time.deltaTime;
        SetAlpha(arrowButton.GetComponentsInChildren<UIWidget>(), arrowCurrentAlpha);
    }
}
