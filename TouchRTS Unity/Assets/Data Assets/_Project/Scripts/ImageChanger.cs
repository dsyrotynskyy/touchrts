using UnityEngine;
using System.Collections;

public class ImageChanger : MonoBehaviour
{

    public UISlicedSprite image;


    public void ChangeImage(GameObject p_button)
    {
        UISlicedSprite imageAux = p_button.GetComponent<UIButton>().tweenTarget.GetComponent<UISlicedSprite>();
        //Debug.Log(imageAux.sprite.name);
        image.sprite = imageAux.sprite;
        image.MarkAsChanged();
    }
}
