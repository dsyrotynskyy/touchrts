using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PanelChanger : MonoBehaviour
{

    public List<TweenPosition> panelList;

    public UISlicedSprite mask;

    public TweenColor fadeMask;

    public int counter;

    public float amountX;

    // Use this for initialization
    void Start()
    {
        panelList = new List<TweenPosition>(GetComponentsInChildren<TweenPosition>());

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (panelList[0].enabled)
                return;
            if (counter <= 0)
                return;

            counter--;

            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Reset();
                panelList[i].from = panelList[i].transform.localPosition;
                panelList[i].to = panelList[i].transform.localPosition + new Vector3(amountX, 0, 0);
                if (counter == i)
                    panelList[i].to.z = -40;
                else
                    panelList[i].to.z = 0;
                panelList[i].Play(true);
            }
        }


        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (panelList[0].enabled)
                return;
            if (counter >= panelList.Count - 1)
                return;

            counter++;

            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Reset();
                panelList[i].from = panelList[i].transform.localPosition;
                panelList[i].to = panelList[i].transform.localPosition + new Vector3(-amountX, 0, 0);
                if (counter == i)
                    panelList[i].to.z = -40;
                else
                    panelList[i].to.z = 0;
                panelList[i].Play(true);
            }
        }
    }

    public void ShowPanel(GameObject p_button)
    {
        if (mask.enabled == false)
            return;
        UITooltip.ShowText(null);
        ProjectData projectData = p_button.GetComponent<ProjectData>();

        if (counter == projectData.index)
            return;

        mask.enabled = false;
        int steps = Mathf.Abs(counter - projectData.index);
        StartCoroutine(EnableMask(1));

        if (counter > projectData.index)
        {
            counter = projectData.index;
            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Reset();
                panelList[i].from = panelList[i].transform.localPosition;
                panelList[i].to = panelList[i].transform.localPosition + new Vector3(amountX * (steps), 0, 0);
                if (counter == i)
                    panelList[i].to.z = -40;
                else
                    panelList[i].to.z = 0;
                panelList[i].Play(true);
            }
        }
        else
        {
            counter = projectData.index;
            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Reset();
                panelList[i].from = panelList[i].transform.localPosition;
                panelList[i].to = panelList[i].transform.localPosition + new Vector3(-amountX * (steps), 0, 0);
                if (counter == i)
                    panelList[i].to.z = -40;
                else
                    panelList[i].to.z = 0;

                panelList[i].Play(true);
            }
        }
    }

    private IEnumerator EnableMask(float p_time)
    {
        yield return new WaitForSeconds(p_time);

        if (Input.mousePosition.y < Screen.height - Screen.height * 0.172f)
        {
            mask.enabled = false;
        }
        else
        {
            mask.enabled = true;
        }
    }

    private void OnEnable()
    {
        fadeMask.Reset();
        fadeMask.Play(true);
    }
}
