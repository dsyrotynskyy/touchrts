using UnityEngine;
using System.Collections;

public class SoundSpec : MonoBehaviour {

    public float[] spectrum;
	
    public int numSamples;
    public GameObject[] cubes;
	
	public float updateTime, scaleTime;
	
	private float timer;
	private Vector3[]	targetScale;

	// Use this for initialization
	void Start () 
    {
		float x = - 9;
        cubes = new GameObject[numSamples];
        for (int i = 0; i < numSamples; i++)
        {
            cubes[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cubes[i].transform.position = transform.position + Vector3.right * x;
            cubes[i].transform.parent = transform;
            x += 0.5f;
        }
		
		targetScale = new Vector3[numSamples];
        	
    }
	
	// Update is called once per frame
	void Update () 
    {
		Vector3 scale;
		for (int i = 0; i < numSamples; i++)
        {
			scale = cubes[i].transform.localScale;
			scale = Vector3.Lerp(scale, targetScale[i], Time.deltaTime * scaleTime);	
			cubes[i].transform.localScale = scale;
		}
		
		timer += Time.deltaTime;
		
		if(timer > updateTime)
		{
			timer =0;
		}
		else
			return;
        
		spectrum = audio.GetOutputData(numSamples,0);
		
		int c = 0;
        for (int i = 0; i < numSamples; i++)
        {
            targetScale[i] = (Vector3.one * 0.2f) + Vector3.up * spectrum[c] * 50;
			c += 1;
        }

	}
}
