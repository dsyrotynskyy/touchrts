Shader "Mobile/Cheap Texture Color" {
Properties 
{
 	_Color ("Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader {
    Pass {
        Material { }
        Lighting Off
        Zwrite On
        Fog { Mode Off }
         SetTexture [_MainTex] {
             constantColor [_Color]
             Combine texture * constant, texture * constant
        }

    }
}
}