
Shader "Mobile/Cheap Texture" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader {
    Pass {
        Material { }
        Lighting Off
        Zwrite On
        Fog { Mode Off }
        SetTexture [_MainTex] {
            Combine texture 
        }

    }
}
}