using UnityEngine;
using System.Collections;

public class HeroRotate : MonoBehaviour {

	private float 	_speed = 0f;
	private float _maxSpeed = 480f;
	private float _speedRatio = 0.1f;
	private float _accelRatio = 0.125f;
	
	private bool _canRotate = false;
	private bool _touchWasBegin = false;
	
	private bool _buttonDown = false;
	
	
	public bool CanRotate
	{
		get { return _canRotate; } 
		
		private set { _canRotate = value; } 
	}

	void Update () {
		
		Vector3 position = Vector3.zero;
		Vector3 deltaPosition = Vector3.zero;
		
		bool _touchDetected = false;
		if (Input.touchCount > 0) {
			Touch touch = Input.touches [0];			
			position = touch.position;
			deltaPosition = touch.deltaPosition;
			_touchDetected = true;
		} else {
			if (Input.GetButtonDown("Fire1")) {
				_buttonDown = true;
			}
			if (Input.GetButtonUp("Fire1")) {
				_buttonDown = false;
			}
			if(_buttonDown) {
				position = Input.mousePosition;
				deltaPosition = new Vector3(Input.GetAxis("Mouse X"), 0, Input.GetAxis("Mouse Y")) * 10;
				_touchDetected = true;
			}
		}
		
		if(_touchDetected)
		{
			if(!_touchWasBegin) {
				if(VerifyTouch(position)) {
					CanRotate = true;
				}
			}
			else if(CanRotate){
				RotateObject(deltaPosition);
			}
			_touchWasBegin = true;
			
		}
		else {
			CanRotate = false;
			_touchWasBegin = false;
		}
		
		if(Mathf.Abs(_speed) < 5f) {
			_speed = 0f;
		}
		else {
			_speed *= Mathf.Pow(_accelRatio, Time.deltaTime);
		}
		float rotateAngle = _speed*Time.deltaTime;
		if((Mathf.Abs(transform.localRotation.eulerAngles.y) < 4 || Mathf.Abs(transform.localRotation.eulerAngles.y) >356)
			&& Mathf.Abs(_speed) < 15f) {
			rotateAngle = -transform.localRotation.eulerAngles.y;
			_speed = 0f;
		}
		transform.Rotate(new Vector3(0, rotateAngle,0), Space.World);

	}
	
	bool VerifyTouch(Vector3 position)
	{
		Ray ray = Camera.main.ScreenPointToRay(position);
        	RaycastHit hit ;
		
   		if (Physics.Raycast (ray, out hit)) 
		{
			if(hit.collider.gameObject == this.gameObject)
				return true;
		}
		return false;
	}
	
	void RotateObject(Vector3 deltaPosition)
	{
		_speed += -deltaPosition.x/Time.deltaTime * _speedRatio;
		if(Mathf.Abs(_speed) > _maxSpeed) {
			_speed = Mathf.Sign(_speed) * _maxSpeed;
		}
	}	
}
