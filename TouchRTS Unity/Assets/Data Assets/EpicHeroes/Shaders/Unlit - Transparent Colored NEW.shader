Shader "Unlit/Transparent Colored NEW"
{
	Properties
	{
		_TintColor ("Tint Color", Color) = (1, 1, 1, 1)
		_MainTex ("Base (RGB), Alpha (A)", 2D) = "white" {}
	}
	
	SubShader
	{
		LOD 100

		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}
		
		Pass
		{
			Cull Off
			Lighting Off
			ZWrite Off
			Fog { Mode Off }
			Offset -1, -1
			ColorMask RGB
			AlphaTest Greater .01
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMaterial AmbientAndDiffuse
			
						
			SetTexture [_MainTex]
			{
				constantColor [_TintColor]
				Combine texture * constant
			}
		}
	}
}