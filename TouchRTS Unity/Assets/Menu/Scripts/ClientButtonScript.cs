using UnityEngine;
using System.Collections;

public class ClientButtonScript : AbstractButtonScript
{
	public void Start () {
		Init ();
	}
	
	#region implemented abstract members of AbstractButtonScript
	protected override void DoActionLogic ()
	{
		menuSceneController.JoinServer ();
	}
	#endregion
	
	public override void Update () {
		this.IsClickable = menuSceneController.CanJoinServer;
		
		base.Update ();
	}
}

