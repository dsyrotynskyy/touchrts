using UnityEngine;
using System.Collections;
using Setup;

public class NStartServerDialog: MonoBehaviour
{	
	public GameObject StartServerButton;
	public GameObject CancelButton;
	
	private MenuSceneController sceneController;
	
	public void Start () {
		sceneController = FindObjectOfType (typeof (MenuSceneController)) as MenuSceneController;
		
		UIEventListener.Get (StartServerButton).onClick += delegate(GameObject go)
		{
			sceneController.StartBattleLevel ();
		};
		
		UIEventListener.Get (CancelButton).onClick += delegate(GameObject go)
		{
			sceneController.CancelServer ();
		};
	}
}

