using UnityEngine;
using System.Collections;
using Setup;

public class NGUIMenuController : MonoBehaviour
{
	private NBattleOptionsDialog battleOptionsDialog;
	private NMissionSelectionDialog missionSelectionDialog;
	protected MenuSceneController menuSceneController;
	
	protected bool IsClickable = true;
	
	public GameObject StartTutorialButton;
	public GameObject StartSingleBattleButton;
	public GameObject StartServerButton;
	public GameObject StartClientButton;
	public GameObject ExitGameButton;
	
	public Camera UICamera3D;
	public GameObject BattleOptionsDialog;
	public GameObject MissionSelectionDialog;
	public GameObject Blocker3DUI;
//	public GameObject StartServerDialog;
//	public GameObject ClientWaitServerDialog;
	
	public void Start () {
		menuSceneController = FindObjectOfType (typeof (MenuSceneController)) as MenuSceneController;

		this.missionSelectionDialog = MissionSelectionDialog.GetComponent<NMissionSelectionDialog> ();
		this.battleOptionsDialog = BattleOptionsDialog.GetComponent<NBattleOptionsDialog> ();
		
		this.missionSelectionDialog.Init ();
		this.battleOptionsDialog.Init ();
		
		this.PrepareButton ();
	}
	
	public void PrepareButton () {
		UIEventListener.Get (StartTutorialButton).onClick += delegate(GameObject go)
		{
			menuSceneController.LoadCampaignLevel ("Tutorial Scene 1");
		};
		
		UIEventListener.Get (StartSingleBattleButton).onClick += delegate(GameObject go)
		{
			this.DisplayChooseMapDialog (false);
		};
		
		UIEventListener.Get (StartServerButton).onClick += delegate(GameObject go)
		{
			this.DisplayChooseMapDialog (true);	
		};
		
		UIEventListener.Get (StartClientButton).onClick += delegate(GameObject go)
		{
			menuSceneController.JoinServer ();
		};
		
		UIEventListener.Get (ExitGameButton).onClick += delegate(GameObject go)
		{
			UnityEngine.Application.Quit ();
		};
	}
	
	// Update is called once per frame
	public void Update ()
	{	
	}
	
	public void OnMainMenuButtonClicked () {
		this.battleOptionsDialog.Hide ();
	}
	
	public void OnStartServer (string mapName) {
		DisplayBattleMapDialog (mapName);		
	}
	
	public void OnStartClient (string missionName) {
		DisplayBattleMapDialog (missionName);	
	}
	
	public void OnClientJoined () {
		this.battleOptionsDialog.UpdateClients ();
	}
	
	public void OnCancelNetwork ()
	{
//		StartServerDialog.SetActive (false);
//		ClientWaitServerDialog.SetActive (false);
	}

	public void DisplayBattleMapDialog (string levelToLoad)
	{
		if (!this.battleOptionsDialog.gameObject.activeSelf) {
			this.battleOptionsDialog.Display (levelToLoad);
		}		
	}

	public void DisplayChooseMapDialog (bool isServer)
	{
		this.missionSelectionDialog.Display (isServer);
	}
}

