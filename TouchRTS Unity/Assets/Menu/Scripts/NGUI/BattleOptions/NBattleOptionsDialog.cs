using UnityEngine;
using System.Collections.Generic;
using GameMapScenario;
using Setup;

public class NBattleOptionsDialog : MonoBehaviour
{
	public SingleMapScenario singleMapScenario = null;
	private MenuSceneController sceneController;
	private List<NMenuPlayerInfoPanel> playerInfoPanels = new List<NMenuPlayerInfoPanel> ();
	
	public GameObject StartBattleButton;
	public GameObject CancelBattleButton;
	public GameObject MapNameLabel;
	
	private UILabel mapNameLabel;
	
	public void Init () {		
		this.sceneController = FindObjectOfType(typeof (MenuSceneController)) as MenuSceneController;
		
		foreach (Transform trans in transform) {
			NMenuPlayerInfoPanel panel = trans.gameObject.GetComponent<NMenuPlayerInfoPanel> ();
			if (panel == null) {
				continue;
			}
			panel.Init (this);
			playerInfoPanels.Add (panel);
		}
		
		mapNameLabel = MapNameLabel.GetComponent<UILabel> ();
		
		UIEventListener.Get (StartBattleButton).onClick += delegate(GameObject go)
		{
			sceneController.StartBattleLevel ();
		};
		
		UIEventListener.Get (CancelBattleButton).onClick += delegate(GameObject go)
		{
			if (sceneController.IsServer) {
				sceneController.CancelServer ();
			}
			if (sceneController.IsClient) {
				sceneController.LeaveNetwork ();
			}
			this.gameObject.SetActive (false);
		};
	}
	
	public void Display (string mapName) {
		this.singleMapScenario = sceneController.SingleMapScenario;
		this.singleMapScenario.LevelName = mapName;
		this.mapNameLabel.text = mapName;
		
		this.gameObject.SetActive (true);
		
		this.singleMapScenario.CreateRandomPlayersData ();
			
		if (!sceneController.IsClient) {
			this.StartBattleButton.SetActive (true);
		} else {
			this.StartBattleButton.SetActive (false);
		}
		
		foreach (NMenuPlayerInfoPanel panel in playerInfoPanels) {
			panel.Display ();			
		}
	}
	
	public void UpdateAllPlayerInfoPanels () {
		foreach (NMenuPlayerInfoPanel panel in playerInfoPanels) {
			panel.UpdateFromPlayerData ();			
		}
	}
	
	public void UpdateClients () {
		foreach (NMenuPlayerInfoPanel panel in playerInfoPanels) {
			panel.UpdateClients ();			
		}
	}
	
	[RPC]
	public void ClientUpdatePlayerInfo (int id, string controllerName, int team, int money) {
		sceneController.UpdatePlayer (id, controllerName, team, money);
		this.UpdateAllPlayerInfoPanels ();	
	}
	
	[RPC]
	public void ServerUpdatePlayerInfo (int id, string controllerName, int team, int money) {
		sceneController.UpdatePlayer (id, controllerName, team, money);
		this.UpdateAllPlayerInfoPanels ();
		
		foreach (NMenuPlayerInfoPanel panel in playerInfoPanels) {
			panel.UpdateClients ();			
		}
	}
		
	public void Hide () {
		this.singleMapScenario = null;
		this.mapNameLabel.text = "";
	}
	
	public void Update () {
		this.gameObject.SetActive (singleMapScenario != null);
	}
}

