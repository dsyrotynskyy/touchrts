using UnityEngine;
using System.Collections;
using GameMapScenario;
using Setup;

public class NMenuPlayerInfoPanel : MonoBehaviour
{
	public const int MAX_PLAYER_MONEY = 10000;
	private MenuSceneController sceneController;
	private NBattleOptionsDialog owner;
	public int playerId;
	public GameObject PlayerNameLabel;
	public GameObject PlayerColorSelector;
	public GameObject PlayerControlType;
	public GameObject TeamPopulList;
	public GameObject PlayerMoneySlider;
	private UILabel playerNameLabel;
	private UIPopupList playerColorSelector;
	private UIPopupList playerControlType;
	private UIPopupList teamPopupList;
	private UISlider moneySlider;
	private Color color;
	private string controllerName;
	private int teamNumber;
	private int money;
	private SystemTimer updatePlayerDataTimer = SystemTimer.CreateFrequency (800);
	
	public void Init (NBattleOptionsDialog owner)
	{
		this.owner = owner;
		
		this.sceneController = FindObjectOfType (typeof(MenuSceneController)) as MenuSceneController;
		
		playerNameLabel = PlayerNameLabel.GetComponent<UILabel> ();
		playerColorSelector = PlayerColorSelector.GetComponent<UIPopupList> ();
		playerControlType = PlayerControlType.GetComponent<UIPopupList> ();
		teamPopupList = TeamPopulList.GetComponent<UIPopupList> ();
		moneySlider = PlayerMoneySlider.GetComponent<UISlider> ();
		
		this.moneySlider.numberOfSteps = 1000;
		
		playerControlType.onSelectionChange = OnControlTypeChanged;
		playerColorSelector.collider.enabled = false;
		teamPopupList.onSelectionChange = OnTeamChanged;
		moneySlider.onValueChange = OnMoneyChanged;
	}
	
	public void Display ()
	{
		this.playerControlType.collider.enabled = true;
		int numOfPlayers = ApplicationManager.Instance.battleMaps [owner.singleMapScenario.LevelName].NumOfPlayers;
		if (playerId > numOfPlayers) {
			this.gameObject.SetActive (false);
			return;
		} else {
			this.gameObject.SetActive (true);
		}
		
		playerNameLabel.text = "Player " + playerId;
		
		this.UpdateFromPlayerData ();
		
		if (sceneController.IsServer) {
			this.UpdateClients ();
		}
		
		playerControlType.items.Clear ();
		if (!sceneController.IsClient) {
			playerControlType.items.Add ("Human");
			playerControlType.items.Add ("Ai");
			playerControlType.items.Add ("Open");
			playerControlType.items.Add ("Closed");
		} else {
			playerControlType.items.Add ("Human");
			playerControlType.items.Add ("Open");
		}
	}
	
	public void OnControlTypeChanged (string selectedItem)
	{
		int i = 0;
		
		foreach (string item in this.playerControlType.items) {
			if (item == selectedItem) {
				if (sceneController.IsClient) {
					switch (i) {
					case (0) : 
						controllerName = ApplicationManager.Instance.DeviceName;
						break;
					case (1) : 
						controllerName = SingleMapScenario.OPEN_CONTROLLER;
						break;
					default : break;
					}
					break;
				} else {
					switch (i) {
					case (0) : 
						controllerName = ApplicationManager.Instance.DeviceName;
						break;
					case (1) : 
						controllerName = SingleMapScenario.AI_CONTROLLER;
						break;
					case (2) : 
						controllerName = SingleMapScenario.OPEN_CONTROLLER;
						break;
					case (3) : 
						controllerName = SingleMapScenario.CLOSED_CONTROLLER;
						break;
					default : break;
					}
					break;
				}
			}
			i++;
		}
		
		UpdatePlayerDataFromWidgets ();
	}
	
	public void OnTeamChanged (string selectedItem)
	{
		this.teamNumber = int.Parse (selectedItem);
		
		UpdatePlayerDataFromWidgets ();
	}
	
	public void OnMoneyChanged (float newMoney)
	{
		this.money = (int)(newMoney * MAX_PLAYER_MONEY);		
			
		UpdatePlayerDataFromWidgets ();
	}
	
	private bool canUpdatePlayerData = true;
	
	public void UpdateFromPlayerData ()
	{		
		if (!gameObject.activeSelf) {
			return;
		}
		canUpdatePlayerData = false;
		updatePlayerDataTimer.ResetFirstTimeCall ();
		
		PlayerInfo playerInfo = mapScenario.PlayersInfo [playerId];
		
		this.controllerName = playerInfo.name;	
		this.teamNumber = playerInfo.teamNumber;
		this.money = playerInfo.money;
		
		PrepareControlNamePopup ();
		
		this.playerColorSelector.selection = this.playerColorSelector.items [playerId];
		this.teamPopupList.selection = this.teamPopupList.items [teamNumber - 1];
		this.moneySlider.sliderValue = (float)money / (float)MAX_PLAYER_MONEY;
		
		canUpdatePlayerData = true;		
		updatePlayerDataTimer.ResetFirstTimeCall ();
	}
	
	private void PrepareControlNamePopup ()
	{
		if (!sceneController.IsClient) {
			if (this.controllerName == SingleMapScenario.AI_CONTROLLER) {
				this.playerControlType.selection = "Ai";
			} else if (this.controllerName == ApplicationManager.Instance.DeviceName) {
				this.playerControlType.selection = "Human";
			} else if (this.controllerName == SingleMapScenario.OPEN_CONTROLLER) {
				this.playerControlType.selection = "Open";
			} else if (this.controllerName == SingleMapScenario.CLOSED_CONTROLLER) {
				this.playerControlType.selection = "Closed";
			} else {
				this.playerControlType.selection = "Used";
			}
		} else {
			if (this.controllerName == SingleMapScenario.AI_CONTROLLER) {
				this.playerControlType.selection = "Ai";
				this.playerControlType.collider.enabled = false;
			} else if (this.controllerName == ApplicationManager.Instance.DeviceName) {
				this.playerControlType.selection = "Human";
				this.playerControlType.collider.enabled = true;
			} else if (this.controllerName == SingleMapScenario.OPEN_CONTROLLER) {
				this.playerControlType.selection = "Open";
				this.playerControlType.collider.enabled = true;
			} else if (this.controllerName == SingleMapScenario.CLOSED_CONTROLLER) {
				this.playerControlType.selection = "Closed";
				this.playerControlType.collider.enabled = false;
			} else {
				this.playerControlType.selection = "Used";
				this.playerControlType.collider.enabled = false;
			}
		}
		
		
	}
	
	public void UpdatePlayerDataFromWidgets ()
	{
		if (!canUpdatePlayerData) {
			return;
		}
		
		if (mapScenario == null) {
			return;
		}
		
		if (!updatePlayerDataTimer.EnoughTimeLeft ()) {
			return;
		}
		
		if (sceneController.IsClient) {
			this.SendUpdateToServer ();
			return;
		}
		
		sceneController.UpdatePlayer (playerId, controllerName, teamNumber, money);
		this.owner.UpdateAllPlayerInfoPanels ();
		
		if (sceneController.IsServer) {
			this.UpdateClients ();
		} 
	}

	public void UpdateClients ()
	{
		if (!gameObject.activeSelf) {
			return;
		}
		networker.RPC ("ClientUpdatePlayerInfo", RPCMode.Others, playerId, controllerName, teamNumber, money);
	}
	
	public void SendUpdateToServer ()
	{
		if (!gameObject.activeSelf) {
			return;
		}
		networker.RPC ("ServerUpdatePlayerInfo", RPCMode.Server, playerId, controllerName, teamNumber, money);
	}
		
	private SingleMapScenario mapScenario {
		get { return owner.singleMapScenario;}
	}
	
	private NetworkView networker {
		get { return owner.networkView;}
	}
}

