using UnityEngine;
using System.Collections;
using Setup;
using System.Collections.Generic;

public class NMissionSelectionDialog : MonoBehaviour
{
	public GameObject MissionElementsGrid;
	public GameObject MissionImage;
	public GameObject MissionLabel;
	public GameObject StartMissionButton;
	public GameObject CancelMissionButton;
	
	private List<NMissionElement> missionElements = new List<NMissionElement> ();	
	private MenuSceneController menuSceneController;
	private UILabel missionLabel;
	private NGUIMenuController nguiMenuController;	
	private string currentMapName;
	private bool startServerGame = false;
	
	public void Init () {
		this.nguiMenuController = FindObjectOfType (typeof (NGUIMenuController)) as NGUIMenuController;
		this.menuSceneController = FindObjectOfType(typeof (MenuSceneController)) as MenuSceneController;		
		
		foreach (Transform trans in MissionElementsGrid.transform) {
			NMissionElement missionElement = trans.gameObject.GetComponent<NMissionElement> ();
			missionElement.Init (this);
			missionElements.Add (missionElement);
		}
		
		missionLabel = MissionLabel.GetComponent<UILabel> ();		
				
		UIEventListener.Get (StartMissionButton).onClick += delegate(GameObject go)
		{
			if (startServerGame) {
				menuSceneController.StartServer (currentMapName);
			} else {
				nguiMenuController.DisplayBattleMapDialog (currentMapName);
			}
			
			Hide ();
		};
		
		UIEventListener.Get (CancelMissionButton).onClick += delegate(GameObject go)
		{
			Hide ();
		};
	}
	
	public void OnElementClicked (NMissionElement element) {
		currentMapName = element.MissionName;
		missionLabel.text = element.MissionTitle;
	}
	
	public void Update () {
	}
	
	public void Display (bool isServer) {
		nguiMenuController.Blocker3DUI.SetActive (true);
		this.gameObject.SetActive (true);
		
		startServerGame = isServer;
		
		int i = 0;
		
		foreach (string key in ApplicationManager.Instance.BattleMaps.Keys) {
			missionElements[i].gameObject.SetActive (true);
			missionElements[i].SetData (ApplicationManager.Instance.BattleMaps[key]);
			i++;
		}
		
		int lastElementNumber = i - 1;
		
		for (; i < MissionElementsGrid.transform.childCount; i++) {
			missionElements[i].gameObject.SetActive (false);
		}
		
		OnElementClicked (missionElements[lastElementNumber]);
	}
	
	public void Hide () {
		nguiMenuController.Blocker3DUI.SetActive (false);
		this.gameObject.SetActive (false);
	}
}

