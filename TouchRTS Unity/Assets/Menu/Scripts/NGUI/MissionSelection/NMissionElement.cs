using UnityEngine;
using System.Collections;
using Setup;

public class NMissionElement : MonoBehaviour
{
	public GameObject LabelObject;	
	private UILabel missionName;
	private NMissionSelectionDialog owner;
	private MapData mapData;
	
	public void Init (NMissionSelectionDialog owner) {
		this.owner = owner;
		missionName = LabelObject.GetComponent<UILabel> ();
	}
	
	public void OnClick () {
		this.owner.OnElementClicked (this);
	}

	public void SetData (MapData newMapData)
	{
		this.mapData = newMapData;
		missionName.text = mapData.Name + " (" + mapData.NumOfPlayers + ")";
	}
	
	public string MissionName {
		get {return this.mapData.Name;}
	}
	
	public string MissionTitle {
		get {return missionName.text;}
	}
}

