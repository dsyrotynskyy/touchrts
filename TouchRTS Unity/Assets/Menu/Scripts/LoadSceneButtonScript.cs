using UnityEngine;
using System.Collections;
using Setup;

public class LoadSceneButtonScript : AbstractButtonScript
{
	public bool isCampaign = false;
	public string levelToLoad;
	
	public void Start () {
		Init ();
	}
	
	#region implemented abstract members of AbstractButtonScript
	protected override void DoActionLogic ()
	{
		if (levelToLoad == "quit") {
			UnityEngine.Application.Quit ();
			return;
		}
		if (isCampaign) {
			menuSceneController.LoadCampaignLevel (levelToLoad);
		} else {
			nguiMenuController.DisplayChooseMapDialog (false);
		}

	}
	#endregion
	
}

