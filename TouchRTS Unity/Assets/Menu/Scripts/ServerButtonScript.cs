using UnityEngine;
using System.Collections;

public class ServerButtonScript : AbstractButtonScript
{
	
	public void Start () {
		Init ();
	}
	
	#region implemented abstract members of AbstractButtonScript
	protected override void DoActionLogic ()
	{
		nguiMenuController.DisplayChooseMapDialog (true);		
	}
	#endregion
}

