using UnityEngine;
using System.Collections;
using Setup;
using GameMapScenario;

public class MenuSceneController : SceneController
{	
	public NGUIMenuController UImenuController;	
	
	public void Start () {
		this.UImenuController = FindObjectOfType (typeof (NGUIMenuController)) as NGUIMenuController;		
		
		this.networkView.observed = this;
	}
	
	public void StartServer (string mapName) {
		if (Network.isServer) {
			return;
		}
		
		NetworkManager.Instance.StartServer ();
		
		StartCoroutine (CreateServerUI (mapName));
	}
	
	public IEnumerator CreateServerUI (string mapName) {
		yield return new WaitForSeconds (0.1f);
		UImenuController.OnStartServer (mapName);
	}
	
	public void StartBattleLevel () {
		if (IsServer) {
			networkView.RPC ("RPCLoadLevel", RPCMode.AllBuffered, singleMapScenario.LevelName);
			return;
		}
		
		LoadLevel (singleMapScenario.LevelName);
	}
	
	public void JoinServer () {	
		if (Network.isClient || !NetworkManager.Instance.CanJoinServer) {
			return;
		}
		
		NetworkManager.Instance.JoinRandomServer ();
	}
	
	private void OnConnectedToServer(){
	}
	
	void OnPlayerConnected(NetworkPlayer player){
		if (!IsServer) {
			return;
		}
		Debug.Log ("Player connected");
		
		networkView.RPC ("RPCStartClient", player, this.singleMapScenario.LevelName);
		
		UImenuController.OnClientJoined ();
 	}
		
	[RPC]
	public void RPCStartClient (string missionName) {
		UImenuController.OnStartClient ("Network Scene"); 	 
	}
	
	private SystemTimer updateTimer = SystemTimer.CreateFrequency (5000);
	
	public void Update()
	{	
		if (NetworkManager.Instance.IsNetworkGame) {
			return;
		}
		if (updateTimer.EnoughTimeLeft ())
	    {
	    	NetworkManager.Instance.RefreshHostList ();
	    }
	}	
	
	public bool CanJoinServer {
		get {
			return NetworkManager.Instance.CanJoinServer;
		}
	}
	
	public void OnDisconnectedFromServer ()
	{
		UImenuController.OnCancelNetwork ();
	}

	public void CancelServer ()
	{
		NetworkManager.Instance.LeaveNetwork ();
		UImenuController.OnCancelNetwork ();
	}
	
	public void LeaveNetwork () {
		NetworkManager.Instance.LeaveNetwork ();
		UImenuController.OnCancelNetwork ();
	}

	public void UpdatePlayer (int playerId, string controllerName, int teamNumber, int money)
	{
		if (controllerName == SingleMapScenario.AI_CONTROLLER) {
			this.singleMapScenario.SetNetworkAiPlayer (playerId, teamNumber, money);
		} else {
			this.singleMapScenario.SetNetworkHumanPlayer (playerId, teamNumber, money, controllerName);
		}		
	}
	
	public bool IsServer {
		get {
			return Network.isServer;
		}
	}
	
	public bool IsClient {
		get {
			return Network.isClient;
		}
	}
}

