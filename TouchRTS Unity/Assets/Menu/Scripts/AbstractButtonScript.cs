using UnityEngine;
using System.Collections;

public abstract class AbstractButtonScript : MonoBehaviour
{
	public AudioClip beep;

	protected bool isClicked = false;
	protected bool IsClickable = true;
	
	protected NGUIMenuController nguiMenuController;
	protected MenuSceneController menuSceneController;
	
	public void Init () {
		menuSceneController = FindObjectOfType (typeof (MenuSceneController)) as MenuSceneController;
		nguiMenuController = FindObjectOfType (typeof (NGUIMenuController)) as NGUIMenuController;
	}
	
	public virtual void Update ()
	{				
		if (this.IsClickable) {
			renderer.material.color = Color.white;
		} else {
			renderer.material.color = Color.gray;
			return;
		}
		
		CheckClicked ();
		if (isClicked) {
			renderer.material.color = Color.red;
			HandleClick ();
		} 
		
		if (Input.GetMouseButtonUp (0)) {
			StartCoroutine (ChangeColor (0.1f));		
		}
	}
	
	private void CheckClicked () {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = nguiMenuController.UICamera3D.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			
			if (Physics.Raycast (ray.origin, ray.direction, out hit)) {	
				if (hit.collider.gameObject == this.gameObject) {
					if (!isClicked) {
						audio.PlayOneShot (beep);
					}
					isClicked = true;
					return;
				} 
			}
		}
	}
	
	SystemTimer handleClickTimer = SystemTimer.CreateFrequency (1000);
	
	private void HandleClick ()
	{		
//		DoLogic ();
		StartCoroutine (DoAction (0.1f));		
	}
	
	IEnumerator DoAction (float waitTime) {
		yield return new WaitForSeconds(waitTime);
		DoLogic ();
	}
	
	IEnumerator ChangeColor (float waitTime) {
		yield return new WaitForSeconds(waitTime);
		renderer.material.color = Color.white;  
		isClicked = false;
	}
	
	private void DoLogic () {	
		if (handleClickTimer.EnoughTimeLeft ()) {
			DoActionLogic ();
		}		
	}
	
	protected abstract void DoActionLogic () ;
}

