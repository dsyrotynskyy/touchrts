using UnityEngine;
using System.Collections;

public class SpellNetworkSerializer : NetworkSerializer
{
	private SpellSubject spellOwner;
//	private TransformSerializator transformSerializator;
	
	public void Init (SpellSubject spellOwner) {
		base.Init ();
		
		this.spellOwner = spellOwner;
		
//		transformSerializator = new TransformSerializator (gameObject, true, true);
	}
	
	public void OnFixedUpdate ()
	{
//		transformSerializator.OnFixedUpdate ();
	}
	
	public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
//		transformSerializator.OnSerialize (stream, info);
		
		stream.Serialize (ref spellOwner.isCastingByPlayer);
    }
}

