using UnityEngine;
using System.Collections;
using Entity;

public class PlayerNetworkSerializer : NetworkSerializer
{
	private PlayerSubject playerOwner;
	private PlayerData playerData { get {return playerOwner.PlayerData;}}
	
	public void Init (PlayerSubject playerOwner) {
		base.Init ();
		
		this.playerOwner = playerOwner;		
	}
	
	public void OnFixedUpdate ()
	{	
	}
	
	public void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
			stream.Serialize (ref playerData.moneyCount);	
			stream.Serialize (ref playerData.usedUnitPlaces);
			stream.Serialize (ref playerData.allUnitPlaces);
		
//        if (stream.isWriting)
//        {
//	    }
//        else
//        {
//        }
    }
}

