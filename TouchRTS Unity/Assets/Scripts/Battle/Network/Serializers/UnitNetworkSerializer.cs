using UnityEngine;
using System.Collections;
using MovementEngine;
using System.Text;
using System.Collections.Generic;
using System;

public class UnitNetworkSerializer : NetworkSerializer
{	
	private UnitSubject unitOwner;
	private BuildingSubject buildingData;
	private HeroSubject heroData;
	private NetworkSerializator transformSerializator;
	
	private char currentStateChar;
	private char newStateChar;
	
	private int currentMana;
	private int newMana;
	
	private int currentHealth;
	private int prevHealth;
	
	public void Init (UnitSubject unitOwner)
	{
		base.Init ();
		
		this.unitOwner = unitOwner;
		buildingData = unitOwner.BuildingData;
		heroData = unitOwner.HeroData;
		if (!unitOwner.IsBuilding) {
			transformSerializator = new UnitTransformSerializator (this.gameObject, unitOwner.UnitDemeanorController);
		}
		
		currentStateChar = unitOwner.UnitDemeanorController.CreateStateCharForClient ();
	}
	
	private Timer updateTimer = Timer.CreateFrequency (1000);	
	
	public void OnSerializeNetworkView (BitStream stream, NetworkMessageInfo info)
	{
		if (unitOwner.IsBuilding) {
			SerializeBuilding (stream, info);
		} else {
			SerializeTroop (stream, info);
		}
	}
	
	private void SerializeBuilding (BitStream stream, NetworkMessageInfo info) {
		float captureProgress = buildingData.CaptureTimerProgress;
	
		stream.Serialize (ref captureProgress);
		
		if (stream.isReading) {
			buildingData.CaptureTimerProgress = captureProgress;
		}
	}
	
	private void SerializeTroop (BitStream stream, NetworkMessageInfo info) {
		if (unitOwner.IsHero) {
			stream.Serialize (ref unitOwner.currentMana);
		}
		
		stream.Serialize (ref unitOwner.currentHealth);	
			
		Vector3 movementCoord = Vector3.zero;
		short xPos = 0;
		short zPos = 0;
		const int multiple = 10;
		if (stream.isWriting) {
			movementCoord = unitOwner.UnitDemeanorController.MovementCoord;
			xPos = (short)(movementCoord.x * multiple);
			zPos = (short)(movementCoord.z * multiple);
			stream.Serialize (ref xPos);
			stream.Serialize (ref zPos);
		} else {
			stream.Serialize (ref xPos);
			stream.Serialize (ref zPos);
			movementCoord.Set ((float)xPos / multiple, NavigationEngine.TerrainNormalHeight, (float)zPos / multiple);
			unitOwner.MoveUnitTo (movementCoord);
		}
			
		if (transformSerializator != null) {
			transformSerializator.OnSerialize (stream, info);
		}
	}
	
	#region Server
	public void OnUnitStateChanged (char stateChar)
	{		
		this.newStateChar = stateChar;
	}
	#endregion
	
	public void OnFixedUpdate ()
	{	
		if (updateTimer.EnoughTimeLeft ()) {
			return;			
		}
		
		if (!unitOwner.IsBuilding) {
			unitOwner.CheckForDeath ();
			transformSerializator.OnFixedUpdate ();
		}		
		
		if (WorldController.Instance.IsNetworkGameServer) {
			if (currentStateChar != newStateChar) {
				currentStateChar = newStateChar;
				networkView.RPC ("RPCClientOnUnitStateChanged", RPCMode.OthersBuffered, Convert.ToInt32( newStateChar));
			}
		}
	}
	
	//Must be called only on client
	[RPC]
	private void RPCClientOnUnitStateChanged (int stateChar)
	{	
		this.currentStateChar = Convert.ToChar( stateChar);
		this.unitOwner.UnitDemeanorController.OnUnitStateChanged (currentStateChar);
	}
	
//	#region Client
//	private LinkedList<Vector3> dataList = new LinkedList<Vector3> ();
//	
//	[RPC]
//	private void RPCOnUnitStateChanged (char stateChar)
//	{	
//		
//	}
//	#endregion

}

