using UnityEngine;
using System.Collections;
using Entity;
using System.Collections.Generic;

public class ServerCommandController : NetworkCommandController
{	
	public override void Init (WorldController worldController)
	{
		base.Init (worldController);		
	}
	
	// Update is called once per frame
	public void OnUpdate ()
	{
	}
	
	public override void PrepareNetwork ()
	{
		base.PrepareNetwork ();
		CreateNetworkViews ();
	}
	
	public override void OnUpgradeFinished (BuildingSubject building)
	{
		base.OnUpgradeFinished (building);
		networkView.RPC ("RPCOnUpgradeFinished", RPCMode.OthersBuffered, building.id);
	}
	
	public override void OnBuildingConstructed(BuildingSubject castle, BuildingType constructionType) {
		base.OnBuildingConstructed (castle, constructionType);
		networkView.RPC ("RPCOnBuildingConstructed", RPCMode.OthersBuffered, castle.id, constructionType.Name);
	}
	
	public override void SpawnUnit (BuildingSubject building, UnitType unitType, UnitGroupSubject unitGroup)
	{
		base.SpawnUnit (building, unitType, unitGroup);
		networkView.RPC ("RPCSpawnUnit", RPCMode.OthersBuffered, building.id, unitType.Name, unitGroup.id);
	}

	public override void BuySpell (HeroSubject hero, SpellType spellType)
	{
		base.BuySpell (hero, spellType);
		networkView.RPC ("RPCBuySpell", RPCMode.OthersBuffered, hero.id, spellType.Name);
	}

	public override GameObject ConstructBuilding (CastleConstructor castleConstructor, BuildingType buildingType, bool freeConstruct)
	{
		GameObject obj = base.ConstructBuilding (castleConstructor, buildingType, freeConstruct);
		
		NetworkViewID viewID = Network.AllocateViewID();
		AttachNetworkView (obj, viewID);
		
		if (IsNetworkGame) {
			string id = WorldController.Instance.CreateId ();
			BuildingSubject building = obj.GetComponent<BuildingSubject> ();
			building.id = id;
			networkView.RPC ("ClientConstructBuilding", RPCMode.OthersBuffered, castleConstructor.Castle.id, buildingType.Name, freeConstruct, building.id ,viewID);
		}
		
		return obj;
	}
	
	public override GameObject CreateUnit (string unitName, Vector3 position, UnitGroupSubject unitGroup, string supType, bool isGarrison, string id = null)
	{
		id = WorldController.Instance.CreateId ();
		GameObject unit = base.CreateUnit (unitName, position, unitGroup, supType, isGarrison, id);
		
		NetworkViewID viewID = Network.AllocateViewID();
		AttachNetworkView (unit, viewID);
		
		if (IsNetworkGame) {
			networkView.RPC ("ClientCreateUnit", RPCMode.OthersBuffered, unitName, position, unitGroup.id, supType, isGarrison, id, viewID);
		}
		return unit;
	}
	
	public override GameObject CreateSpell (SpellType spellType, HeroSubject currentHero, Vector3 touchPosition, PlayerData playerData, string id = null)
	{
		id = WorldController.Instance.CreateId ();
		GameObject spell = base.CreateSpell (spellType, currentHero, touchPosition, playerData, id);
		
		NetworkViewID viewID = Network.AllocateViewID();
		AttachNetworkView (spell.gameObject, viewID);
		
		networkView.RPC ("ClientCreateSpell", RPCMode.OthersBuffered, spellType.Name, currentHero.id, touchPosition, playerData.Id, id, viewID);
		
		return spell;
	}
	
		
	public override void DoSpell (HeroSubject currentHero, SpellType spellType, Vector3 point)
	{
		base.DoSpell (currentHero, spellType, point);
		networkView.RPC ("RPCDoSpell", RPCMode.OthersBuffered, currentHero.id, spellType.Name, point);
	}
	
	public override void StopSpell (HeroSubject currentHero)
	{
		base.StopSpell (currentHero);
		networkView.RPC ("RPCStopSpell", RPCMode.OthersBuffered, currentHero.id);
	}
	
	public override void CaptureBuilding (BuildingSubject building, PlayerData newOwner) {
		base.CaptureBuilding (building, newOwner);
		networkView.RPC ("RPCCaptureBuilding", RPCMode.OthersBuffered, building.id, "" + newOwner.Id);
	}

	public override void AddEffectToUnit (UnitSubject unit, EffectType effectType, EffectPerformer performer) {
		base.AddEffectToUnit (unit, effectType, performer);
		networkView.RPC ("RPCAddEffectToUnit", RPCMode.OthersBuffered, unit.id, effectType.Name, performer.Id);
	}
	
	public override void AffectUnitLife (UnitSubject unit, int power) {
		base.AffectUnitLife (unit, power);
//		networkView.RPC ("RPCAffectUnitLife", RPCMode.OthersBuffered, unit.id, power);
	}
	
	public override void AffectUnitMana (UnitSubject unit, int power) {
		base.AffectUnitMana (unit, power);
//		networkView.RPC ("RPCAffectUnitMana", RPCMode.OthersBuffered, unit.id, power);
	}
	
	public override void MoveGroupToPoint (UnitGroupSubject unitGroup, Vector3 position, bool moveAnyway = false) {
		base.MoveGroupToPoint (unitGroup, position, moveAnyway);
		networkView.RPC ("RPCMoveGroupToPoint", RPCMode.OthersBuffered, unitGroup.id, position, moveAnyway);
	}

	public override void DoInterraction (UnitSubject unit, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToInterract)
	{
		base.DoInterraction (unit, performer, unitsToInterract);
		
		string victimsString = "";
		foreach (UnitSubject victim in unitsToInterract)
		{
			victimsString += victim.Id + ',';
		}
		victimsString.TrimEnd(","[0]);
		
		networkView.RPC ("RPCDoInterraction", RPCMode.OthersBuffered, unit.Id, performer.Id, victimsString);
	}
	
	public override void CastUnitsSkillAbility (UnitGroupSubject unitGroup, AbilityType skill) {
		base.CastUnitsSkillAbility (unitGroup, skill);
		networkView.RPC ("RPCCastUnitsSkill", RPCMode.OthersBuffered, unitGroup.id, skill.Name);
	}

	#region implemented abstract members of NetworkCommandController

	
	[RPC]
	public override void RPCSpawnUnit (string buildingId, string unitTypeName, string unitGroupId)
	{
		BuildingSubject building = WorldController.Instance.FindBuildingById (buildingId);
		UnitType unitType = Entity.DataTypes.Instance.Units.allData [unitTypeName];
		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);
		
		SpawnUnit (building, unitType, unitGroup);
	}
			
	[RPC]
	public override void RPCBuySpell (string heroId, string spellTypeName)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);	
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);

		BuySpell (hero, spellType);
	}
	
	[RPC]
	public override void ServerConstructBuilding (string castleId, string buildingTypeName, bool freeConstruct)
	{
		CastleConstructor castleConstructor = WorldController.Instance.FindBuildingById (castleId).CastleConstructor;
		BuildingType buildingType = Entity.DataTypes.Instance.Buildings.allData [buildingTypeName];

		ConstructBuilding (castleConstructor, buildingType, freeConstruct);
	}
	
	[RPC]
	public override void ServerCreateSpell (string spellTypeName, string heroId, Vector3 touchPosition, int playerId)
	{
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		CreateSpell  (spellType, hero, touchPosition, WorldController.Instance.AllPlayers[playerId]);		
	}

	[RPC]
	public override void RPCDoSpell (string heroId, string spellTypeName, Vector3 point)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);
		
		DoSpell (hero, spellType, point);
	}
	
	[RPC]
	public override void RPCStopSpell (string heroId)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		
		StopSpell (hero);
	}
	
	[RPC]
	public override void RPCCaptureBuilding (string buildingId, string playerId) {
		BuildingSubject building = WorldController.Instance.FindBuildingById (buildingId);
		PlayerData owner = WorldController.Instance.FindPlayerById (playerId).PlayerData;
		
		CaptureBuilding (building, owner);
	}

	[RPC]
	public override void RPCAddEffectToUnit (string unitId, string effectTypeName, string effectPerformerName) {
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
		EffectType effect = DataTypes.Instance.Effects.allData[effectTypeName];
		EffectPerformer performer = WorldController.Instance.FindEffectPerformerById (effectPerformerName);
	
		AddEffectToUnit (unit, effect, performer);
	}
	
	[RPC]
	public override void RPCAffectUnitLife (string unitId, int power) {
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);

		AffectUnitLife (unit, power);
	}
	
	[RPC]
	public override void RPCAffectUnitMana (string unitId, int power) {
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
		
		AffectUnitMana (unit, power);
	}
	
	[RPC]
	public override void RPCMoveGroupToPoint (string unitGroupId, Vector3 position, bool moveAnyway = false) {
		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);

		MoveGroupToPoint (unitGroup, position, moveAnyway);
	}
	
	[RPC]
	public override void RPCDoInterraction (string unitId, string  unitInteractionPerformerId, string unitsToInterractIds)
	{
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
		UnitInteractionPerformer performer = WorldController.Instance.FindInteractionPerformerById (unitInteractionPerformerId);
		LinkedList<UnitSubject> units = new LinkedList<UnitSubject> ();
		foreach (string str in unitsToInterractIds.Split (',')) {
			UnitSubject unitVictim = WorldController.Instance.FindUnitById (str);
			units.AddLast (unitVictim);
		};
		DoInterraction (unit, performer, units);
	}
	
	[RPC]
	public override void RPCCastUnitsSkillAbility (string unitGroupId, string AbilityTypeName) {
		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);
		AbilityType ability = DataTypes.Instance.Abilities.allData[AbilityTypeName];
		
		CastUnitsSkillAbility (unitGroup, ability);
	}
	
	[RPC]
	public override void RPCOnBuildingConstructed (string castleId, string constructionTypeName)
	{
		BuildingSubject building = WorldController.Instance.FindBuildingById (castleId);
		BuildingType type = DataTypes.Instance.Buildings.allData[constructionTypeName];
		OnBuildingConstructed (building, type);
	}
	

	

	
	#endregion
	
	private void CreateNetworkViews () {
		PlayerSubject[] players = FindObjectsOfType (typeof (PlayerSubject)) as PlayerSubject[];
		foreach (PlayerSubject player in players) {
			NetworkViewID viewID = Network.AllocateViewID();
//			AttachNetworkView (player.gameObject, viewID);
			string id = player.Id;
			networkView.RPC("AttachNetworkViewToPlayer", RPCMode.AllBuffered, id, viewID);
		}
		
		HeroSubject[] heroes = FindObjectsOfType (typeof (HeroSubject)) as HeroSubject[];
		foreach (HeroSubject hero in heroes) {
			NetworkViewID viewID = Network.AllocateViewID();
//			AttachNetworkView (hero.gameObject, viewID);
			string id = hero.id;
			networkView.RPC("AttachNetworkViewToHero", RPCMode.AllBuffered, id, viewID);
		}
		
		BuildingSubject[] buildings = FindObjectsOfType (typeof (BuildingSubject)) as BuildingSubject[];
		foreach (BuildingSubject building in buildings) {
			if (building.BuildingType.isOutbuilding) {
				continue;
			}
			
			NetworkViewID viewID = Network.AllocateViewID();
//			AttachNetworkView (building.gameObject, viewID);
			string id = building.id;
			networkView.RPC("AttachNetworkViewToBuilding", RPCMode.AllBuffered, id, viewID);
		}
		
		UnitSubject[] units = FindObjectsOfType (typeof (UnitSubject)) as UnitSubject[];
		foreach (UnitSubject unit in units) {
			NetworkViewID viewID = Network.AllocateViewID();
//			AttachNetworkView (unit.gameObject, viewID);
			string id = unit.id;
			networkView.RPC("AttachNetworkViewToUnit", RPCMode.AllBuffered, id, viewID);
		}
	}
}

