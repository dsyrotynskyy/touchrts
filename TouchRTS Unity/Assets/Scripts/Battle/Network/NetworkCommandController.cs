using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Setup;

public abstract class NetworkCommandController : CommandController
{
	
	public override bool IsNetworkGame {
		get {
			return true;
		}
	}
	
	public override void PrepareNetwork ()
	{
		this.networkView.enabled = true;
	}
	
	public void OnDisable ()
	{
		NetworkManager.Instance.LeaveNetwork ();
	}

	public override void Resurrect (HeroSubject hero)
	{
		base.Resurrect (hero);
		
		networkView.RPC ("RPCResurrect", RPCMode.OthersBuffered, hero.id);		
	}
	
	public override void StartUpgradeBuilding (BuildingSubject building)
	{
		base.StartUpgradeBuilding (building);
		networkView.RPC ("RPCStartUpgradeBuilding", RPCMode.OthersBuffered, building.id);
	}
	
	[RPC]
	public void RPCResurrect (string heroId)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		hero.Resurrect ();
	}
	
	[RPC]
	public virtual void ClientConstructBuilding (string castleId, string buildingTypeName, bool freeConstruct, string buildingId, NetworkViewID viewID)
	{
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void ClientCreateUnit (string unitName, Vector3 position, string unitGroupId, string supType, bool isGarrison, string unitId , NetworkViewID viewID)
	{		
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void ClientCreateSpell (string spellTypeName, string heroId, Vector3 touchPosition, int playerId, string id, NetworkViewID viewID)
	{
		throw new System.NotImplementedException ();
	}
//	
//	[RPC]
//	public virtual void ServerSpawnUnit (string buildingId, string unitTypeName, string unitGroupId)
//	{		
//		throw new System.NotImplementedException ();
//	}
	
	[RPC]
	public virtual void ServerConstructBuilding (string castleId, string buildingTypeName, bool freeConstruct)
	{
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void ServerCreateSpell (string spellTypeName, string heroId, Vector3 touchPosition, int playerId)
	{
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public abstract void RPCOnBuildingConstructed(string castleId, string constructionTypeName);
	
	[RPC]
	public void RPCStartUpgradeBuilding (string id)
	{	
		BuildingSubject building = WorldController.Instance.FindBuildingById (id);
		base.StartUpgradeBuilding (building);
	}
	
	[RPC]
	public abstract void RPCSpawnUnit (string buildingId, string unitTypeName, string unitGroupId);

	[RPC]
	public abstract void RPCBuySpell (string heroId, string spellTypeName);
	
	/****************************************************************/
	
	[RPC]
	public virtual void RPCDoSpell (string heroId, string spellTypeName, Vector3 point)
	{
		throw new System.NotImplementedException ();
//		currentHero.DoSpell (this, spellType, point);
	}
	
	[RPC]
	public virtual void RPCStopSpell (string heroId)
	{
		throw new System.NotImplementedException ();
//		currentHero.StopSpellCast (this);
	}
	
	[RPC]
	public virtual void RPCCaptureBuilding (string buildingId, string playerId) {
		throw new System.NotImplementedException ();
//		building.CaptureBuildingByPlayer (this, newOwner);
	}

	[RPC]
	public virtual void RPCAddEffectToUnit (string unitId, string effectTypeName, string effectPerformerName) {
//		unit.AddEffect (this, effectType, performer);
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void RPCAffectUnitLife (string unitId, int power) {
//		unit.AffectLife (this, power);
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void RPCAffectUnitMana (string unitId, int power) {
//		unit.AffectMana (this, power);
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void RPCMoveGroupToPoint (string unitGroupId, Vector3 position, bool moveAnyway = false) {
//		unitGroup.MoveGroupToPoint (this, position, moveAnyway);
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void RPCDoInterraction (string unitId, string  unitInteractionPerformerId, string unitsToInterract)
	{
//		AttackAnimationController.DoInterraction (this, unit, performer, unitsToInterract);	
		throw new System.NotImplementedException ();
	}
	
	[RPC]
	public virtual void RPCCastUnitsSkillAbility (string unitGroupId, string AbilityTypeName) {
//		unitGroup.CastUnitsSkill (this, skill);
		throw new System.NotImplementedException ();
	}
	
	#region Create Network Views

	
	[RPC]
	public void AttachNetworkViewToPlayer (string id,  NetworkViewID viewID) {
		PlayerSubject player = WorldController.Instance.FindPlayerById (id);
		AttachNetworkView (player.gameObject, viewID);				
	}
	
	[RPC]
	public void AttachNetworkViewToHero (string id,  NetworkViewID viewID) {
		HeroSubject hero = WorldController.Instance.FindHeroById (id);
		AttachNetworkView (hero.gameObject, viewID);
	}
	
	[RPC]
	public void AttachNetworkViewToBuilding (string id,  NetworkViewID viewID) {
		BuildingSubject building  = WorldController.Instance.FindBuildingById (id);
		AttachNetworkView (building.gameObject, viewID);
	}
	
	[RPC]
	public void AttachNetworkViewToUnit (string id,  NetworkViewID viewID) {
		UnitSubject unit = WorldController.Instance.FindUnitById (id);
		AttachNetworkView (unit.gameObject, viewID);
	}

	[RPC]
	public virtual void RPCOnUpgradeFinished (string id)
	{
		throw new System.NotImplementedException ();
	}	
		
	protected void AttachNetworkView (GameObject obj, NetworkViewID viewID) {		
		NetworkView view = obj.GetComponent<NetworkView> ();
		if (obj.networkView == null) {
			view = obj.AddComponent<NetworkView> ();
		}
		view.viewID = viewID;
	}
	
	# endregion
}

