package bb.network.message;

public class TrainUnitRequestData {
	private String unitType;
	private int castleId;

	public String getUnitType() {
		return unitType;
	}

	public int getCastleId() {
		return castleId;
	}

	public void make(String unitType, int castleId) {
		this.unitType = unitType;
		this.castleId = castleId;
	}

	private final static StringBuilder strBuilder = new StringBuilder();

	@Override
	public String toString() {
		strBuilder.delete(0, strBuilder.length());

		strBuilder.append(this.unitType);
		strBuilder.append(";");
		strBuilder.append(this.castleId);
		strBuilder.append(";");

		return strBuilder.toString();
	}

	public void readFromString(String str) {
		String[] strs = str.split(";");
		this.unitType = strs[0];
		this.castleId = Integer.parseInt(strs[1]);

	}

}
