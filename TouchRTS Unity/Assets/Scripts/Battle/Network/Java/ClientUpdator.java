package bb.network.client;

import bb.network.message.BBResponse;
import engine.Engine;
import engine.core.controller.map.MapParams;
import engine.core.controller.map.SkirmishMapParams;
import engine.core.persistance.object.UnitObject;

public class ClientUpdator {

	private boolean isWorking;
	private String networkGameId;

	public synchronized void prepare(MapParams params) {
		isWorking = params.isNetworkClientOnline();
		if (!isWorking) {
			return;
		}
		try {
			networkGameId = ((SkirmishMapParams) params).getNetworkGameId();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setIsWorking(boolean isWorking) {
		this.isWorking = isWorking;
	}

	public boolean isWorking() {
		return isWorking;
	}

	public void processTask() {
		RequestSender client = RequestSender.getInstance();
		BBResponse response = client
				.makeGetLastWorldStateRequest(networkGameId);
		try {
			if (response.getResultCode() == 0) {
				switch (response.getCommandCode()) {
				case (BBResponse.TURN_END):
					processTurnEnd(response);
					break;
				case (BBResponse.UPDATE_WORLD):
					updateGameWorldFromResponse(response.getBody());
					break;
				case (BBResponse.GAME_END):
					raiseGameEnd();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void raiseGameEnd() {
		Engine.getInstance().getGameWorld().raiseGameEnd();
	}

	private void processTurnEnd(BBResponse response) {
		RequestSender.getInstance().processTurnEnd(response);
	}

	private void updateGameWorldFromResponse(String worldState) {
		if (worldState == null) {
			return;
		}
		String[] unitStrs = worldState.split("!");
		UnitObject[] units = Engine.getInstance().getGameWorld().getMap()
				.getUnitPool();
		for (int i = 0; i < unitStrs.length; i++) {
			synchronized (units[i]) {
				units[UnitObject.getUnitIdFromString(unitStrs[i])]
						.initFromString(unitStrs[i]);
			}
		}

	}

	public String getOurNetworkId() {
		return null;
	}

	public String getNetworkGameId() {
		return this.networkGameId;
	}
}
