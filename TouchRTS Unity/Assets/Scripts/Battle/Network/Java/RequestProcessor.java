package bb.network.server;

import java.net.Socket;

import utils.Consol�;
import bb.network.message.BBRequest;
import bb.network.message.BBResponse;
import bb.network.message.ScreenCoordsRequestData;
import bb.network.message.TouchRequestData;
import bb.network.message.TrainUnitRequestData;
import engine.core.controller.map.SkirmishMapParams;

public class RequestProcessor {

	private BBResponse response = new BBResponse();
	private SkirmishMapParams mapParams = null;
	private Connection currentConnection = null;

	private final Socket connectionSocket;

	public RequestProcessor(Socket connectionSocket,
			SkirmishMapParams skirmishGameParams, String gameName) {
		this.connectionSocket = connectionSocket;
		this.mapParams = skirmishGameParams;
	}

	public BBResponse process(String requestString) {
		BBResponse res = processCorrectRequest(requestString);
		if (res == null) {
			res = response.errorMessage("Unknown request");
		}

		currentConnection = null;

		return res;
	}

	private BBResponse processCorrectRequest(String requestString) {
		BBRequest request = BBRequest.fromString(requestString);
		int requestType = request.getRequestType();

		try {
			switch (requestType) {
			case (BBRequest.GET_GAMES): {
				return sendDataAboutGame(request);
			}
			case (BBRequest.JOIN_GAME): {
				return joinGame(request);
			}
			case (BBRequest.LEAVE_GAME): {
				return leaveGame(request);
			}
			case (BBRequest.GET_STATE_LAST): {
				return getLastState(request);
			}
			case (BBRequest.TOUCH_COMMAND): {
				return processTouch(request);
			}
			case (BBRequest.CENTER_COORDS): {
				return centerWorldCoords(request);
			}
			case (BBRequest.TRAIN_UNIT_COMMAND): {
				return trainUnit(request);
			}
			case (BBRequest.TURN_END_COMMAND): {
				return processTurnEnd(request);
			}
			case (BBRequest.GET_PLAYER_PARAMS): {
				return getPlayerParams(request);
			}
			case (BBRequest.SET_PLAYER_PARAMS): {
				return setPlayerParams(request);
			}
			}
			;
		} catch (Exception e) {
			e.printStackTrace();
			return response.errorMessage("Error processing request");
		}
		return null;
	}

	private BBResponse sendDataAboutGame(BBRequest request) {
		response.message(0, BBResponse.INFO_ABOUT_GAME, mapParams.getMapName()
				.toString());
		return response;
	}

	private BBResponse setPlayerParams(BBRequest request) {
		if (checkInGameProperties(request)) {
			mapParams.initPlayersDataFromString(request.getRequestBody());
			response.message(0, BBResponse.OK, "");
		}
		currentConnection = null;
		return response;
	}

	private BBResponse getPlayerParams(BBRequest request) {
		if (checkInGameProperties(request)) {

			if (!WorldNetworkController.isGameLaunched()) {
				response.message(0, BBResponse.UPDATE_MAP_PARAMS,
						mapParams.getAllPlayersInfoString());
			} else {
				response.message(0, BBResponse.LAUNCH_GAME,
						mapParams.getAllPlayersInfoString());
			}
		}
		currentConnection = null;
		return response;
	}

	private BBResponse leaveGame(BBRequest request) {
		if (checkInGameProperties(request)) {
			this.processConnectionLeaving();
			response.message(0, BBResponse.LEAVE_GAME, "Game left");
		}
		currentConnection = null;
		return response;
	}

	private void processConnectionLeaving() {
		String conId = currentConnection.getNetworkId();
		mapParams.removePlayer(conId);
		WorldNetworkController.removeConnection(conId);
	}

	private BBResponse joinGame(BBRequest request) {
		Consol�.writeln("Sender id = " + request.getSenderId());
		Consol�.writeln("Has joined: " + request.getGameName());

		if (!WorldNetworkController.containsConnection(request.getSenderId())) {
			Connection newConnection = new Connection(request.getSenderId(),
					this.connectionSocket);
			WorldNetworkController.addConnection(newConnection);
			response.message(0, BBResponse.OK, mapParams.getXmlData());
		} else {
			response.errorMessage("Connection already in game! Can't join");
		}

		return response;
	}

	private BBResponse getLastState(BBRequest request) {
		if (checkInGameProperties(request)) {
			return WorldNetworkController.getState(currentConnection);
		}

		return response;
	}

	private boolean checkInGameProperties(BBRequest request) {
		currentConnection = WorldNetworkController.getConnection(request
				.getSenderId());
		if (currentConnection == null) {
			response.errorMessage("unexisting connection!");
			return false;
		}

		if (!WorldNetworkController.containsConnection(currentConnection
				.getNetworkId())) {
			response.errorMessage("Player hadn't join the game!");
			return false;
		}

		return true;
	}

	private BBResponse processTurnEnd(BBRequest request) {
		if (checkInGameProperties(request)) {
			if (!currentConnection.isCurrentTurn()) {
				response.message(1, BBResponse.ERROR, "Not Your turn");
			} else {
				response.message(0, BBResponse.OK,
						WorldNetworkController.processTurnEnd());
			}
		}
		return response;
	}

	private final TrainUnitRequestData trainUnitRequestData = new TrainUnitRequestData();

	private BBResponse trainUnit(BBRequest request) {
		if (checkInGameProperties(request)) {
			trainUnitRequestData.readFromString(request.getRequestBody());
			response.message(0, BBResponse.OK,
					WorldNetworkController.process(trainUnitRequestData));
		}
		return response;
	}

	private final TouchRequestData touchRequestData = new TouchRequestData();

	private BBResponse processTouch(BBRequest request) {
		if (checkInGameProperties(request)) {
			touchRequestData.readFromString(request.getRequestBody());
			response.message(0,
					WorldNetworkController.process(touchRequestData), "");
		}
		return response;
	}

	private final ScreenCoordsRequestData screenCoordsRequestData = new ScreenCoordsRequestData();

	private BBResponse centerWorldCoords(BBRequest request) {
		if (checkInGameProperties(request)) {
			screenCoordsRequestData.readFromString(request.getRequestBody());
			response.message(0, WorldNetworkController.process(
					screenCoordsRequestData, currentConnection), "");
		}
		return response;
	}

	protected void destroy() {
		this.currentConnection = null;
		this.mapParams = null;
		this.response = null;

	}
}
