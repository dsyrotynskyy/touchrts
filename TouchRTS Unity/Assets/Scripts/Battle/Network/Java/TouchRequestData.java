package bb.network.message;

public class TouchRequestData {
	private int isLongClick;
	private int xCoord;
	private int yCoord;
	
	public void make(int isLongClick, int xCoord, int yCoord) {
		this.isLongClick = isLongClick;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	public boolean getIsLongClick() {
		return isLongClick != 0;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}
	
	private final static StringBuilder strBuilder = new StringBuilder();	
	
	@Override
	public String toString() {
		strBuilder.delete(0, strBuilder.length());
		
		strBuilder.append(this.isLongClick);
		strBuilder.append(";");
		strBuilder.append(this.xCoord);
		strBuilder.append(";");		
		strBuilder.append(this.yCoord);
		strBuilder.append(";");		
				
		return strBuilder.toString();
	}
	
	public void readFromString(String str) {		
		String[] strs = str.split(";");
		this.isLongClick = Integer.parseInt(strs[0]);
		this.xCoord = Integer.parseInt(strs[1]);
		this.yCoord = Integer.parseInt(strs[2]);
	}
}
