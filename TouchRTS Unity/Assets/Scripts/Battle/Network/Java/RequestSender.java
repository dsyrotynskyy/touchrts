package bb.network.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import utils.Converter;

import android.content.Context;
import bb.network.message.BBRequest;
import bb.network.message.BBResponse;
import bb.network.message.ScreenCoordsRequestData;
import bb.network.message.TouchRequestData;
import bb.network.message.TrainUnitRequestData;
import engine.Engine;
import engine.core.controller.game.GameWorldController;
import engine.core.persistance.object.BuildingObject;
import engine.core.persistance.object.types.UnitType;

public class RequestSender {
	private static final RequestSender instance = new RequestSender();

	public static RequestSender getInstance() {
		return instance;
	}

	private NetworkClient client = NetworkClient.getInstance();
	private BBRequest request = BBRequest.getInstance();

	public BBResponse makeCreateNewGameRequest(String gameName, String mapPath,
			Context context) {
		InputStream is;
		String body = null;
		try {
			is = context.getAssets().open(mapPath);
			body = Converter.convertStreamToString(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return makeRequest(BBRequest.CREATE_GAME, gameName, body);
	}

	public BBResponse makeJoinGameRequest(String gameName) {
		return makeRequest(BBRequest.JOIN_GAME, gameName, null);
	}

	public BBResponse makeLeaveGameRequest(String gameName) {
		BBResponse res = makeRequest(BBRequest.LEAVE_GAME, gameName, null);
		try {
			client.closeConnection();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public BBResponse makeGetLastWorldStateRequest(String gameName) {
		return makeRequest(BBRequest.GET_STATE_LAST, gameName, null);
	}

	private synchronized BBResponse makeRequest(int requestType,
			String gameName, String requestBody) {
		Engine.getInstance().setNetworkId(NetworkClient.getInstance().getSenderId());
		request.clearData();
		BBResponse res = BBResponse.getInstance();
		res.message(1, BBResponse.CONNECTION_EXCEPTION, "Connection problem");

		String responseStr = "Exception";
		try {
			request.setRequestType(requestType);
			request.setRequestBody(requestBody);
			request.setGameName(gameName);
			request.setSenderId(client.getSenderId());

			client.writeRequest(request.toString());

			responseStr = client.readResponse();
			BBResponse.fromString(responseStr);
			return res;

		} catch (Exception e) {
			e.printStackTrace();
			if (Engine.getInstance().getGameWorld() != null) {
				Engine.getInstance().getNetworkUpdator().setIsWorking(false);
				Engine.getInstance().getGameWorld()
						.raiseGameEnd("Problems with connection");
			}
		}

		return res;
	}

	private final List<String> networkGamesList = new LinkedList<String>();

	public List<String> getExistingGames() {
		networkGamesList.clear();
		return networkGamesList;
	}

	private ExecutorService threadExecutor = Executors.newFixedThreadPool(4);

	private final TouchRequestData touchRequest = new TouchRequestData();
	private final TrainUnitRequestData trainUnitRequest = new TrainUnitRequestData();
	private final ScreenCoordsRequestData screenCoordsRequestData = new ScreenCoordsRequestData();

	private ProcessClickRunnable processClickRunnable = new ProcessClickRunnable();

	private class ProcessClickRunnable implements Runnable {

		int xCoord, yCoord;
		boolean isLongClick = false;

		public void setData(int xCoord, int yCoord, boolean isLongClick) {
			this.xCoord = xCoord;
			this.yCoord = yCoord;
			this.isLongClick = isLongClick;
		}

		@Override
		public void run() {
			processClickRunnable.setData(xCoord, yCoord, isLongClick);

			if (Engine.getInstance().getGameWorld().isZoomed()) {
				xCoord = xCoord * GameWorldController.BIG_CELL_SIZE
						/ GameWorldController.LITTLE_CELL_SIZE;
				yCoord = yCoord * GameWorldController.BIG_CELL_SIZE
						/ GameWorldController.LITTLE_CELL_SIZE;
			}
			touchRequest.make(isLongClick ? 1 : 0, xCoord, yCoord);
			makeRequest(BBRequest.TOUCH_COMMAND, Engine.getInstance()
					.getNetworkUpdator().getNetworkGameId(),
					touchRequest.toString());
		}

	}

	public void makeTouchRequest(boolean isLongClick, int xCoord, int yCoord) {
		processClickRunnable.setData(xCoord, yCoord, isLongClick);
		threadExecutor.execute(processClickRunnable);
	}
	
	public void makeTrainUnitRequest(UnitType unitType, BuildingObject building) {
		trainUnitRequest.make(unitType.getName(), building.getId());
		makeRequest(BBRequest.TRAIN_UNIT_COMMAND, Engine.getInstance()
				.getNetworkUpdator().getNetworkGameId(),
				trainUnitRequest.toString());
	}
	
	private final ScreenCenteringRequest screenCenteringRequest = new ScreenCenteringRequest();

	private class ScreenCenteringRequest implements Runnable {

		@Override
		public void run() {
			screenCoordsRequestData.make(Engine.getInstance().getGameWorld()
					.getMap().getCenterXCoord(), Engine.getInstance()
					.getGameWorld().getMap().getCenterYCoord());
			makeRequest(BBRequest.CENTER_COORDS, Engine.getInstance()
					.getNetworkUpdator().getNetworkGameId(),
					screenCoordsRequestData.toString());
		}
	}

	public void sendTurnEndMessage() {
		if (Engine.getInstance().isOurDeviceCurrent()) {
			this.threadExecutor.execute(this.turnEndRequest);
		}
	}

	private final TurnEndRequest turnEndRequest = new TurnEndRequest();

	private class TurnEndRequest implements Runnable {
		@Override
		public void run() {
			makeRequest(BBRequest.TURN_END_COMMAND, Engine.getInstance()
					.getNetworkUpdator().getNetworkGameId(), "");
		}
	}

	public void processTurnEnd(BBResponse response) {
		Engine.getInstance().getGameWorld().endTurn();
	}

	public BBResponse makePlayerSettingsRequest(String gameName) {
		return makeRequest(BBRequest.GET_PLAYER_PARAMS, gameName, "");
	}

	public void makeUpdatePlayerParamsRequest(String gameName,
			String allPlayerInfoString) {
		makeRequest(BBRequest.SET_PLAYER_PARAMS, gameName, allPlayerInfoString);
	}
}
