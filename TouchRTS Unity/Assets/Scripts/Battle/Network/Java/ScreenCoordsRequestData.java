package bb.network.message;

public class ScreenCoordsRequestData {
	private int xCoord;
	private int yCoord;
	public int getxCoord() {
		return xCoord;
	}
	public int getyCoord() {
		return yCoord;
	}
	
	public void make(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}
	
	private final static StringBuilder strBuilder = new StringBuilder();
	
	@Override
	public String toString() {
		strBuilder.delete(0, strBuilder.length());

		strBuilder.append(this.xCoord);
		strBuilder.append(";");
		strBuilder.append(this.yCoord);
		strBuilder.append(";");

		return strBuilder.toString();
	}

	public void readFromString(String str) {
		String[] strs = str.split(";");
		this.xCoord = Integer.parseInt(strs[0]);
		this.yCoord = Integer.parseInt(strs[1]);

	}
}
