package bb.network.message;

import java.io.Serializable;
import java.util.regex.Pattern;

public class BBRequest implements Serializable {
	private static final long serialVersionUID = 4798699456945599352L;

	private static BBRequest request = new BBRequest();
	
	public static BBRequest getInstance() {
		return request;
	}
	
	
	public static final int GET_GAMES = 1000;
	public static final int CREATE_GAME = 1100;
	public static final int JOIN_GAME = 1200;
	public static final int LEAVE_GAME = 1300;
	public static final int START_GAME = 1400;

	public static final int GET_PLAYER_PARAMS = 1500;
	public static final int SET_PLAYER_PARAMS = 1550;
	public static final int GET_STATE_LAST_UNIT = 1600;
	public static final int GET_STATE_LAST = 1651;

	public static final int TOUCH_COMMAND = 1700;
	public static final int TURN_END_COMMAND = 1800;
	public static final int TRAIN_UNIT_COMMAND = 1900;
	public static final int CENTER_COORDS = 20000;

	private String senderId;
	private int requestType;
	private String gameName;
	private String requestBody;
	
	private BBRequest() {
		
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public int getRequestType() {
		return requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public void clearData() {
		this.gameName = null;
		this.senderId = null;
		this.requestBody = null;
		this.requestType = 0;
	}

	private final static StringBuilder strBuilder = new StringBuilder();

	@Override
	public String toString() {
		strBuilder.delete(0, strBuilder.length());

		strBuilder.append(this.requestType);
		strBuilder.append("#");
		strBuilder.append(this.senderId);
		strBuilder.append("#");
		strBuilder.append(this.gameName);
		strBuilder.append("#");
		strBuilder.append(this.requestBody);

		return strBuilder.toString();
	}

	static Pattern p = Pattern.compile("#");
	public static BBRequest fromString(String requestString) {		
		String[] strs = p.split(requestString);
		request.requestType = Integer.parseInt(strs[0]);
		request.senderId = strs[1];
		request.gameName = strs[2];
		if (strs.length > 3) {
			request.requestBody = strs[3];
		}
		
		return request;
	}
}
