package bb.network.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observer;

import bb.network.message.BBRequest;
import bb.network.message.BBResponse;
import bb.network.server.ServerConnector;

public class NetworkGameFinder {
	public static final String refreshGameList = "REFRESH_LIST";
	public static final String stopSearch = "STOP_SEARCH";

	private static Observer networkGameList = null;

	private NetworkGameFinder() {
	}

	private static List<String> avalaibleNetworks = new LinkedList<String>();
	private static List<String> avalaibleAddresses = new LinkedList<String>();
	private static HashMap<String, String> gamesMap = new HashMap<String, String>();

	public static Map<String, String> getGames(Observer gameList) {
		avalaibleAddresses.clear();
		avalaibleAddresses.clear();
		gamesMap.clear();

		networkGameList = gameList;
		seekAvalaibleNetwork();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				seekAvalaibleAddresses();
			}
		});
		t.setDaemon(true);
		t.setName("Game Finder Thread");
		t.start();

		return gamesMap;
	}

	private static void seekAvalaibleNetwork() {
		avalaibleNetworks.clear();
		try {
			for (NetworkInterface ni : Collections.list(NetworkInterface
					.getNetworkInterfaces())) {
				for (InetAddress ia : Collections.list(ni.getInetAddresses())) {
					String ip = ia.getHostAddress();

					try {
						if (ip.equals(InetAddress.getLocalHost()
								.getHostAddress())) {
							continue;
						}
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}

					String networkIp = ip.substring(0, ip.lastIndexOf('.') + 1);
					avalaibleNetworks.add(networkIp);
				}
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}

	private static volatile boolean isSeekingGames = false;

	public static boolean isSeekingForGames() {
		return isSeekingGames;
	}

	public static void stopSeekingGames() {
		isSeekingGames = false;
	}

	private static void seekAvalaibleAddresses() {
		isSeekingGames = true;
		avalaibleAddresses.clear();
		while (isSeekingGames) {
			for (String network : avalaibleNetworks) {
				for (int i = 1; i < 255; i++) {
					if (!isSeekingGames) {
						break;
					}

					try {
						String address = network + i;

						if (InetAddress.getByName(address).isReachable(50)) {
							isGameAddress(address);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		networkGameList.update(null, stopSearch);
		networkGameList = null;
	}

	private static BBRequest request = BBRequest.getInstance();
	private static BBResponse response = BBResponse.getInstance();

	private static void isGameAddress(String serverIp) {
		Socket clientSocket;
		try {
			clientSocket = new Socket(serverIp, ServerConnector.SERVER_PORT);
			ObjectOutputStream os = new ObjectOutputStream(
					clientSocket.getOutputStream());

			request.setRequestType(BBRequest.GET_GAMES);
			os.writeObject(request.toString());

			ObjectInputStream in = new ObjectInputStream(
					clientSocket.getInputStream());
			String res = (String) in.readObject();
			try {
				BBResponse.fromString(res);
				if (response.getCommandCode() == BBResponse.INFO_ABOUT_GAME) {
					putDataAboutGame(response.getBody(), serverIp);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			clientSocket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void putDataAboutGame(String gameName, String ip) {
		synchronized (gamesMap) {
			gamesMap.put(gameName, ip);
			networkGameList.update(null, refreshGameList);
		}
	}
}
