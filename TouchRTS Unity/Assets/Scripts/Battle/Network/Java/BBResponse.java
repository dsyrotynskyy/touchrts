package bb.network.message;

import java.io.Serializable;
import java.util.regex.Pattern;

import engine.core.persistance.object.CellObject;
import engine.core.persistance.object.PlayerData;
import engine.core.persistance.object.UnitObject;

public class BBResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1882177817278913322L;
	public final static int INFO_ABOUT_GAME = 96;
	public static final int CONNECTION_EXCEPTION = 97;
	public static final int OK = 98;
	public static final int ERROR = 99;
	public static final int UPDATE_WORLD = 100;
	public static final int SELECT_UNIT = 101;
	public static final int CLICK_CELL = 102;
	public static final int LONG_CLICK_CELL = 1021;
	public static final int MOVE_UNIT = 103;
	public static final int DESELECTION_UNIT = 105;
	public static final int TURN_END = 106;
	public static final int NOT_YOUR_TURN = 107;
	public static final int LEAVE_GAME = 108;
	public static final int GAME_END = 109;
	public static final int UPDATE_MAP_PARAMS = 110;
	public static final int LAUNCH_GAME = 111;

	private int resultCode;
	private int command;
	private String body;
	
	public BBResponse() {
		
	}
	
	public static BBResponse getInstance() {
		return res;
	}

	public void clearData() {
		this.body = null;
		this.resultCode = 0;
	}

	private final static StringBuilder strBuilder = new StringBuilder();

	public int getResultCode() {
		return resultCode;
	}

	public int getCommandCode() {
		return this.command;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public BBResponse message(int code, int commandCode, String body) {
		this.body = body;
		this.command = commandCode;
		this.resultCode = code;
		return this;
	}

	public BBResponse errorMessage(String body) {
		this.resultCode = 1;
		this.command = ERROR;
		this.body = body;

		return this;
	}

	@Override
	public String toString() {
		strBuilder.delete(0, strBuilder.length());

		strBuilder.append(this.resultCode);
		strBuilder.append("#");
		strBuilder.append(this.command);
		strBuilder.append("#");
		strBuilder.append(this.body);

		return strBuilder.toString();
	}
	
	private static BBResponse res = new BBResponse();

	static Pattern p = Pattern.compile("#");
	public static synchronized BBResponse fromString(String requestString) {		
		String[] strs = p.split(requestString);
		res.resultCode = Integer.parseInt(strs[0]);
		res.command = Integer.parseInt(strs[1]);
		if (strs.length == 3) {
			res.body = strs[2];
		}
		return res;
	}
	
	public void createClickResponse(CellObject cellProper) {
		this.resultCode = 0;
		this.command = CLICK_CELL;
		this.body = String.valueOf(cellProper.getId());
	}
	
	public int getCellIdForClickResponse() throws CommandCastException {
		if (this.command != CLICK_CELL && this.command != LONG_CLICK_CELL) {
			throw new CommandCastException();
		}
		return Integer.parseInt(this.body);
	}

	public void createMovementResponse(UnitObject unit, CellObject destinationCell) {
		strBuilder.delete(0, strBuilder.length());
		this.resultCode = 0;
		this.command = MOVE_UNIT;
		this.body = unit.getId() + ";" + destinationCell.getId();
	}

	public void createLongClickResponse(CellObject cellProper) {
		this.resultCode = 0;
		this.command = LONG_CLICK_CELL;
		this.body = String.valueOf(cellProper.getId());
	}
	
	public void createDeselectionResponse() {
		this.resultCode = 0;
		this.command = DESELECTION_UNIT;
	}
	
	public class CommandCastException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 985350762576806219L;
		
	}
	
	public void createEndTurneResponse(PlayerData playerData) {
		this.resultCode = 0;
		this.command = TURN_END;
		this.body = playerData.getNetworkId();		
	}
}
