using System;
using Entity;
using UnityEngine;
using System.Collections.Generic;

public class ClientCommandController : NetworkCommandController
{
	public ClientCommandController ()
	{
	}

	#region implemented abstract members of CommandController

	
	public override void OnBuildingConstructed(BuildingSubject castle, BuildingType constructionType) {
	}

	public override void SpawnUnit (BuildingSubject building, UnitType unitType, UnitGroupSubject unitGroup)
	{
		networkView.RPC ("RPCSpawnUnit", RPCMode.Server, building.id, unitType.Name, unitGroup.id);
	}

	public override void BuySpell (HeroSubject hero, SpellType spellType)
	{
		networkView.RPC ("ServerBuySpell", RPCMode.Server, hero.id, spellType.Name);
	}	

//	public override void Resurrect (HeroSubject hero)
//	{
//		throw new NotImplementedException ();
//	}
	
	public override GameObject ConstructBuilding (CastleConstructor castleConstructor, BuildingType buildingType, bool freeConstruct)
	{
		networkView.RPC ("ServerConstructBuilding", RPCMode.Server, castleConstructor.Castle.id, buildingType.Name, freeConstruct);
		return null;
	}
	
	public override GameObject CreateUnit (string unitName, UnityEngine.Vector3 position, UnitGroupSubject unitGroup, string supType, bool isGarrison, string id = null)
	{
		//Unit created by server
//		throw new NotImplementedException ();
		return null;
	}

	public override GameObject CreateSpell (SpellType spellType, HeroSubject currentHero, UnityEngine.Vector3 touchPosition, PlayerData playerData, string id = null)
	{
		//Spell created by server
//		throw new NotImplementedException ();
		return null;
	}
	
	/**************************/
	
	public override void DoSpell (HeroSubject currentHero, SpellType spellType, Vector3 point)
	{
//		currentHero.DoSpell (this, spellType, point);
//		currentHero.RotateHeroTo (point);
		networkView.RPC ("RPCDoSpell", RPCMode.Server, currentHero.id, spellType.Name, point);
	}
	
	public override void StopSpell (HeroSubject currentHero)
	{
		currentHero.StopSpellCast (this);
		networkView.RPC ("RPCStopSpell", RPCMode.Server, currentHero.id);
	}
	
	public override void CaptureBuilding (BuildingSubject building, PlayerData newOwner) {
//		building.CaptureBuildingByPlayer (this, newOwner);
	}

	public override void AddEffectToUnit (UnitSubject unit, EffectType effectType, EffectPerformer performer) {
//		unit.AddEffect (this, effectType, performer);
	}
	
	public override void AffectUnitLife (UnitSubject unit, int power) {
//		unit.AffectLife (this, power);
	}
	
	public override void AffectUnitMana (UnitSubject unit, int power) {
//		unit.AffectMana (this, power);
	}
	
	public override void MoveGroupToPoint (UnitGroupSubject unitGroup, Vector3 position, bool moveAnyway = false) {
		networkView.RPC ("RPCMoveGroupToPoint", RPCMode.Server, unitGroup.id, position, moveAnyway);
	}

	public override void DoInterraction (UnitSubject unit, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToInterract)
	{
//		AttackAnimationController.DoInterraction (this, unit, performer, unitsToInterract);	
	}
	
	public override void CastUnitsSkillAbility (UnitGroupSubject unitGroup, AbilityType skill) {
		base.CastUnitsSkillAbility (unitGroup, skill);
		networkView.RPC ("RPCCastUnitsSkill", RPCMode.Server, unitGroup.id, skill.Name);
	}	
	#endregion
	
	#region RPC
	
	[RPC]
	public override void RPCSpawnUnit (string buildingId, string unitTypeName, string unitGroupId)
	{
		BuildingSubject building = WorldController.Instance.FindBuildingById (buildingId);
		UnitType unitType = Entity.DataTypes.Instance.Units.allData [unitTypeName];
		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);
		
		base.SpawnUnit (building, unitType, unitGroup);
	}
	
			
	[RPC]
	public override void RPCBuySpell (string heroId, string spellTypeName)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);	
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);

		base.BuySpell (hero, spellType);
	}
	
	[RPC]
	public override void ClientConstructBuilding (string castleId, string buildingTypeName, bool freeConstruct, string buildingId, NetworkViewID viewID)
	{
		CastleConstructor castleConstructor = WorldController.Instance.FindBuildingById (castleId).CastleConstructor;
		BuildingType buildingType = Entity.DataTypes.Instance.Buildings.allData [buildingTypeName];

		GameObject buildingObject = base.ConstructBuilding (castleConstructor, buildingType, freeConstruct);
		buildingObject.GetComponent<BuildingSubject> ().id = buildingId;
		AttachNetworkView (buildingObject, viewID);
	}
	
	[RPC]
	public override void ClientCreateUnit (string unitName, Vector3 position, string unitGroupId, string supType, bool isGarrison, string unitId, NetworkViewID viewID)
	{		
		GameObject unit = base.CreateUnit (unitName, position, WorldController.Instance.FindUnitGroupById (unitGroupId), supType, isGarrison, unitId);
		AttachNetworkView (unit.gameObject, viewID);
	}
	
	[RPC]
	public override void ClientCreateSpell (string spellTypeName, string heroId, Vector3 touchPosition, int playerId, string id, NetworkViewID viewID)
	{
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		SpellSubject spell = base.CreateSpell  (spellType, hero, touchPosition, WorldController.Instance.AllPlayers[playerId], id).GetComponent<SpellSubject> ();		
		AttachNetworkView (spell.gameObject, viewID);	
	}
	
	#endregion
	
	#region RPC
	[RPC]
	public override void RPCDoSpell (string heroId, string spellTypeName, Vector3 point)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		SpellType spellType = Entity.DataTypes.Instance.Spells.GetSpellByName (spellTypeName);
		
		base.DoSpell (hero, spellType, point);
	}
	
	[RPC]
	public override void RPCStopSpell (string heroId)
	{
		HeroSubject hero = WorldController.Instance.FindHeroById (heroId);
		
		base.StopSpell (hero);
	}
	
	[RPC]
	public override void RPCCaptureBuilding (string buildingId, string playerId) {
		BuildingSubject building = WorldController.Instance.FindBuildingById (buildingId);
		PlayerData owner = WorldController.Instance.FindPlayerById (playerId).PlayerData;
		
		base.CaptureBuilding (building, owner);
	}

	[RPC]
	public override void RPCAddEffectToUnit (string unitId, string effectTypeName, string effectPerformerName) {
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
		EffectType effect = DataTypes.Instance.Effects.allData[effectTypeName];
		EffectPerformer performer = WorldController.Instance.FindEffectPerformerById (effectPerformerName);
	
		base.AddEffectToUnit (unit, effect, performer);
	}
	
//	[RPC]
//	public override void RPCAffectUnitLife (string unitId, int power) {
//		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
//
//		base.AffectUnitLife (unit, power);
//	}
//	
//	[RPC]
//	public override void RPCAffectUnitMana (string unitId, int power) {
//		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
//		
//		base.AffectUnitMana (unit, power);
//	}
	
	[RPC]
	public override void RPCMoveGroupToPoint (string unitGroupId, Vector3 position, bool moveAnyway = false) {
		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);

		base.MoveGroupToPoint (unitGroup, position, moveAnyway);
	}
	
	[RPC]
	public override void RPCDoInterraction (string unitId, string  unitInteractionPerformerId, string unitsToInterractIds)
	{
		UnitSubject unit = WorldController.Instance.FindUnitById (unitId);
		UnitInteractionPerformer performer = WorldController.Instance.FindInteractionPerformerById (unitInteractionPerformerId);
		LinkedList<UnitSubject> units = new LinkedList<UnitSubject> ();
		foreach (string str in unitsToInterractIds.Split (',')) {
			UnitSubject unitVictim = WorldController.Instance.FindUnitById (str);
			units.AddLast (unitVictim);
		};
		if (unit == null || performer == null || units.Count <= 0) {
			return;
		}		
			
		base.DoInterraction (unit, performer, units);
	}
	
//	[RPC]
//	public override void RPCCastUnitsSkillAbility (string unitGroupId, string AbilityTypeName) {
//		UnitGroupSubject unitGroup = WorldController.Instance.FindUnitGroupById (unitGroupId);
//		AbilityType ability = DataTypes.Instance.Abilities.allData[AbilityTypeName];
//		
//		base.CastUnitsSkillAbility (unitGroup, ability);
//	}
	
	[RPC]
	public override void RPCOnBuildingConstructed (string castleId, string constructionTypeName)
	{
		BuildingSubject building = WorldController.Instance.FindBuildingById (castleId);
		BuildingType type = DataTypes.Instance.Buildings.allData[constructionTypeName];
		base.OnBuildingConstructed (building, type);
	}
	
	[RPC]
	public override void RPCOnUpgradeFinished (string id)
	{
		BuildingSubject building = WorldController.Instance.FindBuildingById (id);
		building.OnUpgradeFinished ();		
	}
	#endregion
}


