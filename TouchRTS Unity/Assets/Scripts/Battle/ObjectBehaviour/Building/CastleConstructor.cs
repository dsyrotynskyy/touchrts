using System;
using System.Collections.Generic;
using Entity;
using UnityEngine;

public class CastleConstructor
{
	private BuildingSubject castle;
	private Dictionary<BuildingType, bool> outbuildingsTypes = new Dictionary<BuildingType, bool> ();
	private Dictionary<BuildingType.StructurePositionData, BuildingSubject> outbuildingPositions = new Dictionary<BuildingType.StructurePositionData, BuildingSubject> ();
	private Dictionary<BuildingType.StructurePositionData, BuildingSubject> towerPositions = new Dictionary<BuildingType.StructurePositionData, BuildingSubject> ();
	private List<BuildingSubject> outbuildings = new List<BuildingSubject> ();
	private bool createBuildingConstructed = true;//building will be created already constructed
	
	public CastleConstructor (BuildingSubject castle, string constructedBuildings)
	{
		this.castle = castle;
		
		foreach (BuildingType building in this.castle.BuildingType.Outbuildings) {
			this.outbuildingsTypes.Add (building, false);
		}
		
		foreach (BuildingType.StructurePositionData data in this.castle.BuildingType.towerPositions) {
			this.towerPositions.Add (data, null);
		}
		
		foreach (BuildingType.StructurePositionData data in this.castle.BuildingType.OutbuildingPositions) {
			this.outbuildingPositions.Add (data, null);
		}
		
		if (!WorldController.Instance.IsNetworkGameClient)
			this.ConstructFromString (constructedBuildings);
		
		createBuildingConstructed = false;
	}
	
	private GameObject ConstructTower (BuildingType buildingType)
	{	
		if (buildingType == null) {
			return null;
		}
		
		BuildingType.StructurePositionData towerPositionData = BuildingType.StructurePositionData.zero;
		
		foreach (BuildingType.StructurePositionData data in this.towerPositions.Keys) {
			if (this.towerPositions [data] != null && !this.towerPositions [data].IsDestroyed) {
				continue;
			}
						
			towerPositionData = data;
			break;
		}
		
		if (towerPositionData.IsZeroPosition) {
			return null;
		} 
		
		GameObject outbuilding = ObjectController.CreateOutbuilding (buildingType.Name, this.castle, towerPositionData, castle.BuildingType.Race.Name, this.createBuildingConstructed);
		this.outbuildings.Add (outbuilding.GetComponent<BuildingSubject> ());
		this.towerPositions [towerPositionData] = outbuilding.GetComponent<BuildingSubject> ();
		return outbuilding;
	}
	
	private GameObject ConstructOutbuilding (BuildingType buildingType)
	{
		if (buildingType == null) {
			return null;
		}
		
		BuildingType.StructurePositionData outbuildingPositionData = BuildingType.StructurePositionData.zero;
		
		foreach (BuildingType.StructurePositionData data in this.outbuildingPositions.Keys) {
			if (this.outbuildingPositions [data] != null) {
				continue;
			}
						
			outbuildingPositionData = data;
			break;
		}
		
		if (outbuildingPositionData.IsZeroPosition) {
			return null;
		} 
		
		GameObject outbuilding = ObjectController.CreateOutbuilding (buildingType.Name, this.castle, outbuildingPositionData, castle.BuildingType.Race.Name, this.createBuildingConstructed);
		this.outbuildingPositions [outbuildingPositionData] = outbuilding.GetComponent<BuildingSubject> ();
		this.outbuildings.Add (outbuilding.GetComponent<BuildingSubject> ());
		this.outbuildingsTypes [buildingType] = true;
		return outbuilding;
	}
	
	public bool HasFreePlaceForTowers ()
	{
		foreach (BuildingType.StructurePositionData data in this.towerPositions.Keys) {
			if (this.towerPositions [data] == null || this.towerPositions [data].IsDestroyed) {
				this.towerPositions [data] = null;
				return true;
			}			
		}
		return false;
	}
	
	public bool HasFreePlaceForOutbuildings ()
	{
		foreach (BuildingType.StructurePositionData data in this.outbuildingPositions.Keys) {
			if (this.outbuildingPositions [data] == null) {
				return true;
			}			
		}
		return false;
	}
	
	public void OnCastleCaptured (PlayerData newOwner)
	{
	}
	
	public Dictionary<BuildingType, bool> OutbuildingsTypes {
		get {
			return this.outbuildingsTypes;
		}
	}
	
	public PlayerData PlayerOwner {
		get {
			return this.castle.PlayerOwner;
		}
	}
	
	public GameObject Construct (CommandController controller, BuildingType buildingType, bool freeConstruct)
	{
		GameObject result = null;
		if (buildingType.IsTower) {
			result = ConstructTower (buildingType);
		} else {
			result = ConstructOutbuilding (buildingType);
		}	
		if (result != null && !freeConstruct) {
			this.castle.PlayerOwner.DecreaseMoney (buildingType.Price);				
		}
		return result;
	}
	
	/*
	 * 
	 * */
	public bool CanConstruct (BuildingType buildingType)
	{
		if (buildingType.IsTower) {
			return castle.CanSpawn (buildingType) && HasFreePlaceForTowers ();
		}
		
		foreach (BuildingType b in outbuildingsTypes.Keys) {
			if (b == buildingType) {
//				bool castleCanSpawn = castle.CanSpawn (buildingType);
//				bool outbuildings = !outbuildingsTypes [b];
//				bool hasFreePlaces = HasFreePlaceForOutbuildings ();
//				return castleCanSpawn && outbuildings && hasFreePlaces;

				return castle.CanSpawn (buildingType) && !outbuildingsTypes [b] && HasFreePlaceForOutbuildings ();
			}
		}
		return false;
	}
	
	private void ConstructFromString (string constructionString)
	{
		if (constructionString == null || constructionString.Equals ("")) {
			return;
		}
		string[] buildings = constructionString.Split (',');
		foreach (string buildingString in buildings) {
			int number = -1;
			try {
				number = Int32.Parse (buildingString.Replace (";", ""));
			} catch (Exception) {		
				continue;
			}
			
			ConstructFromInt (number);		
		}
	}
	
	private void ConstructFromInt (int number)
	{
//		if (this.castle.BuildingType.Race.Name.Equals(RacesTemplate.KINGDOM_RACE_NAME)) {
//			int i = 0;
//			i++;
//		}
		BuildingType buildingType = null;
		switch (number) {
		case (0):
			buildingType = GetBuildingTypeByClass (BuildingType.TOWER);
			break;
		case (1) :
			buildingType = GetBuildingTypeByClass (BuildingType.CASARM);
			break;
		case (2) :
			buildingType = GetBuildingTypeByClass (BuildingType.MAGIC_TOWER);						
			break;
		case (3) :
			buildingType = GetBuildingTypeByClass (BuildingType.BLACKSMITH);						
			break;
		}
		
		if (buildingType == null) {
			return;
		}
		
		CommandController.Instance.ConstructBuilding (this, buildingType, true);
//		
//		if (buildingType.IsTower) {
//			ConstructTower (buildingType);			
//		} else {
//			ConstructOutbuilding (buildingType);						
//		}
	}
	
	private BuildingType GetBuildingTypeByClass (int buildingClass) {
		foreach (BuildingType buildingType in outbuildingsTypes.Keys) {
			if (buildingType.BuildingClass == buildingClass) {
				return buildingType;
			}
		}
		return null;
	}

	public BuildingSubject Castle {
		get {
			return this.castle;
		}
	}

	public List<BuildingSubject> Outbuildings {
		get {
			return this.outbuildings;
		}
	}
}


