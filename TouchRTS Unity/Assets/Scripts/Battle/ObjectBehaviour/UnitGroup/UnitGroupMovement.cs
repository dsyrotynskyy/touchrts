using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;
using Setup;

public class UnitGroupMovement
{
	public static int TARGET_FOLLOW_RADIUS = 10;
	public static int MAX_DISTANCE_UNIT = 25;
	private UnitGroupSubject ourGroup;
	private Transform targetToFollow;
	private Transform currentMovementTransform;
	private Vector3 currentMovementPosition;
	GameObject movementTargetObject;
	private Timer followTargetTimer;
	
	public UnitGroupMovement (UnitGroupSubject unitGroup)
	{
		this.ourGroup = unitGroup;
		this.Init ();
	}
	
	private void Init ()
	{
		movementTargetObject = ObjectController.CreateUnitGroupFlag (this.ourGroup);
		movementTargetObject.SetActive (false);
		
		if (this.ourGroup.IsBuilding) {
			movementTargetObject.transform.position = new Vector3 (ourGroup.MainObject.transform.position.x - 8, NavigationEngine.TerrainNormalHeight, ourGroup.MainObject.transform.position.z - 8);		
		} else {
			movementTargetObject.transform.position = ourGroup.MainObject.transform.position;
		}
		
		this.targetToFollow = movementTargetObject.transform;
		this.currentMovementTransform = movementTargetObject.transform;
		this.currentMovementPosition = this.currentMovementTransform.position;
		this.followTargetTimer = Timer.CreateFrequency (300);
	}
	
	public void Process ()
	{
		movementTargetObject.SetActive (this.ourGroup == InputController.CurrentUnitGroup);
		
		if (this.followTargetTimer.EnoughTimeLeft ()) {
			this.CheckMovementCoord ();
		}
		
		if (this.troopShouldFollowHero > 0) {
			troopShouldFollowHero--;			
			if (troopShouldFollowHero == 0) {
				CommandGroupFollowHero ();
			}
		}
		
		this.currentMovementPosition = this.currentMovementTransform.position;
	}
	
	private void CheckMovementCoord ()
	{
		if (targetToFollow == null || !ourGroup.IsHeroGroup) {
			return;
		}
		
		if (targetToFollow.position != this.currentMovementTransform.position) {
			Vector3 target = targetToFollow.position;	
			this.FollowPosition (target);
		} else {
			if (Vector3.Distance (ourGroup.GroupHero.transform.position, this.currentMovementTransform.position) < TARGET_FOLLOW_RADIUS) {
				StopGroupMovement ();
			}
		}
	}
	
	public void FollowTarget (Transform targetObject, bool sendHoleGroup = false)
	{
		this.targetToFollow = targetObject;
		this.FollowPosition (targetObject.position, sendHoleGroup);	
	}

	public bool IsFollowingTarget (Transform targetObject)
	{
		return this.targetToFollow == targetObject;
	}
	
	/*
	 * Move unit to the ymodified coord on the map
	 * */
	public void MoveGroupToPoint (Vector3 target, bool moveAnyway = false)
	{	
		if (WorldController.Instance.IsNetworkGameClient) {
			this.currentMovementTransform.position = target;	
			movementTargetObject.SetActive (this.ourGroup == InputController.CurrentUnitGroup);
			return;
		}
		
		if (IsCoordProhibited (target)) {
			return;
		}

		this.currentMovementTransform.position = target;	
		
		this.UnitGroup.GroupHero.HeroesUnit.MoveUnitTo (target);
		troopShouldFollowHero = 5;
	}

	private Vector3 prevForward = Vector3.zero;
	private bool revertGroupPosition = false;
	private int troopShouldFollowHero = 0;
	
	private void CommandGroupFollowHero () {
		int closeCombatNum = 0;
		int farCombatNum = 0;
		
		Vector3 heroTargetPosition = this.UnitGroup.GroupHero.HeroesUnit.MovementPosition;
		
		Vector3 forward = this.UnitGroup.GroupHero.HeroesUnit.GetForwardToMovementTarget ();
		if (prevForward == Vector3.zero) {
			prevForward = forward;
		}
		float angle = Vector3.Angle (forward, prevForward);
		if (angle > 90) {
			revertGroupPosition = !revertGroupPosition;
		};
		prevForward = forward;

		foreach (UnitSubject unit in ourGroup.Units) {
			if (unit.IsHero) {
				continue;
			}
			
			int num;
			if (unit.UnitType.IsArcher) {
				farCombatNum++;
				num = farCombatNum;
			} else {
				closeCombatNum++;
				num = closeCombatNum;
			}
			
			Vector3 positionForUnit = this.unitGroupInfo.CreatePosition (unit.UnitType, num, heroTargetPosition, forward, revertGroupPosition);
			if (!WorldController.Instance.NavigationEngine.IsCoordProhibited (positionForUnit)) {
				unit.MoveUnitTo (positionForUnit, false);
			} else {
				unit.MoveUnitTo (heroTargetPosition, true);
			};
		} 
	}

	public void StopGroupMovement ()
	{		
		this.targetToFollow = null;	
		
		foreach (UnitSubject unit in ourGroup.Units) {
			unit.StopMovement ();
		}
		this.currentMovementTransform.position = ourGroup.GroupHero.transform.position;
	}
	
	private void FollowPosition (Vector3 target, bool sendHoleGroup = false)
	{		
		if (!IsCoordProhibited (target)) {
			return;
		}				
		this.currentMovementTransform.position = target;		
		
		if (sendHoleGroup) {
			MoveGroupToPoint (target);
		} else {
			Hero.HeroesUnit.MoveUnitTo (target);
		}		
	}
	
	private bool IsCoordProhibited (Vector3 target)
	{
		return WorldController.Instance.NavigationEngine.IsCoordProhibited (target);
	}
	
	public Transform MovementTarget {
		get {
			return this.currentMovementTransform;
		}
	}

	public UnitGroupSubject UnitGroup {
		get {
			return this.ourGroup;
		}
	}
	
	public HeroSubject Hero {
		get {
			return this.ourGroup.GroupHero;
		}
	}

	public void DisableMovementTarget ()
	{
		this.targetToFollow = null;
	}

	public Vector3 CurrentMovementPosition {
		get {
			return this.currentMovementPosition;
		}
	}
	
	private UnitGroupFormator unitGroupInfo {
		get {
			return this.UnitGroup.UnitGroupInfo;
		}
	}
}


