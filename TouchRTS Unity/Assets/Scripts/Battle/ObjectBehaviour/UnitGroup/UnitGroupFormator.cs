using System;
using Entity;
using UnityEngine;

/* Class calculates units formation
 * */
public class UnitGroupFormator
{
	private UnitGroupSubject unitGroup;
	
	private int numCloseBattle;
	private int numArchers;
	
	public UnitGroupFormator (UnitGroupSubject unitGroup)
	{
		this.unitGroup = unitGroup;
	}
	
	public void OnUnitAdded (UnitType unitType) {
		if (unitType.IsArcher) {
			numArchers++;
		} else {
			numCloseBattle++;
		}
	}
	
	public void OnUnitRemoved (UnitType unitType) {
		if (unitType.IsArcher) {
			numArchers--;
		} else {
			numCloseBattle--;
		}
	}

//	public int NumArchers {
//		get {
//			return this.numArchers;
//		}
//	}
//
//	public int NumCloseBattle {
//		get {
//			return this.numCloseBattle;
//		}
//	}
	
	public Vector3 CreatePosition (UnitType unitType, int unitNumber, Vector3 target, Vector3 forward, bool revert)
	{
		const float distance = 2.8f;
		int ROW_SIZE;
		int offset = 4;
		int multipleOffset = 1;
		
		int numOfCategory = 0;
		
		bool inAvangard = !unitType.IsArcher;
		if (unitGroup.AreEnemiesAround () && Vector3.Distance (target, unitGroup.MovementTarget.position) < 10f) {
			inAvangard = !inAvangard;
		}
		
		if (inAvangard) {
			numOfCategory = numCloseBattle;	
		} else {					
			numOfCategory = numArchers;
			offset = 4;
			multipleOffset *= -1;
		}
		
		ROW_SIZE = CalculateRowSize (numOfCategory);
					
		float newX;
		newX = (unitNumber % ROW_SIZE);
		if (revert) {
			newX = ROW_SIZE - newX;
		}
		newX *= distance;
		newX -= distance * ROW_SIZE / 2;
		
		float newZ = (unitNumber / (ROW_SIZE + 1));
		newZ *= distance;
		newZ -= offset;
		newZ *= multipleOffset;
		
		Vector3 positionForUnit = new Vector3 (newX, 0, newZ);
		positionForUnit = Quaternion.FromToRotation (Vector3.forward, -forward) * positionForUnit;
		
		return positionForUnit + target;
	}
	
	private static int CalculateRowSize (int numberOfUnits) {
		double x = Math.Sqrt (numberOfUnits) / 8;
		int res = (int) (x * 16);
		if (res <= 0) {
			res = 1;
		} 
		return res;
	}

	public int NumFarCombat {
		get {
			return this.numArchers;
		}
	}

	public int NumCloseBattle {
		get {
			return this.numCloseBattle;
		}
	}
}


