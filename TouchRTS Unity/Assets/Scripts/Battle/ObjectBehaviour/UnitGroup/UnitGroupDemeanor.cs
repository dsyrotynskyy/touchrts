using UnityEngine;
using System.Collections.Generic;

using Entity;

public class UnitGroupDemeanor
{
	private UnitGroupSubject ourGroup;
	private int numOurCastlesAround = 0;
	private int numOurBuildingsAround = 0;
	private readonly LinkedList<UnitGroupSubject> closestUnitGroups = new LinkedList<UnitGroupSubject> ();
	private readonly LinkedList<UnitSubject> closestFriendUnits = new LinkedList<UnitSubject> ();
	private readonly LinkedList<UnitSubject> closestEnemyUnits = new LinkedList<UnitSubject> ();
	
	private bool DoCheckForClosestUnits = false;
	
	public UnitGroupDemeanor (UnitGroupSubject unitGroup)
	{
		this.ourGroup = unitGroup;	 
	}
	
	public void PrepareToClosestUnitsRecalculation () {
		DoCheckForClosestUnits = true;
		lock (this.closestUnitGroups) {
			this.closestUnitGroups.Clear ();
		}
	}
	
	public void ProcessAsyncUnitGroupCalculation ()
	{
		if (!DoCheckForClosestUnits) {
			return;
		} 
		
		this.CheckForClosestUnitGroups ();
	}
	
	public void ProcessAsyncUnitCalculation () {
		if (!DoCheckForClosestUnits) {
			return;
		} else {
			DoCheckForClosestUnits = false;
		}
		
		this.CalculateClosestUnits ();
		
		if (!this.ourGroup.IsHeroGroup) {
			AttachUnitsToClosestHero ();
		}
	}

	public void OnPlayerOwnerHasChanged ()
	{
		this.CheckForClosestUnits ();
	}
	
	private LinkedList<UnitSubject> unitsForRepartition = new LinkedList<UnitSubject>();
	
	private void AttachUnitsToClosestHero () {
		foreach (UnitGroupSubject heroUnitGroup in this.closestUnitGroups) {
			if (heroUnitGroup.IsHeroGroup && heroUnitGroup.PlayerOwner.Equals(this.PlayerOwner)) {
				foreach (UnitSubject unit in ourGroup.Units) {
					if (unit.IsHero || unit.IsGarrisonUnit) {
						continue;
					}
					
					unitsForRepartition.AddLast (unit);
				}
				
				foreach (UnitSubject unit in unitsForRepartition) {
					unit.UnitGroup = heroUnitGroup;
				}	
				unitsForRepartition.Clear ();
				break;
			}
		}
	}
	
	private static HashSet <UnitGroupSubject> tempUnitGroups = new HashSet<UnitGroupSubject> ();
	
	private void CheckForClosestUnits ()
	{
		tempUnitGroups.Clear ();
		
		foreach (UnitGroupSubject unitGroup in this.closestUnitGroups) {
				tempUnitGroups.Add (unitGroup);
				foreach (UnitGroupSubject farGroup in unitGroup.ClosestUnitGroups ) {
					tempUnitGroups.Add (farGroup);				
				}
			}
		
		lock (this.closestFriendUnits) {
		lock (this.closestEnemyUnits) {
			this.closestEnemyUnits.Clear ();
			this.closestFriendUnits.Clear ();
			
				foreach (UnitGroupSubject unitGroup in tempUnitGroups) {
					foreach (UnitSubject unit in unitGroup.Units) {
					if (unit.PlayerOwner.IsEnemyPlayer (this.PlayerOwner)) {
						if (!unit.IsBuilding) {
							this.closestEnemyUnits.AddFirst (unit);
						}						
					} else {
						this.closestFriendUnits.AddFirst (unit);					
					}							
				}									
			}
		}
		}
	}
	
	private int numEnemiesGroupsBefore = 0;
	private int numEnemiesGroupAfter = 0;
	
	private void CheckForClosestUnitGroups ()
	{
		numOurBuildingsAround = 0;
		numOurCastlesAround = 0;
		
		numEnemiesGroupsBefore = numEnemiesGroupAfter;
		numEnemiesGroupAfter = 0;
		
		foreach (PlayerData player in WorldController.Instance.GetAllPlayers ()) {
			foreach (UnitGroupSubject unitGroup in player.UnitsGroups) {
				Vector3 victimUnitGroup = unitGroup.MainTransformPosition;
				
				if (unitGroup == this.ourGroup) {
					continue;
				}
				
				if (unitGroup.IsHeroGroup) {
					victimUnitGroup = unitGroup.GroupHero.HeroesUnit.TransformPosition;
				}
				lock (this.closestUnitGroups) {
					if (unitGroup.ClosestUnitGroups.Contains (this.ourGroup) || Vector3.Distance (victimUnitGroup, ourGroup.MainTransformPosition) <= UnitGroupSubject.GROUP_INTERACTION_DISTANCE) {
						this.closestUnitGroups.AddFirst (unitGroup);	
						if (unitGroup.PlayerOwner.IsEnemyPlayer(this.PlayerOwner)) {
							numEnemiesGroupAfter++;
						} else if (unitGroup.IsBuilding) {
							numOurBuildingsAround++;
							if (unitGroup.BuildingSubject.IsCastle) {
								numOurCastlesAround++;
							}
						}
					}
				}
			}
		}
	}
	
	private void CalculateClosestUnits () {
		this.CheckForClosestUnits ();
		
		if (numEnemiesGroupsBefore == 0 && numEnemiesGroupAfter > 0 && this.CheckEnemiesCloseToOurTargetPos()) {
			if (ourGroup.IsHeroGroup) {
				Loom.QueueOnMainThread(()=>{
					ourGroup.AttackEnemies();
				});
			}			
		}
		
		if (numEnemiesGroupsBefore > 0 && numEnemiesGroupAfter == 0) {
			Loom.QueueOnMainThread(()=>{
				CommandController.Instance.MoveGroupToPoint (this.ourGroup, this.ourGroup.MovementTarget.transform.position, true);			
	        });
		}
	}

	private bool CheckEnemiesCloseToOurTargetPos ()
	{
		foreach (UnitGroupSubject unitGroup in closestUnitGroups) {
			if (unitGroup.PlayerOwner.IsEnemyPlayer(this.PlayerOwner) && Vector3.Distance(unitGroup.MainTransformPosition, ourGroup.MovementPosition) < UnitGroupSubject.GROUP_INTERACTION_DISTANCE) {
				return true;
			}
		}
		return false;
	}
	
	public LinkedList<UnitGroupSubject> ClosestUnitGroups 
	{
		get {
			return this.closestUnitGroups;
		}
	}
	
	public LinkedList<UnitSubject> ClosestEnemies 
	{
		get {
			return this.closestEnemyUnits;
		}
	}
	
	public LinkedList<UnitSubject> ClosestFriendUnits 
	{
		get {		
			return this.closestFriendUnits;
		}
	}
	
	public HeroSubject Hero {
		get {
			return this.ourGroup.GroupHero;
		}
	}
	
	public PlayerData PlayerOwner {
		get {
			return this.ourGroup.PlayerOwner;
		}
	}
	
	public bool IsFightingEnemies () {
		return this.closestEnemyUnits.Count > 0;
	}
	
	public bool IsDefendingCastleFromEnemies () {
		return numOurCastlesAround > 0 && IsFightingEnemies ();
	}
	
	public bool IsDefendingBuildingFromEnemies () {
		return numOurBuildingsAround > 0 && IsFightingEnemies ();
	}
}


