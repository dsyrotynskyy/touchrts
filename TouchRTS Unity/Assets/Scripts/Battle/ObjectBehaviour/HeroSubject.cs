using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class HeroSubject : TMonoBehaviour
{
	public string id;
	public int playerId = 1;
	public bool killOnStart = false;
	private HeroType heroType;
	private UnitGroupSubject heroesGroup;
	private UnitSubject heroesUnit;
	private bool isInited = false;
	private bool isGhost = false;
	
	/*
	 * This method must be called from WorldController
	 * */
	internal void Init (WorldController worldController)
	{
		if (this.transform.parent != null) {
			this.heroesGroup = (UnitGroupSubject)this.transform.parent.GetComponent (typeof(UnitGroupSubject));		
		}
		if (heroesGroup == null) {			
			UnitGroupSubject unitGroup = ObjectController.CreateUnitGroupObject (this.gameObject.name);
			this.heroesGroup = unitGroup;
		}
		
		this.heroesUnit = (UnitSubject)this.gameObject.GetComponent (typeof(UnitSubject));
		if (heroesUnit == null) {
			this.heroesUnit = gameObject.AddComponent<UnitSubject> ();
		}
		
		this.heroesGroup.Init (playerId, this);
		this.heroType = PlayerOwner.Types.Heroes.Get (this.gameObject.name);	
		
		this.heroesUnit.InitHeroUnit (this);
		this.heroesGroup.SetHeroUnit (this.gameObject);	
			
		foreach (SpellType spell in this.heroType.Spells) {
			this.StudySpell (spell);
		}
					
		this.isInited = true;
	}
	
	private bool firstStart = true;
	
	public void OnFixedUpdate ()
	{
		if (firstStart) {
			if (this.PlayerOwner.IsHuman ()) {
				InputController.SetCurrentGroup (this.HeroesGroup, true);
			}
			firstStart = false;
		}
		
		if (!isInited) {
			return;
		}
		
		if (killOnStart) {
			this.heroesUnit.currentHealth = 0;
			killOnStart = false;
		}
	}
	
	/**
	 * Move hero to unmodified y position
	 * */
	public void MoveHeroGroupToUnmodifiedPoint (Vector3 point)
	{
		CommandController.Instance.MoveGroupToPoint (this.heroesGroup, new Vector3 (point.x,NavigationEngine.TerrainNormalHeight, point.z));
	}
	
	public float GetManaPercent ()
	{
		return this.heroesUnit.GetManaPercent ();
	}

	public void AffectMana (int power)
	{
		CommandController.Instance.AffectUnitMana (heroesUnit, power);
	}
	
	public HeroType GetHeroType ()
	{
		return (HeroType)this.heroesUnit.UnitType;
	}
	
	#region SpellBehaviour
	private const int OBJECT_SPAWN_PERIOD = 300;

	private SpellSubject currentSpell;
	private List<SpellType> spells = new List<SpellType> ();
	private Timer objectSpawnTimer = Timer.CreateFrequency (OBJECT_SPAWN_PERIOD);	
 	
	public bool DoSpell (CommandController commandController, SpellType spellType, Vector3 point)
	{
		if (!this.CanSpawnSpell (spellType)) {
			CommandController.Instance.StopSpell (this);
			
			return false;
		}
		
		if (spellType.IsCreatingObjectOnMap() && !objectSpawnTimer.EnoughTimeLeft ()) {
			return false;
		}
		
		if (this.IsSpawningSpell && !spellType.IsCreatingObjectOnMap ()) {
			this.ContinueSpawnSpell (spellType, point);
		}
		
		this.RotateHeroTo (point);		
		
		if (this.HeroesGroup.IsEnoughCloseForSpellSpawn (point)) {
			this.HeroesGroup.StopMovement ();
			this.CreateNewSpell (spellType, point);	
			return true;
		} else {
			this.MoveHeroGroupToUnmodifiedPoint (point);
			return false;
		}
	}
	
	private void CreateNewSpell (SpellType spellType, Vector3 pointOriginal)
	{	
		if (currentSpell != null) {
			if (spellType == currentSpell.SpellType && !spellType.IsCreatingObjectOnMap()) {
				return;
			}
			CommandController.Instance.StopSpell (this);
		}
				
		Vector3 point = new Vector3 (pointOriginal.x, NavigationEngine.TerrainNormalHeight, pointOriginal.z);
		
		int manaCost = spellType.ManaCost;
		if (this.heroesUnit.CurrentMana - manaCost > 0) {
			CommandController.Instance.AffectUnitMana (this.heroesUnit, manaCost);
			ServerCommandController.Instance.CreateSpell (spellType, this, point, PlayerOwner);
			heroesGroup.StopMovement ();
			heroesUnit.PlayAttackAnimation ();
			
			if (spellType.IsCreatingObjectOnMap ()) {
				heroesUnit.OnUnitStartedSpellCasting ();
			} else 
			if (spellType.IsMovableSpell () || spellType.IsRaySpell ()) {
				heroesUnit.OnUnitStartedSpellCasting ();				
			} 			
		} else {
			heroesUnit.PlayIdleAnimation ();
		}
	}
	
	private void ContinueSpawnSpell (SpellType spellType, Vector3 pointOriginal)
	{
		if (spellType.IsCreatingObjectOnMap ()) {
			return;
		}
		
		if (currentSpell.ShouldAffectMana ()) {
			this.AffectMana (spellType.ManaCost);
		}
		
		Vector3 point = new Vector3 (pointOriginal.x, NavigationEngine.TerrainNormalHeight, pointOriginal.z);
		
		heroesUnit.PlayAttackAnimation ();
		
		if (spellType.IsMovableSpell ()) {			
			currentSpell.transform.position = point;
		}
		
		if (spellType.IsRaySpell ()) {			
		}
	}
	
	public void StopSpellCast (CommandController commandController)
	{
		if (currentSpell != null) {
			if (!currentSpell.SpellType.IsCreatingObjectOnMap ()) {				
				currentSpell.StopCasting ();	
			}
			heroesUnit.PlayIdleAnimation ();		
			heroesUnit.OnUnitEndedSpellCasting ();
			currentSpell = null;
		}
	}
	
	public bool CanTrainSpell (SpellType spellType)
	{
		int maxLevelOfSpell = 0;
		
		foreach (SpellType ourSpell in Spells) {
			if (ourSpell.Name.Equals (spellType.Name)) {
				maxLevelOfSpell = ourSpell.SpellLevel;
			}
		}
		
		if (spellType.SpellLevel - maxLevelOfSpell == 1) {//nextLevel of Spell
			return true;
		}
		
		return false;
	}

	public bool HasEnoughMoneyToBuySpell (SpellType spellType)
	{
		if (spellType.Price > PlayerOwner.MoneyCount) {
			return false;
		}
		
		return true;
	}
	
	public bool HasSpell (SpellType spellType)
	{		
		return spells.Contains (spellType);
	}
	
	public void BuySpell (CommandController controller, SpellType spellType)
	{
		PlayerOwner.DecreaseMoney (spellType.price);
		StudySpell (spellType);
	}
	
	public void StudySpell (SpellType spellType)
	{
		int numberOfSpellToReplace = -1;
		int i = 0;
		foreach (SpellType prevSpell in spells) {
			if (prevSpell.Name.Equals (spellType.Name)) {
				numberOfSpellToReplace = i;
				break;
			}
			i++;			
		}
		
		if (numberOfSpellToReplace != -1) {
			spells.RemoveAt (numberOfSpellToReplace);
		}
		
		spells.Add (spellType);	
	}
	
	public bool CanSpawnSpell (SpellType spellType)
	{
		return spellType.ManaCost < this.heroesUnit.CurrentMana;
	}
	
	public bool IsSpawningSpell 
	{
		get {
			return currentSpell != null;
		}
	}
	#endregion

	public UnitGroupSubject HeroesGroup {
		get {
			return this.heroesGroup;
		}
	}

	public UnitSubject HeroesUnit {
		get {
			return this.heroesUnit;
		}
	}

	public PlayerData PlayerOwner {
		get {
			return this.heroesGroup.PlayerOwner;
		}
	}

	public HeroType HeroType {
		get {
			return this.heroType;
		}
	}
	
	public void RotateHeroTo (Vector3 point)
	{
		HeroesUnit.PerformRotation (point);	
	}
	
	public bool IsDead {
		get {
			return this.heroesUnit.IsDead;
		}
	}

	public void OnDeath ()
	{
		this.isGhost = true;
		CommandController.Instance.StopSpell (this);
	}

	public void Resurrect ()
	{
		this.isGhost = false;
		this.heroesUnit.Resurrect ();
	}

	public List<SpellType> Spells {
		get {
			return this.spells;
		}
	}

	public bool IsGhost {
		get {
			return this.isGhost;
		}
	}

	public SpellSubject CurrentSpell {
		get {
			return this.currentSpell;
		}
		set {
			currentSpell = value;
		}
	}
}

