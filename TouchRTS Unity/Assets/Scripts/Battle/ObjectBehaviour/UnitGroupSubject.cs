using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class UnitGroupSubject : TMonoBehaviour {
	public static int UNIT_ADDED = 1;
	public static int UNIT_REMOVED = 2;
	public static int HERO_DIES = 3;
	
	public static int TARGET_FOLLOW_RADIUS = 10;
	public static int GROUP_INTERACTION_DISTANCE = 40;
	
	public string groupName;
	private int aliveUnitCount = 0;
	private Transform playerMovementPosition;
	private readonly HashSet<UnitSubject> units = new HashSet<UnitSubject> ();
	private readonly Dictionary<AbilityType, Timer> unitsSkills = new Dictionary<AbilityType, Timer> ();
	private readonly LinkedList<UnitGroupObserver> unitGroupObservers = new LinkedList<UnitGroupObserver> ();
	private PlayerData playerOwner = null;
	private BuildingSubject buildingSubject;
	private HeroSubject groupHero;
	private UnitSubject groupMainUnit;
	private UnitGroupDemeanor groupDemeanor;
	private UnitGroupMovement groupMovement;
	private UnitGroupFormator unitGroupInfo;
	private Timer castleCaptureTimer;
	private bool IsInited = false;
	
	private Vector3 mainTransformPosition;
	
	private static bool DO_ASYNC = false;
	
//	private static int unitGroupTotalCount = 0;
	
	/* Must be called on scene start
	 * */
	public static void Prepare () {
//		unitGroupTotalCount = 0;
	}
	
	internal void Init (int playerId, HeroSubject hero)
	{			
		this.groupMainUnit = hero.HeroesUnit;
		Init (playerId, hero, null);
	}
	
	internal void Init (int playerId, BuildingSubject buildingSubject)
	{	
		this.groupMainUnit = buildingSubject.BuildingUnitSubject;
		Init (playerId, null, buildingSubject);
	}
	
	private void Init (int playerId, HeroSubject hero, BuildingSubject buildingSubject)
	{
		this.buildingSubject = buildingSubject;
		this.unitGroupInfo = new UnitGroupFormator (this);
		this.groupHero = hero;
		this.playerOwner = WorldController.Instance.AllPlayers [playerId];
		this.playerOwner.AddUnitGroup (this);
		
		groupDemeanor = new UnitGroupDemeanor (this);
		groupMovement = new UnitGroupMovement (this);
				
		this.AddAllAttachedUnitsToGroup ();
		this.castleCaptureTimer = Timer.CreateFrequency (500);
				
		IsInited = true;
		
		WorldController.Instance.RegisterNewUnitGroup (this);
		
		this.mainTransformPosition = this.MainTransform.position;
	}
	
	internal void OnDisable ()
	{
		if (WorldController.Instance == null) {
			return;
		}
		playerOwner.RemoveUnitGroup (this);
		WorldController.Instance.WorldData.OnUnitGroupDestroyed ();
	}

	private void AddAllAttachedUnitsToGroup ()
	{
//		int i = 1;
		foreach (Transform unit in gameObject.transform) {		
//			Debug.Log ("Num " + i++ + " total " + gameObject.transform.childCount);
			AddAndActivateUnit (unit.gameObject);	
		}
	}
		
	/**
	 * Activates unit
	 * */
	public void AddAndActivateUnit (GameObject unit, bool isGarrison = false)
	{
		if (unit.gameObject == MainObject || unit.tag != "Unit") {
			return;
		}
		
		if (unit.transform.parent != gameObject.transform) {
			unit.transform.parent = gameObject.transform;
		}
//		unit.transform.parent = WorldController.Instance.UnitsCategoryGameObject.transform;	

		UnitSubject unitBeh = (UnitSubject)unit.GetComponent (typeof(UnitSubject));
		unitBeh.IsGarrisonUnit = isGarrison;
		unitBeh.InitSimpleUnit (this);

		this.AddUnit (unitBeh);
	}
	
	public void SetHeroUnit (GameObject unit)
	{
		UnitSubject unitBeh = (UnitSubject)unit.GetComponent (typeof(UnitSubject));
		this.AddUnit (unitBeh);
//		unitBeh.groupName = this.name;	
	}

	public void SetNewOwner (PlayerData newOwner)
	{
		PlayerData oldOwner = this.playerOwner;
		this.playerOwner.RemoveUnitGroup (this);
		
		this.playerOwner = newOwner;
		newOwner.AddUnitGroup (this);
		
		Units.RemoveWhere (UnitIsDeadCondition);
		
		this.groupDemeanor.OnPlayerOwnerHasChanged ();
		
		foreach (UnitSubject unit in Units) {		
			unit.OnPlayerOwnerChanged ( oldOwner);		
		}
	}
	
	private bool UnitIsDeadCondition (UnitSubject unit)
	{
		if (unit.UnitType.IsBuilding) {
			return false;
		}
		return unit.IsDead;
	}

	public HeroSubject GroupHero {
		get {
			return this.groupHero;
		}
	}
	
	public void AddUnit (UnitSubject victim)
	{
		//Register spell skill in unitGroup
		if (victim.UnitType.Skill != null && !unitsSkills.ContainsKey (victim.UnitType.Skill)) {
			unitsSkills.Add (victim.UnitType.Skill, Timer.CreateLifePeriod (victim.UnitType.SkillRechargePeriod));
		}
		
//		victim.groupName = this.name;	
		units.Add (victim);
		unitGroupInfo.OnUnitAdded (victim.UnitType);
		
		OnGroupStateChanged (UNIT_ADDED);
		this.playerOwner.AddUnit (victim);
		WorldController.Instance.WorldData.OnNewUnitAdded ();
		
		if (!victim.IsBuilding && !victim.IsHero) {
			aliveUnitCount++;
		}		
	}
	
	public void RemoveUnit (UnitSubject victim)
	{	
		if (!victim.IsBuilding && !victim.IsHero) {
			aliveUnitCount--;
		}
		
		units.Remove (victim);
		unitGroupInfo.OnUnitRemoved (victim.UnitType);
		
		OnGroupStateChanged (UNIT_REMOVED);
		playerOwner.RemoveUnit (victim);
		WorldController.Instance.WorldData.OnUnitRemoved ();
		
		//Check remove spell skill from squad
		AbilityType skill = victim.UnitType.Skill;
		if (skill == null) {
			return;
		}
		bool isPresent = false;
		foreach (UnitSubject unit in units) {
			if (unit.UnitType.Skill == skill) {
				isPresent = true;
				break;
			}
		}
		if (!isPresent) {
			unitsSkills.Remove (skill);
		}
	}
	
	public bool CanCastUnitsSkill (AbilityType skill) {
		if (skill != null && !unitsSkills.ContainsKey (skill)) {
			throw new System.Exception ();
		}
		
		if (!this.UnitGroupDemeanor.IsFightingEnemies ()) {
			return false;
		}
		
		Timer timer = unitsSkills[skill];
		
		return !timer.IsActive;
	}
	
	public void CastUnitsSkill (CommandController controller, AbilityType skill) {
		if (skill == null) {
			throw new System.Exception ();
		}
		
		if (skill != null && !unitsSkills.ContainsKey (skill)) {
			throw new System.Exception ();
		}
		
		Timer timer = unitsSkills[skill];
		
		if (timer.IsActive) {
			return;
		}
		
		foreach (UnitSubject unit in units) {
			if (unit.UnitType.Skill == skill) {
				unit.CastSkill ();
			}
		}
		
		timer.Restart ();
	}
	
	public void OnHeroDies () {
		if (!this.IsHeroGroup || !this.GroupHero.IsDead) {
			return;
		}
		
		OnGroupStateChanged (HERO_DIES);
	}
	
	public LinkedList<UnitGroupSubject> ClosestUnitGroups 
	{
		get {
			return this.groupDemeanor.ClosestUnitGroups;
		}
	}
	
	public LinkedList<UnitSubject> ClosestEnemies 
	{
		get {
			return this.groupDemeanor.ClosestEnemies;
		}
	}
	
	public LinkedList<UnitSubject> ClosestFriendUnits 
	{
		get {
			return this.groupDemeanor.ClosestFriendUnits;
		}
	}
	
	/* FixedUpdate method */
	public void OnFixedUpdate ()
	{		
		if (!IsInited) {
			return;
		}
		
		if (WorldController.Instance.IsNetworkGameClient) {
			return;
		}
		
		if (!DO_ASYNC) {
			groupDemeanor.ProcessAsyncUnitGroupCalculation ();
			groupDemeanor.ProcessAsyncUnitCalculation ();		
		}
		groupMovement.Process ();
		
		CheckCastleInteraction ();
		
		this.mainTransformPosition = this.MainTransform.position;
	}
		
	public void FollowTarget (Transform targetObject, bool sendHoleGroup = false)
	{
		if (!CanMove ()) {
			return;
		}
		this.groupMovement.FollowTarget (targetObject, sendHoleGroup);
	}
	
	public bool IsFollowingTarget (Transform targetObject)
	{
		return this.groupMovement.IsFollowingTarget (targetObject);
	}	
	
	/*
	 * Move unit to the ymodified coord on the map
	 * */
	public void MoveGroupToPoint (CommandController commandController, Vector3 target, bool moveAnyway = false)
	{	
		if (!CanMove ()) {
			return;
		}
		
		this.groupMovement.DisableMovementTarget ();
		this.groupMovement.MoveGroupToPoint (target, moveAnyway);
	}

	public void StopMovement ()
	{		
		this.groupMovement.StopGroupMovement ();
	}
	
	public int GetNumUnits ()
	{
		return this.units.Count;
	}
	
	public bool IsHeroGroup 
	{ 
		get {return this.groupHero != null;}
	}

	public HashSet<UnitSubject> Units {
		get {
			return this.units;
		}
	}
	
	public int AliveUnitsCount {
		get {
			int num = aliveUnitCount;
			
			if (this.IsHeroGroup && !this.groupHero.IsDead) {
				num++;
			}
			
			return num;
		}
	}

	public PlayerData PlayerOwner {
		get {
			return this.playerOwner;
		}
	}
	
	public Transform MovementTarget {
		get {
			return this.groupMovement.MovementTarget;
		}
	}

	public BuildingSubject BuildingSubject {
		get {
			return this.buildingSubject;
		}
	}
	
	public bool IsBuilding 
	{
		get {
			return this.buildingSubject != null;
		}
	}
	
	public bool CanMove ()
	{
		if (this.groupHero != null && this.groupHero.HeroesUnit.IsUnavailableForCommand) {
			return false;
		}
		
		return this.groupHero != null;
	}
	
	public int GetGroupSizeCoeficient ()
	{
		return this.units.Count / 5 + 1;
	}

	public bool IsEnoughClose (Vector3 position, float distanse)
	{
		return Vector3.Distance (position, MainObject.transform.position) < GROUP_INTERACTION_DISTANCE;
	}

	public bool IsEnoughCloseForSpellSpawn (Vector3 point)
	{
		return IsEnoughClose (point, GROUP_INTERACTION_DISTANCE);
	}

	public bool IsEnoughCloseForUnitSpawn (BuildingSubject buildingSubject)
	{
		return IsEnoughClose (buildingSubject.BuildingUnitGroup.MovementTarget.position, GROUP_INTERACTION_DISTANCE);
	}

	public bool IsEnoughCloseForInteraction (UnitGroupSubject unitGroup)
	{
		return IsEnoughClose (unitGroup.MainTransform.position, GROUP_INTERACTION_DISTANCE);
	}
	
	public Vector3 MovementPosition {
		get {
			if (groupMovement == null) {
				return mainTransformPosition;
			}
			return this.groupMovement.CurrentMovementPosition;
		}	
	}

	public void AttackEnemies ()
	{
		foreach (UnitSubject unit in this.Units) {
			if (unit.IsDead) {
				continue;
			}
			unit.StopMovement ();
			unit.AttackClosestEnemy ();
		}
	}
	
	#region Interaction
	private void CheckCastleInteraction ()
	{
		if (!castleCaptureTimer.EnoughTimeLeft ()) {
			return;
		}
		
		if (AliveUnitsCount <= 0) {
			return;
		}
		
		bool canCapture = ClosestEnemies.Count <= 0;
		
		foreach (UnitGroupSubject closeGroup in this.ClosestUnitGroups) {
			if (closeGroup.IsBuilding && closeGroup.PlayerOwner.IsEnemyPlayer (this.PlayerOwner)) {
				if (canCapture) {
					closeGroup.BuildingSubject.DoCapture (this);
				} else {
					closeGroup.BuildingSubject.BreakCapture ();
				}
			}
		}
	}
	#endregion
	
	private void PrepareUnitGroup() {
		this.groupDemeanor.PrepareToClosestUnitsRecalculation ();
		this.groupDemeanor.ProcessAsyncUnitGroupCalculation ();
		this.groupDemeanor.ProcessAsyncUnitCalculation ();		
	}

	public UnitGroupFormator UnitGroupInfo {
		get {
			return this.unitGroupInfo;
		}
	}
	
	public bool AreEnemiesAround () {
		return this.UnitGroupDemeanor.IsFightingEnemies ();
	}
			
	public bool IsGroupStopped () {
		int numStopped = 0;
		const float percentStopped = 70;
		foreach (UnitSubject unit in this.units) {
			if (unit.UnitDemeanorController.IsFree()) {
				numStopped++;
			}
			int minNumStopped = (int)(percentStopped / 100 * units.Count);
			if (numStopped > minNumStopped) {
				return true;
			}
		}
		return false;
	}
	
	public void OnGroupStateChanged (int actionId) {
		foreach (UnitGroupObserver observer in unitGroupObservers) {
			observer.OnGroupStateChanged (actionId);
		}
	}
	
	public void RegisterObserver(UnitGroupObserver unitGroupObserver) {
		unitGroupObservers.AddLast (unitGroupObserver);
	}
	
	public void RemoveObserver (UnitGroupObserver unitGroupObserver) {
		unitGroupObservers.Remove (unitGroupObserver);
	}
	
	public interface UnitGroupObserver {
		void OnGroupStateChanged (int action);
	}

	public UnitGroupDemeanor UnitGroupDemeanor {
		get {
			return this.groupDemeanor;
		}
	}

	public UnitSubject GroupMainUnit {
		get {
			return this.groupMainUnit;
		}
	}

	public Dictionary<AbilityType, Timer> UnitsSkills {
		get {
			return this.unitsSkills;
		}
	}
	
	public string id {
		get {
			if (this.IsHeroGroup) {
				return this.GroupHero.id;
			}
			return this.buildingSubject.id;
		}
	}

	public void SetImmortal (bool immortal)
	{
		foreach (UnitSubject unit in this.units) {
			unit.Immortal = immortal;
		}
	}
	
//	public GameObject gameObject {
//		get {
//			throw new System.NotImplementedException ();
//		}
//	}
//	
//	public Transform transform {
//		get {
//			throw new System.NotImplementedException ();
//		}
//	}
	
	public GameObject MainObject {
		get {
//			if (this.IsHeroGroup) {
//				return this.GroupHero.gameObject;
//			} else {
//				return this.BuildingSubject.gameObject;
//			}
			return GroupMainUnit.gameObject;
		}
	}
	
	public Transform MainTransform {
		get {
			return MainObject.transform;
		}
	}
	
	public Vector3 MainTransformPosition {
		get {
			return this.mainTransformPosition;
		}
	}
	
	public void UpdateUnitPathLogic () {
		if (this.IsHeroGroup) {			
			this.GroupMainUnit.UnitDemeanorController.MovementConroller.UpdateAsync ();
		}
		foreach (UnitSubject unit in Units) {	
			if (unit == this.groupMainUnit) {
				continue;
			}
			
			if (unit.UnitDemeanorController.MovementConroller != null) {
				unit.UnitDemeanorController.MovementConroller.UpdateAsync ();
			}
		}
	}
	
	#region Demeanor
	private static Timer findClosestUnitsTimer = Timer.CreateFrequency (1500);
	
	public static bool ShouldCalculateClosestUnits () {
		return findClosestUnitsTimer.EnoughTimeLeft ();
	}
	
	public static void CalculateClosestUnits (IEnumerable<UnitGroupSubject> allUnitGroups) {
		foreach (UnitGroupSubject unitGroup in allUnitGroups) {
			unitGroup.groupDemeanor.PrepareToClosestUnitsRecalculation ();
		}
				
		foreach (UnitGroupSubject unitGroup in allUnitGroups) {
			unitGroup.UnitGroupDemeanor.ProcessAsyncUnitGroupCalculation ();
		}
		
		foreach (UnitGroupSubject unitGroup in allUnitGroups) {
			unitGroup.UnitGroupDemeanor.ProcessAsyncUnitCalculation ();
		}
	}
	#endregion	

}
