using UnityEngine;
using System.Collections;

//Tyran Monobahaviour
public abstract class TMonoBehaviour : MonoBehaviour
{
	private string asyncName;
	
	public void Awake () {
		asyncName = this.name;
	}
	
	public static bool operator ==(TMonoBehaviour a, TMonoBehaviour b)
	{
		return System.Object.ReferenceEquals(a, b);
	}
	
	public static bool operator !=(TMonoBehaviour a, TMonoBehaviour b)
	{
	    return !(a == b);
	}
	
	public override string ToString()
    {
    	return this.asyncName;
    }
			
	public override bool Equals (object obj) {
		return this == obj;
	}
}

