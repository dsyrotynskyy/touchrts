using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;
using System;
using Setup;

public class UnitSubject : TMonoBehaviour, UnitInteractionPerformer
{
	public string id;
	private static float CLICKABLE_AREA_SIDE_SIZE = 6f;
	public static float UNIT_BODY_SIDE_SIZE = 0.25f;//must be little becouse of unit scaling
//	public static float UNIT_Y_POS_MODIFIER = 0.4f;
	public static float Y_COORDS_RANGE = 1.8f;
	private bool isInited = false;
	
	/*
	 * If is true - unit will be removed
	 * */
	public bool shouldDestroyUnit = false;
	public string unitTypeName;
//	public string groupName;
	private UnitGroupSubject unitGroup;
	private UnitType unitType;
	public int currentMana;
	public int currentHealth;
	public bool Immortal = false;
	private UnitDemeanor unitDemeanorController;
	private UnitViewController unitViewController;
	private Vector3 transformPosition;
	
	/*
	 * If is false - unit will be always attached to its unitGroup
	 * */
	private bool isGarrisonUnit = false;
	private bool deathProcessed = false;
	
	/*
	 * If unit is hero
	 * */
	private HeroSubject heroData;
	private BuildingSubject buildingData;
	private HashSet<EffectData> effectsOnUnit = new HashSet<EffectData> ();
	private Timer restoreHealthTimer;
	private Timer restoreManaTimer;
	private UnitSpawnManager unitSpawnManager;
	private float bodyRadius;
	private SphereCollider bodySphere;
	private UnitNetworkSerializer networkSerializer;
		
	/*
	 * Use this function if unit is not hero
	 * */
	public void InitSimpleUnit (UnitGroupSubject unitGroupData)
	{			
		this.unitTypeName = gameObject.name;
		this.unitType = unitGroupData.PlayerOwner.Types.Units.Get (this.unitTypeName);
		
		if (WorldController.Instance.IsNetworkGameClient) {
			unitDemeanorController = new ClientDemeanor ();
		} else {
			unitDemeanorController = new TroopDemeanor ();
		}

		this.unitGroup = unitGroupData;

		this.Init ();

		if (this.unitGroup.AreEnemiesAround ()) {
			this.unitDemeanorController.OnMovementHasEnded ();
			this.unitDemeanorController.InteractBestTarget ();
		} 
	}
	
	/*
	 * Use this function if unit is hero
	 * */
	public void InitHeroUnit (HeroSubject heroData)
	{
		this.gameObject.layer = GameSettings.Layer.UNIT;
		this.id = heroData.id;
		this.heroData = heroData;
		this.unitTypeName = gameObject.name;
		this.unitType = heroData.HeroType;

		if (WorldController.Instance.IsNetworkGameClient) {
			unitDemeanorController = new ClientDemeanor ();
		} else {
			unitDemeanorController = new TroopDemeanor ();
		}

		this.unitGroup = heroData.HeroesGroup; //Simple unit group change

		this.Init ();	
	}

	public void InitBuildingUnit (BuildingSubject buildingData)
	{
		this.id = buildingData.id;
		this.buildingData = buildingData;
		this.unitType = buildingData.BuildingType;
		this.IsGarrisonUnit = true;
		if (WorldController.Instance.IsNetworkGameClient) {
			unitDemeanorController = new ClientDemeanor ();
		} else {
			unitDemeanorController = new BuildingDemeanor ();
		}
		this.unitGroup = buildingData.BuildingUnitGroup; //Simple unit group change
		this.Init ();
	}
    
	private void Init ()
	{	
		transformPosition = new Vector3 (gameObject.transform.position.x , NavigationEngine.TerrainNormalHeight, gameObject.transform.position.z);
		if (this.unitType.IsBuilding) {
			this.gameObject.tag = GameSettings.Tag.BUILDING;
			this.gameObject.layer = GameSettings.Layer.OBSTACLES;
		} else {
			this.gameObject.tag = GameSettings.Tag.UNIT;
			this.gameObject.layer = GameSettings.Layer.UNIT;

		}

		this.PrepareView ();
		this.PrepareColliders ();
				
		this.shouldDestroyUnit = false;		
		
		this.currentHealth = unitType.MaxHealth;		
		this.currentMana = unitType.maxMana;	
		
		this.name = this.unitType.Name;	
		
		this.restoreHealthTimer = Timer.CreateFrequency (this.unitType.healthRestorationFrequency);
		if (this.unitType.maxMana > 0) {
			this.restoreManaTimer = Timer.CreateFrequency (this.unitType.manaRestorationPeriod);
		}
		
		unitDemeanorController.Init (this);
		
		this.unitSpawnManager = new UnitSpawnManager (this);
		
		this.isInited = true;
		
		WorldController.Instance.RegisterNewUnit (this);

		this.PrepareAnimation ();
		
		if (WorldController.Instance.IsNetworkGame) {
			networkSerializer = this.gameObject.AddComponent<UnitNetworkSerializer> ();
			networkSerializer.Init (this);
		}
	}

	private void PrepareView ()
	{
		transform.position = new Vector3 (transform.position.x, NavigationEngine.TerrainNormalHeight, transform.position.z);			
		
//		foreach (Transform child in this.transform) {
//			Destroy (child.gameObject);
//		}
		
		unitViewController = new UnitViewController (this);
	}
	
	private void PrepareColliders ()
	{
		bool isBuilding = unitType.IsBuilding;
		if (!isBuilding) {
			BoxCollider clickBox = this.gameObject.AddComponent<BoxCollider> ();
			clickBox.isTrigger = true;
			clickBox.size = new Vector3 (CLICKABLE_AREA_SIDE_SIZE, 3f, CLICKABLE_AREA_SIDE_SIZE);
				
			this.bodySphere = this.modelGameObject.GetComponent<SphereCollider> ();
			if (bodySphere == null) {
				bodySphere = this.modelGameObject.AddComponent<SphereCollider> ();
				((SphereCollider)bodySphere).radius = UNIT_BODY_SIDE_SIZE;
			}
			
			bodyRadius = ((SphereCollider)bodySphere).radius;
			WorldController.Instance.UnitColliders.AddLast (bodySphere);			
		} else {
			SphereCollider collider = ModelGameObject.GetComponent<SphereCollider> ();
			if (collider != null) {
				WorldController.Instance.ObstacleColliders.AddLast (collider);				
			}
			bodySphere = collider;
		}
	}
	
	private bool staticObstacleRegistered = false;
	
//	public void OnUpdate () {
//		if (!this.IsBuilding) {
//			UnitDemeanorController.MovementConroller.OnUpdate ();
//		}
//	}
	
	/* FixedUpdate method*/
	public void OnFixedUpdate ()
	{		
		if (shouldDestroyUnit) {
			this.PerformUnitDestroying ();
		}
			
		if (!isInited) {
			return;
		} 
		
		transformPosition = new Vector3 (gameObject.transform.position.x , NavigationEngine.TerrainNormalHeight, gameObject.transform.position.z);
				
		if (!staticObstacleRegistered) {
			if (unitType.IsBuilding) WorldController.Instance.NavigationEngine.AddStaticObstacleObject (bodySphere);
			staticObstacleRegistered = true;
		}
		
		if (networkSerializer != null) {
			networkSerializer.OnFixedUpdate ();
		}
		
		if (this.heroData != null) {
			this.heroData.OnFixedUpdate ();
		}
		
		if (this.buildingData != null) {
			this.buildingData.OnFixedUpdate ();
		}	
		
		if (this.unitType.IsInvulnerable || !this.IsDead) {
			this.unitSpawnManager.Process ();
		}	
		
		unitViewController.UpdateView ();		
		
		if (WorldController.Instance.IsNetworkGameClient) {
			unitDemeanorController.OnFixedUpdate ();
		} else {
			OnServerFixedUpdate ();
		}
		
//		OnAsyncUpdate ();				
	}
	
	public void OnAsyncUpdate () {
		if (!WorldController.Instance.IsNetworkGameClient) {
			this.RestoreParams ();
			
			unitDemeanorController.OnAsyncUpdate ();
		}
	}
	
	private void OnServerFixedUpdate ()
	{		
		this.CheckForDeath ();
		
		if (this.IsUnavailableForCommand) {//dead hero has a behaviour
			return;
		}	
		
		if (this.IsDead) {//even if it is hero
			return;
		}
				
		this.ProcessEffects ();
		
		if (!unitType.IsBuilding) {
			unitDemeanorController.OnFixedUpdate (); // Buildings do not have fixed update
		}
	}
	
	public void DrawGUI ()
	{	
		this.unitViewController.DrawGUI ();
	}
	
	public bool IsUnavailableForCommand {
		get {
			if (heroData == null) {
				return this.IsDead;
			} else {
				return (this.IsDead && !this.heroData.IsGhost);
			}
		}
	}
	
	private void RestoreParams ()
	{
		if (!isInited) {
			return;
		}
		
		if (this.restoreManaTimer != null && this.restoreManaTimer.EnoughTimeLeft ()) {
			this.currentMana += unitType.manaRestorationValue;
			if (this.currentMana > unitType.maxMana) {
				this.currentMana = unitType.maxMana;
			}
		}
		
		if (this.restoreHealthTimer.EnoughTimeLeft ()) {
			this.currentHealth += unitType.healthRestorationValue;
			if (this.currentHealth > unitType.maxHealth) {
				this.currentHealth = unitType.maxHealth;
			}
		}
	}
	
	private Timer processEffectsTimer = Timer.CreateFrequency (50);
	
	public void ProcessEffects ()
	{
		if (!processEffectsTimer.EnoughTimeLeft ()) {
			return;
		}
		
		if (IsDead) {
			effectsOnUnit.Clear ();
			return;
		}
			
		foreach (EffectData effect in this.effectsOnUnit) {
			effect.DoEffect ();
		}			
		this.effectsOnUnit.RemoveWhere (isDeactivated);
	}
		
	private void ProcessDeath ()
	{
		currentHealth = 0;	
		float courotineLength = 5f;
		this.unitViewController.OnUnitStartedDying ();

		unitViewController.StartUnitDestroyingAnimation ((int)courotineLength * 100);
		
		if (!unitType.IsBuilding || unitType.IsTower) {
			Collider collider = this.modelGameObject.GetComponent<Collider> ();
			WorldController.Instance.UnitColliders.Remove (collider);	
			WorldController.Instance.UnitColliders.Remove (this.gameObject.GetComponent<Collider> ());
		} else {
			WorldController.Instance.ObstacleColliders.Remove (this.gameObject.GetComponent<SphereCollider> ());
		}
		
		if (!unitType.IsHero) {
			this.unitGroup.RemoveUnit (this);
		}
		StartCoroutine (OnDeathAnimationEnded (courotineLength));	
	}
	
	IEnumerator OnDeathAnimationEnded (float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		unitViewController.OnUnitDied ();
		
		if (unitType.IsHero) {
			if (this.heroData == InputController.CurrentHero) {
				InputController.SetRTSControlType ();
				UnitGroup.OnHeroDies ();
			}
			this.heroData.OnDeath ();
		} else {
			this.shouldDestroyUnit = true;
		}
	}
	
	private void PerformUnitDestroying ()
	{		
		this.unitGroup = null;		
		ObjectController.Destroy (gameObject);	
		this.shouldDestroyUnit = false;
		this.unitType = null;
	
		this.isInited = false;				
	}
	#region AffectObject
	public bool ProcessClick ()
	{
		if (playerOwner.IsHuman ()) {
			InputController.CurrentUnitGroup = this.unitGroup;
			return true;
		} else {
			return false;
		}
	}
		
	public void AddEffect (CommandController controller, EffectType effectType, EffectPerformer effectPerformer)
	{	
		if (!EffectAlreadyExist (effectType)) {
			EffectData effect = WorldController.Instance.CreateEffect (effectType, effectPerformer, this);
			effectsOnUnit.Add (effect);
		} 
		
		unitViewController.DisplayEffect (effectType.Name);
	}
	
	private bool EffectAlreadyExist (EffectType effectType)
	{
		foreach (EffectData effect in effectsOnUnit) {
			if (effect.GetEffectType () == effectType) {
				effect.RenewEffect ();
				return true;
			}
		}
		return false;
	}
		
	/*
		 * if power < 0 - units life will be healed
		 * */
	public void AffectLife (CommandController commandController, int power)
	{
		if (Immortal) {
			return;
		}
		
		if (currentHealth >= 0) {
			currentHealth -= power;										
		} 
		
		this.unitViewController.DisplayLifeAffect (power);
		this.unitDemeanorController.OnUnitAttacked ();
	}
	
	public void AffectMana (CommandController commandController, int power)
	{
		if (currentMana > 0) {
			currentMana -= power;				
			if (currentMana <= 0) {
				currentMana = 0;
			}				
		} 
	}

	public void Resurrect ()
	{
		this.currentMana = 0;
		this.currentHealth = this.unitType.maxHealth / 10;
		this.unitViewController.ResurrectUnit ();
		this.deathProcessed = false;
	}
	#endregion	
	
	#region behaviour
	public void MoveUnitTo (Vector3 movementPosition, bool randomize = true)
	{
		this.unitDemeanorController.MoveUnitTo (movementPosition, randomize);
	}
	
	public void StopMovement ()
	{
		this.unitDemeanorController.OnMovementHasEnded ();
	}
	
	public void OnServerUnitStateChanged (char stateChar)
	{
		if (WorldController.Instance.IsNetworkGameServer) {
			networkSerializer.OnUnitStateChanged (stateChar);
		} else {
			throw new System.Exception ();
		}
	}
	#endregion
		
	#region getterSetterMethods
	private bool isDeactivated (EffectData subj)
	{
		bool shouldRemove = !subj.IsActive;
		if (shouldRemove) {
			unitViewController.SetOriginalMaterial ();
		}
		return shouldRemove;
	}

	public int CurrentMana {
		get {
			return this.currentMana;
		}
	}
	
	public PlayerData GetPlayerOwner ()
	{
		return this.unitGroup.PlayerOwner;
	}
       
	public PlayerData PlayerOwner {
		get {			
			return this.playerOwner;
		}
	}
	
	public bool IsInited { 
		get { return this.isInited;}		
	}
	
	public bool IsHero {
		get { return this.unitType.IsHero;}
		
	}

	public UnitGroupSubject UnitGroup {
		get { return this.unitGroup;}
		set {		
			UnitGroupSubject newUnitGroup = value;

			if (this.unitGroup != null) {
				if (this.playerOwner != newUnitGroup.PlayerOwner) {
					return;
				}

				this.unitGroup.RemoveUnit (this);
			}

			if (this.unitGroup != null && (this.unitType.IsHero || this.IsGarrisonUnit || this.IsBuilding)) {
				throw new System.ArgumentException ();
			}

			this.unitGroup = newUnitGroup;	
		
			this.unitGroup.AddUnit (this);

			if (WorldController.Instance.IsNetworkGameClient || IsBuilding) {
				return;
			}

			if (!this.unitGroup.AreEnemiesAround ()) {
				Loom.QueueOnMainThread(()=>{
					this.MoveUnitTo (newUnitGroup.MovementTarget.position);
				});				
			} else {
				this.unitDemeanorController.OnMovementHasEnded ();
				this.unitDemeanorController.InteractBestTarget ();

			}
		}
	}
		
	public float GetLifePercent ()
	{
		if (unitType == null || unitType.MaxHealth == 0) {
			return 0;
		}
		return (float)this.currentHealth / unitType.MaxHealth;
	}

	public UnitType UnitType {
		get {
			return this.unitType;
		}
	}

	public int CurrentHealth {
		get { return currentHealth;}
	}
	
	public float GetManaPercent ()
	{
		return (float)this.currentMana / this.unitType.maxMana;
	}
	#endregion

	public bool IsGarrisonUnit {
		get {
			return this.isGarrisonUnit;
		}
		set {
			isGarrisonUnit = value;
		}
	}

	public Vector3 MovementPosition 
	{
		get {
			return this.unitDemeanorController.MovementCoord;
		}
	}

	public GameObject ModelGameObject {
		get {
			return this.modelGameObject;
		}
	}
	
	#region Animation
	private void PrepareAnimation ()
	{
		this.unitViewController.PrepareAnimation ();
	}
	
	public void PlayRunAnimation ()
	{
		this.unitViewController.PlayRunAnimation ();		
	}
	
	public void PlayIdleAnimation ()
	{
		this.unitViewController.PlayIdleAnimation ();	
	}
	
	public void PlayAttackAnimation ()
	{
		this.unitViewController.PlayAttackAnimation ();		
	}

	public void StopAnimation ()
	{
		this.unitViewController.StopAnimation ();
	}
	#endregion

	public void OnUnitEndedSpellCasting ()
	{
		unitDemeanorController.OnUnitEndedSpellCasting ();
	}

	public void OnUnitStartedSpellCasting ()
	{
		unitDemeanorController.OnStartedSpellCasting ();
	}

	public void PerformRotation (Vector3 point)
	{	
		unitDemeanorController.OnRotationChanged (point);
	}
	
	public void OnEnemiesAroundDissapeared ()
	{
		if (this.IsHero && this.UnitGroup.GroupHero == InputController.CurrentHero) {
			InputController.SetRTSControlType ();
		}
//		UnitDemeanorController.OnEnemiesDissapear ();
	}

	public void AttackClosestEnemy ()
	{
		this.unitDemeanorController.InteractBestTarget ();
	}

	public PlayerData playerOwner {
		get { return this.unitGroup.PlayerOwner;}
	}

	public void OnPlayerOwnerChanged (PlayerData oldOwner)
	{
		oldOwner.RemoveUnit (this);
		this.playerOwner.AddUnit (this);
		this.unitViewController.PlayerOwnerChanged ();	
		this.UnitSpawnManager.Reset ();
		this.unitDemeanorController.OnPlayerOwnerHasChanged ();		
	}

	public UnitSpawnManager UnitSpawnManager {
		get {
			return this.unitSpawnManager;
		}
	}
	
	private GameObject modelGameObject {
		get { return this.unitViewController.ModelGameObject;}
	}
	
	public BuildingSubject BuildingData {
		get { return this.buildingData;}
	}
	
	public bool IsBuilding {
		get { return this.unitType.IsBuilding;}
	}

	public Vector3 GetForwardToMovementTarget ()
	{
		return this.unitDemeanorController.GetForwardToMovementTarget ();
	}
	
	public UnitDemeanor UnitDemeanorController {
		get {
			return this.unitDemeanorController;
		}
	}

	public bool RenewEffect (EffectPerformer effectPerformer)
	{
		bool isPresent = false;
		foreach (EffectData effect in effectsOnUnit) {
			if (effectPerformer.Equals (effect.EffectPerformer)) {
				effect.RenewEffect ();
				isPresent = true;
			}
		}
		
		return isPresent;
	}
	
	public void OnConstructionStarted ()
	{
		unitViewController.OnConstructionStarted ();
	}
	
	public void OnUpgradeStarted ()
	{
		unitViewController.OnUpgradeStarted ();
	}
	
	public void OnUpgradeFinished ()
	{
		unitViewController.OnUpgradeFinished ();
	}
	
	public void OnConstructionFinished ()
	{
		unitViewController.OnConstructionFinished ();
	}
	
	public Collider UnitBody {
		get { return bodySphere;}
	}
	
	public float BodyRadius {
		get {
			return bodyRadius;
		}
	}

	#region UnitInteractionPerformer implementation
	public bool PositiveForTarget {
		get {
			return this.unitType.PositiveForTarget;
		}
	}

	public float ActionArea {
		get {
			return this.unitType.ActionArea;
		}
	}

	public MagicAnimationType MagicAnimationType {
		get {
			return this.unitType.MagicAnimationType;
		}
	}

	public LinkedList<EffectType> Effects {
		get {
			return this.unitType.Effects;
		}
	}

	public float ManaUsePerInteraction {
		get {
			return this.unitType.ManaUsePerInteraction;
		}
	}
	#endregion

	#region InteractionPerformer implementation
	public int InteractionPower {
		get {
			return this.unitType.InteractionPower + this.PlayerOwner.UnitAttackModification;
		}
	}

	public int InteractionPowerPercentRandomOffset {
		get {
			return this.unitType.InteractionPowerPercentRandomOffset;
		}
	}
	#endregion
	
	public int Protection {
		get {
			return this.unitType.UnitsProtection + this.PlayerOwner.UnitProtectionModification;
		}
	}

	public void CastSkill ()
	{
		this.unitDemeanorController.CastSkill (UnitType.Skill);
	}

	public HeroSubject HeroData {
		get {
			return this.heroData;
		}
	}

	public HashSet<EffectData> EffectsOnUnit {
		get {
			return this.effectsOnUnit;
		}
	}

	#region UnitInteractionPerformer implementation
	public string Id {
		get {
			if (this.IsBuilding) {
				return buildingData.id;
			}
			
			if (this.IsHero) {
				return heroData.id;
			}
			return this.id;
		}
	}
	#endregion

	private bool doDeath = false;
	
	public bool CheckForDeath ()
	{
		if (unitType == null || this.unitType.IsInvulnerable) {
			return true;
		}
			
		if (this.currentHealth <= 0 && !deathProcessed) {
			this.deathProcessed = true;
			this.unitSpawnManager.Reset ();
			this.ProcessDeath ();
		}
		return deathProcessed;
	}
	
	public bool IsDead 
	{
		get {
			if (unitType == null || this.unitType.IsInvulnerable) {
				return true;
			}
			return this.currentHealth <= 0;
		}
	}
	
	public void Destroy (GameObject victim) {
		MonoBehaviour.Destroy (victim);
	}
	
	//Used for async pathfinding
	public Vector3 TransformPosition {
		get {return transformPosition;}
	}
}
