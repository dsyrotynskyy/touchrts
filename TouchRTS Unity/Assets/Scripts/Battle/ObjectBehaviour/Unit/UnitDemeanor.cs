using System;
using UnityEngine;
using Entity;
using System.Collections.Generic;

public interface UnitDemeanor
{
	void Init(UnitSubject unit);
	
	Vector3 GetForwardToMovementTarget ();
	
	Vector3 MovementCoord 
	{
		get;
	}
	
	void OnMovementHasEnded ();
			
	void MoveUnitTo (UnityEngine.Vector3 movementPosition, bool randomize);
	
	void OnUnitAttacked ();
	
	void OnFixedUpdate ();
	
	void OnAsyncUpdate ();
	
	bool IsFree ();
	
	void OnUnitEndedSpellCasting ();
	
	void OnStartedSpellCasting ();
	
	void OnRotationChanged (Vector3 point);
	
	void InteractBestTarget ();
	
	void OnPlayerOwnerHasChanged ();
	
	void CastSkill (AbilityType skill);
	
	void OnEnemiesDissapear ();
	
	//Client server logic
	
	void OnUnitStateChanged (char stateChar);
	void OnUnitPositionChanged (Vector3 position, Quaternion rotation);
	
	char CreateStateCharForClient ();
	
	//Must be used only by playerAi
	bool IsInFightingState {
		get ;		
	}
	
	UnitSubject TargetUnit {
		get;
	}
	
	//Available only in server troop
	IMovementController MovementConroller {
		get;
	}
}


