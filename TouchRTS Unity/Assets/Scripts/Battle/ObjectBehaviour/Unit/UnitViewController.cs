using System;
using UnityEngine;
using Entity;
using System.Collections.Generic;
using Setup;

public class UnitViewController
{	
	GameObject shadow;
	GameObject selectionCircle;
	private GameObject gameObject;
	private UnitSubject unit;
	private UnitType unitType;
	private Transform transform;
	private Material[] originalMaterials = null;
	private GameObject miniMapGameObject;
	private GameObject modelGameObject;
	
	public UnitViewController (UnitSubject unit)
	{
		this.gameObject = unit.gameObject;
		this.unit = unit;
		this.unitType = unit.UnitType;
		this.transform = unit.transform;
		this.Init ();
	}
	
	private void Init ()
	{
		this.CreateModel ();
		
		selectionCircle = ObjectController.CreateSelectionCircle ();
		selectionCircle.layer = GameSettings.Layer.EFFECT;	
		selectionCircle.transform.position = gameObject.transform.position;
		selectionCircle.transform.parent = gameObject.transform;
		selectionCircle.SetActive(false);
		
		if (unit.IsBuilding) {
			InputController.UI.CreateUnitControlPanel (this.unit);
		} 
		InputController.UI.CreateUnitInfoView (this.unit);
		
		AddMiniMapItem ();
		
		this.InitOriginalMaterial ();
	}
	
	private void CreateModel ()
	{
		modelGameObject = null;
		foreach (Transform transform in this.unit.transform) {
			if (transform.name == this.unit.name) {
				modelGameObject = transform.gameObject;
//				unit.Destroy (modelGameObject);
//				modelGameObject = null;
				break;
			}
		}
		if (modelGameObject == null) {
			modelGameObject = ObjectController.CreateModelGameObject (unit.name, unit.transform.position, unit.UnitType.Race.Name);		
		}
		modelGameObject.transform.rotation = new Quaternion ();
		modelGameObject.transform.parent = unit.transform;
		modelGameObject.transform.localPosition = new Vector3 ();
		modelGameObject.transform.rotation = unit.transform.rotation;
		if (unitType.IsBuilding) {
			modelGameObject.transform.Rotate (unitType.buildingXRotation, 180, 0);	
			modelGameObject.layer = GameSettings.Layer.OBSTACLES;					
		} else {
			modelGameObject.layer = GameSettings.Layer.UNIT_BODY;	
		}
	}
	
	private void InitOriginalMaterial ()
	{
		Transform pTransform = this.modelGameObject.GetComponent<Transform> ();		
		foreach (Transform trs in pTransform) {	
			SkinnedMeshRenderer renderer = (SkinnedMeshRenderer)trs.gameObject.GetComponent (typeof(SkinnedMeshRenderer));				
			if (renderer != null) {
				originalMaterials = renderer.materials;
			}			    
		}
	}
	
	private string currentAnimationName = null;
	
	public void UpdateView ()
	{
		if (selectionCircle == null) {
			return;
		}
		
		if (WorldController.IsWorldPaused) {
			this.StopAnimation ();
		}
		
		selectionCircle.SetActive(InputController.CurrentUnitGroup == unit.UnitGroup);
						
		UpdateAnimation ();
	}
	
	public void DrawGUI () {
	}
	
	public void DisplayEffect (string effectName)
	{
//		this.SetBurnMaterial ();
	}
	
	private void SetBurnMaterial ()
	{
		Transform pTransform = this.modelGameObject.GetComponent<Transform> ();		
		foreach (Transform trs in pTransform) {
			if (originalMaterials == null) {
				continue;
			}
			Material mat = (Material)Resources.Load ("Materials/CubusFeuer");
				
			Material[] burnMaterials = new Material[originalMaterials.Length];
			for (int i = 0; i < burnMaterials.Length; i++) {
				burnMaterials[i] = mat;
			}
			
			SetMaterial (trs, burnMaterials);
		}       				
	}

	private void AddMiniMapItem ()
	{
		PlayerData playerOwner = unit.PlayerOwner;
				
		float size = WorldController.Instance.WorldData.XSize + WorldController.Instance.WorldData.ZSize;
		size /= 300;
		
		if (this.gameObject.tag == GameSettings.Tag.UNIT) {
			this.CreateMiniMapItemObject (3.0f * size, playerOwner.PlayerColor);
		} else if (this.gameObject.tag == GameSettings.Tag.HERO) {
			this.CreateMiniMapItemObject (4.5f * size, playerOwner.PlayerColor);
		} else if (this.gameObject.tag == GameSettings.Tag.BUILDING) {
			this.CreateBuildingMiniMapItemObject (5.5f * size, playerOwner.PlayerColor);
		}
	}
	
	private void CreateMiniMapItemObject (float size, Color color)
	{
		miniMapGameObject = ObjectController.CreateMiniMapSphere ();
		miniMapGameObject.name = "MapBounds";
		miniMapGameObject.layer = GameSettings.Layer.MINI_MAP_ELEMENT;
		MonoBehaviour.Destroy (miniMapGameObject.collider);
		miniMapGameObject.transform.parent = transform;
		miniMapGameObject.transform.localScale = new Vector3 (size, size, size);
		miniMapGameObject.transform.localPosition = new Vector3 (0, 40, 0);
		miniMapGameObject.renderer.material.color = color;
	}
	
	private void CreateBuildingMiniMapItemObject (float size, Color color)
	{
		miniMapGameObject = GameObject.CreatePrimitive (PrimitiveType.Cube);
		miniMapGameObject.name = "MapBounds";
		miniMapGameObject.layer = GameSettings.Layer.MINI_MAP_ELEMENT;
		MonoBehaviour.Destroy (miniMapGameObject.collider);
		miniMapGameObject.transform.parent = transform;
		miniMapGameObject.transform.localScale = new Vector3 (size, size, size);
		miniMapGameObject.transform.localPosition = new Vector3 (0, 40, 0);
		miniMapGameObject.renderer.material.color = color;
	}
	
	public void SetOriginalMaterial ()
	{
		Transform pTransform = this.modelGameObject.GetComponent<Transform> ();		
		foreach (Transform trs in pTransform) {
			this.SetMaterial (trs, originalMaterials);		       
		}		       
	}
	
	private void SetMaterial (Transform trs, Material[] materials) {
		SkinnedMeshRenderer renderer = (SkinnedMeshRenderer)trs.gameObject.GetComponent (typeof(SkinnedMeshRenderer));				
		if (renderer == null) {
			return;
		}	
		renderer.materials = materials;	
	}
	
	private bool disableAnimationPlay = false;
	
	public void OnUnitStartedDying ()
	{
		this.PlayDeathAnimation ();	
		disableAnimationPlay = true;
	}
	
	public void ResurrectUnit ()
	{
		disableAnimationPlay = false;
	}
	
	public void OnUnitDied ()
	{	
		if (!unit.IsHero) {
			unit.StopAnimation ();
		}
		this.SetOriginalMaterial ();
	}
	
	public void DisplayDeadHero ()
	{
		transform.animation.Play ("idle");
		this.SetOriginalMaterial ();
	}

	public Entity.UnitType UnitType {
		get {
			return this.unit.UnitType;
		}
	}

	public void PlayerOwnerChanged ()
	{				
		miniMapGameObject.renderer.material.color = this.unit.playerOwner.PlayerColor;
	}
	
	public GameObject ModelGameObject {
		get { return this.modelGameObject;}
	}
	
	private bool isAnimationPrepared = false;
	
	#region Animation
	public void PrepareAnimation ()
	{
		isAnimationPrepared = true;
		
		if (modelGameObject.animation == null) {
			return;
		}
		
		if (!unitType.IsBuilding) {						
			modelGameObject.animation.CrossFade ("idle");
			
			float frequesy = unitType.AttackFrequency;
			float animationLength = modelGameObject.animation ["attack"].length * 1000f;
			float animationSpeed = animationLength / frequesy;
			modelGameObject.animation ["attack"].speed = animationSpeed;
		}
	}
	
	public void PlayRunAnimation ()
	{
		this.PlayAnimation ("run");		
	}
	
	public void PlayIdleAnimation ()
	{
		this.CrossFadeAnimation ("idle");	
	}
	
	public void PlayAttackAnimation ()
	{
		if (modelGameObject.animation == null) {
			return;
		}
		try {
			this.PlayAnimation ("attack");
		} catch (Exception) {
		}		
	}
	
	public void PlayDeathAnimation ()
	{			
		this.PlayAnimation ("die");		
	}
	
	private void PlayAnimation (string name) {
		if (disableAnimationPlay || modelGameObject.animation == null) {
			return;
		}

		try {
			ModelGameObject.animation.Play (name);
		} catch (Exception) {
		}
		
		currentAnimationName = null;
	}
	
	private void CrossFadeAnimation (string name) {		
		if (disableAnimationPlay || modelGameObject.animation == null) {
			return;
		}

		currentAnimationName = name;
	}

	public void StopAnimation ()
	{
		if (modelGameObject.animation == null) {
			return;
		}
		ModelGameObject.animation.Stop ();
	}
	#endregion

	public bool IsAnimationPrepared {
		get {
			return this.isAnimationPrepared;
		}
	}
	
	private void UpdateAnimation () {
		if (isAnimating) {
			UpdateMovingAnimation ();			
		}
		
		if (currentAnimationName != null) {
			ModelGameObject.animation.CrossFade (currentAnimationName);
		}
	}
	
	private bool isAnimating = false;	
	private Vector3 startPosition;
	private Vector3 endPosition;
	private Vector3 startScale;
	private Vector3 endScale;
	private Timer timer;
	
	private void StartMovingAnimation (Vector3 startPosition, Vector3 endPosition, Timer timer) {
		Vector3 startScale = this.modelGameObject.transform.localScale;
		this.StartMovingResizingAnimation (startPosition, endPosition, startScale, startScale, timer);
	}
	
	private void StartResizingAnimation (Vector3 startScale, Vector3 endScale, Timer timer) {
		Vector3 position = this.modelGameObject.transform.localPosition;
		this.StartMovingResizingAnimation (position, position, startScale, endScale, timer);
	}
	
	private void StartMovingResizingAnimation (Vector3 startPosition, Vector3 endPosition, Vector3 startScale, Vector3 endScale, Timer timer) {
		this.isAnimating = true;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
		this.startScale = startScale;
		this.endScale = endScale;
		this.timer = timer;		
		this.modelGameObject.transform.localScale = startScale;
		this.modelGameObject.transform.localPosition = startPosition;
	}
	
	private void UpdateMovingAnimation () {
		if (!isAnimating) {
			return;
		}
		float progress = this.timer.GetProgress () / 100f;
		if (!timer.IsActive) {
			OnMovingResizingAnimationHasEnded ();
			return;
		}
		
		if (startPosition != endPosition) {
			Vector3 currentPosition = Vector3.Lerp ( startPosition, endPosition, progress);
			this.modelGameObject.transform.localPosition = currentPosition;
		}
		
		if (startScale != endScale) {
			Vector3 currentScale = Vector3.Lerp ( startScale, endScale, progress);
			this.modelGameObject.transform.localScale = currentScale;
		}
	}
	
	private void OnMovingResizingAnimationHasEnded () {
		isAnimating = false;
		this.modelGameObject.transform.localPosition = endPosition;
		this.modelGameObject.transform.localScale = endScale;
	}
	
	public void OnUpgradeStarted ()
	{
		if (!unit.IsBuilding) {
			return;
		}
		Vector3 startScale = this.modelGameObject.transform.localScale;
		float scale = 1.1f;
		Vector3 endScale = startScale * scale;
		StartResizingAnimation (startScale, endScale, unit.BuildingData.GetBuildingConstructionTimer ());
	}
	
	public void OnConstructionStarted ()
	{
		if (!unit.IsBuilding) {
			return;
		}
		Vector3 normalBuildingPosition = this.modelGameObject.transform.localPosition;
		float height = 5f;
		Vector3 beginningBuildingPosition = new Vector3(normalBuildingPosition.x, normalBuildingPosition.y - height, normalBuildingPosition.z);
		StartMovingAnimation (beginningBuildingPosition, normalBuildingPosition, unit.BuildingData.GetBuildingConstructionTimer ());
	}	
	
	public void OnConstructionFinished ()
	{
		OnMovingResizingAnimationHasEnded ();
	}

	public void DisplayLifeAffect (int power)
	{
	//	unitInfoView.DisplayLifeAffect (power);
	}	
	
	public void StartUnitDestroyingAnimation (int deathTime)
	{		
		isAnimating = true;
		Vector3 startingPosition = this.modelGameObject.transform.localPosition;
		float height;
		if (unit.IsBuilding) {
			height = 5f;
		} else {
			height = 0.5f;
		}
		Vector3 desiredPosition = new Vector3(startingPosition.x, startingPosition.y - height, startingPosition.z);
		Timer deathAnimationTimer = Timer.CreateLifePeriod (deathTime);
		StartMovingAnimation (startingPosition, desiredPosition, deathAnimationTimer);
	}
	
	public void OnUpgradeFinished () 
	{
		OnMovingResizingAnimationHasEnded ();
	}


}


