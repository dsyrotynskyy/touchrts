using System;
using UnityEngine;
using System.Collections.Generic;
using Entity;

public abstract class AbstractUnitDemeanor : UnitDemeanor
{	
	public const char FollowingPlayerCommandChar = 'M';
	public const char InteractingTargetChar = 'A';
	public const char PerformingSpellChar = 'S';
	public const char PeaceChar = 'P';	
	
	public enum UnitActivityType
	{
		FollowingPlayerCommand,
		InteractingTarget,
		PerformingSpell,
		Peace,
	}
	
	public const int UNIT_MOVEMENT_DISTANCE_TO_CENTER = 30;
	public const int UNIT_MOVEMENT_BIG_DISTANCE = 10;
	public const int UNIT_RANDOM_PERIOD_CHECK = 5000;

	protected GameObject gameObject;
	protected UnitSubject unit;
	protected Transform transform;
	protected UnitType unitType;
	
	protected Timer atackTimer;
	protected Timer unitRandomMovementTimer;
	protected readonly LinkedList<UnitSubject> unitsToInterract = new LinkedList<UnitSubject> ();
	protected Vector3 prevUnitToAtackPosition;
	protected Vector3 movementCoord;
	protected float lerpValue = 0f;
	protected bool performingRotation = false;
	protected Timer behaveTimer = Timer.CreateRandomizeFrequency (15, 20);
	
	public AbstractUnitDemeanor ()
	{
	}
	
	public virtual void Init (UnitSubject unit) {
		this.gameObject = unit.gameObject;
		this.unit = unit;
		this.unitType = unit.UnitType;
		this.transform = unit.transform;
		this.movementCoord = this.unit.TransformPosition;
		
		this.unitRandomMovementTimer = Timer.CreateFrequency (Randomizer.Next (UNIT_RANDOM_PERIOD_CHECK) + UNIT_RANDOM_PERIOD_CHECK / 2);
		this.atackTimer = Timer.CreateFrequency (this.unitType.attackFrequency);		
	}
	
	protected UnitSubject GetRandomTarget (UnitInteractionPerformer ability) {
		LinkedList<UnitSubject> units = unit.UnitGroup.ClosestEnemies;	
		int numer = Randomizer.Next (units.Count);
		int i = 0;
		foreach (UnitSubject victimUnit in units) {
			if (victimUnit.IsDead || victimUnit.IsBuilding) {
				continue;
			}
			if (i++ == numer) {
				return victimUnit;
			}
		}
		return null;
	}
	
	protected UnitSubject GetBestTarget (UnitInteractionPerformer ability)
	{
		UnitSubject bestTarget;
		if (ability.PositiveForTarget) {
			bestTarget = GetBestTargetForPositiveAbility (ability);
		} else {
			bestTarget = GetBestTargetForAttack (ability);
		}
		return bestTarget;
	}
	
	private UnitSubject GetBestTargetForPositiveAbility (UnitInteractionPerformer abilit) {
//		LinkedList<UnitSubject> units = unit.UnitGroup.GetClosestFriendUnits ();
		
		UnitSubject candidate = null;
		float minDistance = float.MaxValue;
//		float minLifePercent = 100;
//		foreach (UnitSubject victimUnit in units) {
//			if (victimUnit.IsDead || victimUnit == this.unit) {
//				continue;
//			}
//			
//			float distance = Vector3.Distance (victimUnit.transform.position, gameObject.transform.position);	
//			if (distance < minDistance && victimUnit.GetLifePercent () < 1f) {
//				candidate = victimUnit;
//				minDistance = distance;
//			}
//		}
		
		foreach (UnitSubject victimUnit in this.UnitGroup.Units) {
			if (victimUnit.IsDead || victimUnit.IsBuilding) {
				continue;
			}
			
			float distance = 0;
			distance = (victimUnit.TransformPosition - unit.TransformPosition).sqrMagnitude;	
			if (distance < minDistance && victimUnit.GetLifePercent () < 1f) {
				candidate = victimUnit;
				minDistance = distance;
			}
		}
		return candidate;
	}
	
	private UnitSubject GetBestTargetForAttack (UnitInteractionPerformer ability) {
		LinkedList<UnitSubject> units = unit.UnitGroup.ClosestEnemies;
		if (units.Count <= 0) {
			return null;
		}
		
		UnitSubject candidate = null;
		float minDistance = float.MaxValue;
		foreach (UnitSubject victimUnit in units) {
			if (victimUnit.IsDead || victimUnit.IsBuilding) {
				continue;
			}
			
			float distance =  (victimUnit.TransformPosition - unit.TransformPosition).sqrMagnitude ;	
			if (distance < minDistance) {
				candidate = victimUnit;
				minDistance = distance;
			}
		}
		return candidate;
	}
	
	//Must be called only on Main Thread!
	protected void PerformInterractingTarget (UnitInteractionPerformer performer, UnitSubject targetUnit) //if ability is null - do attack
	{
		if (targetUnit == null) {
			return;
		}
		
		unitsToInterract.Clear ();
		unitsToInterract.AddFirst (targetUnit);
		
		float actionArea = performer.ActionArea;
		bool isPositive = performer.PositiveForTarget;
			
		LinkedList<UnitSubject> candidates;
		if (isPositive) {
			candidates = UnitGroup.ClosestFriendUnits ;
		} else {
			candidates = UnitGroup.ClosestEnemies ;
		}
		
		if (actionArea > 0) {
			foreach (UnitSubject unit in candidates) {
				if (unit == targetUnit || unit.IsDead) {
					continue;
				}
				
				float distance = Vector3.Distance (unit.TransformPosition, targetUnit.TransformPosition);	
				if (distance < unitType.AttackArea) {
					unitsToInterract.AddLast (unit);
				}
			}
		} 	
		
		CommandController.Instance.DoInterraction (this.unit, performer, unitsToInterract);			
	}
	
	private void DoAttackInteraction () {
	}
	
	public UnitGroupSubject UnitGroup { 
		get {
			return this.unit.UnitGroup;
		}
	}
	
	//used only if is Network Client
	public virtual void OnUnitStateChanged (char stateString) {
		throw new System.NotImplementedException ();
	}
	
	//used only if is Network Server and Troop Unit
	public virtual char CreateStateCharForClient () {
		return PeaceChar;
	}

	#region UnitDemeanor implementation
	public virtual void OnFixedUpdate ()
	{
		throw new NotImplementedException ();
	}

	public virtual void OnAsyncUpdate ()
	{
		throw new NotImplementedException ();
	}

	public virtual bool IsFree () {
		throw new NotImplementedException ();
	}

	public virtual void InteractBestTarget () {
		throw new NotImplementedException ();
	}
	
	public virtual UnityEngine.Vector3 GetForwardToMovementTarget ()
	{
		throw new NotImplementedException ();
	}

	public virtual Vector3 MovementCoord 
	{
		get {throw new NotImplementedException ();}
	}

	public virtual void MoveUnitTo (UnityEngine.Vector3 movementPosition, bool randomize)
	{
		throw new NotImplementedException ();
	}

	public virtual void OnUnitAttacked ()
	{
		throw new NotImplementedException ();
	}

	public virtual void OnUnitEndedSpellCasting ()
	{
		throw new NotImplementedException ();
	}

	public virtual void OnStartedSpellCasting ()
	{
		throw new NotImplementedException ();
	}

	public virtual void OnRotationChanged (UnityEngine.Vector3 point)
	{
		throw new NotImplementedException ();
	}

	public virtual void CastSkill (Entity.AbilityType skill)
	{
		throw new NotImplementedException ();
	}
	
	public virtual void OnMovementHasEnded ()
	{
		throw new NotImplementedException ();
	}
	
	public virtual void OnUnitPositionChanged (Vector3 position, Quaternion rotation) 
	{
		throw new System.NotImplementedException ();
	}
	#endregion

	#region UnitDemeanor implementation
	public virtual void OnPlayerOwnerHasChanged ()
	{		
	}
	#endregion

	#region UnitDemeanor implementation
	public virtual void OnEnemiesDissapear ()
	{
	}
	#endregion

	#region IEnumerable[Vector3] implementation
	public IEnumerator<Vector3> GetEnumerator ()
	{
		throw new NotImplementedException ();
	}
	#endregion

	#region UnitDemeanor implementation
	public virtual bool IsInFightingState {
		get {
			throw new NotImplementedException ();
		}
	}
	
	public virtual UnitSubject TargetUnit {
		get {
			throw new NotImplementedException ();
		}
	}
	#endregion

	#region UnitDemeanor implementation
	public virtual void UpdatePathSeeking ()
	{
	}

	public virtual void UpdatePathPassing ()
	{
	}
	
	public virtual IMovementController MovementConroller {
		get {return null;}
	}
	
	#endregion
}


