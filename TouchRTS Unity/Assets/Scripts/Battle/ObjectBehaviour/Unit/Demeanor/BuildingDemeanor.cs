using System;
using UnityEngine;
using Entity;

public class BuildingDemeanor : AbstractUnitDemeanor
{
	private UnitSubject targetUnit;

	
	public BuildingDemeanor ()
	{
	}
	
	public override void Init (UnitSubject unit)
	{
		base.Init (unit);
	}
//	
//	public void OnFixedUpdate ()
//	{
//	}

	public override void OnAsyncUpdate ()
	{
		if (unit.BuildingData.IsConstructing || !unit.BuildingData.IsGuarded) {
			return;
		}
		if (unit.UnitType.IsTower) {
			if (atackTimer.EnoughTimeLeft ()) {
				BehaveTower ();
			}					
		}
	}
	
	private void BehaveTower ()
	{
		if (targetUnit == null || targetUnit.IsDead) {	
			targetUnit = GetBestTarget (this.unit);
			if (targetUnit == null) {
				return;
			}
		} 
		
		float distance = 0;
		distance = Vector3.Distance (targetUnit.TransformPosition, unit.TransformPosition);	
		if (distance <= unitType.attackDistance) {
			Loom.QueueOnMainThread(()=>{
				PerformInterractingTarget (this.unit, targetUnit);
			});
			return;	
		} 
	}

	public override bool IsFree ()
	{
		return true;
	}

	public override void OnUnitAttacked ()
	{
	}
	
	#region UnitDemeanor implementation
	public override void OnPlayerOwnerHasChanged ()
	{	
		this.targetUnit = null;
	}
	#endregion
}

