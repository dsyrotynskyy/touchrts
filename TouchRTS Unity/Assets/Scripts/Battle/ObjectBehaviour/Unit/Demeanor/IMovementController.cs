using System;
using UnityEngine;

public interface IMovementController
{
	void StopMovement ();
	
	void Init (TroopDemeanor unit);

	void OnFixedUpdate ();
	
	void OnUpdate ();
	
	Vector3 GetPathDestination();
	
	Vector3 GetForwardToMovementTarget ();
	
	void UpdateAsync ();
	
	Quaternion DesiredRotation {
		set;
	}
	
	Vector3 DesiredUnitPos {
			get ;
			set ;		
		}
}
