using System;
using UnityEngine;
using System.Collections.Generic;

public class ClientDemeanor : AbstractUnitDemeanor
{
	private UnitSubject targetUnit;
	private UnitActivityType unitActivity = UnitActivityType.Peace;
	private Quaternion desiredRotation;
	private Vector3 desiredPosition;
	private Vector3 movementPosition;
	private bool doFastRotation = false;
	
	public override void Init (UnitSubject unit)
	{
		base.Init (unit);
		desiredRotation = transform.rotation;
		desiredPosition = transform.position;
	}
	
	public override void OnFixedUpdate ()
	{
		if (unit.IsBuilding) {
			return;
		}
		
		if (unit.IsDead) {
			return;
		}
						
		this.UpdateUnitPosition ();
		
		if (unit.IsHero && unit.HeroData.IsSpawningSpell) {
			PerformSpellState ();
			return;
		}
		
		switch (unitActivity) {
		case UnitActivityType.FollowingPlayerCommand:	
			PerformMovingState ();
			return;
		case UnitActivityType.InteractingTarget:
			PerformInteractingState ();
			return;
		case UnitActivityType.Peace:
			PerformPeaceState ();
			return;	
		case UnitActivityType.PerformingSpell:
			PerformSpellState ();
			return;
		}
	}
	
	private float desiredToRealPositionDistance = 0f;
	private float STOP_MOVEMENT_DISTANCE = 0.5f;
	
	private void UpdateUnitPosition ()
	{
		float desiredToRealPositionDistance = Vector3.Distance (transform.position, desiredPosition);
		if (desiredToRealPositionDistance < STOP_MOVEMENT_DISTANCE) {
			this.unit.transform.position = desiredPosition;
		} else {
			this.unit.transform.position = (Vector3.Slerp (transform.position, desiredPosition, SpeedForStep / 1.3f * Time.deltaTime));
		}
		
		float rotationSpeed;
		if (doFastRotation) {
			rotationSpeed = 0.9f;
		} else {
			rotationSpeed = 3f * Time.deltaTime;
		}
		this.unit.transform.rotation = (Quaternion.Slerp (transform.rotation, desiredRotation, rotationSpeed));
	}
	
	public override void MoveUnitTo (Vector3 pathTarget, bool randomize = true)
	{					
		this.movementPosition = pathTarget;
	}
	
	public override Vector3 MovementCoord 
	{
		get {
			return this.movementPosition;
		}
	}
	
	public override void OnUnitPositionChanged (Vector3 position, Quaternion rotation)
	{
		this.desiredPosition = position;
		this.desiredRotation = rotation;
//		this.transform.position = position;
//		this.transform.rotation = rotation;
	}
		
	public override void OnUnitStateChanged (char stateChar)
	{
		if (unit.IsBuilding) {
			return;
		}
		
		switch (stateChar) {
		case FollowingPlayerCommandChar:			
			this.SetMovementState ();
			break;
		case InteractingTargetChar:	
			this.SetInteractingState ();
			break;
		case PerformingSpellChar:			
			this.SetSpellCastingState ();
			break;
		case PeaceChar:			
			this.SetPeaceState ();		
			break;			
		}
	}
	
	private void PerformPeaceState ()
	{
		if (desiredToRealPositionDistance < STOP_MOVEMENT_DISTANCE) {
			this.unit.PlayIdleAnimation ();
		} else {
			this.unit.PlayRunAnimation ();
		}
		doFastRotation = false;
	}

	private void PerformSpellState ()
	{
		doFastRotation = true;
		this.unit.PlayAttackAnimation ();
	}
	
	private bool startedInteractingTarget = false;
	
	private void PerformInteractingState ()
	{
		float distanceToEnemy = Vector3.Distance (movementPosition, gameObject.transform.position);	
		float attackDistance = unitType.attackDistance + this.unit.BodyRadius;
		if (startedInteractingTarget) {
			attackDistance *= 1.25f;
		}
		startedInteractingTarget = false;
		if (distanceToEnemy < attackDistance) {
			doFastRotation = true;
			this.unit.PlayAttackAnimation ();
			return;
		}
		 			
		PerformMovingState ();		
	}
		
	private void PerformMovingState ()
	{
		doFastRotation = false;
		this.unit.PlayRunAnimation ();		
	}
	
	private void SetMovementState ()
	{
		this.unitActivity = UnitActivityType.FollowingPlayerCommand;
	}
	
	private void SetSpellCastingState ()
	{
		this.unitActivity = UnitActivityType.PerformingSpell;
	}
	
	private void SetPeaceState ()
	{
		this.unitActivity = UnitActivityType.Peace;
	}
	
	private void SetInteractingState ()
	{
		this.unitActivity = UnitActivityType.InteractingTarget;
		startedInteractingTarget = true;
	}
	
	public override void OnRotationChanged (Vector3 point)
	{
//		point.Set (point.x, NavigationEngine.TerrainNormalHeight, point.z);
//		Vector3 lookDirection;
//		lookDirection = point - InputController.CurrentHero.transform.position;
//		Quaternion desiredRotationQuaternion = Quaternion.LookRotation (lookDirection);
//		desiredRotation = (Quaternion.Slerp (transform.rotation, desiredRotationQuaternion, 0.8f));
//		transform.rotation = desiredRotation;
	}
	
	public override void OnUnitAttacked ()
	{
	}
	
	public override void OnMovementHasEnded ()
	{
	}
	
	public override void OnStartedSpellCasting ()
	{
	}
	
	public override void OnUnitEndedSpellCasting ()
	{
	}
	
	private const float SPEED_MAX = 10;
	private const float SPEED_NORMAL = 6;
	private const float SPEED_LITTLE = 3;
	
	private float SpeedForStep {
		get {
			float minSpeed;
			float offset;
			{
				offset = SPEED_MAX - SPEED_LITTLE;
				minSpeed = SPEED_LITTLE;
			}
			float stepModif = offset * (Randomizer.Next (100)) / 100;
			return minSpeed + stepModif;
		}
	}
}


