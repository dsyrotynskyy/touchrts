package engine.core.controller.game;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import engine.core.persistance.object.CellObject;
import engine.core.persistance.object.UnavalaibleForMoveCellException;
import engine.core.persistance.object.UnitObject;
import engine.core.persistance.object.UnitObject.UnitNotInitedException;

/**
 * 
 * @author Danylo Syrotynskyy
 * 
 *         This class calculate the shortest paths for unit from cell to cell
 * 
 */
class ShortestPathCalculator {
	private final CellObject cellObjects[][];
	private final Collection<CellObject> cellsAvalaibleForMove = new LinkedList<CellObject>();
	private final int NUM_CELLS_HORIZONTAL;
	private final int NUM_CELLS_VERTICAL;

	private int movementPoints;
	private CellObject startCell;
	private final List<CellObject> cellsWithFriendUnits = new LinkedList<CellObject>();
	private Map<CellObject, LinkedList<CellObject>> nodeMap = new IdentityHashMap<CellObject, LinkedList<CellObject>>();
	private Map<CellObject, LinkedList<CellObject>> pathMap = new IdentityHashMap<CellObject, LinkedList<CellObject>>();

	private UnitObject startingUnit;

	protected ShortestPathCalculator(CellObject[][] cells,
			UnitObject[] units, int numCellsHorizontal,
			int numCellsVertical) {
		this.NUM_CELLS_HORIZONTAL = numCellsHorizontal;
		this.NUM_CELLS_VERTICAL = numCellsVertical;
		this.cellObjects = cells;
	}

	protected void calculateCellsAvalaibleForMove(UnitObject unitObject) throws UnitNotInitedException {
		this.clearMovementProcessingMemory();
		
		this.startingUnit = unitObject;
		this.prepareForProcessing(this.startingUnit);
		do {
			Object[] nodes = nodeMap.keySet().toArray();
			for (Object node : nodes) {
				this.processNodeCell((CellObject) node);
			}
		} while (!nodeMap.isEmpty());

		this.pathMap.remove(this.startCell);
		
		for (CellObject cellWithUnit : this.cellsWithFriendUnits) {
			this.pathMap.remove(cellWithUnit);
		}
		
		for (CellObject avalaibleCell : this.pathMap.keySet()) {
			if (!avalaibleCell.isOccupied()) {
				try {
					this.cellObjects[avalaibleCell.getYPos()][avalaibleCell
							.getXPos()].setAvalaibleForMove(true);
				} catch (UnavalaibleForMoveCellException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * This function prepares class data for processing shortest path
	 * 
	 * @param unit
	 * @throws UnitNotInitedException 
	 */
	private void prepareForProcessing(UnitObject unit) throws UnitNotInitedException {
		if (!unit.isInited()) {
			throw unit.new UnitNotInitedException();
		}
		
		this.movementPoints = unit.getMovementPoints();
		this.startCell = unit.getCellProper();

		int xPos = unit.getCellProper().getXPos();
		int yPos = unit.getCellProper().getYPos();

		CellObject unitCell = cellObjects[yPos][xPos];

		this.processNodeCell(unitCell);
	}

	private void processNodeCell(CellObject nodeCell) {
		int xPos = nodeCell.getXPos();
		int yPos = nodeCell.getYPos();

		this.createNewNode(xPos, yPos + 1, nodeCell);
		this.createNewNode(xPos + 1, yPos, nodeCell);
		this.createNewNode(xPos, yPos - 1, nodeCell);
		this.createNewNode(xPos - 1, yPos, nodeCell);

		nodeMap.remove(nodeCell);
	}

	private void createNewNode(int x, int y, CellObject parentCell) {
		if ((x < 0 || NUM_CELLS_HORIZONTAL <= x)
				|| (y < 0 || NUM_CELLS_VERTICAL <= y)) {
			return;
		} else {
			CellObject nodeCell = cellObjects[y][x];
			if (nodeCell.isDisabledForMovement()
					|| nodeCell == parentCell) {
				return;
			}
			if (nodeCell.isOccupied()) {
				if (nodeCell.getUnitOnCell().getPlayerOwner().getTeam() != this.startingUnit
						.getPlayerOwner().getTeam()) {
					return;
				} else {
					this.cellsWithFriendUnits.add(nodeCell);
				}
			}

			this.scalePathForNode(nodeCell, parentCell);
		}
	}

	private void scalePathForNode(CellObject nodeCell, CellObject parentCell) {
		LinkedList<CellObject> previousPath = pathMap.get(nodeCell);
		LinkedList<CellObject> parentPath = pathMap.get(parentCell);

		int movementPenaltyNew = nodeCell.getMovementPenalty();
		LinkedList<CellObject> newPath = new LinkedList<CellObject>();
		if (parentPath != null) {
			for (CellObject cell : parentPath) {
				movementPenaltyNew += cell.getMovementPenalty();
				newPath.add(cell);
			}
		} else {
			newPath.add(parentCell);
		}
		newPath.add(nodeCell);

		if (previousPath == null) {
			if (this.movementPoints - movementPenaltyNew > 0) {
				nodeMap.put(nodeCell, newPath);
				pathMap.put(nodeCell, newPath);
			}
		} else {
			int movementPenaltyOld = 0;
			for (CellObject cell : previousPath) {
				movementPenaltyOld += cell.getMovementPenalty();
			}
			if (this.movementPoints - movementPenaltyNew > 0
					&& movementPenaltyOld > movementPenaltyNew) {
				nodeMap.put(nodeCell, newPath);
				
				pathMap.put(nodeCell, newPath);
			}
		}
	}

	public Queue<CellObject> getPathToCell(CellObject cell) {
		return this.pathMap.get(cell);
	}

	protected Collection<CellObject> getCellsAvalaibleForMove() {
		return this.pathMap.keySet();
	}

	protected void clearMovementProcessingMemory() {
		this.cellsWithFriendUnits.clear();

		for (CellObject cellObject : this.getCellsAvalaibleForMove()) {
			try {
				cellObject.setAvalaibleForMove(false);
			} catch (UnavalaibleForMoveCellException e) {
				e.printStackTrace();
			}
		}

		this.cellsAvalaibleForMove.clear();
		this.pathMap.clear();
	}
}