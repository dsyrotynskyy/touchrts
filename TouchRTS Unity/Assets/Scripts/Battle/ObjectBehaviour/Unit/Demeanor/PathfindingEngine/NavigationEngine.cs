using System;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class NavigationEngine
{
	public static void Clear ()
	{
		PathTrianglesCalculator.OnDestroy ();
	}
	
	public static int noUnitLinecastMask;
	public static int unitLinecastMask;
	public static int waterMask;
	public static int onlyUnitBodyMask;
	public static int groundMask;
	public static int obstacleMask;
	public static int unitBodyMask;
	private static float terrainNormalHeight = -1;
	
	public NavigationEngine ()
	{
	}
	
	public void Prepare (WorldController controller, List<NavigationNode> navigationObjects, List<NavigationNode> obstacles)
	{				 
		InitFigureNavigation (navigationObjects, obstacles);
//		PathTrianglesCalculator.Prepare (controller.NavigationTriangles);
	}
	
	public static float TerrainNormalHeight {
		get {				
			return WorldController.DEFAULT_TERRAIN_HEIGHT;
		}
		set {
			terrainNormalHeight = value;
		}
	}
	
	private static bool paramsPrepared = false;
	
	public static void PrepareParams ()
	{
		if (!paramsPrepared) {
			paramsPrepared = true;
		} else {
			return;
		}
		terrainNormalHeight = -1;
		
		int water = LayerMask.NameToLayer ("Water");
		waterMask = 1 << water;
		
		obstacleMask = 1 << Setup.GameSettings.Layer.OBSTACLES;
		unitBodyMask = 1 << Setup.GameSettings.Layer.UNIT_BODY; 
		
		int unit = 1 << Setup.GameSettings.Layer.UNIT;
		int unitBody = 1 << Setup.GameSettings.Layer.UNIT_BODY; 
		int effect = 1 << Setup.GameSettings.Layer.EFFECT;
		int defaultLayer = 1 << LayerMask.NameToLayer ("Default");
		int navigationLayer = 1 << Setup.GameSettings.Layer.NAVIGATION;
		
		noUnitLinecastMask = unit | unitBody | effect | defaultLayer | navigationLayer; // Or, (1 << layer1) | (1 << layer2)
		noUnitLinecastMask = ~noUnitLinecastMask;
		
		unitLinecastMask = unit | effect | defaultLayer | navigationLayer;
		unitLinecastMask = ~unitLinecastMask;
		
		onlyUnitBodyMask = unitBody;
		
		groundMask = 1 << Setup.GameSettings.Layer.GROUND;
	}
	
//	public static void InitNormalTerrainHeight (Vector3 pos)
//	{
//		if (terrainNormalHeight >= 0) {
//			return;
//		}
//		
//		terrainNormalHeight = WorldController.Instance.WorldData.Height;
//	}
	
//	public static float GetTerrainHeight (Vector3 position)
//	{
//		try {
//			Ray ray = new Ray (position, new Vector3 (0, -1, 0));
//			RaycastHit hit;
//		
//			if (Physics.Raycast (ray.origin, ray.direction, out hit)) {	
//				if (hit.collider.gameObject != null && hit.collider.gameObject.tag == "Ground") {				
//					float y = hit.point.y;
//					return y;
//				}			
//			}
//		} catch (Exception e) {
//			MonoBehaviour.print (e);
//		}
//		
//		return 0;
//	}
	
	public bool IsDynamicObstacle (Vector3 startPos, Vector3 fwd, float distance, UnitSubject unit)
	{	
		bool useRay = false;
		if (useRay) {
			return IsDynamicObstacleRay (startPos, fwd, distance);
//			return false;
		} else {
			float modifier = 0.1f;
//			modifier = Randomizer.NextBool () ? 0.1f : 0.0f;
			return IsDynamicObstacleBetween (startPos + fwd * modifier, startPos + fwd * 0.5f, unit);
		}
	}
	
	private static bool IsDynamicObstacleRay (Vector3 startPos, Vector3 fwd, float distance)
	{
		int lineCastMask = 0;
		RaycastHit ray;
		lineCastMask = unitBodyMask;
		bool isRaycast = Physics.Raycast (startPos, fwd, out ray, distance, lineCastMask);
		
		if (isRaycast) {
			return true;
		}
		 
		return false;
	}
	
	public bool IsCoordProhibited (Vector3 targetPosition)
	{		
		return IsCoordOutOfMesh (targetPosition) || IsPositionOcupiedByObstacle (targetPosition);
	}

	private List<Vector3> drawNodes = new List<Vector3> ();
	private readonly List<Vector2Pair> vectorPairs = new List<Vector2Pair> ();
//	private LinkedList<StaticNavigationObstacle> staticNavigationObstacles = new LinkedList<StaticNavigationObstacle> ();
	private StaticNavigationObstacle[] staticNavigationObstacles = new StaticNavigationObstacle[200];
//	private readonly Vector2[] navigationNodes;
	private NavigationNode[] obstacleNodes;
	private int navigationNodeCount = -1;
	private int obstacleNodeCount = -1;
	
	public void InitFigureNavigation (List<NavigationNode> navigationObjects, List<NavigationNode> obstacleNodes)
	{		
		navigationNodeCount = navigationObjects.Count;
		obstacleNodeCount = obstacleNodes.Count;
		
		this.obstacleNodes = new NavigationNode[obstacleNodes.Count];
		
		foreach (NavigationNode node in navigationObjects) {
			vectorPairs.Add (new Vector2Pair (node));
		}	
		
		int j = 0;
		foreach (NavigationNode node in obstacleNodes) {
			node.PrepareForNavigationCalculation ();
			this.obstacleNodes [j++] = node;
		}
	}
		
	public bool IsDynamicObstacleBetween (Vector3 startPos, Vector3 endPos, UnitSubject ourUnit) {
		testPosition1.Set (startPos.x, startPos.z);
		testPosition2.Set (endPos.x, endPos.z);
		
		lock (WorldController.Instance.AllUnits) {
			foreach (UnitSubject unit in WorldController.Instance.AllUnits) {
				if (!unit.IsInited) {
					continue;
				}
				if (unit.UnitDemeanorController.MovementConroller == null || ourUnit.id == unit.id) {
					continue;
				}
				
				testPosition3.Set (unit.UnitDemeanorController.MovementConroller.DesiredUnitPos.x, unit.UnitDemeanorController.MovementConroller.DesiredUnitPos.z);
				
				if (Vector2.Distance (testPosition2, testPosition3) > 5f) {
					continue;
				}
				
				float distance = MinimumDistanceLineSegmentToPoint ( testPosition1, testPosition2, testPosition3);
				if (distance < 0.6f) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static float MinimumDistanceLineSegmentToPoint(Vector2 v, Vector2 w, Vector2 p) {
	  // Return minimum distance between line segment vw and point p
	  float l2 = (w - v).sqrMagnitude;  // i.e. |w-v|^2 -  avoid a sqrt
	  if (l2 == 0.0) return Vector2.Distance(p, v);   // v == w case
	  // Consider the line extending the segment, parameterized as v + t (w - v).
	  // We find projection of point p onto the line. 
	  // It falls where t = [(p-v) . (w-v)] / |w-v|^2
	  float t = (Vector2.Dot(p - v, w - v)) / l2;
	  if (t < 0.0) 
			return Vector2.Distance(p, v);       // Beyond the 'v' end of the segment
	  else if (t > 1.0) return Vector2.Distance(p, w);  // Beyond the 'w' end of the segment
	  Vector2 projection = v + t * (w - v);  // Projection falls on the segment
	  return Vector2.Distance(p, projection);
	}
	
	public bool IsPositionOcupiedByObstacle (Vector3 position, Collider ignoreCollider = null)
	{
		lock (staticNavigationObstacles) {
			foreach (StaticNavigationObstacle obstacle in staticNavigationObstacles) {
				if (obstacle == null) {
					break;
				}
				
				if (obstacle.OccupiesPoint (position)) {
					return true;
				}
			}
		}
		return false;
	}
	
	#region LandscapeNavigator implementation
	
	private static Vector2 testPosition1 = Vector2.zero;
	private static Vector2 testPosition2 = Vector2.zero;
	private static Vector2 testPosition3 = Vector2.zero;
	private static Vector2 testPosition4 = Vector2.zero;
	
	public bool IsStaticObstacleBetween (Vector3 startPos, Vector3 endPos)
	{
		bool isObstacle = IsStaticObjectObstacle (startPos, endPos);
		if (isObstacle) {
			return true;
		}
		
		testPosition1.Set (startPos.x, startPos.z);
		testPosition2.Set (endPos.x, endPos.z);
		
		for (int i = 0; i < navigationNodeCount - 1; i++) {
			if (CheckIntersection (vectorPairs [i].pos1, vectorPairs [i].pos2, testPosition1, testPosition2)) {
				return true;
			}
		}
		
		for (int i = 0; i < obstacleNodeCount; i++) {
			NavigationNode node = obstacleNodes [i];
			if (node.PrevNavigationNode == null) {
				continue;
			}
			if (CheckIntersection (node.CurrentVector2Position, node.PrevNodeVector2Position, testPosition1, testPosition2)) {
				return true;
			}		
		}
		
		return false;
	}
	
	private static Vector2 otherCordinate1 = new Vector2 (100000, 100000);
	
	private static NavigationTriangle prevNavigationTriangle;
	
	public bool IsCoordOutOfMesh (Vector3 position)
	{
		if (prevNavigationTriangle != null && prevNavigationTriangle.ContainsPoint (position)) {
			return false;
		}
		foreach (NavigationTriangle triangle in WorldController.Instance.NavigationTriangles) {
			if (triangle.ContainsPoint (position)) {
				return false;
			}
		}
		return true;
	}
	#endregion
	
	public static bool CheckIntersection (Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
	{
		Vector2 a = p2 - p1;
		Vector2 b = p3 - p4;
		Vector2 c = p1 - p3;
 
		float alphaNumerator = b.y * c.x - b.x * c.y;
		float alphaDenominator = a.y * b.x - a.x * b.y;
		float betaNumerator = a.x * c.y - a.y * c.x;
		float betaDenominator = alphaDenominator; /*2013/07/05, fix by Deniz*/
		
		bool doIntersect = true;
 
		if (alphaDenominator == 0 || betaDenominator == 0) {
			doIntersect = false;
		} else {
			if (alphaDenominator > 0) {
				if (alphaNumerator < 0 || alphaNumerator > alphaDenominator) {
					doIntersect = false;
				}
			} else if (alphaNumerator > 0 || alphaNumerator < alphaDenominator) {
				doIntersect = false;
			}
 
			if (doIntersect && betaDenominator > 0) {
				if (betaNumerator < 0 || betaNumerator > betaDenominator) {
					doIntersect = false;
				}
			} else if (betaNumerator > 0 || betaNumerator < betaDenominator) {
				doIntersect = false;
			}
		}
 
		return doIntersect;
	}
	
	private int prevNum = 0;

	#region LandscapeNavigator implementation
	public void AddStaticObstacleObject (SphereCollider collider)
	{
		StaticNavigationObstacle obstacle = new StaticNavigationObstacle (collider);
		lock (staticNavigationObstacles) {
			for (int i = prevNum; i < staticNavigationObstacles.Length; i++) {
				if (staticNavigationObstacles [i] == null) {
					staticNavigationObstacles [i] = obstacle;
					prevNum = i + 1;
					break;
				}
			}
		}
	}
	
	private bool IsStaticObjectObstacle (Vector3 startPos, Vector3 endPos)
	{
		bool doRay = false;
		if (doRay) {
			return Physics.Linecast (startPos, endPos, NavigationEngine.obstacleMask);
		}
		
		testPosition1.Set (startPos.x, startPos.z);
		testPosition2.Set (endPos.x, endPos.z);
		
		lock (staticNavigationObstacles) {
			foreach (StaticNavigationObstacle obstacle in staticNavigationObstacles) {
				if (obstacle == null) {
					break;
				}
				Vector2Pair pair1 = obstacle.Pair1;
				if (CheckIntersection (pair1.pos1, pair1.pos2, testPosition1, testPosition2)) {
					return true;
				}
				
				Vector2Pair pair2 = obstacle.Pair2;
				if (CheckIntersection (pair2.pos1, pair2.pos2, testPosition1, testPosition2)) {
					return true;
				}
			}
		}
		
		return false;
	}
	#endregion
	
		public void OnUpdate ()
	{
		foreach (Vector2Pair pair in vectorPairs) {
			pair.DrawInDebug ();
		}
		
		lock (staticNavigationObstacles) {
			foreach (StaticNavigationObstacle obstacle in staticNavigationObstacles) {
				if (obstacle == null) {
					break;
				}
				obstacle.DrawInDebug ();
			}
		}
		
		lock (WorldController.Instance.AllUnits) {
			foreach (UnitSubject unit in WorldController.Instance.AllUnits) {
				if (!unit.IsInited) {
					continue;
				}
				if (unit.IsBuilding) {
					continue;
				}
				
				Vector3 center = unit.TransformPosition;
				const float diameter = 0.5f;				
				 			
				Vector3 testPosition1 = Vector3.zero;
				Vector3 testPosition2 = Vector3.zero;
				
				testPosition1.Set (center.x + diameter, NavigationEngine.TerrainNormalHeight + 5, center.z + diameter);
				testPosition2.Set (center.x - diameter, NavigationEngine.TerrainNormalHeight + 5, center.z - diameter);		
				
				Debug.DrawLine (testPosition1, testPosition2);
				
				testPosition1.Set (center.x - diameter, NavigationEngine.TerrainNormalHeight + 5, center.z + diameter);
				testPosition2.Set (center.x + diameter, NavigationEngine.TerrainNormalHeight + 5, center.z - diameter);
				
				Debug.DrawLine (testPosition1, testPosition2); 
			}
		}
	}

}

[System.Serializable]
public class Vector2Pair
{
	[SerializeField]
	public Vector2 pos1;
	[SerializeField]
	public Vector2 pos2;
	
	public Vector2Pair ()
	{
	}
	
	public Vector2Pair (NavigationNode node)
	{
		Vector2 pos1 = new Vector2 (node.transform.position.x, node.transform.position.z);
		Vector2 pos2 = new Vector2 (node.PrevNavigationNode.transform.position.x, node.PrevNavigationNode.transform.position.z);
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
	
	public Vector2Pair (Vector2 pos1, Vector2 pos2)
	{
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
	
	public override int GetHashCode ()
	{
		return (int)(this.pos1.x * this.pos1.y + this.pos2.x * this.pos2.y * 10); 
	}
	
	public override bool Equals (object obj)
	{
		Vector2Pair otherVector = obj as Vector2Pair; 
		if (otherVector == null) {
			return false;
		} else {
			return pos1.Equals (otherVector.pos1) && pos2.Equals (otherVector.pos2);
		}
	}
	
	public void DrawInDebug ()
	{
		Vector3 p1;
		Vector3 p2;
		p1 = new Vector3 (pos1.x, NavigationEngine.TerrainNormalHeight + 10, pos1.y);
		p2 = new Vector3 (pos2.x, NavigationEngine.TerrainNormalHeight + 10, pos2.y);
		
		Debug.DrawLine (p1, p2);
	}
}


