using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class NavigationNode : TMonoBehaviour, IComparable
{
	public static readonly string NAVIGATION_NODE_NAME = "NavigationNode";
	public static readonly string OBSTACLE_NODE_NAME = "ObstacleNode";
	
	[SerializeField]
	public int Number;
	[SerializeField]
	public NavigationNode prevNavigationNode;
	[SerializeField]
	private bool isLast;
	[SerializeField]
	private bool isSelected;
	[SerializeField]
	public bool isObstacle;
	
	public void Init (WorldController worldController)
	{
//		this.worldController = worldController;
		this.IsSelected = false;
	}

	public NavigationNode PrevNavigationNode {
		get {
			return this.prevNavigationNode;
		}
		set {
			prevNavigationNode = value;
		}
	}
	
//	void Start ()
//	{
//		MeshRenderer meshRenderer = this.gameObject.GetComponent<MeshRenderer> ();
//		meshRenderer.enabled = false;
//	}
	
	public bool IsLast {
		get {
			return this.isLast;
		}
		set {
			isLast = value;
		}
	}

	#region IComparable implementation
	public int CompareTo (object obj)
	{
		NavigationNode node = (NavigationNode)obj;
		
		if (node.Number < this.Number)
			return 1;
		if (node.Number > this.Number)
			return -1;
		else
			return 0;
	}
	#endregion

	public bool IsObstacle {
		get {
			return this.isObstacle;
		}
		set {
			isObstacle = value;
		}
	}
	
	private Vector2 currentVector2Position;
	private Vector2 prevNodeVector2Position;

	public Vector2 CurrentVector2Position {
		get {
			return this.currentVector2Position;
		}
	}

	public Vector2 PrevNodeVector2Position {
		get {
			return this.prevNodeVector2Position;
		}
	}
	
	public void PrepareForNavigationCalculation () {
		Vector3 pos1 = this.transform.position;
		if (this.PrevNavigationNode == null) {
			return;
		}
		Vector3 pos2 = this.PrevNavigationNode.transform.position;
			
		currentVector2Position = new Vector2(pos1.x, pos1.z);
		prevNodeVector2Position = new Vector2(pos2.x, pos2.z);
	}

	public bool IsSelected {
		get {
			return this.isSelected;
		}
		set {
			if (value == isSelected) {
				return;
			}
			
			isSelected = value;
			
			if (value) {
				this.transform.localScale = new Vector3 (4f, 30f, 4f);		
			} else {
				this.transform.localScale = new Vector3 (3f, 6f, 3f);			
			}
		}
	}
	
	public void Deselect () {
		this.IsSelected = false;
	}

	public void Normalize ()
	{
		if (this.isObstacle) {
			this.gameObject.name = OBSTACLE_NODE_NAME;
		} else {
			this.gameObject.name = NAVIGATION_NODE_NAME;
		}
		this.gameObject.name += Number;
	}
	
	public Vector2 ToVector2 () {
		return new UnityEngine.Vector2 (transform.position.x, transform.position.z);
	}
}
