using System;
using System.Collections.Generic;
using UnityEngine;

namespace MovementEngine
{
	public class MovementPath
	{
		private int MAX_NUM_ITERATION = 30;
		private static int linecastMask;
		private List<Vector3> nodes = new List<Vector3> ();
		private bool wasOptimized = false;
		private float length = 0;
		private bool isFinished;
		private PathIterator pathIterator;
		private PathWalker pathWalker = new PathWalker ();
		private Vector3 forwardToMovementTarget;
		private NavigationEngine navigationEngine;
		
		public MovementPath ()
		{
			this.navigationEngine = WorldController.Instance.NavigationEngine;
			pathIterator = new PathIterator (this);
		}
	
		static MovementPath ()
		{
			var layer1 = LayerMask.NameToLayer ("Unit");
			var layer3 = LayerMask.NameToLayer ("Effect");
			linecastMask = 1 << layer1 | 1 << layer3; // Or, (1 << layer1) | (1 << layer2)
			linecastMask = ~linecastMask;
		}
	
		private void AddNode (Vector3 node)
		{
			lock (nodes) {
				node.Set (node.x, NavigationEngine.TerrainNormalHeight, node.z);
				nodes.Add (node);
			}
		}
		
		LinkedList<Vector3> tempNodes = new LinkedList<Vector3> ();
	
		public void CalculateForward () {
			Vector3 newForward = Vector3.zero;
			lock (nodes) {
				if (this.nodes.Count < 2) {
				} else {
					newForward = this.nodes [nodes.Count - 1] - this.nodes [nodes.Count - 2];
				}
			}
			if (newForward != Vector3.zero) {
				forwardToMovementTarget = newForward; 
			}
		}
		
		public void Optimize ()
		{
			lock (nodes) {
				wasOptimized = true;
				MAX_NUM_ITERATION = nodes.Count + 10;
				tempNodes.Clear ();
				int numIter = 0;
				while (true) {
					numIter++;
					if (MAX_NUM_ITERATION < numIter) {
						Debug.LogError ("Too mcuh iterations in path optimization");
						break;
					}
					if (nodes.Count <= 0) {
						break;
					}
				
					Vector3 curNode = nodes [0];
					nodes.RemoveAt (0);
				
					if (tempNodes.Count == 0 || nodes.Count == 0) {
						tempNodes.AddLast (curNode);
						continue;
					} 
				
					for (int i = nodes.Count - 1; i >= 0; i--) {
						Vector3 last = nodes [i];
						bool isLinecast = navigationEngine.IsStaticObstacleBetween (curNode, last);
						if (!isLinecast) {
							nodes.RemoveRange (0, i);
							Vector3 nextNode = nodes [0];
							tempNodes.AddLast ((curNode + nextNode) / 2);
							tempNodes.AddLast (nextNode);
							break;
						}
					}			
				}
				this.nodes.Clear ();
				this.nodes.AddRange (tempNodes);
				
//				this.length = CalculateLength (nodes);	
			}
		}		
				
		public Vector3 GetForwardToMovementTarget ()
		{
			return forwardToMovementTarget;			
		}
	
		public void CalculateLength ()
		{
			lock (nodes) {
				this.length = CalculateLength (this.nodes);
			}
		}
	
		public static float CalculateLength (List<Vector3> nodes)
		{
			float distance = 0;
		
			for (int i = 0; i < nodes.Count - 1; i++) {
				distance += Vector3.Distance (nodes [i], nodes [i + 1]);
			}
			return distance;
		}

//		public System.Collections.Generic.List<UnityEngine.Vector3> Nodes {
//			get {
//				return this.nodes;
//			}
//		}

		public bool IsFinished {
			get {
				return this.isFinished;
			}
		}
		
		public void SetFinished (bool finished) {			
			this.isFinished = finished;			
		}

		public float Length {
			get {
				if (!this.isFinished) {
					return float.MaxValue;
				}
				return this.length;
			}
		}
			
		public PathIterator getPathIterator ()
		{
			pathIterator.Reset ();
			return pathIterator;
		}
	
		internal PathWalker CreatePathWalker (Vector3 pos1, Vector3 endPos)
		{
			this.pathWalker.Init (this, pos1, endPos);
			return this.pathWalker;
		}

		public Vector3 EndPos 
		{
			get {
				Vector3 result = Vector3.zero;
				lock (nodes) {
					if (nodes.Count >= 1) {
						result = this.nodes [nodes.Count - 1];
					}
				}
				
				return result;
			}			
		}
	
		public class PathIterator
		{
			private MovementPath path;
//			private int pos = 0;
		
			internal PathIterator (MovementPath path)
			{
				this.path = path;
			}
		
			internal void Reset ()
			{
//				pos = 0;
			}
			
			public Vector3 GetFirstNode (Vector3 currentNodeTarget) {
				Vector3 result = currentNodeTarget;
				
				lock (path.nodes) {				
					if (path.nodes.Count > 0) {
						result = path.nodes [0];
					}					
				}
				
				return result;
			}

			public Vector3 GetNextNode (Vector3 currentNodeTarget)
			{
				lock (path) {
					if (path.nodes.Count <= 0) {
						return currentNodeTarget;
					}
					
					if (path.nodes.Count > 1) {
						path.nodes.RemoveAt (0);
					}
				
					Vector3 current = path.nodes [0];
					return current;
				}
			}
			
			public Vector3 GetLaterNode () {
				Vector3 result = path.EndPos;
				lock (path.nodes) {
					if (path.nodes.Count > 1) {
						result = path.nodes[1];
					} else if (path.nodes.Count == 0) {
						result = Vector3.zero;
					}
				}
				return result;
			}
		}

		internal class PathWalker
		{
			private Vector3 currentPos;
			private Vector3 endPos;
			private Vector3 targetForward;
			private Vector3 currentForward;
			private MovementPath path;
		
			public PathWalker ()
			{			
			}
		
			internal void Init (MovementPath path, Vector3 startPos, Vector3 endPos)
			{
				this.path = path;
				this.currentPos = startPos;
				this.endPos = endPos;
	
				this.targetForward = endPos - currentPos;
				this.currentForward = this.targetForward;
						
				lock (path.nodes) {
					this.path.nodes.Add (startPos);
				}
			}
		
			internal void DoNextStep (Vector3 nextPos, Vector3 newForward)
			{
				if (nextPos == endPos) {
					this.currentPos = nextPos;
					this.path.AddNode (nextPos);
					return;
				}		
				SetNextPos (nextPos, newForward);				
			}
		
			private void SetNextPos (Vector3 nextPos, Vector3 newForward)
			{
				this.currentPos = new Vector3 (nextPos.x, this.currentPos.y, nextPos.z);			
				this.currentForward = newForward;	
				
				this.path.AddNode (this.currentPos);
				this.targetForward = endPos - currentPos;	
			}
		
			public UnityEngine.Vector3 CurrentPos {
				get {
					return this.currentPos;
				}
			}

			public UnityEngine.Vector3 EndPos {
				get {
					return this.endPos;
				}
			}

			public Vector3 TargetForward {
				get {
					return this.targetForward;
				}
			}

			public Vector3 CurrentForward {
				get {
					return this.currentForward;
				}
			}

			public void SetFinished ()
			{
				this.path.isFinished = true;
			}
		}

		public bool WasOptimized {
			get {
				return this.wasOptimized;
			}
		}
	
		public void Clear ()
		{	
			lock (nodes) {
				nodes.Clear ();
			}
			wasOptimized = false;
			length = 0;
			isFinished = false;
		
			pathIterator.Reset ();
		}
	
		public void CloneFrom (MovementPath otherPath)
		{
			Clear ();
		
			AddNodesFromPath (otherPath);
			this.wasOptimized = otherPath.wasOptimized;
		}
		
		public void AddNodesFromPath (MovementPath otherPath)
		{
			lock (nodes) {			
				otherPath.nodes.ForEach (x => nodes.Add (x));				
			}
		
			this.length += otherPath.length;
			this.isFinished = otherPath.isFinished;
		}
	
		private bool IsPositionOk (Vector3 position)
		{
			return true;
		}
	
		public void BuildFrom (MovementPath sourcePath, Vector3 startNode, Vector3 endNode)
		{
			Clear ();
			lock (nodes) {			
				nodes.Add (startNode);			
				if (WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (startNode, endNode)) {
					this.InterpolateMovementNodes (sourcePath, startNode, endNode);			
				}						
				nodes.Add (endNode);				
			}
			
			Optimize ();
				
			this.length = sourcePath.length;
			this.isFinished = sourcePath.isFinished;
			this.wasOptimized = sourcePath.wasOptimized;	
		}
		
		private void InterpolateMovementNodes (MovementPath sourcePath, Vector3 startNode, Vector3 endNode) {
			int i = 0;
			
			int numOperations = 0;
			
			while (i < sourcePath.nodes.Count) {
				Vector3 sourceNode = sourcePath.nodes[i];
				
				float RANGE = 5f;
				Vector3 newNode = new Vector3 ();				
				int numIter = 0;
				bool isCoordProhibited = true;
				Vector3 prevCoord;
				
				if (i > 0) {
					prevCoord = sourcePath.nodes[i - 1];
				} else {
					prevCoord = startNode;
				}
				Vector3 nextNode;
				if (i == sourcePath.nodes.Count - 1) {
					nextNode = endNode;
				} else {
					nextNode = sourcePath.nodes[i + 1];
				}
				Vector3 forward = prevCoord - sourceNode;
				do {								
					if (numIter++ > 5) {
						newNode = sourceNode;
						isCoordProhibited = false;
					} else {
						float nx = Randomizer.Next (RANGE) + 5f;			
						float nz = Randomizer.Next (RANGE) + 5f;		
					 
						newNode.Set (nx, 0 , nz);		
						
						newNode = Quaternion.FromToRotation (Vector3.forward, forward) * newNode;
						newNode += sourceNode;										
						isCoordProhibited = navigationEngine.IsCoordProhibited (newNode) && navigationEngine.IsStaticObstacleBetween (newNode, nextNode);
						if (!isCoordProhibited) {
							isCoordProhibited = navigationEngine.IsStaticObstacleBetween (newNode, prevCoord);		
						}
					}
					
				} while (isCoordProhibited);
				nodes.Add (newNode);
				
				i++;
			}
		}
		
		public void DrawPath () {
			lock (nodes) {
				for (int i = 0; i < nodes.Count - 1; i++) {
					Debug.DrawLine (nodes [i], nodes [i + 1]);
				}
			}
		}
		
		public void BuildFromTriangles (Vector3 start, Vector3 end) {
			Clear ();
			
			NavigationTriangle startTriangle = null;
			NavigationTriangle endTriangle = null;
			
			foreach (NavigationTriangle triangle in WorldController.Instance.NavigationTriangles) {
				if (triangle.ContainsPoint (start)) {
					startTriangle = triangle;
				}
				
				if (triangle.ContainsPoint (end)) {
					endTriangle = triangle;
				}
				
				if (endTriangle != null && startTriangle != null) {
					break;
				}
			}
			
			LinkedList<NavigationTriangle> trianglePath = PathTrianglesCalculator.CalculateTrianglePath (WorldController.Instance.NavigationTriangles, startTriangle)[endTriangle];
			
			lock (nodes) {			
				nodes.Add (start);			
				foreach (NavigationTriangle triangle in trianglePath) {
					nodes.Add (triangle.CenterPosition);
				}
				nodes.Add (end);				
			}
			
			this.Optimize ();
			
			this.SetFinished (true);
		}
		

//		public void BuildFrom (Vector3 currentUnitPos, Vector3 endPos)
//		{
//			Clear ();
//			nodes.Add (currentUnitPos);
//			nodes.Add (endPos);
//		}
	}

}