using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PathTrianglesCalculator
{
	public static List<NavigationTriangle> pathTriangles;
	
	private static Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> nodeMap = new Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>>();
	private static Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> pathMap = new Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>>();
	private static LinkedList<NavigationTriangle> tempList = new LinkedList<NavigationTriangle> ();
		
	private static void Prepare (IEnumerable<NavigationTriangle> allNavigationTriangles) {
		ResetData ();
		pathTriangles = new List<NavigationTriangle> ();
		foreach (NavigationTriangle triangle in allNavigationTriangles) {
			if (triangle.IsWorldPathTriangle) {
				pathTriangles.Add (triangle);
			}
		}
	}
	
	public static Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> CalculateTrianglePath (IEnumerable<NavigationTriangle> allNavigationTriangles, NavigationTriangle startTriangle) {
		Prepare (allNavigationTriangles);
		foreach (NavigationTriangle sonTriangle in startTriangle.WorldPathTriangles) {
			CreateNewNode (sonTriangle, startTriangle);
		}
		
		int maxNumOperations = 40;
		int i = 0;
		do {	
			if (i++ > maxNumOperations) {
				Debug.Log ("Dijkstra CRAP");
				return null;
			}
			
			foreach (NavigationTriangle triangle in nodeMap.Keys) {
				if (triangle.name != startTriangle.name) { 
					tempList.AddLast (triangle);
				}
			}
			
			foreach (NavigationTriangle triangle in tempList) {
				ProcessNodeTriangle (triangle);
			}
			tempList.Clear ();
			
			if (nodeMap.ContainsKey(startTriangle)) {				
				nodeMap.Remove (startTriangle);			
			}
		} while (nodeMap.Count > 0);
		
		
		foreach (LinkedList<NavigationTriangle> triangleList in pathMap.Values) {
			string path = "";
			foreach (NavigationTriangle triangle in triangleList) {
				path += triangle.name + " ";
			}
			Debug.Log (path);
		}
		
		nodeMap.Clear ();
		
		return pathMap;
	}
	
	private static void ProcessNodeTriangle(NavigationTriangle sourceTriangle) {
		foreach (NavigationTriangle sonTriangle in sourceTriangle.WorldPathTriangles) {
			CreateNewNode (sonTriangle, sourceTriangle);
		}

		nodeMap.Remove(sourceTriangle);
	}
	
	private static void CreateNewNode(NavigationTriangle sonTriangle, NavigationTriangle parentTriangle) {
		LinkedList<NavigationTriangle> sonPathOld = null;
		if (pathMap.ContainsKey (sonTriangle)) {
			sonPathOld = pathMap[sonTriangle];
		} 
		LinkedList<NavigationTriangle> parentPath = null;
		if (pathMap.ContainsKey (parentTriangle)) {
			parentPath = pathMap[parentTriangle];
		} 
		LinkedList<NavigationTriangle> newPath = new LinkedList<NavigationTriangle> ();
		float movementPenaltyNew = Vector3.Distance (parentTriangle.CenterPosition, sonTriangle.CenterPosition);
		if (parentPath != null) {
			NavigationTriangle prevTriangle = null;
			foreach (NavigationTriangle triangle in parentPath) {
				newPath.AddLast(triangle);
				if (prevTriangle != null) {
					movementPenaltyNew += Vector3.Distance (triangle.CenterPosition, prevTriangle.CenterPosition);
				}
				prevTriangle = triangle;
			}
		} else {
			newPath.AddLast(parentTriangle);
		}
		newPath.AddLast(sonTriangle);

		if (sonPathOld == null) {
			if (!nodeMap.ContainsKey (sonTriangle)) {
				nodeMap.Add(sonTriangle, newPath);
			}	else {
				nodeMap[sonTriangle] = newPath;
			}
				
			if (!pathMap.ContainsKey (sonTriangle)) {
				pathMap.Add(sonTriangle, newPath);
			}	else {
				pathMap[sonTriangle] = newPath;	
			}			
		} else {
			float movementPenaltyOld = 0;

			NavigationTriangle prevTriangle = null;			
			foreach (NavigationTriangle triangle in sonPathOld) {
				if (prevTriangle != null) {
					movementPenaltyOld += Vector3.Distance (triangle.CenterPosition, prevTriangle.CenterPosition);
				}
				prevTriangle = triangle;
			}
			
			if (movementPenaltyOld > movementPenaltyNew) {
				if (!nodeMap.ContainsKey (sonTriangle)) {
					nodeMap.Add(sonTriangle, newPath);
				}	else {
					nodeMap[sonTriangle] = newPath;
				}
				
				if (!pathMap.ContainsKey (sonTriangle)) {
					pathMap.Add(sonTriangle, newPath);
				}	else {
					pathMap[sonTriangle] = newPath;	
				}
			}
		}
	}
	
	private static void ResetData() {
		pathMap = new Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> ();
		nodeMap.Clear ();
	}
	
	public static void OnDestroy () {
		ResetData ();
		if (pathTriangles != null) pathTriangles.Clear ();
		pathTriangles = null;
	}
}


