using System;
using UnityEngine;
using System.Collections.Generic;
using MovementEngine;

[System.Serializable]
public class NavigationEngineClient
{
	private List<NavigationTriangle> triangles;
	private static Vector2 otherCordinate1 = new Vector2 (1000, 1000);
	private Timer calculationTimer = Timer.CreateFrequency (3000);
	private NavigationEngine navigationEngine;
	private TroopDemeanor unitOwner;
	
	public NavigationEngineClient (TroopDemeanor unitOwner, Vector3 currentPosition)
	{
		this.unitOwner = unitOwner;
		this.navigationEngine = WorldController.Instance.NavigationEngine;
		triangles = WorldController.Instance.NavigationTriangles;
		
		this.PreparePosition (currentPosition);
	}
	
	private void PreparePosition (Vector3 currentPosition)
	{
		if (currentTriangle != null) {
			currentTriangle = TriangleContainingPoint (currentPosition);
			return;
		} else {		
			foreach (NavigationTriangle triangle in triangles) {
				if (triangle.ContainsPoint (currentPosition)) {
					currentTriangle = triangle;
					break;
				}
			}
		}
	}
	
	public void CalculatePath (MovementPath sourcePath, Vector3 startPos, Vector3 endPos, NavigationTriangle startTriangle = null) {
		lock (sourcePath) {
			if (currentTriangle == null) {
				this.PreparePosition (startPos);
			}
			
			NavigationTriangle endTriangle;
			
			sourcePath.Clear ();
			if (unitOwner.IsInteractingTarget || currentTriangle.ContainsPoint (endPos) || Vector3.Distance (startPos, endPos) < 25f) {
				MovementPathSeeker.CalculateShortestPath (sourcePath, startPos, endPos);
				sourcePath.Optimize ();
				return;
			} 
			
			if (startTriangle == null) {
				startTriangle = currentTriangle.ClosestWorldPathTriangle;
			}
			endTriangle = NavigationTriangle.CalculateClosestWorldPathTriangle (WorldController.Instance, endPos);			
			
			if (startTriangle == endTriangle) {
				MovementPathSeeker.CalculateShortestPath (sourcePath, startPos, endPos);
				sourcePath.Optimize ();
				return;
			}
			
			NavigationTriangle[] list = startTriangle.GetPathTo (endTriangle);		
			
			Vector3 prevTrianglePosition = Vector3.zero; 
			int i = 0;
			foreach (NavigationTriangle triangle in list) {
				Vector3 currentTrianglePosition = RandomizePoint (triangle.CenterPosition);
				if (i == 1) {
					prevTrianglePosition = startPos;
				}
				
				if (i == list.Length - 1) {
					break;
				}
				
				if (prevTrianglePosition != Vector3.zero) {
					MovementPathSeeker.AddNewPathSegment (sourcePath, prevTrianglePosition, currentTrianglePosition);
				}
				prevTrianglePosition = currentTrianglePosition;
				i++;
			}
			
			MovementPathSeeker.AddNewPathSegment (sourcePath, prevTrianglePosition, endPos);
			sourcePath.Optimize ();
		}
	}
	
	private Vector3 RandomizePoint (Vector3 sourceNode) {
		float RANGE = 5f;
		
		bool isCoordProhibited = false;
		Vector3 newNode = Vector3.zero;
		int numIter = 0;
		do {								
			if (numIter++ > 5) {
				isCoordProhibited = false;
			} else {
				float nx = Randomizer.Next (RANGE) - RANGE / 2;			
				float nz = Randomizer.Next (RANGE) - RANGE / 2;		
					 
				newNode.Set (nx, 0 , nz);		
						
//				newNode = Quaternion.FromToRotation (Vector3.forward, forward) * newNode;
				newNode += sourceNode;										
				isCoordProhibited = navigationEngine.IsCoordProhibited (newNode);
			}				
		} while (isCoordProhibited);
		return newNode;
	}
	
	public bool IsStaticObstacleBetween (Vector3 startPos, Vector3 endPos)
	{
		return navigationEngine.IsStaticObstacleBetween (startPos, endPos);		
	}
	
	public bool IsDynamicObstacle (Vector3 startPos, Vector3 fwd, float distance, UnitSubject unit)
	{	
		return navigationEngine.IsDynamicObstacle (startPos, fwd, distance, unit);		
	}
	
	private Dictionary<Vector2Pair, bool> calculatedEdges = new Dictionary<Vector2Pair, bool> ();
	private NavigationTriangle currentTriangle;
	
	public bool IsStaticObstacleForward (Vector3 position, Vector3 fwd)
	{					
		if (currentTriangle == null) {
			return WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (position, position + fwd);
		}
		
		calculatedEdges.Clear ();
		Vector3 point = position + fwd;
		
		if (currentTriangle != null && currentTriangle.ContainsPoint (point)) {
			return false;
		}
		
		if (WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (position, position + fwd)) {
			return true;
		}
		
		return false;
	}
	
	private NavigationTriangle TriangleContainingPoint (Vector3 point)
	{
		if (currentTriangle.ContainsPoint (point)) {
			return currentTriangle; 
		}
		
		foreach (NavigationTriangle triangle in currentTriangle.edgeTriangles) {
			if (triangle.ContainsPoint (point)) {
				return triangle;
			}
		}
		
		return null;
	}
	
	public void OnStepStarted (Vector3 currentPosition)
	{
		this.PreparePosition (currentPosition);
	}
	
	static Vector2 testPoint = new Vector2 ();
	
//	public static bool ContainsPoint (NavigationTriangle triangle, Vector3 point)
//	{
//		Vector2Pair[] edges = triangle.Edges;
//		int count = 0;
//		testPoint.Set (point.x, point.z);
//		
//		for (int i = 0; i < 3; i++) {
//			if (CheckIntersection (edges [i], point)) {
//				count++;
//			}				
//		}
//		
//		bool result = count % 2 == 1;
//
//		return result;
//	}
//	
//	public static bool CheckIntersection (Vector2Pair edge, Vector3 point)
//	{
////		if (calculatedEdges.ContainsKey (edge)) {
////			return calculatedEdges [edge];
////		}
//		
//		bool intersection = FigureNavigator.CheckIntersection (edge.pos1, edge.pos2, testPoint, otherCordinate1);
////		calculatedEdges.Add (edge, intersection);
//		return intersection;
//	}
}



