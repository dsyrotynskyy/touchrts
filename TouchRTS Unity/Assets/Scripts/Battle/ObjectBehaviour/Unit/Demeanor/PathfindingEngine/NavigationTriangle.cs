using System;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class NavigationTriangle : TMonoBehaviour
{
	public static readonly string NAVIGATION_TRIANGLE_NAME = "Navigation Triangle";
	[SerializeField]
	public List<NavigationTriangle> edgeTriangles = new List<NavigationTriangle> ();
	[SerializeField]
	public List<NavigationTriangle> nodeTriangles = new List<NavigationTriangle> ();
	[SerializeField]
	public List<string> nodeTrianglesNames = new List<string> ();
	
	[SerializeField]
	public NavigationNode[] nodes = new NavigationNode[3];
	[SerializeField]
	private Vector2Pair[] edges = new Vector2Pair[3];
	[SerializeField]
	private Vector3 centerPosition;
	[SerializeField]
	public bool isWorldPathTriangle;
	
	//is himself if is central triangle
	[SerializeField]
	private NavigationTriangle closestWorldPathTriangle;
	[SerializeField]
	private List<NavigationTriangle> worldPathTriangles = new List<NavigationTriangle> (); 
	[SerializeField]
	private Map<NavigationTriangle, List<NavigationTriangle>> pathMap = new Map<NavigationTriangle, List<NavigationTriangle>> ();
	
//	private Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> pathMap;
	
	private void CheckEdgeMemory () {
		if (edges[0] == null) {
			edges [0] = new Vector2Pair ();
			edges [1] = new Vector2Pair ();
			edges [2] = new Vector2Pair ();
		}
	}
	
	public void Init (WorldController controller, LinkedList<NavigationNode> sourceNodes)
	{
		this.CheckEdgeMemory ();
		
		int i = 0;
		foreach (NavigationNode node in sourceNodes) {
			nodes [i] = node;
			i++;
		}
		
		this.gameObject.name = NAVIGATION_TRIANGLE_NAME + controller.NavigationTriangles.Count;
		
		this.PrepareTriangle (controller);
	}
	
	public void PrepareTriangle (WorldController controller)
	{
		this.gameObject.transform.position = CalculateCenterPosition ();
		if (nodes [0] == null || nodes [1] == null || nodes [2] == null) {
			GameObject.DestroyImmediate (this.gameObject);
			return;
		}
		
		this.CheckEdgeMemory ();
//		this.gameObject.transform.position = this.centerPosition;
		
		edges [0].pos1 = nodes [0].ToVector2 ();
		edges [0].pos2 = nodes [1].ToVector2 ();
		
		edges [1].pos1 = nodes [1].ToVector2 ();
		edges [1].pos2 = nodes [2].ToVector2 ();
		
		edges [2].pos1 = nodes [2].ToVector2 ();
		edges [2].pos2 = nodes [0].ToVector2 ();
		
		this.CalculateClosestTriangles (controller);
		
		this.centerPosition = gameObject.transform.position;
	}
	
	public void CalculateClosestTriangles (WorldController controller)
	{
		if (nodes [0] == null || nodes [1] == null || nodes [2] == null) {
			return;
		}
		edgeTriangles.Clear ();
		nodeTriangles.Clear ();
		foreach (NavigationTriangle triangle in controller.NavigationTriangles) {
			int sameNodes = 0;
			for (int i = 0; i < 3; i++) {
				NavigationNode node = triangle.nodes [i];
				foreach (NavigationNode ourNode in nodes) {
					if (ourNode.Equals (node)) {
						sameNodes++;	
						break;
					}
				}		
			}
			if (sameNodes == 1) {
				nodeTriangles.Add (triangle);
			} else if (sameNodes == 2) {
				edgeTriangles.Add (triangle);
			}
		}	
		this.closestWorldPathTriangle = CalculateClosestWorldPathTriangle (controller, this.centerPosition);
		CalculateShortestPathToOtherWorldPathTriangles (controller);
	}
	
	#region WorldPath
	public static NavigationTriangle CalculateClosestWorldPathTriangle (WorldController controller, Vector3 position) {
		float minimumPath = -1;
		NavigationTriangle closestWorldTriangle = null;
		foreach (NavigationTriangle triangle in controller.NavigationTriangles) {
			if (!triangle.IsWorldPathTriangle) {
				continue;
			}
			float distance = Vector3.Distance (position, triangle.centerPosition);
			if (minimumPath < 0 || distance < minimumPath) {
				minimumPath = distance;
				closestWorldTriangle = triangle;
			} 
		}
		return closestWorldTriangle;
	}
	
	public void CalculateShortestPathToOtherWorldPathTriangles (WorldController controller) {
		this.worldPathTriangles.Clear ();
		foreach (NavigationTriangle triangle in controller.NavigationTriangles) {
			if (!triangle.IsWorldPathTriangle || triangle == this) {
				continue;
			}
			
			if (!controller.NavigationEngine.IsStaticObstacleBetween (this.centerPosition, triangle.centerPosition)) {
				this.worldPathTriangles.Add (triangle);
			}
		}
	}		
		
	public Vector3 CalculateCenterPosition ()
	{
		if (nodes [0] == null || nodes [1] == null || nodes [2] == null) {
			return Vector3.zero;
		}
		return (nodes [0].transform.position + nodes [1].transform.position + nodes [2].transform.position) / 3f;
	}
	#endregion
	
	#region map
	//Becouse stupid unit do not want to serialize map!
	[SerializeField]
	private NavigationTriangle[] keysList;
	[SerializeField]
	private NavigationTrianglePath[] valuesList;
	
	public void CalculatePathMap (WorldController controller)
	{
		Dictionary<NavigationTriangle, LinkedList<NavigationTriangle>> source = PathTrianglesCalculator.CalculateTrianglePath (controller.NavigationTriangles ,this);
		
		this.keysList = new NavigationTriangle[source.Keys.Count];
		this.valuesList = new NavigationTrianglePath[source.Keys.Count];
		int i = 0;
		foreach (NavigationTriangle key in source.Keys) {				
			this.keysList[i] = key;
			
			NavigationTriangle[] list = new NavigationTriangle [source[key].Count];
			int j = 0;
			LinkedList<NavigationTriangle> sourceList = source[key];
			foreach (NavigationTriangle triangleValue in sourceList) {
				list[j] = triangleValue;
				j++;
			}
			this.valuesList[i] = new NavigationTrianglePath ();
			this.valuesList[i].ValuesList = list;
			i++;
		}
	}
	
	public NavigationTriangle[] GetPathTo (NavigationTriangle triangle) {
		
		int keyIndex = 0;
		for (int i = 0; i < keysList.Length; i++) {
			if (keysList [i].Equals (triangle)) {
				keyIndex = i;
				break;
			}
		}
 
		NavigationTriangle[] list = valuesList [keyIndex].ValuesList;
		
		return list;
	}
	
	[System.Serializable]
	public class NavigationTrianglePath {
		[SerializeField]
		private NavigationTriangle[] valuesList;
	
		public NavigationTriangle[] ValuesList {
			get {
				return this.valuesList;
			}
			set {
				valuesList = value;
			}
		}
	}
	

	#endregion	
	
//	public bool ContaintsPoint (Vector3 point) {
//		return ContainsPoint ();
//	}
	
	#region calculation
	
	private static List<Vector2> vectors = new List<Vector2> ();
	
	public bool ContainsPoint (Vector3 point)
	{
		bool res;
		lock (vectors) {
			vectors.Clear ();
			foreach (Vector2Pair pair in edges) {
				if (!vectors.Contains (pair.pos1)) {
					vectors.Add (pair.pos1);
				}
				
				if (!vectors.Contains (pair.pos2)) {
					vectors.Add (pair.pos2);
				}
			}
	
			res = PointInTriangle (new Vector2(point.x, point.z), vectors [0], vectors [1], vectors [2]);
		}
		return res;
	}
	
	private bool IsInside (Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
	{
		return IsInside (v1.x, v1.y, v2.x, v2.y, v3.x, v3.y, pt.x, pt.y);
	}
 
	/* A function to check whether point P(x, y) lies inside the triangle formed 
	   by A(x1, y1), B(x2, y2) and C(x3, y3) */
	private bool IsInside (float x1, float y1, float x2, float y2, float x3, float y3, float x, float y)
	{   
		/* Calculate area of triangle ABC */
		float A = area (x1, y1, x2, y2, x3, y3);
	 
		/* Calculate area of triangle PBC */  
		float A1 = area (x, y, x2, y2, x3, y3);
	 
		/* Calculate area of triangle PAC */  
		float A2 = area (x1, y1, x, y, x3, y3);
	 
		/* Calculate area of triangle PAB */   
		float A3 = area (x1, y1, x2, y2, x, y);
	   
		/* Check if sum of A1, A2 and A3 is same as A */
		return Math.Abs (A - (A1 + A2 + A3)) < 0.000000001f;
	}
	
	private	float area (float x1, float y1, float x2, float y2, float x3, float y3)
	{
		return (float)Math.Abs ((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
	}
	
	private bool PointInTriangle (Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
	{
		bool b1, b2, b3;
	
		b1 = sign (pt, v1, v2) < 0.0f;
		b2 = sign (pt, v2, v3) < 0.0f;
		b3 = sign (pt, v3, v1) < 0.0f;
	
		return ((b1 == b2) && (b2 == b3));
	}
	
	private float sign (Vector2 p1, Vector2 p2, Vector2 p3)
	{
		return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}
	
	#endregion

	public NavigationTriangle ClosestWorldPathTriangle {
		get {return closestWorldPathTriangle;}
	}
	
	public Vector3 CenterPosition {
		get {
			return this.centerPosition;
		}
	}
	
//	public Vector3 PositionCloseToCenter {
//		get {
//			return this.centerPosition;
//		}
//	}

	public Vector2Pair[] Edges {
		get {
			return this.edges;
		}
	}
	
	public bool IsWorldPathTriangle {
		get {return isWorldPathTriangle;}
		set {isWorldPathTriangle = value;}
	}	
	
	public void DrawGizmos ()
	{
		if (nodes [0] == null || nodes [1] == null || nodes [2] == null) {
			return;
		}
		Gizmos.color = Color.green;
				
		Gizmos.DrawLine (nodes [0].transform.position, nodes [1].transform.position);
		Gizmos.DrawLine (nodes [1].transform.position, nodes [2].transform.position);
		Gizmos.DrawLine (nodes [2].transform.position, nodes [0].transform.position);
		
		Gizmos.color = Color.red;
			
		if (IsWorldPathTriangle) {
			foreach (NavigationTriangle triangle in worldPathTriangles) {
				if (triangle != null)
					Gizmos.DrawLine (this.transform.position, triangle.transform.position);
			}
		} else {
//			Gizmos.DrawLine (centerPosition, closestWorldPathTriangle.centerPosition);
		}
		
		Gizmos.color = Color.black;	
		
		Gizmos.DrawLine (nodes [0].transform.position, centerPosition);
		Gizmos.DrawLine (nodes [1].transform.position, centerPosition);
		Gizmos.DrawLine (nodes [2].transform.position, centerPosition);
	}

	public List<NavigationTriangle> WorldPathTriangles {
		get {
			return this.worldPathTriangles;
		}
	}
}




