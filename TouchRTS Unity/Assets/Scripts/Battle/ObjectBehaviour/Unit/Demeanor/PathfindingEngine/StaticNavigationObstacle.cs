using System;
using UnityEngine;
using Setup;

public class StaticNavigationObstacle
{
	private Vector2Pair pair1;
	private Vector2Pair pair2;
	
	private Vector3 center;
	private float radius;
	
	public StaticNavigationObstacle (SphereCollider collider)
	{
		CreateFromCollider (collider);
	}
	
	private void CreateFromCollider (SphereCollider collider) {
		center = collider.transform.position;
		radius = collider.radius;
		float diameter = radius	* 2;
		
		pair1 = new Vector2Pair (new Vector2 (center.x + diameter, center.z + diameter),
				new Vector2 (center.x - diameter, center.z - diameter));
		
		pair2 = new Vector2Pair (new Vector2 (center.x - diameter, center.z + diameter),
				new Vector2 (center.x + diameter, center.z - diameter));
	}
	
	private void CreateMinimapObject (SphereCollider collider) {
		float size = collider.radius * 2;
		GameObject miniMapGameObject = ObjectController.CreateMiniMapSphere2 ();
		miniMapGameObject.name = "MapBounds";
		miniMapGameObject.layer = GameSettings.Layer.MINI_MAP_ELEMENT;
		miniMapGameObject.transform.parent = collider.transform;
		miniMapGameObject.transform.localScale = new Vector3 (size, size, size);
		miniMapGameObject.transform.localPosition = new Vector3 (0, 2, 0);
		miniMapGameObject.renderer.material.color = Color.green;
	}
	
	public bool OccupiesPoint (Vector3 point) {
		return Vector3.Distance (point, center) < radius;
	}
	
	public Vector2Pair Pair1 {
		get {
			return this.pair1;
		}
	}

	public Vector2Pair Pair2 {
		get {
			return this.pair2;
		}
	}

	public void DrawInDebug ()
	{
		this.pair1.DrawInDebug ();
		this.pair2.DrawInDebug ();
	}
}


