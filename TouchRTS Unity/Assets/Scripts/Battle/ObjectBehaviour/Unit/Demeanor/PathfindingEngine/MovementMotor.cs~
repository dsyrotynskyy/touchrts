using System;
using UnityEngine;

namespace MovementEngine
{
	public class MovementMotor
	{
		private const int STEPS_STRAIGHT_BEFORE_ROTATE = 4;
		private const float SPEED_MAX = 12;
		private const float SPEED_NORMAL = 8;
		private const float SPEED_LITTLE = 5;
		private const float STEP_DIST = 1.5f;
		
		public static readonly float MIN_DISTANCE_TO_STATIC_TARGET = 1.5f;
		public static readonly float MIN_DISTANCE_TO_MOVING_TARGET = 3f;
		
		private const int MAX_NUM_ITERATION = 5;
		private const int IGNORE_TIME_TO_STOP = -1;
		private const int STOP_TIME = 5500;
		private const int MIN_MOVEMENT_TIME = 2000;
		private const float NORMAL_ANGLE_STEP = 30;
		private float angleStep = NORMAL_ANGLE_STEP;
		private bool isMoving = false;
		private MovementController movementController;
		private MovementPath.PathIterator pathIterator;
//		private	Vector3 oldCoord;
		private Vector3 currentNodeTarget;
		private Vector3 targetForward;
		private Vector3 currentForward;
		private Vector3 prevNodeTarget;
		private float distanceToTarget;
		private float prevGoodDistanceToTarget;
//		private int steps = 0;
//		private int startMovementTime;
		
		private Timer stopMovementTimer = Timer.CreateFrequencyWithLife (STOP_TIME, STOP_TIME * 2);
		
//		private int timeToStop = IGNORE_TIME_TO_STOP;
	
		public MovementMotor (MovementController movementController)
		{
			this.movementController = movementController;			
		}
		
		internal void OnNewMovementStarted ()
		{		
			isMoving = true;
			pathIterator = this.movementController.path.getPathIterator ();
			this.currentNodeTarget = pathIterator.GetFirstNode (this.currentNodeTarget);	
			prevGoodDistanceToTarget = float.MaxValue;
			stopMovementTimer.Deactivate ();
			prevNodeTarget = this.currentNodeTarget;
			
			this.CalculateTargetForward ();
			this.currentForward = targetForward;
			if (Randomizer.NextBool ()) {
				this.RevertAngle ();
			}		
		}
		
		internal void DoMovement ()
		{	
			if (!isMoving) {
				return;
			}
			
			if (this.currentNodeTarget == Vector3.zero || pathIterator == null) {
				movementController.StopMovement ();
				return;
			}
		
			this.FinishClosestNodes ();	
			this.CalculateTargetForward ();
			
			if (!isMoving) {
				return;
			}
			
			if (CheckForStopMovement ()) {	
				movementController.StopMovement ();
				return;
			}
			
			movementController.NavigationClient.OnStepStarted (this.currentUnitPos);
			
			DoStep ();
		}
		
		private Timer endPosTimer = Timer.CreateFrequency (500);
		
		private void FinishClosestNodes ()
		{
			float minDistance;				
						
			if (this.unit.IsFollowingPlayerCommand) {
				minDistance = MIN_DISTANCE_TO_STATIC_TARGET;
			} else {
				minDistance = MIN_DISTANCE_TO_MOVING_TARGET;
			}
			
			if (endPosTimer.EnoughTimeLeft ()) {
				float dist = Vector3.Distance (currentUnitPos, this.endPos);
				if (dist < minDistance) {
					movementController.StopMovement ();
					return;
				}
			}
			
			int numIter = 0;
			while (true) {	
				numIter++;
				if (MAX_NUM_ITERATION < numIter) {
					Debug.LogError ("Too much nodes in finishing closest nodes");
					return;
				}
				distanceToTarget = Vector3.Distance (currentUnitPos, this.currentNodeTarget);
			
				bool setTargetNextNode = distanceToTarget < minDistance;
				
				if (!setTargetNextNode && this.CurrentNodeTarget != this.endPos) {
					Vector3 laterNode = pathIterator.GetLaterNode ();			
					if (laterNode != Vector3.zero && laterNode != endPos && !this.movementController.NavigationClient.IsStaticObstacleBetween (currentUnitPos, laterNode)) {
						setTargetNextNode = true;
					}
				}			
				
				if (setTargetNextNode) {			
					if (this.currentNodeTarget == this.endPos || Vector3.Distance (currentUnitPos, this.endPos) < minDistance) {
						movementController.StopMovement ();
						return;
					}		
				
					Vector3 nextNode = pathIterator.GetNextNode (this.currentNodeTarget);
					if (nextNode != currentNodeTarget) {
						prevNodeTarget = this.currentNodeTarget;
						
						this.currentNodeTarget = nextNode;
						prevGoodDistanceToTarget = float.MaxValue;				
					} 
				} else {
					break;
				}
				
				this.CalculateTargetForward ();
				this.currentForward = targetForward;
			}
		}
		
		private Timer staticObstacleCheckTimer = Timer.CreateFrequency (250);
		private Timer angleCheckTimer = Timer.CreateFrequency (60);
		private bool isStaticObstacleBetweenTargetNode = false;
		
		private void DoStep ()
		{	
			bool obstacleAppeared = isStaticObstacleBetweenTargetNode;
			if (staticObstacleCheckTimer.EnoughTimeLeft ()) {
				isStaticObstacleBetweenTargetNode = this.movementController.NavigationClient.IsStaticObstacleBetween (this.currentUnitPos, this.currentNodeTarget);			
			}
			
			obstacleAppeared = !obstacleAppeared && isStaticObstacleBetweenTargetNode;	
			
			if (!isStaticObstacleBetweenTargetNode || 
				angleCheckTimer.EnoughTimeLeft ()) {
				this.currentForward = targetForward;				
			}
			 
			if (obstacleAppeared && prevNodeTarget != Vector3.zero) {
				this.currentForward = this.prevNodeTarget - currentUnitPos;
			}
			
			bool wasStep = false;
			if (isStaticObstacleBetweenTargetNode) {
				wasStep = TryStepForward ();
			} else {
				wasStep = TryStepSmooth ();
			}			
			
			if (!wasStep) {				
				DoFirstPossibleStep ();				
			} 
		}
		
//		private void CheckAngle ()
//		{
//			if (isStaticObstacleBetweenTargetNode) {
//				return;
//			}
//			
//			float angle = Vector3.Angle (currentForward, targetForward);	
//			
//			if (angle > 90) {
//				this.RevertAngle ();			
//			}
//		}
		
		int canStop = STEPS_STRAIGHT_BEFORE_ROTATE;
		
		private bool TryStepForward ()
		{
			bool stepPossible = false;
			stepPossible = CanStep (currentForward);			
			
			if (stepPossible) {
				Step (currentForward);
				canStop = STEPS_STRAIGHT_BEFORE_ROTATE;
				return true;
			} else {
				if (canStop > 0) {
					canStop--;
					return true;
				}
				return false;
			}
		}
		
		private bool TryStepSmooth ()
		{		
			Vector3 bestForward = Vector3.zero;
			Vector3 tempForward = currentForward;
			bool stepPossible = false;
			int numIter = 0;
			while (true) {
				numIter++;
				if (MAX_NUM_ITERATION < numIter) {
					Debug.LogError ("Too much iteration trying step smooth : movement motor");
					break;
				}
	
				stepPossible = CanStep (tempForward);
				if (stepPossible) {
					bestForward = tempForward;
				
					if (tempForward != targetForward) {
						float angle = Vector3.Angle (tempForward, targetForward);
						if (angle < Math.Abs (angleStep)) {
							tempForward = targetForward;	
							break;
						} else {
							tempForward = Quaternion.Euler (0, -angleStep, 0) * tempForward;
						}
					} else {
						break;
					}		
				} else {
					break;
				}	
			}
		
			if (bestForward != Vector3.zero) {
				Step (bestForward);
				currentForward = bestForward;
				return true;
			} else {
				return false;
			}
		}
		
		private void DoFirstPossibleStep ()
		{
			bool canStep = false;
			int numIter = 0;
			bool isFwd1StaticObstacle = false;
			Vector3 forward1 = Quaternion.Euler (0, angleStep, 0) * currentForward;
			
			bool isFwd2StaticObstacle = false;
			Vector3 forward2 = Quaternion.Euler (0, -angleStep, 0) * currentForward;
			while (!canStep) {
				numIter++;
				if (Mathf.Abs (180 / angleStep) < numIter) {					
					if (angleStep > 5) {
						angleStep /= 2;
					}
//					Debug.Log (this.currentUnitPos + " - Too much iterations in first possible step : mov motor");
					return;
				}				
				
				isFwd1StaticObstacle = IsStaticObstacle (forward1);
				canStep = !isFwd1StaticObstacle && !IsDynamicObstacle (forward1);
				if (!canStep) {
					forward1 = Quaternion.Euler (0, angleStep, 0) * forward1;
				} else {					
					currentForward = forward1;					
					break;
				}					
				
							
				if ((isStaticObstacleBetweenTargetNode || isFwd1StaticObstacle)) {
					isFwd2StaticObstacle = IsStaticObstacle (forward2);
					canStep = !isFwd2StaticObstacle && !IsDynamicObstacle (forward2);
					if (!canStep) {
						forward2 = Quaternion.Euler (0, -angleStep, 0) * forward2;
					} else {
						currentForward = forward2;					
						break;
					}									
				}				
			}
			
			if (angleStep > 0) {
				angleStep = NORMAL_ANGLE_STEP;
			} else {
				angleStep = -NORMAL_ANGLE_STEP;
			}
			
			
			if (isFwd1StaticObstacle) {
				this.RevertAngle ();
			}
		
			Step (currentForward);		
		}
		
		private void Step (Vector3 forward)
		{
			if (forward == Vector3.zero) {
				return;
			}
			
			float mult = SpeedForStep * MovementController.DELTA_TIME;
//			distanceToTarget = Vector3.Distance (currentUnitPos, this.endPos);
			if (distanceToTarget < mult * 2) {
				mult /= 3;
			}
			if (distanceToTarget < mult) {
				movementController.DesiredUnitPos = this.endPos;
			} else {
				movementController.DesiredUnitPos = currentUnitPos + forward.normalized * mult;	
			}	
			movementController.DesiredRotation = Quaternion.LookRotation (forward.normalized);			
		}
		
		public bool CanStep (Vector3 fwd)
		{
			if (IsDynamicObstacle (fwd)) {
				return false;
			}
			
			if (IsStaticObstacle (fwd)) {
				return false;
			}
			return true;
		}
				
		private bool IsStaticObstacle (Vector3 fwd)
		{
//			float MIN = 10f;					
			if (movementController.NavigationClient.IsStaticObstacleForward (currentUnitPos, fwd.normalized * STEP_DIST)) {
				return true;
			}
			return false;
		}
		
		private bool IsDynamicObstacle (Vector3 fwd)
		{
			if (movementController.NavigationClient.IsDynamicObstacle (currentUnitPos, fwd, STEP_DIST, unit.Unit)) {
				return true;
			}
			
			return false;
		}
		
		private Timer stopMovementAnywayTimer = Timer.CreateFrequency (5000);
	
		private bool CheckForStopMovement ()
		{	
			if (!isMoving) {
				return true;
			}
			
			if (this.CheckTimeStop ()) {
				return true;
			}	
			
			if (Vector3.Distance (currentUnitPos, this.endPos) < MIN_DISTANCE_TO_MOVING_TARGET * 2) {
				prevGoodDistanceToTarget = distanceToTarget;
			}	

			if (prevGoodDistanceToTarget != float.MaxValue && prevGoodDistanceToTarget > distanceToTarget && this.currentNodeTarget == this.endPos) {
				movementController.StopMovement ();
				return true;
			}
		
			return false;
		}
		
		private bool CheckTimeStop ()
		{
			if (unit.IsInFightingState || unit.IsCastingSpell) {
				return false;
			}
			
			if (!stopMovementTimer.IsActive && Vector3.Distance (endPos, currentUnitPos) < 10f) {
				stopMovementTimer.Restart ();		
			}
			
			if (stopMovementTimer.EnoughTimeLeft ()) {
				movementController.StopMovement ();
				return true;
			}
			
			return false;
		}
		
		internal void CalculateTargetForward ()
		{
			distanceToTarget = Vector3.Distance (currentUnitPos, this.currentNodeTarget);
			
			this.targetForward = this.currentNodeTarget - this.currentUnitPos;
		}
		
		internal void OnMovementHasEnded ()
		{
			this.stopMovementTimer.Deactivate ();
			isMoving = false;
		}

		public Vector3 currentUnitPos {
			get {
				return this.movementController.DesiredUnitPos;
			}
		}

		public Vector3 endPos {
			get {
//				if (this.movementController.path.EndPos == Vector3.zero) {
//					return this.currentUnitPos;
//				}
				return this.movementController.endPos;
			}
		}
		
		public TroopDemeanor unit {
			get {
				return this.movementController.unit;
			}
		}

		private void RevertAngle ()
		{
			angleStep *= -1;
		}

		public bool IsMoving {
			get {
				return this.isMoving;
			}
		}
		
		private float SpeedForStep {
			get {
				float minSpeed;
				float offset;
				if ((canStop == STEPS_STRAIGHT_BEFORE_ROTATE || !isStaticObstacleBetweenTargetNode) && Randomizer.Next (100) > 20) {
					offset = SPEED_MAX - SPEED_NORMAL;
					minSpeed = SPEED_NORMAL;
				} else {
					offset = SPEED_MAX - SPEED_LITTLE;
					minSpeed = SPEED_LITTLE;
				}
				float stepModif = offset * (Randomizer.Next (100)) / 100;
				return minSpeed + stepModif;
			}
		}

		public UnityEngine.Vector3 CurrentNodeTarget {
			get {
				return this.currentNodeTarget;
			}
		}

		public float DistanceToTarget {
			get {
				return this.distanceToTarget;
			}
		}
	}
}



