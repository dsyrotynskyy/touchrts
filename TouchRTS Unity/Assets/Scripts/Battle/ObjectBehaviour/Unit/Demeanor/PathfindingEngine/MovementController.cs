using System;
using System.Collections.Generic;
using UnityEngine;
using Setup;

namespace MovementEngine
{
	public class MovementController : IMovementController
	{
		private Vector3 desiredMovementCoord;
		private MovementMotor rayMovementMotor;
		private NavigationEngineClient navigationClient;

		internal TroopDemeanor unit;
		internal MovementPath path = new MovementPath ();
		internal Vector3 endPos;
		private Quaternion desiredRotation;
		public static float DELTA_TIME;
		private NavigationEngine NavigationEngine;
	
		public MovementController ()
		{			
			this.NavigationEngine = WorldController.Instance.NavigationEngine;
			this.rayMovementMotor = new MovementMotor (this);
		}

		#region MovementController implementation
	
		public void Init (TroopDemeanor unit)
		{			
			this.unit = unit;
			desiredUnitPos = unit.Unit.transform.position;
			endPos = desiredUnitPos;
			desiredMovementCoord = desiredUnitPos;
			currentUnitPos = desiredUnitPos;
			this.navigationClient = new NavigationEngineClient (unit, desiredUnitPos);		
		}
		
		private void DoRotationIfNeed () {
			if (!rayMovementMotor.IsMoving) {
				return;
			}
			
			float angle = Quaternion.Angle (unit.UnitsTransform.rotation, desiredRotation);
			if (angle <= 0f) {
				return;
			}
			float lerpValue = 0f;
			if (angle > 15) {
				lerpValue = 0.2f;
			} else {
				lerpValue = 0.15f;
			}
			
			this.unit.UnitsTransform.rotation = Quaternion.Slerp (unit.UnitsTransform.rotation, desiredRotation, lerpValue);
		}
	
		public void DisplayPath ()
		{				
			if (path == null) {
				return;
			}
			path.DrawPath ();	
			
			Debug.DrawLine (rayMovementMotor.currentUnitPos, rayMovementMotor.CurrentNodeTarget);
	
			return;
		}
		
		public Vector3 GetForwardToMovementTarget ()
		{
			return this.path.GetForwardToMovementTarget ();
		}

		private void MoveTransform (Vector3 newPosition)
		{
			if (unit.UnitsTransform.position == newPosition) {
				return;
			}
			unit.PlayRunAnimation ();
			newPosition.Set (newPosition.x, NavigationEngine.TerrainNormalHeight, newPosition.z);
			unit.UnitsTransform.position = Vector3.Slerp(unit.UnitsTransform.position, newPosition, 0.7f);		
		}		

		public Quaternion DesiredRotation 
		{
			set {desiredRotation = value;}
		}
		
		public void OnFixedUpdate ()
		{	
			DELTA_TIME = TWorldTime.deltaTime;
			if (this.rayMovementMotor.IsMoving) {
				this.MoveTransform (desiredUnitPos);
			} else {
				if (unit.IsPeace) {
					unit.Unit.PlayIdleAnimation ();
				}
			}
			
			if (doStopMovement) {
				this.unit.OnMovementHasEnded ();
				doStopMovement = false;
			}
			
			currentUnitPos = unit.UnitsTransform.position;
			
			this.DoRotationIfNeed ();	
			if (Vector3.Distance ( desiredMovementCoord , unit.MovementCoord) > 0.2f) {
				this.desiredMovementCoord = unit.MovementCoord;
			}
			this.DisplayPath ();
		}
		#endregion
		
		#region InterfaceWithAsync
		public Vector3 currentUnitPos;
		private Vector3 desiredUnitPos;
		private bool doStopMovement = false;
		
		public Vector3 DesiredUnitPos {
			get {
				return desiredUnitPos;
			}
			set {
				desiredUnitPos = value;
			}			
		}
		
		public Vector3 CurrentUnitPos {
			get {
				return currentUnitPos;	
			}
		}
		
		public Vector3 GetPathDestination ()
		{
			return this.path.EndPos;
		}
		
		#endregion

		#region Async				
		public void UpdateAsync () {
			CalculatePath ();
			
			if (rayMovementMotor.IsMoving) {
				this.rayMovementMotor.DoMovement ();
			}
		}
		
		public void StopMovement ()
		{
			this.path.Clear ();			
			this.rayMovementMotor.OnMovementHasEnded ();
			this.doStopMovement = true;
		}
		
		public void CalculatePath ()
		{					
			if (Vector3.Distance (endPos, desiredMovementCoord) < 0.1f) {
				this.CheckRecalculate ();
				return;
			}
			
			if (unit.IsInteractingTarget) {
				float distance = Vector3.Distance (desiredMovementCoord, this.endPos);
				if (distance < 0.1f) {
					return;
				}
			}
			
			this.endPos = desiredMovementCoord;
			this.CalculateShortestPath ();	
			this.path.CalculateForward ();			
		}
		
		private Timer recalculateTimer = Timer.CreateFrequency (2000);
		
		private Vector3 prevCoord = Vector3.zero;
		private float prevDistanceToTarget;
		
		private void CheckRecalculate () {		
			if (recalculateTimer.EnoughTimeLeft ()) {							
//				if (Vector3.Distance (prevCoord, this.currentUnitPos) < 4f) {
					prevDistanceToTarget = rayMovementMotor.DistanceToTarget;
					this.endPos = desiredMovementCoord;
					this.CalculateShortestPath ();							
//				}
				prevDistanceToTarget = rayMovementMotor.DistanceToTarget;
				prevCoord = this.currentUnitPos;	
				prevDistanceToTarget = rayMovementMotor.DistanceToTarget + 1f;
				return;
			}		
		}
		
		private void CalculateShortestPath ()
		{
//			if (CanCreatePathFromHeroPath ()) {
//				UnitSubject heroUnit = unit.UnitGroup.GroupHero.HeroesUnit;
//				TroopDemeanor demeanor = (TroopDemeanor)heroUnit.UnitDemeanorController;
//				MovementController heroMovement = (MovementController)demeanor.UnitMovementController;
//				this.path.BuildFrom (heroMovement.path, currentUnitPos, this.endPos);
//			}  else {
//				navigationClient.CalculatePath (this.path, this.currentUnitPos, this.endPos);					
//			}		
//			
			NavigationTriangle startTriangle = null;
			if (!unit.IsHero && unit.UnitGroup.IsHeroGroup) {
				startTriangle = NavigationTriangle.CalculateClosestWorldPathTriangle (WorldController.Instance, unit.UnitGroup.GroupMainUnit.TransformPosition);
			}
			navigationClient.CalculatePath (this.path, this.currentUnitPos, this.endPos, startTriangle);					

//			MovementPathSeeker.CalculateShortestPath (this.path, this.currentUnitPos, this.endPos);					
			this.rayMovementMotor.OnNewMovementStarted ();						
		}
		
		private bool CanCreatePathFromHeroPath () {
			bool isGoodSituation = unit.IsFollowingPlayerCommand && !unit.IsHero && unit.UnitGroup.IsHeroGroup;
			if (!isGoodSituation) {
				return false;
			}
			
			UnitSubject heroUnit = unit.UnitGroup.GroupHero.HeroesUnit;
			if (NavigationEngine.IsStaticObstacleBetween (unit.Unit.TransformPosition, heroUnit.TransformPosition)) {
				return false;
			}
			
			if (Vector3.Distance (unit.Unit.TransformPosition, heroUnit.TransformPosition) > 25f) {
				return false;
			}
			
			return true;
		}	
		
//		internal void SetMovementPathFinished ()
//		{						
//			this.path.Clear ();
//
//			rayMovementMotor.OnMovementHasEnded ();
//		}
		#endregion	

		#region IMovementController implementation

		public void OnUpdate ()
		{
		}
		#endregion

		public NavigationEngineClient NavigationClient {
			get {
				return this.navigationClient;
			}
		}
	}
}
