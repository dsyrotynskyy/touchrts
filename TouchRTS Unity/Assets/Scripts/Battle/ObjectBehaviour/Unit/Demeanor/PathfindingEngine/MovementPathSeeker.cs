using System;
using System.Collections.Generic;
using UnityEngine;

namespace MovementEngine
{
	class MovementPathSeeker
	{	
		public const int STEP_ANGLE = 20;
		public const float MOVEMENT_STEP = 5f;
		private static int MAX_NUM_ITERATION = 150;
		private static int MAX_NUM_SHORT_ITERATION = 150;
		static MovementPath firstPath;
		static MovementPath secondPath;
	
		static MovementPathSeeker ()
		{
			firstPath = new MovementPath ();
			secondPath = new MovementPath ();
		}
		
		//Calculates and adds to path data new path segment
		public static void AddNewPathSegment (MovementPath pathData, Vector3 segmentStart, Vector3 segmentEnd)
		{
			firstPath.Clear ();
			secondPath.Clear ();
			
			CalculatePathWithPathwalker (firstPath, secondPath, segmentStart, segmentEnd, STEP_ANGLE);
		
			MovementPath finishedPath;
			if (firstPath.IsFinished) {
				finishedPath = firstPath;
			} else if (secondPath.IsFinished) {
				finishedPath = secondPath;
			} else {
				return;
			}
			
			pathData.AddNodesFromPath (finishedPath);
		}
		
//		private static void CalculatePath2 (MovementPath path1, MovementPath path2, Vector3 pos1, Vector3 endPos, int angleStep)
//		{
//			path1.BuildFromTriangles (pos1, endPos);
//		}
		
		public static void CalculateShortestPath (MovementPath sourcePath, Vector3 pos1, Vector3 endPos)
		{
			if (WorldController.Instance.NavigationEngine.IsCoordProhibited (endPos)) {
				return;
			}
			
			firstPath.Clear ();
			secondPath.Clear ();
		
			CalculatePathWithPathwalker (firstPath, secondPath, pos1, endPos, STEP_ANGLE);
		
			MovementPath finishedPath;
			if (firstPath.IsFinished) {
				finishedPath = firstPath;
			} else if (secondPath.IsFinished) {
				finishedPath = secondPath;
			} else {
				return;
			}
		
			finishedPath.Optimize ();
			sourcePath.CloneFrom (finishedPath);
		}
	
		private static void CalculatePathWithPathwalker (MovementPath path1, MovementPath path2, Vector3 pos1, Vector3 endPos, int angleStep)
		{
			MovementPath.PathWalker pathWalker1 = path1.CreatePathWalker (pos1, endPos);
			MovementPath.PathWalker pathWalker2 = path2.CreatePathWalker (pos1, endPos);
			int numIter = 0;			
			while (true) {
				numIter++;
				if (MAX_NUM_ITERATION < numIter) {
					Debug.LogError ("Too much pathseeker steps");
					return;
				}
				if (TryPath (pathWalker1, -angleStep)) {
					return;
				}
				if (TryPath (pathWalker2, angleStep)) {
					return;
				}
			}
		}
	
		private static bool TryPath (MovementPath.PathWalker pathWalker, int angleStep)
		{	
			if (WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (pathWalker.CurrentPos, pathWalker.EndPos)) {
				if (!TryStepSmooth (pathWalker, angleStep)) {
					DoFirstPossibleStep (pathWalker, angleStep);
				} 			
				return false;
			}
			pathWalker.DoNextStep (pathWalker.EndPos, pathWalker.CurrentForward);
			pathWalker.SetFinished ();
			
			return true;
		}

		/*
	 * Rotate pathWalker to the movement target;
	 * Trying to find the closest angle
	 * */
		private static bool TryStepSmooth (MovementPath.PathWalker pathWalker, int angleStep)
		{		
			Vector3 bestPos = Vector3.zero;
			Vector3 bestForward = Vector3.zero;
			Vector3 curForward = pathWalker.CurrentForward;
			
			int numIter = 0;
			while (true) {
				numIter++;
				if (MAX_NUM_SHORT_ITERATION < numIter) {
					Debug.LogError ("Too much steps in smooth error : movement path seeker");
					break;
				}
				Vector3 nextPos = GetNextPosition (pathWalker.CurrentPos, curForward);
				if (nextPos != Vector3.zero) {
					bestPos = nextPos;
					bestForward = curForward;
				
					if (curForward != pathWalker.TargetForward) {
						float angle = Vector3.Angle (curForward, pathWalker.TargetForward);
						if (angle < Math.Abs(angleStep)) {
							curForward.Set (pathWalker.TargetForward.x, pathWalker.TargetForward.y, pathWalker.TargetForward.z);						
						} else {
							curForward = Quaternion.Euler (0, angleStep, 0) * curForward;
						}
					} else {
//						Vector3 movementPos = bestPos; 
//						float val = 1f;
//						do {
//							movementPos = Vector3.Lerp (bestPos, pathWalker.EndPos, val);
//							val /= 2f;
//						} while (WorldController.Instance.NavigationEngine.IsStaticObstacleBetween 
//							(movementPos, nextPos) && val > 0.1f);
//						bestPos = movementPos;
						break;
					}		
				} else {
					break;
				}	
			}
		
			if (bestPos != Vector3.zero) {
				pathWalker.DoNextStep (bestPos, bestForward);
				return true;
			} else {
				return false;
			}
		}
	
		/**
	 * Rotated walker to the first movable position
	 * */
		private static void DoFirstPossibleStep (MovementPath.PathWalker pathWalker, int angleStep)
		{
			Vector3 curForward = pathWalker.CurrentForward;
			Vector3 nextPos = Vector3.zero;
			int numIter = 0;
			while (nextPos == Vector3.zero) {
				numIter++;
				if (MAX_NUM_SHORT_ITERATION < numIter) {
					Debug.LogError ("Too much iteration: do first possible step in PATH SEEKER");
					return;
				}
				Vector3 possiblePos = GetNextPosition (pathWalker.CurrentPos, curForward);
				if (possiblePos == Vector3.zero) {
					curForward = Quaternion.Euler (0, -angleStep, 0) * curForward;
				} else {
					nextPos = possiblePos;
					break;
				}
			}
		
			curForward = Quaternion.Euler (0, -angleStep, 0) * curForward;
			pathWalker.DoNextStep (pathWalker.CurrentPos, curForward);
		}

		public static Vector3 GetNextPosition (Vector3 currentPosition, Vector3 originalForward, float step = MOVEMENT_STEP)
		{		
			Vector3 nextPos = currentPosition + step * originalForward.normalized;
				
			if (WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (currentPosition, nextPos)) {
				return Vector3.zero;
			}	
			return nextPos;
		}
	}
}