using System;
using UnityEngine;
using Entity;
using System.Collections.Generic;
using MovementEngine;

public class TroopDemeanor : AbstractUnitDemeanor
{
	public TroopDemeanor ()
	{
	}
	
//	private int MAX_NUM_ITERATION = 30;
	private Quaternion desiredRotationQuaternion;
	private bool startedInteractingTarget = false;
	private UnitActivityType unitActivity = UnitActivityType.Peace;
	private IMovementController movementController;
	private UnitSubject targetUnit;
	private UnitInteractionPerformer currentInteractionPerformer = null;
	private Timer checkDangerTimer = Timer.CreateFrequency (100);
	
	public override void Init (UnitSubject unit)
	{
		base.Init (unit);
		
		this.movementController = new MovementController ();
		this.movementController.Init (this);
	}
	
	public override void OnFixedUpdate ()
	{
		if (!this.IsCastingSpell) {
			this.movementController.OnFixedUpdate ();
		}
	}

	public override void OnAsyncUpdate ()
	{
		if (behaveTimer.EnoughTimeLeft ()) {
			BehaveHumanoidUnit ();	
		}
	}
	
	private void BehaveHumanoidUnit ()
	{				
		if (unit.IsDead && !unit.IsHero) {
			return;
		}
		
		if (UnitActivity != UnitActivityType.FollowingPlayerCommand && !this.CheckForDistanceToCenter ()) {	
			this.CheckForTargets (false);
		}
		
		switch (UnitActivity) {
		case UnitActivityType.FollowingPlayerCommand:	
			this.CheckGarrisonMovement ();
			this.CheckDangerMovement ();
			return;
		case UnitActivityType.InteractingTarget:
			ContinueInteractingTargetUnit ();
			return;
		case UnitActivityType.Peace:
			PerformRandomCommand ();
			CheckForTargets ();
			return;			
		}
	}
	
	public override void OnEnemiesDissapear ()
	{
//		checkForTargetsTimer.ForceTime ();
//		tryInteractBestTargetTimer.ForceTime ();
//		if (unitActivity != UnitActivityType.FollowingPlayerCommand && !this.CheckForDistanceToCenter ()) {	
//			if (!this.CheckForTargets (false)) {
//				UnitReturnToGroup ();
//			};
//		}
	}
		
	private void CheckDangerMovement ()
	{
		if (!this.unitType.IsArcher || !checkDangerTimer.EnoughTimeLeft () || !this.UnitGroup.AreEnemiesAround ()) {
			return;
		}
		
		LinkedList<UnitSubject> units = unit.UnitGroup.ClosestEnemies;
		
		foreach (UnitSubject victimUnit in units) {
			if (victimUnit.IsDead || victimUnit.IsBuilding) {
				continue;
			}
			
			float distance = Vector3.Distance (victimUnit.TransformPosition, movementCoord);	
			if (distance < 10f) {
				this.SetPeace ();
				return;
			}
		}
	}
	
	private void CheckGarrisonMovement ()
	{
		if (!UnitGroup.IsHeroGroup && this.UnitActivity == UnitActivityType.FollowingPlayerCommand) {
			if (unitRandomMovementTimer.EnoughTimeLeft ()) {
				SetPeace ();
			}
		}
	}
	
	private void ProcessDoingRandomCommand ()
	{
		ProcessFollowingTarget ();
		CheckForTargets (false);
	}
	
	private bool CheckForDistanceToCenter ()
	{
		if (this.UnitActivity != UnitActivityType.FollowingPlayerCommand) {
			int distance = (int)Vector3.Distance (TransformPosition, unit.UnitGroup.GroupMainUnit.TransformPosition);
			
			int minDistanceToCenter = UnitGroupSubject.GROUP_INTERACTION_DISTANCE;
			if (UnitActivity == UnitActivityType.Peace) {
				minDistanceToCenter = UnitGroupSubject.GROUP_INTERACTION_DISTANCE / 3;
			}
			
			if (distance > minDistanceToCenter) {	
				this.UnitReturnToGroup ();
				return true;
			}
		}
		return false;
	}
	
	private void UnitReturnToGroup ()
	{
		UnitActivityType type = UnitActivityType.FollowingPlayerCommand;
				
		if (UnitGroup.IsHeroGroup) {
			type = UnitActivityType.FollowingPlayerCommand;
		} 				
		this.MoveUnitTo (UnitGroup.MovementPosition, 6f, type);
	}
	
	private void PerformRandomCommand ()
	{
		if (IsCastingSpell) {
			return;
		}
		if (performingRotation) {
			if (lerpValue >= 1f) {
				this.SetPeace ();
			}			
			SetRotation (Quaternion.Slerp (transform.rotation, desiredRotationQuaternion, lerpValue));
			lerpValue += 0.01f;
		} 		
	}
	
	public override void MoveUnitTo (Vector3 pathTarget, bool randomize = true)
	{					
		this.UnitActivity = UnitActivityType.FollowingPlayerCommand;
		pathTarget.Set (pathTarget.x, NavigationEngine.TerrainNormalHeight, pathTarget.z);
		this.CreateNewTargetPos (pathTarget, !this.unitType.IsHero && randomize);		
	}
	
	private void MoveUnitTo (Vector3 pathTarget, float range, UnitActivityType activityType)
	{	
		pathTarget.Set (pathTarget.x, NavigationEngine.TerrainNormalHeight, pathTarget.z);
		this.CreateNewTargetPos (pathTarget, !this.unitType.IsHero, range);
		this.UnitActivity = activityType;		
	}
	
	private void CreateNewTargetPos (Vector3 position, bool randomize = true, float range = 2f)
	{	
		if (!randomize) {			
			this.movementCoord = position;		
			return;				
		}
		
		Vector3 temp = new Vector3 ();				
		int numIter = 0;
		bool coordIsProhibited = true;
		do {				
			float nx = position.x;
			float nz = position.z;
			
			nx += Randomizer.Next (2 * range) - range;			
			nz += Randomizer.Next (2 * range) - range;		
			
			temp.Set (nx, NavigationEngine.TerrainNormalHeight, nz);
						
			if (numIter++ > 5) {
				return;
			}	
					
			coordIsProhibited = WorldController.Instance.NavigationEngine.IsCoordProhibited (temp);
		} while (coordIsProhibited);
		this.movementCoord = temp;	
	}
	
	private bool IsCurrentUnitPositionOk ()
	{
		float desiredY = NavigationEngine.TerrainNormalHeight;
		float currentY = this.TransformPosition.y;
		if (Mathf.Abs (desiredY - currentY) > UnitSubject.Y_COORDS_RANGE) {
			return false;
		}		
		return true;
	}
		
	private void ContinueInteractingTargetUnit ()
	{	
		if (this.IsCastingSpell) {
			return;
		}
		if (targetUnit == null || targetUnit.IsDead || this.unit.IsDead) {
			if (!this.CheckForTargets (false)) {
				this.SetPeace ();
			}
			;		
			return;
		}
				
		float distanceToEnemy = Vector3.Distance (targetUnit.TransformPosition, TransformPosition);	
		float attackDistance = unitType.attackDistance;
		if (startedInteractingTarget) {
			attackDistance *= 1.25f;
		}
		if (distanceToEnemy > attackDistance) {
			startedInteractingTarget = false;
			this.ProcessFollowingTarget ();
		} else {
			this.ProcessAttackingEnemy ();
		}
	}
	
	private void ProcessAttackingEnemy ()
	{
		this.UnitMovementController.StopMovement ();
		if (this.IsFollowingPlayerCommand) {
			return;
		}
		this.movementCoord = this.unit.TransformPosition;
			
		Vector3 look = targetUnit.TransformPosition - this.unit.TransformPosition;
		
		startedInteractingTarget = true; //to fix attack animation offset
		Quaternion targetRotation = new Quaternion ();
		if (look != Vector3.zero) {
			targetRotation = Quaternion.LookRotation (targetUnit.TransformPosition - TransformPosition);  			
		}							
		Loom.QueueOnMainThread (() => {		
			if (look != Vector3.zero) {
				this.SetRotation (Quaternion.Lerp (transform.rotation, targetRotation, 0.1f));				
				transform.rotation.Set (transform.rotation.x, transform.rotation.y, 90f, transform.rotation.w);				
			}	
			unit.PlayAttackAnimation ();	
			if (atackTimer.EnoughTimeLeft ()) {	
				PerformInterractingTarget (currentInteractionPerformer, targetUnit);
			}
		});										
	}
	
	public override void OnUnitAttacked ()
	{
		if (this.IsCastingSpell) {
			return;
		}
		
		if (UnitActivity == UnitActivityType.FollowingPlayerCommand) {
			return;
		}
		
		if (targetUnit != null && !targetUnit.IsDead) {
			float distance = Vector3.Distance (targetUnit.TransformPosition, unit.TransformPosition);	
			if (distance <= unitType.attackDistance) {
				return;	
			}
		}
		this.TryInteractBestTarget ();
	}
	
	private Timer tryInteractBestTargetTimer = Timer.CreateFrequency (500);
	
	private bool TryInteractBestTarget ()
	{
		if (targetUnit != null && !tryInteractBestTargetTimer.EnoughTimeLeft ()) {
			return true;
		}
		foreach (AbilityType ability in unit.UnitType.Abilities) {
			UnitSubject target = GetBestTarget (ability);
			if (target != null) {
				this.currentInteractionPerformer = ability;
				return StartInteractingTarget (target);
			}
		}	
		this.currentInteractionPerformer = this.unit;
		UnitSubject bestTarget = GetBestTarget (this.unit);
		
		return StartInteractingTarget (bestTarget);
	}
	
	private bool StartInteractingTarget (UnitSubject unit)
	{
		if (targetUnit != null && targetUnit == unit) {
			return true;
		}
		targetUnit = unit;
		
		if (targetUnit == null) {
			return false;
		}
		
		this.CreateNewTargetPos (targetUnit.TransformPosition, false);
		this.UnitActivity = UnitActivityType.InteractingTarget;
		return true;
	}
	
	private void ProcessFollowingTarget ()
	{
		if (targetUnit == null) {
			return;
		}
		
		float distanceFromEnemyToMovingCoord = Vector3.Distance (targetUnit.TransformPosition, this.movementCoord);
		float distanceFromUsToEnemy = Vector3.Distance (unit.TransformPosition, targetUnit.TransformPosition);
		if (distanceFromEnemyToMovingCoord > 5f || distanceFromUsToEnemy < 10f) {			
			if (!WorldController.Instance.NavigationEngine.IsStaticObstacleBetween (targetUnit.TransformPosition, this.unit.TransformPosition)) {
				this.CreateNewTargetPos (targetUnit.TransformPosition, false);
			} else if (this.movementCoord != targetUnit.MovementPosition) {
				this.CreateNewTargetPos (targetUnit.MovementPosition, false);
			}			
		} 
	}
	
	private Timer checkForTargetsTimer = Timer.CreateRandomizeFrequency (300, 10);
	
	private bool CheckForTargets (bool doMovementOnPease = true)
	{
		if (!checkForTargetsTimer.EnoughTimeLeft ()) {
			return targetUnit != null;
		}
		
		if (unit.IsDead) {
			return false;
		}
		
		if (this.TryInteractBestTarget ()) {
			return targetUnit != null;
		} else {	
			return false;
		}
	}
	
	public override Vector3 MovementCoord {
		get {
			return this.movementCoord;
		}
	}
	
	public override void OnMovementHasEnded ()
	{	
		if (Vector3.Distance (this.movementCoord, unit.TransformPosition) > 2f) {
			return;
		}
		if (UnitActivity == UnitActivityType.InteractingTarget || UnitActivity == UnitActivityType.PerformingSpell || IsPeace) {
			return;
		}
		
		this.movementCoord = unit.TransformPosition;
		this.SetPeace ();
	}
	
	public void PlayRunAnimation ()
	{
		unit.PlayRunAnimation ();
	}
	
	private void SetPeace ()
	{
		targetUnit = null;
		Loom.QueueOnMainThread (() => {
			unit.PlayIdleAnimation ();
		});
		this.UnitActivity = UnitActivityType.Peace;
	}

	public UnityEngine.Transform UnitsTransform {
		get {
			return this.transform;
		}
	}

	public override UnitSubject TargetUnit {
		get {
			return this.targetUnit;
		}
	}
	
	public bool IsAttackingTower {
		get {
			if (targetUnit == null || targetUnit.UnitType == null) {			
				return false;
			}
			return this.targetUnit.UnitType.IsTower;
		}
	}
	
	public bool IsFollowingPlayerCommand {
		get { return this.UnitActivity == UnitActivityType.FollowingPlayerCommand;}		
	}
	
	public override bool IsFree ()
	{
		return this.UnitActivity == UnitActivityType.Peace;
	}
		
	public bool IsHero {
		get { return this.unit.IsHero;}		
	}
	
	public bool IsInteractingTarget {
		get { return this.UnitActivity == UnitActivityType.InteractingTarget;}		
	}
	
	public bool IsCastingSpell {
		get { 
			if (this.unit.IsHero && this.unit.HeroData.IsSpawningSpell) {
				return true;
			}
			return false;
		}		
	}
	
	public UnitSubject Unit {
		get {
			return this.unit;
		}
	}

	public override void OnRotationChanged (Vector3 point)
	{
		point.Set (point.x, NavigationEngine.TerrainNormalHeight, point.z);
		Vector3 lookDirection;
		lookDirection = point - this.unit.transform.position;
		desiredRotationQuaternion = Quaternion.LookRotation (lookDirection);
//		float angle = Quaternion.Angle (transform.rotation, desiredRotationQuaternion);
//		float lerpValue = 0;
//		if (angle > 30) {
//			lerpValue = 0.7f;
//		} else {
//			lerpValue = 0.2f;
//		}
		SetRotation (Quaternion.Slerp (transform.rotation, desiredRotationQuaternion, 1f));
//		MovementConroller.DesiredRotation = desiredRotationQuaternion;
	}
	
	private void SetRotation (Quaternion quaternion)
	{
		this.transform.rotation = quaternion;
	}

	public override void OnStartedSpellCasting ()
	{
		if (this.UnitActivity == UnitActivityType.InteractingTarget) {
			this.UnitMovementController.StopMovement ();
			targetUnit = null;
		}
		this.UnitActivity = UnitActivityType.PerformingSpell;
	}

	public override void OnUnitEndedSpellCasting ()
	{
		this.SetPeace ();
	}

	public IMovementController UnitMovementController {
		get {
			return this.movementController;
		}
	}

	public override void InteractBestTarget ()
	{
		this.CheckForTargets ();
	}

	public override Vector3 GetForwardToMovementTarget ()
	{
		return UnitMovementController.GetForwardToMovementTarget ();
	}
	
	public Collider UnitBody {
		get { return unit.UnitBody;}
	}

	public override void CastSkill (AbilityType skill)
	{
		UnitSubject target = GetRandomTarget (skill);
		if (target == null) {
			return;
		}
		
		Loom.QueueOnMainThread (() => {
			this.PerformInterractingTarget (skill, target);
		});
	}
	
	//used only if is Network Server and Troop Unit
	public override char CreateStateCharForClient ()
	{
		switch (UnitActivity) {
		case UnitActivityType.FollowingPlayerCommand:			
			return FollowingPlayerCommandChar;
		case UnitActivityType.InteractingTarget:	
			return InteractingTargetChar;
		case UnitActivityType.PerformingSpell:			
			return PerformingSpellChar;
		case UnitActivityType.Peace:			
			return PeaceChar;			
		}
		return PeaceChar;
	}
	
	private UnitActivityType UnitActivity {
		get {
			return this.unitActivity;
		}
		set {			
			this.unitActivity = value;
			if (WorldController.Instance.IsNetworkGame) {
				this.Unit.OnServerUnitStateChanged (this.CreateStateCharForClient ());
			}
		}
	}
	
	public override bool IsInFightingState {
		get {
			return this.targetUnit != null && Vector3.SqrMagnitude (this.unit.TransformPosition - this.targetUnit.TransformPosition) < 25f;
		}
	}
	
	public bool IsPeace {
		get {
			return this.unitActivity == AbstractUnitDemeanor.UnitActivityType.Peace;
		}
	}
	
//	public override UnitSubject TargetUnit {
//		get {
//			return this.targetUnit;
//		}
//	}
	
//	public override void UpdatePathSeeking ()
//	{
//		if (!this.IsCastingSpell) {
//			this.movementController.UpdatePathSeeking ();
//		}
//	}

//	public override void UpdatePathPassing ()
//	{
//		if (!this.IsCastingSpell) {
//			this.movementController.UpdatePathPassing ();
//		}
//	}
	
	public override IMovementController MovementConroller {
		get { return this.movementController;}
	}
	
	public Vector3 TransformPosition {
		get { return unit.TransformPosition;}
	}
}


