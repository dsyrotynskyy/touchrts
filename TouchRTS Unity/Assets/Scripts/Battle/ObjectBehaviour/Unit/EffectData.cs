using System;

namespace Entity
{
	public class EffectData
	{
		private int id;
		private EffectType effectType;
		private UnitSubject unitToEffect;
		private EffectPerformer effectPerformer;
		
		private Timer effectTimer;
		
		public void Init (EffectType type, EffectPerformer effectPerformer, UnitSubject unit)
		{
			this.effectType = type;
			this.unitToEffect = unit;
			this.effectPerformer = effectPerformer;
			effectTimer = Timer.CreateFrequencyWithLife(effectType.Frequency, effectPerformer.TimeOfEffectActivity);
		}
		
		public EffectData (int id)
		{
			this.id = id;
		}
		
		public void DoEffect ()
		{			
			if (effectTimer.EnoughTimeLeft()) {
				ProcessEffect ();
			}		
		}
		
		private void ProcessEffect ()
		{
			if (effectType.InteractionType == EffectType.INTERACTION_TYPE_OFFENSE_LIFE) {
				CommandController.Instance.AffectUnitLife (this.unitToEffect, this.effectPerformer.EffectPower);
			}			
		}
		
		public bool IsActive 
		{
			get {
				return effectTimer.IsActive;
			}
		}
		
		public EffectType GetEffectType ()
		{
			return this.effectType;
		}
		
		public override int GetHashCode ()
		{
			return this.effectType.Name.Length;
		}
		
		public override bool Equals(Object obj) {
			if (obj == null || GetType() != obj.GetType()) {
				return false;
			}
			EffectData effect = (EffectData)obj;
      		return this.effectType == effect.effectType;
	  	}
		
		public void RenewEffect() {
			this.effectTimer.Restart();
		}
		
		public int GetId() {
			return this.id;
		}

		public EffectPerformer EffectPerformer {
			get {
				return this.effectPerformer;
			}
		}
	}
}

