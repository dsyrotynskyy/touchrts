using System;
using System.Collections;
using System.Collections.Generic;
using Entity;
using UnityEngine;

public class UnitSpawnManager
{
	private UnitSubject unit;
	private Timer taskTimer;
	
	//Unit to spawn	
	private Vector3 point;
	private Queue<KeyValuePair<UnitType, UnitGroupSubject>> unitsForSpawnWithGroups = new Queue<KeyValuePair<UnitType, UnitGroupSubject>> ();
	private Dictionary<UnitType, int> numOfUnitsToSpawn = new Dictionary<UnitType, int> ();
	
	private UnitGroupSubject desiredGroupForUnit;
	private UnitType currentUnitType;
	
	public UnitSpawnManager (UnitSubject unit)
	{
		this.unit = unit;
		taskTimer = Timer.CreateEmpty ();
		taskTimer.Deactivate ();
	}

	public void Process ()
	{		
		if (taskTimer.IsActive || currentUnitType == null) {
			return;
		} 

		this.SpawnUnit ();
	}
	
	private void SpawnUnit ()
	{
		this.OnUnitForSpawnRemovedFromList (currentUnitType);
		
		Vector3 point = unit.transform.position;
		
		point.x -= Randomizer.Next (5f, -4f) + 2f;
		point.z -= Randomizer.Next (4.5f) + 4f + 2f;
		
		UnitGroupSubject usedGroup;	
		if (this.desiredGroupForUnit.IsEnoughCloseForInteraction (unit.UnitGroup)) {
			usedGroup = this.desiredGroupForUnit;
		} else {
			usedGroup = unit.UnitGroup;
		}
		
		if (currentUnitType.IsHero) {
//			ObjectController.CreateHero (currentUnitType.Name, point, usedGroup.PlayerOwner.Id, unit.UnitType.Race.Name);		
		} else {
			CommandController.Instance.CreateUnit (currentUnitType.Name, point, usedGroup, currentUnitType.Race.Name, false);	
		}

		numOfUnitsToSpawn [currentUnitType]--;
		
		if (unitsForSpawnWithGroups.Count > 0) {
			KeyValuePair<UnitType, UnitGroupSubject> pair = unitsForSpawnWithGroups.Dequeue ();
			this.currentUnitType = pair.Key;
			this.desiredGroupForUnit = pair.Value;
		} else {
			this.currentUnitType = null;
			this.desiredGroupForUnit = null;
		}		
		
		if (this.currentUnitType != null) {					
			taskTimer.SetNewLifePeriod (this.currentUnitType.TimeSpawn);
		}
	}

	public void Reset ()
	{
		if (currentUnitType != null) {
			this.OnUnitForSpawnRemovedFromList (currentUnitType);
			currentUnitType = null;
		}
		
		foreach (KeyValuePair<UnitType, UnitGroupSubject> pair in unitsForSpawnWithGroups) {
			UnitType unit = pair.Key;
			OnUnitForSpawnRemovedFromList (unit);
		}
		
		unitsForSpawnWithGroups.Clear ();
	}
	
	private void OnUnitForSpawnAddedToList (UnitType unitType) {
		playerOwner.UseUnitPlaces (unitType.useUnitPlaces);
	}
	
	private void OnUnitForSpawnRemovedFromList (UnitType unitType) {
		playerOwner.FreeUnitPlaces (unitType.useUnitPlaces);		
	}
	
	public float Progress
	{ 	
		get {
			if (!taskTimer.IsActive) {
				return 123;
			}
			return taskTimer.GetProgress ();
		}
	}
	
	public UnitType CurrentUnitType () {
		return currentUnitType;
	}

	public int GetNumUnits (UnitType unitType)
	{
		if (numOfUnitsToSpawn.ContainsKey (unitType)) {
			return numOfUnitsToSpawn [unitType];			
		} else {
			return 0;
		}
	}

	public void SpawnUnit (UnitType unitType)
	{
		SpawnUnit (unitType, unit.UnitGroup);
	}
	
	public void SpawnUnit (UnitType unitType, UnitGroupSubject desiredUnitGroup)
	{	
		this.OnUnitForSpawnAddedToList (unitType);
		
		if (numOfUnitsToSpawn.ContainsKey (unitType)) {
			numOfUnitsToSpawn [unitType]++;			
		} else {
			numOfUnitsToSpawn.Add (unitType, 1);
		}

		if (currentUnitType == null) {
			this.currentUnitType = unitType;
			if (desiredUnitGroup == null) {
				this.desiredGroupForUnit = unit.UnitGroup;
			} else {
				this.desiredGroupForUnit = desiredUnitGroup;
			}			
			taskTimer.SetNewLifePeriod (this.currentUnitType.TimeSpawn);
		} else {
			unitsForSpawnWithGroups.Enqueue (new KeyValuePair<UnitType, UnitGroupSubject> (unitType, desiredUnitGroup));
		}		
	}
	
	public bool IsSpawningUnit () {
		return this.currentUnitType != null;
	}
	
	public bool IsSpawningUnitForOtherUnitGroup () {
		if (desiredGroupForUnit == unit.UnitGroup) {
			return true;
		}
		
		if (this.desiredGroupForUnit.IsEnoughCloseForInteraction (unit.UnitGroup)) {
			return true;
		} else {			
			return false;
		}
	}

	public bool IsSpawningUnitFor (UnitGroupSubject unitGroup)
	{
		foreach (KeyValuePair<UnitType, UnitGroupSubject> pair in unitsForSpawnWithGroups) {
			if (pair.Value == unitGroup) {
				return true;
			}
		}
		return false;		
	}
	
	public PlayerData playerOwner {
		get { return this.unit.PlayerOwner;}
	}
}


