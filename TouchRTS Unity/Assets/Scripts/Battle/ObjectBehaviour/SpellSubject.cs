using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class SpellSubject : TMonoBehaviour, EffectPerformer
{	
	private int MAX_NUM_ITERATION = 30;
	private bool isInited = false;
	public int lifePeriod = 180;

	/*
	 * While spell is casting, its destroying doesn't start
	 * */
	public bool isCastingByPlayer = true;
	private SpellType spellType;
	private PlayerData playerOwner;
	private HeroSubject hero;
	private LinkedList<UnitSubject> victims = new LinkedList<UnitSubject> ();
	private Timer spellTimer;
	private Timer deathTimer;
	private Timer renewEffectTimer;
	private bool startedDestroying = false;
	private string id;
	
	private SpellNetworkSerializer networkSerializer;
	
	public void Init (SpellType spellType, PlayerData playerData, HeroSubject hero, string id)
	{		
		this.id = id;
		this.playerOwner = playerData;
		this.spellType = spellType;	
		this.hero = hero;
		this.hero.CurrentSpell = this;
		lifePeriod = spellType.SpellObjectLifePeriod;
		
		if (spellType.IsCreatingObjectOnMap ()) {
			spellTimer = Timer.CreateLifePeriod (spellType.SpellObjectLifePeriod);
		}
		
		renewEffectTimer = Timer.CreateFrequency (10);
		
		if (spellType.IsMovableSpell () || spellType.IsRaySpell ()) {
			spellTimer = Timer.CreateFrequency (spellType.ManaUseFrequency);
		}
			
		if (!spellType.IsRaySpell ()) {
			Collider collider = gameObject.GetComponent<Collider> ();
			if (collider == null) {
				collider = gameObject.AddComponent<BoxCollider> ();
			}
			if (!(collider is BoxCollider)) {
				Destroy (collider);
				collider = gameObject.AddComponent<BoxCollider> ();
			}  
			BoxCollider boxCollider = (BoxCollider)collider;
			boxCollider.size = new Vector3 (10, 1, 10);
		}		
		
		if (WorldController.Instance.IsNetworkGame) {
			networkSerializer = this.gameObject.AddComponent<SpellNetworkSerializer> ();
			networkSerializer.Init (this);
		}
		
		WorldController.Instance.RegisterSpell (this);
		isCastingByPlayer = true;
		this.isInited = true;
	}
	
	public void OnFixedUpdate ()
	{		
		if (!isInited) {
			return;
		}
		
		if (networkSerializer != null) {
			networkSerializer.OnFixedUpdate ();
		}
		
		if (spellType.IsRaySpell () && !isCastingByPlayer) {	
			ParticleSystem system = this.GetComponent<ParticleSystem> ();
			if (system != null) {
				system.Stop ();
			}			
			StartCoroutine (DestroySpell (1.0f));	
		}
		
		if (spellType.IsCreatingObjectOnMap ()) {
			if (!spellTimer.IsActive) {
				StartCoroutine (DestroySpell (1.0f));
			}
		}
		
		if (!isCastingByPlayer) {
			startedDestroying = true;
		}
		
		if (spellType.IsMovableSpell () && !isCastingByPlayer) {
			this.gameObject.transform.Translate (0f, 1f, 0f);
		}	
		
		if (!WorldController.Instance.IsNetworkGameClient) {
			if (renewEffectTimer.EnoughTimeLeft ()) {
				this.CheckEnemyContained ();
			}
		}
	}
	
	public bool ShouldAffectMana ()
	{
		if (spellType.IsMovableSpell () || spellType.IsRaySpell () && isCastingByPlayer) {
			if (spellTimer.EnoughTimeLeft ()) {
				return true;
			}
		}
		return false;
	}
	
	IEnumerator DestroySpell (float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		WorldController.Instance.RemoveSpell (this);
		ObjectController.Destroy (gameObject);
	}
	
	private void CheckEnemyContained ()
	{
		var node = victims.First;
		int numIter = 0;
			
		while (node != null) {
			numIter++;
			if (MAX_NUM_ITERATION < numIter) {
				return;
			}
			if (node.Value.IsDead || startedDestroying) {
				victims.Remove (node.Value);
			} else if (this.collider.bounds.Contains (node.Value.transform.position)) {
				if (!node.Value.RenewEffect (this)) {
					this.AddEffects (node.Value);
				}
			}
			node = node.Next;
		}		
	}

	public void StopCasting ()
	{
		isCastingByPlayer = false;
	}
	
	public void OnTriggerEnter (Collider other)
	{ 
		string tag = other.gameObject.tag;
		if (tag.Equals ("Unit") || tag.Equals ("Hero")) {
			UnitSubject behaviour = (UnitSubject)other.gameObject.GetComponent (typeof(UnitSubject));
			
			if (this.spellType.IsRaySpell () || this.spellType.effectArea > Vector3.Distance (this.gameObject.transform.position, other.gameObject.transform.position)) {
				this.EffectUnit (behaviour);
			}		
		}		
	}
		
	public void EffectUnit (UnitSubject unit)
	{			
		if (!unit.IsInited || unit.IsDead || unit.UnitType.IsBuilding) {
			return;
		}
		
		victims.AddFirst (unit);
		if (spellType.HasPositiveSingleInteraction ()) {
			if (this.playerOwner.Team == unit.PlayerOwner.Team) {
				CommandController.Instance.AffectUnitLife (unit, this.OffenseLifeSinglePower);
			}		
		} else if (this.playerOwner.Team != unit.PlayerOwner.Team) {
			CommandController.Instance.AffectUnitLife (unit, this.OffenseLifeSinglePower);
		}
		
		this.AddEffects (unit);
	}
	
	private void AddEffects (UnitSubject unit)
	{
		foreach (EffectType effectType in spellType.Effects) {
			if (this.playerOwner.Team == unit.PlayerOwner.Team && effectType.IsPositiveEffect ()) {
				CommandController.Instance.AddEffectToUnit (unit, effectType, this);
			}
				
			if (this.playerOwner.Team != unit.PlayerOwner.Team && !effectType.IsPositiveEffect ()) {
				CommandController.Instance.AddEffectToUnit (unit, effectType, this);
			}
		}
	}

	public HeroSubject Hero {
		get {
			return this.hero;
		}
		set {
			hero = value;
		}
	}

	public PlayerData PlayerOwner {
		get {
			return this.playerOwner;
		}
		set {
			playerOwner = value;
		}
	}

	public SpellType SpellType {
		get {
			return this.spellType;
		}
		set {
			spellType = value;
		}
	}
	
	public int OffenseLifeSinglePower {
		get { 
			return this.spellType.OffenseLifeSingleValue + PlayerOwner.SpellSinglePowerModification;
		}	
	}
	
	#region EffectPerformer implementation
	public int TimeOfEffectActivity {
		get {
			return this.spellType.TimeOfEffectActivity;
		}
	}

	public int EffectPower {
		get {
			return this.spellType.EffectPower + PlayerOwner.SpellEffectPowerModification;
		}
	}

	public bool IsSpell {
		get {
			return true;
		}
	}
	#endregion

	#region EffectPerformer implementation
	public string Id {
		get {			
			return this.id;
		} 
	}
	#endregion
}
