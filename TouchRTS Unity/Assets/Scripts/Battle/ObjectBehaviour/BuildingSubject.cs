using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class BuildingSubject : TMonoBehaviour
{
	public const int GARRISON_SPAWN_PERIOD = 15000;
	public string id;
	public int playerId;
	
	//0 - tower, 1 - farm, 2 - mage guild, 3 - hero building
	public string constructedBuildings;
	private BuildingSubject castleOwner;
	private BuildingType buildingType;
	private UnitSubject buildingUnitSubject;
	private UnitGroupSubject buildingUnitGroup;
	private Timer garrisonSpawnTimer;
	private Timer moneyBonusTimer;
	private bool inited;
	private GameObject miniMapGameObject;
	private CastleConstructor castleConstructor;
	private readonly List<UnitType> unitsToSpawn = new List<UnitType> ();
	private readonly LinkedList<SpellType> spells = new LinkedList<SpellType> ();
	
	private Timer buildingRepairTimer;	
	
	private float captureTimerProgress;
	private float buildingConstructionProgress;
	
	private bool isConstructingBuilding;
	private bool isUpgradingBuilding;
	private Timer buildingConstructionTimer = Timer.CreateLifePeriod(0, false);
	
	private int buildingLevel = 1;

	public NCastleGemController castleGemController;
	
	void Start ()
	{
	}
	
	/*
	 * This method must be called from WorldController
	 * */
	internal void Init (WorldController worldController)
	{
		UnitGroupSubject unitGroup = null;
		if (this.transform.parent != null) {
			unitGroup = (UnitGroupSubject)this.transform.parent.GetComponent (typeof(UnitGroupSubject));		
		}
		if (unitGroup == null) {
			unitGroup = ObjectController.CreateUnitGroupObject (this.gameObject.name);
		}
		
		this.buildingUnitSubject = (UnitSubject)this.gameObject.GetComponent (typeof(UnitSubject));
		if (buildingUnitSubject == null) {
			this.buildingUnitSubject = gameObject.AddComponent<UnitSubject> ();
		}
		
		unitGroup.Init (playerId, this);	
		
		this.Init (unitGroup, true);			
		
		this.garrisonSpawnTimer = Timer.CreateFrequency (GARRISON_SPAWN_PERIOD);	
		this.garrisonSpawnTimer.ForceTime ();
		this.castleConstructor = new CastleConstructor (this, constructedBuildings);
	}
	
	/*
	 * If building is outbuilding
	 * */
	internal void Init (BuildingSubject castle, bool createConstructed)
	{
		this.buildingUnitSubject = (UnitSubject)this.gameObject.GetComponent (typeof(UnitSubject));
		if (buildingUnitSubject == null) {
			this.buildingUnitSubject = gameObject.AddComponent<UnitSubject> ();
		}
		
		this.castleOwner = castle;
		this.Init (castle.BuildingUnitGroup, createConstructed);
		if (!createConstructed) {
			this.buildingConstructionTimer.SetNewLifePeriod (this.buildingType.timeSpawn);
			this.isConstructingBuilding = true;
			buildingUnitSubject.OnConstructionStarted ();
		}
	}
	
	/*
	 * Provides common init - if building is castle or castles outbuilding
	 * */
	private void Init (UnitGroupSubject buildingUnitGroup, bool createConstructed)
	{
		this.buildingUnitGroup = buildingUnitGroup;		
		this.buildingType = PlayerOwner.Types.Buildings.Get (this.gameObject.name);
		
		if (this.IsCastle) {
			CommandController.Instance.OnBuildingConstructed (this, this.buildingType);
		} else {
			CommandController.Instance.OnBuildingConstructed (this.castleOwner, this.buildingType);
		}		

		this.buildingUnitSubject.InitBuildingUnit (this);
		this.BuildingUnitGroup.AddUnit (this.buildingUnitSubject);
		
		this.moneyBonusTimer = Timer.CreateFrequency (this.buildingType.moneyBonusFrequency);		
		this.inited = true;
	}
	
	public bool CanUpgrade {
		get {
			if (isConstructingBuilding) {
				return false;
			}		
			if (buildingType.levelsData.Count <= 0) {
				return false;
			}
			BuildingType.BuildingLevelData data = buildingType.GetBuildingDataByLevel(buildingLevel + 1);
			if (data == null) {
				return false;
			}
			if (data.Cost > PlayerOwner.MoneyCount) {
				return false;
			}
			if (isUpgradingBuilding) {
				return false;
			}
			return true;
		}
	}
	
	/* Return true even if there is no money for upgrade
	 * */
	public bool HasUpgradePossibility {
		get {
			if (buildingType.levelsData.Count <= 0) {
				return false;
			}
			BuildingType.BuildingLevelData data = buildingType.GetBuildingDataByLevel(buildingLevel + 1);
			if (data == null) {
				return false;
			}
			return true;
		}
	}
	
	public void StartUpgrade (CommandController commandController) {
		if (!CanUpgrade) {
			throw new System.NotSupportedException ();
		}
		BuildingType.BuildingLevelData data = buildingType.GetBuildingDataByLevel(buildingLevel + 1);
		buildingConstructionTimer.SetNewLifePeriod (data.TimeUpgrade);
		this.PlayerOwner.DecreaseMoney (data.Cost);
		this.isUpgradingBuilding = true;
		buildingUnitSubject.OnUpgradeStarted ();
		
	}
	
	public void OnUpgradeFinished () {
		isUpgradingBuilding = false;
		buildingLevel += 1;
		PlayerOwner.OnBuildingUpgraded (this);
		buildingUnitSubject.OnUpgradeFinished ();
	}
		
	public void OnFixedUpdate ()
	{	
		if (!inited) {
			return;
		}
		
		if (WorldController.Instance.IsNetworkGameClient) {
			OnClientFixedUpdate ();
		} else {
			OnServerFixedUpdate ();
		}
	}
	
	private void OnServerFixedUpdate () {
		this.captureTimerProgress = captureTimer.GetProgress ();
		if (buildingConstructionTimer != null) {
			this.buildingConstructionProgress = buildingConstructionTimer.GetProgress ();
		}
		
		if (isConstructingBuilding) {
			if (!buildingConstructionTimer.IsActive) {
				isConstructingBuilding = false;
				buildingUnitSubject.OnConstructionFinished ();
				CommandController.Instance.OnBuildingConstructed (this.castleOwner, this.buildingType);
			} else {
				return;
			}			
		}
		
		if (isUpgradingBuilding) {
			if (!buildingConstructionTimer.IsActive) {				
				CommandController.Instance.OnUpgradeFinished (this);
//				this.OnUpgradeFinished ();
			}
		}
		
		if (this.moneyBonusTimer.EnoughTimeLeft ()) {
			this.buildingUnitGroup.PlayerOwner.AddMoney (this.buildingType.MoneyBonus);
			this.PlayerOwner.AddPowerPoints (this.buildingType.powerPoints);			
		}
				
		if (this.IsCastle && this.garrisonSpawnTimer.EnoughTimeLeft ()) {
			this.ProcessGarrison ();
		}
	}
	
	private void OnClientFixedUpdate () {
	}
	
	public void HandleClick ()
	{   
		if (castleGemController != null) {
			castleGemController.SetClosed (false);
			return;
		}

		if (InputController.CurrentHero != null) {
			InputController.CurrentHero.HeroesGroup.FollowTarget (this.buildingUnitGroup.MovementTarget);
		}
	}
	
	public bool CanSpawn (UnitType unitType)
	{
		if (!PlayerOwner.HasEnoughFreePlacesToSpawnUnit (unitType)) {
			return false;
		}
		int money = this.buildingUnitGroup.PlayerOwner.MoneyCount;
		return unitType.Price < money;
	}
	
	public void Spawn (CommandController commandController, UnitType unitType, UnitGroupSubject desiredUnitGroup = null)
	{
		UnitGroupSubject unitsGroup;
		if (desiredUnitGroup == null) {
			unitsGroup = this.buildingUnitGroup;
		} else {
			unitsGroup = desiredUnitGroup;
		}
		
		if (CanSpawn (unitType)) {
			this.buildingUnitGroup.PlayerOwner.DecreaseMoney (unitType.Price);			
			buildingUnitSubject.UnitSpawnManager.SpawnUnit (unitType, unitsGroup);
		}		
	}
	
	private UnitGroupSubject prevEnemy;
	private Timer captureTimer = Timer.CreateLifePeriod (5000, false);
	
	public void BreakCapture ()
	{
		captureTimer.Deactivate ();
		prevEnemy = null;
	}
	
	public float DoCapture (UnitGroupSubject captureGroup)
	{
		PlayerData player = captureGroup.PlayerOwner;
		
		if ((captureGroup.AliveUnitsCount > 0 && !this.PlayerOwner.IsEnemyPlayer (player)) || this.IsGuarded) {				
			captureTimer.Restart ();
			return 1;
		}
		
		if (captureGroup != prevEnemy && captureGroup.AliveUnitsCount > 0) {
			if (prevEnemy == null || prevEnemy.PlayerOwner.IsEnemyPlayer (captureGroup.PlayerOwner)) {
				prevEnemy = captureGroup;
				captureTimer.Restart ();
				garrisonSpawnTimer.Restart ();
			}
		}
		
		float progress = captureTimer.GetProgress ();
		
		if (!captureTimer.IsActive) {
			CommandController.Instance.CaptureBuilding (this, player);
		}
		
		return progress;
	}
	
	public void CaptureBuildingByPlayer (CommandController commandController, PlayerData newOwner)
	{		
		this.buildingUnitGroup.SetNewOwner (newOwner);
		this.castleConstructor.OnCastleCaptured (newOwner);
	}

	public void OnBuildingConstruction (CommandController controller, BuildingType newBuildingType)
	{
		foreach (UnitType unitType in newBuildingType.unitsToSpawn) {
			this.unitsToSpawn.Add (unitType);
		}
		
		foreach (SpellType spellType in newBuildingType.spells) {
			if (!this.spells.Contains (spellType)) {
				this.spells.AddLast (spellType);
			}
		}
		
		if (newBuildingType.IsCapital) {
			CapitalBuildingType capitalType = (CapitalBuildingType)newBuildingType;
			Vector3 point = CreatePointForGarrisonUnit ();
			CommandController.Instance.CreateUnit (capitalType.CapitalGuardianUnitType.Name, point, this.buildingUnitGroup, capitalType.CapitalGuardianUnitType.Race.Name, true);	
		}
	}

	public UnitGroupSubject BuildingUnitGroup {
		get {
			return this.buildingUnitGroup;
		}
	}
	
	public PlayerData PlayerOwner {
		get {
			return this.buildingUnitGroup.PlayerOwner;
		}
	}
	
	public bool CanSpawnUnits {
		get {
			return this.unitsToSpawn.Count > 0;
		}
	}
	
	public bool CanConstructStructures {
		get {
			return this.buildingType.CanConstructStructures;
		}
	}
	
	public bool HasMageGuild {
		get {
			return spells.Count > 0;
		}
	}
	
	#region Interaction
	
	void ProcessGarrison ()
	{
		foreach (UnitSubject unit in BuildingUnitGroup.Units) {
			if (unit.IsGarrisonUnit && !unit.IsBuilding) {
				return;
			}
		}
		
		for (int i = 0; i < buildingType.GarrisonUnitNumber; i++) {
			Vector3 point = CreatePointForGarrisonUnit ();
		
			string unitName;
			string raceName;
			
			if (PlayerOwner.RaceType == null) {
				unitName = this.buildingType.GarrisonUnitType.Name;
				raceName = this.buildingType.Race.Name;
			} else {
				unitName = PlayerOwner.RaceType.GarrisonUnitType.Name;
				raceName = PlayerOwner.RaceType.Name;
			}
			
			ServerCommandController.Instance.CreateUnit (unitName, point, this.buildingUnitGroup, raceName, true);	
		}
	}
	
	private Vector3 CreatePointForGarrisonUnit ()
	{
		Vector3 point = new Vector3 ();
		point = gameObject.transform.position;
		
		point.x -= Randomizer.Next (5f, -4f) + 4f;
		point.z -= Randomizer.Next (4.5f) + 4f;
		
		return point;
	}
		
	#endregion

	public BuildingType BuildingType {
		get {
			return this.buildingType;
		}
	}

	public UnitSubject BuildingUnitSubject {
		get {
			return this.buildingUnitSubject;
		}
	}
	
	public bool IsDestroyed {
		get { return BuildingUnitSubject.IsDead;}
	}
	
	public bool IsCastle {
		get { return this.castleOwner == null;}
	}

	public CastleConstructor CastleConstructor {
		get {
			return this.castleConstructor;
		}
	}

	public LinkedList<SpellType> Spells {
		get {
			return this.spells;
		}
	}
	
	public List<UnitType> UnitsToSpawn {
		get {
			return this.unitsToSpawn;
		}
	}
	
	public bool SpawnOnlyHeroes {
		get {
			foreach (UnitType unitType in this.unitsToSpawn) {
				if (!unitType.IsHero) {
					return false;
				}
			}
			return true;
		}
	}
	
	public UnitSpawnManager UnitSpawnManager {
		get {
			return this.BuildingUnitSubject.UnitSpawnManager;
		}
	}
	
	public Timer GetBuildingConstructionTimer ()
	{
		return buildingConstructionTimer;
	}
	
//	public float GetConstructionProgress ()
//	{
//		if (buildingConstructionTimer == null) {
//			return 0f;
//		}
//		return buildingConstructionTimer.GetProgress ();
//	}
	
	public bool IsGuarded {
		get { 
			if (BuildingUnitGroup.AliveUnitsCount > 0) {
				return true;
			}
					
			foreach (UnitGroupSubject unitGroup in BuildingUnitGroup.ClosestUnitGroups) {
				if (!unitGroup.PlayerOwner.IsEnemyPlayer (this.BuildingUnitGroup.PlayerOwner)) {
					if (unitGroup.AliveUnitsCount > 0) {
						return true;
					} 
				}
			}
			
			return false;
		}
	}
		
	public bool IsConstructing {
		get { return this.isConstructingBuilding;}
	}
	
//	public float CaptureProgress {
//		get {			
//			return this.captureTimer.GetProgress ();
//		}
//	}

	public bool CanResurrect {
		get {
			return this.IsCapital;
		}
	}

	public int BuildingLevel {
		get {
			return this.buildingLevel;
		}
	}
	
	public bool IsCapital {
		get {
			return this.buildingType.IsCapital;
		}
	}

	public int OfferedUnitPlaces {
		get { 
			return this.buildingType.GetOfferedUnitPlaces (this.BuildingLevel);
		}
	}
	
	public int PrevLevelOfferedUnitPlaces {
		get {
			if (this.BuildingLevel == 1) {
				return 0;
			}
			return this.buildingType.GetOfferedUnitPlaces (this.BuildingLevel - 1);
		}		
	}
	
	public int PlayerUnitAttackPowerModification {
		get {
			return this.buildingType.GetPlayerAttackPowerModification (this.BuildingLevel);
		}		
	}
	
	public int PrevPlayerUnitAttackPowerModification {
		get {
			if (this.BuildingLevel == 1) {
				return 0;
			}
			return this.buildingType.GetPlayerAttackPowerModification (this.BuildingLevel - 1);
		}		
	}
	
	public int PlayerUnitProtectionPowerModification {
		get {
			return this.buildingType.GetPlayerProtectionPowerModification (this.BuildingLevel);
		}		
	}
		
	public int PrevPlayerUnitProtectionPowerModification {
		get {
			if (this.BuildingLevel == 1) {
				return 0;
			}
			return this.buildingType.GetPlayerProtectionPowerModification (this.BuildingLevel - 1);
		}		
	}
	
	public int SpellSingleAttackPowerModification {
		get {
			return this.buildingType.GetPlayerSpellSinglePowerModification (this.BuildingLevel);
		}		
	}
	
	public int PrevSpellSingleAttackPowerModification {
		get {
			if (this.BuildingLevel == 1) {
				return 0;
			}
			return this.buildingType.GetPlayerSpellSinglePowerModification (this.BuildingLevel - 1);
		}		
	}	
	
	public int SpellEffectPowerModification {
		get {
			return this.buildingType.GetPlayerSpellEffectPowerModification (this.BuildingLevel);
		}		
	}
	
	public int PrevSpellEffectPowerModification {
		get {
			if (this.BuildingLevel == 1) {
				return 0;
			}
			return this.buildingType.GetPlayerSpellEffectPowerModification (this.BuildingLevel - 1);
		}		
	}
	
	public float CaptureTimerProgress {
		get {
			return captureTimerProgress;
		}
		set {
			captureTimerProgress = value;
		}
	}
	
	public float ConstructionProgress {
		get {
			return this.buildingConstructionProgress;
		}
	}

//	public Timer CaptureTimer {
//		get {
//			return this.captureTimer;
//		}
//	}

	public bool IsUpgradingBuilding {
		get {
			return this.isUpgradingBuilding;
		}
	}
	
	public IEnumerable<BuildingSubject> Outbuildings {
		get {
			if (this.castleConstructor == null) {
				return null;
			}
			return this.castleConstructor.Outbuildings;
		}
	}
}
