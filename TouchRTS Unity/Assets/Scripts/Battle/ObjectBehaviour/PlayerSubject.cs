using UnityEngine;
using System.Collections;
using Entity;
using AI;
using GameMapScenario;
using Setup;

public class PlayerSubject : TMonoBehaviour
{
	public int id;
	public string playerName;
	private int team;
	private bool isHuman = false;
	private bool isAi = false;
	private PlayerData playerData;
	private AiPlayerMind aiPlayerMind;
	public Color color;
		
	/*
	 * This method must be called from WorldController
	 * */
	internal void Init (WorldController worldController, PlayerInfo playerInfo)
	{
		if (!playerInfo.IsNill ()) {
			if (playerInfo.name == ApplicationManager.Instance.DeviceName) {
				this.isHuman = true;
			} else {
				this.isHuman = playerInfo.isDefinedAsHuman;
			}
			this.team = playerInfo.teamNumber;
			this.isAi = playerInfo.isDefinedAsAi;			
		} else {
			this.isHuman = false;
			this.team = this.id;
			this.isAi = false;
		}
		
		if (isHuman) {
			isAi = false;
		}
		
		this.playerData = WorldController.Instance.CreatePlayer (id, playerName, team, isHuman, color);
		this.playerData.moneyCount = playerInfo.money;
		
		if ((isAi || playerInfo.name == SingleMapScenario.AI_CONTROLLER) && !worldController.IsNetworkGameClient) {
			const float period = 2.51f;
			this.aiPlayerMind = new AiPlayerMind (this.playerData);
			this.playerData.AiPlayerMind = this.aiPlayerMind;
			InvokeRepeating ("ProcessAiRules", period, period);
			InvokeRepeating ("ProcessAiFastInteraction", 0.2f, 0.2f);
		}	
		
		if (worldController.IsNetworkGame) {
			PlayerNetworkSerializer serializer = this.gameObject.AddComponent<PlayerNetworkSerializer> ();
			serializer.Init (this);
		}
	}
	
	private void ProcessAiRules ()
	{
		if (!TWorldTime.Active) {
			return;
		}
		if (!this.playerData.IsDefeated && this.aiPlayerMind != null) {
			this.aiPlayerMind.ProcessRule ();
		}
	}
	
	private void ProcessAiFastInteraction ()
	{
		if (!TWorldTime.Active) {
			return;
		}
		if (!this.playerData.IsDefeated && this.aiPlayerMind != null) {
			this.aiPlayerMind.ProcessAiFastInteraction ();
		}
	}

	public PlayerData PlayerData {
		get {
			return this.playerData;
		}
	}
	
//	public void SetActive (bool Active) {
//		isPlayerActive = false;
//	}
	
	public string Id {
		get {
			return id + "";
		}
	}
}
