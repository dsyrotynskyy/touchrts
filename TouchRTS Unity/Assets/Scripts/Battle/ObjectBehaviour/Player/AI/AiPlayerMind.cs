using System;
using Entity;
using AI;
using System.Collections.Generic;
using System.Collections;

namespace AI
{
	public class AiPlayerMind : PlayerData.AiMind
	{
		public static readonly int LOW_PRIORITY = 10;
		public static readonly int MIDDLE_PRIORITY = 5;
		public static readonly int HIGH_PRIORITY = 1;
		
		private PlayerData playerData;
		private LinkedList<AiUnitGroup> aiUnitGroups = new LinkedList<AiUnitGroup> ();
		private bool isActive = true;
		
		public AiPlayerMind (PlayerData playerData)
		{
			this.playerData = playerData;
		}
	
		/* Main function.
		 * all calculation performs here
		 * */
		public void ProcessRule ()
		{
			if (!isActive) {
				return;
			}
			foreach (AiUnitGroup aiGroup in this.aiUnitGroups) {
				aiGroup.PerformAction ();
			}
		}

		public void ProcessAiFastInteraction ()
		{
			if (!isActive) {
				return;
			}
			foreach (AiUnitGroup aiGroup in this.aiUnitGroups) {
				aiGroup.PerformFastInteraction ();
			}
		}
		
		#region AiMind implementation
		public void OnUnitGroupRemoved (UnitGroupSubject unitGroup)
		{
			AiUnitGroup victim = null;
			foreach (AiUnitGroup aiGroup in this.aiUnitGroups) {
				if (unitGroup == aiGroup.UnitGroup) {
					victim = aiGroup;
					break;
				}
			}
			if (victim != null) {
				victim.OnDeactivate ();
				aiUnitGroups.Remove (victim);
			}			
		}

		public void AddUnitGroup (UnitGroupSubject unitGroup)
		{		
			this.aiUnitGroups.AddLast (new AiUnitGroup (unitGroup, this));
		}
		
		public void OnPlayerStateChanged (int action)
		{
		}
		#endregion
		
		public bool PlayerHasOnlyOneCastleAttackedByEnemies ()
		{
			return this.playerData.Castles.Count == 1 && PlayerHasCastleAttackedByEnemies ();
		}
		
		public bool PlayerHasCastleAttackedByEnemies () {
			foreach (BuildingSubject castle in this.playerData.Castles) {
				if (castle.BuildingUnitGroup.UnitGroupDemeanor.IsFightingEnemies ()) {
					return true;
				}
			}
			return false;
		}

		#region AiMind implementation
		public bool Active 
		{
			get {
				return isActive;
			}
			set {
				isActive = value;
			}
		}
		#endregion
	}
}


