using System;
using System.Collections.Generic;
using Entity;
using UnityEngine;

namespace AI
{
	public class AiUnitGroup : UnitGroupSubject.UnitGroupObserver
	{
		private UnitGroupSubject unitGroup;
//		private BuildingSubject buildingSubject;
		private HeroSubject heroSubject;
		
		private AiUnitGroupMemory aiUnitGroupMemory;
		private AiRule currentRule;
		private Queue<AiTask> tasks = new Queue<AiTask> ();
		
		private AiPlayerMind aiPlayerMind;
		
		public AiUnitGroup (UnitGroupSubject unitGroup, AiPlayerMind aiPlayerMind)
		{
			this.unitGroup = unitGroup;
//			this.buildingSubject = unitGroup.BuildingSubject;
			this.heroSubject = unitGroup.GroupHero;
			
			this.unitGroup.RegisterObserver (this);
			
			this.aiPlayerMind = aiPlayerMind;
			this.aiUnitGroupMemory = new AiUnitGroupMemory (this);			
		}
		
		/* Main function
		 * Check for the best rule and start new tasks
		 * */
		public void PerformAction ()
		{
			AiRule bestRule = AiWorldAnalyzer.Instance.GetBestRule (this);	
			
			if (bestRule != currentRule && bestRule != null) {
				this.StartPerformingRule (bestRule);
			}
			this.PerformRule ();
		}			
		
		private void StartPerformingRule (AiRule aiRule) {
			this.currentRule = aiRule;
			this.tasks.Clear ();
			foreach (AiTask task in this.currentRule.AiTasks) {
				this.tasks.Enqueue (task);
			}
			if (this.tasks.Count > 0) {
				this.aiUnitGroupMemory.StartPerforming (this.tasks.Dequeue ());
			}			
		}
		
		private void PerformRule () {
			if (!this.aiUnitGroupMemory.IsPerformed ()) {
				return;
			}
			
			if (this.tasks.Count <= 0) {
				this.SetRulePerformingFinished ();
				return;
			} 
			
			AiTask nextTask = tasks.Dequeue ();
			this.aiUnitGroupMemory.StartPerforming (nextTask);
		}
		
		private void SetRulePerformingFinished () {
			this.currentRule = null;
		}
		
		public void PerformFastInteraction ()
		{
			if (!this.IsHeroGroup) {
				return;
			}
//			UnityEngine.Debug.Log ("is hero fighting " + isHeroFighting);
			TrySpawnSpell ();
		}
		
		/* This method must be called
		 * on Ai Unit Group Removed
		 * */
		public void OnDeactivate () {
			this.UnitGroup.RemoveObserver (this);
		}

		#region UnitGroupObserver implementation
		public void OnGroupStateChanged (int action)
		{
//			switch (action) {
//				case (UnitGroupSubject.UNIT_ADDED) : break;
//				case (UnitGroupSubject.UNIT_REMOVED) : break;
//				case (UnitGroupSubject.HERO_DIES) : break;
//			}
		}
		
		#endregion
		
		public bool IsHeroGroup {
			get {
				return this.unitGroup.IsHeroGroup;
			}
		}
		
		public bool IsBuildingGroup {
			get {
				return this.unitGroup.IsBuilding;
			}
		}
		
		public UnitGroupSubject UnitGroup {
			get {
				return this.unitGroup;
			}
		}
		
		public PlayerData PlayerOwner {
			get {
				return this.unitGroup.PlayerOwner;
			}
		}

		public AiRule CurrentRule {
			get {
				return this.currentRule;
			}
		}

		public AiUnitGroupMemory AiUnitGroupMemory {
			get {
				return this.aiUnitGroupMemory;
			}
		}

		public AI.AiPlayerMind AiPlayerMind {
			get {
				return this.aiPlayerMind;
			}
		}
		
		private bool wasFighting = false;
		private SpellType spellType;
		
		private void TrySpawnSpell () {
			bool isHeroFighting = this.heroSubject.HeroesUnit.UnitDemeanorController.IsInFightingState;
			if (isHeroFighting) {
				if (!wasFighting) {
					spellType = this.heroSubject.Spells[Randomizer.Next (heroSubject.Spells.Count)];
				}
				Vector3 targetPoint = this.heroSubject.HeroesUnit.UnitDemeanorController.TargetUnit.transform.position;
				CommandController.Instance.DoSpell (heroSubject, spellType, targetPoint);
			} else {
				CommandController.Instance.StopSpell (heroSubject);
			}
			wasFighting = isHeroFighting;
		}
		
		private void DoFighting () {
		}
	}
}

