using System;
using System.Collections.Generic;
using Entity;
using UnityEngine;

namespace AI
{
	public class AiCommand
	{
		static AiCommand ()
		{
		}
		
		/* Is actual returns true if is performing
		 * */		
		public static bool CapturePositionTask (AiUnitGroup aiUnitGroup, UnitGroupSubject target)
		{ 
			if (!aiUnitGroup.IsHeroGroup) {
				throw new CommandNotSupportedByAiGroupException ();
			}
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			AiUnitGroupMemory memory = aiUnitGroup.AiUnitGroupMemory;	
			memory.unitGroupTarget = target;
			memory.targetIsReached = false;			
			
			CommandController.Instance.MoveGroupToPoint (
			unitGroup, target.MovementTarget.position);
			return true;
		}
		
		public static bool CapturePositionIsPerforming (AiUnitGroup aiUnitGroup)
		{
			if (!aiUnitGroup.IsHeroGroup) {
				throw new CommandNotSupportedByAiGroupException ();
			}
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			AiUnitGroupMemory memory = aiUnitGroup.AiUnitGroupMemory;		
			
			float distance = unitGroup.AreEnemiesAround () ? UnitGroupSubject.GROUP_INTERACTION_DISTANCE : UnitGroupSubject.GROUP_INTERACTION_DISTANCE / 2;
			
			if (unitGroup.IsEnoughClose (memory.unitGroupTarget.MainTransform.position, distance) && !memory.targetIsReached) {
				memory.targetIsReached = true;
				unitGroup.StopMovement ();
			}
			
			if (memory.targetIsReached) {
				return unitGroup.AreEnemiesAround (); 
			} else {
				if (Vector3.Distance (unitGroup.MovementTarget.position,  memory.unitGroupTarget.MovementTarget.position) > 5f) {
					CommandController.Instance.MoveGroupToPoint (unitGroup, memory.unitGroupTarget.MovementTarget.position);	
				}
			}
						
			return true;
		}
		
		public static bool TryRestoreHeroGroup (AiUnitGroup aiUnitGroup, BuildingSubject castle)
		{
			int needUnits = 0;
			needUnits = aiUnitGroup.AiUnitGroupMemory.GetUnitOffsetToPowerEnoughGroup ();
						
//			if (aiUnitGroup.AiUnitGroupMemory.IsUnitGroupPowerEnoughForFight ()) {
//				return false;
//			}
//			
//			AiCommand.TakeUnitsFromGroupTask (aiUnitGroup, castle.BuildingUnitGroup, needUnits);
//			needUnits = aiUnitGroup.AiUnitGroupMemory.GetUnitOffsetToPowerEnoughGroup ();
			
			if (aiUnitGroup.AiUnitGroupMemory.IsUnitGroupPowerEnoughForFight ()) {
				return false;
			}
			
			return AiCommand.SpawnRandomUnitsTask (aiUnitGroup, castle, needUnits);

		}
		
		public static bool TryRestoreHeroUnit (AiUnitGroup aiUnitGroup, BuildingSubject castle)
		{					
			if (aiUnitGroup.UnitGroup.GroupHero.IsDead) {
				aiUnitGroup.UnitGroup.GroupHero.Resurrect ();
			}
			
			if (aiUnitGroup.UnitGroup.GroupHero.HeroesUnit.GetLifePercent () < 0.5f) {
				return true;
			}
			
			return false;
		}
		
		public static bool SpawnRandomUnitsTask (AiUnitGroup aiUnitGroup, int numOfUnits)
		{
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			if (unitGroup.IsHeroGroup || !unitGroup.BuildingSubject.IsCastle) {
				throw new CommandNotSupportedByAiGroupException ();
			}			
			return SpawnRandomUnitsTask (aiUnitGroup, unitGroup.BuildingSubject, numOfUnits);
		}
				
		public static bool SpawnRandomUnitsTask (AiUnitGroup aiUnitGroup, BuildingSubject constructor, int numOfUnits)
		{
			AiUnitGroupMemory memory = aiUnitGroup.AiUnitGroupMemory;			
			memory.unitGroupTarget = constructor.BuildingUnitGroup;
			
			if (!SpawnUnitsCondition (aiUnitGroup)) {
				return false;
			}
			
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			PlayerData playerOwner = unitGroup.PlayerOwner;
				
			List<UnitType> units = aiUnitGroup.PlayerOwner.UnitsToSpawn;
					
			for (int i = 0; i < numOfUnits; i++) {
				int unitTypeNum = Randomizer.Next (units.Count);
				unitTypeNum = Randomizer.Next (unitTypeNum);
				UnitType unitType = units [unitTypeNum];
				while (unitType.IsHero) {
					unitTypeNum = Randomizer.Next (units.Count);
					unitType = units [unitTypeNum];
				}
				
				if (playerOwner.CanSpawnUnitTheoretically (unitType)) {
					CommandController.Instance.SpawnUnit (constructor, unitType, unitGroup);
//					constructor.Spawn (unitType, unitGroup);
				}
			}			
			
			return true;
		}
		
		public static bool SpawnRandomUnitsIsPerforming (AiUnitGroup aiUnitGroup)
		{
			if (!SpawnUnitsCondition (aiUnitGroup)) {
				return false;
			}
			
			UnitSubject unitConstructor = aiUnitGroup.AiUnitGroupMemory.unitGroupTarget.GroupMainUnit;
			return unitConstructor.UnitSpawnManager.IsSpawningUnitFor (aiUnitGroup.UnitGroup);
		}
		
		private static bool SpawnUnitsCondition (AiUnitGroup aiUnitGroup)
		{
			BuildingSubject constructor = aiUnitGroup.AiUnitGroupMemory.unitGroupTarget.BuildingSubject;
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			
			if (aiUnitGroup.PlayerOwner != constructor.PlayerOwner) {
				return false;
			}
					
			if (!constructor.CanSpawnUnits || (unitGroup.IsBuilding && unitGroup != constructor.BuildingUnitGroup)) {
				return false;
			}
			
			List<UnitType> units = constructor.UnitsToSpawn;
			
			if (units.Count <= 0 || constructor.SpawnOnlyHeroes) {
				return false;
			} 
			return true;
		}
		
		private static bool TakeUnitsFromGroupTask (AiUnitGroup aiUnitGroup, UnitGroupSubject sourceGroup, int number)
		{
			throw (new NotSupportedException ());
			
			if (aiUnitGroup.PlayerOwner != sourceGroup.PlayerOwner) {
				return false;
			}
			
			if (!aiUnitGroup.UnitGroup.IsEnoughCloseForInteraction (sourceGroup)) {
				return false;
			}
			
			HashSet<UnitSubject> units = sourceGroup.Units;
			
			for (int i = 0; i < number; i++) {
				UnitSubject unitVictim = null;
				foreach (UnitSubject unit in units) {
					if (unit.IsHero || unit.IsBuilding || unit.IsGarrisonUnit) {
						continue;
					}
					unitVictim = unit;
				}
				if (unitVictim == null) {
					break;
				} else {
					unitVictim.UnitGroup = aiUnitGroup.UnitGroup;
				}
			}		
			return false;
		}
		
		public static bool ConstructBuildingTask (AiUnitGroup aiUnitGroup, BuildingType buildingType)
		{
			if (!aiUnitGroup.IsBuildingGroup || !aiUnitGroup.UnitGroup.BuildingSubject.CanConstructStructures) {
				throw new CommandNotSupportedByAiGroupException ();
			}
			
			BuildingSubject building = aiUnitGroup.UnitGroup.BuildingSubject;
			if (building.CastleConstructor.CanConstruct (buildingType)) {
				ServerCommandController.Instance.ConstructBuilding (building.CastleConstructor, buildingType, false);
				return true;
			}
			
			return false;
		}
		
		public static bool UpgradeBuildingTask (AiUnitGroup aiUnitGroup, BuildingSubject building)
		{
			if (!aiUnitGroup.IsBuildingGroup) {
				throw new CommandNotSupportedByAiGroupException ();
			}
			
			if (building.CanUpgrade) {
				ServerCommandController.Instance.StartUpgradeBuilding (building);
				return true;
			}
			
			return false;
		}
		
		public class CommandNotSupportedByAiGroupException : Exception
		{
		}
	}
}

