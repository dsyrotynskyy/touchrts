using System;
using Entity;
using UnityEngine;

namespace AI
{
	public class AiUnitGroupMemory
	{
		private AiUnitGroup aiUnitGroup;
		private AiTask currentTask;
		
		/* Data fields
		 * */
		public UnitType unitType;
		public BuildingSubject buildingSubject;
		public UnitGroupSubject unitGroupTarget;
		public bool targetIsReached;
		public int numberOfUnits;
		
		public AiUnitGroupMemory (AiUnitGroup aiUnitGroup)
		{
			this.aiUnitGroup = aiUnitGroup;
		}
		
		/* Starts performing Rule
		 * */
		public void StartPerforming(AiTask aiTask) {
			this.ResetMemory ();
			this.currentTask = aiTask;
			if (!aiTask.StartPerforming (this.aiUnitGroup)) {
				this.currentTask = null;
			};
		}
		
		/* Check is the command performed
		 * */
		public bool IsPerformed () {
			if (currentTask == null) {
				return true;
			}
			
			if (currentTask.TryPerform (this.aiUnitGroup)) {
				this.currentTask = null;
				return true;
			};
			
			return false;
		}
		
		private void ResetMemory () {
			this.currentTask = null;
			
			this.unitType = null;
			this.unitGroupTarget = null;
			this.targetIsReached = false;
			this.numberOfUnits = 0;
		}

		public AiUnitGroup AiUnitGroup {
			get {
				return this.aiUnitGroup;
			}
		}
		
		public PlayerData PlayerData {
			get {
				return this.aiUnitGroup.UnitGroup.PlayerOwner;
			}
		}
		
		public AiPlayerMind AiPlayerMind {
			get {
				return this.aiUnitGroup.AiPlayerMind;
			}
		}
		
		public BuildingSubject GetOurClosestCastleAttackedByEnemies () {
			BuildingSubject closestCastle = null;
			float minDistance = float.PositiveInfinity;
			Vector3 ourPosition = aiUnitGroup.UnitGroup.MainObject.transform.position;
			foreach (BuildingSubject building in this.PlayerData.Castles) {
				if (Vector3.Distance (building.gameObject.transform.position, ourPosition) < minDistance && building.BuildingUnitGroup.UnitGroupDemeanor.IsFightingEnemies ()) {
					closestCastle = building;
				}
			}
			return closestCastle;
		}
		
		public BuildingSubject GetOurClosestCastle () {
			return GetClosestPlayerCastle (this.PlayerData);
		}
		
		public BuildingSubject GetClosestPlayerCastle (PlayerData player) {
			BuildingSubject closestCastle = null;
			float minDistance = float.PositiveInfinity;
			Vector3 ourPosition = aiUnitGroup.UnitGroup.MainObject.transform.position;
			foreach (BuildingSubject building in player.Castles) {
				if (Vector3.Distance (building.gameObject.transform.position, ourPosition) < minDistance) {
					closestCastle = building;
				}
			}
			
			return closestCastle;
		}
		
		public BuildingSubject GetOurClosestCastleFreeForRestoreUnitGroup () {
			BuildingSubject closestCastle = null;
			float minDistance = float.PositiveInfinity;
			Vector3 ourPosition = aiUnitGroup.UnitGroup.MainObject.transform.position;
			foreach (BuildingSubject building in PlayerData.Castles) {
				if (Vector3.Distance (building.gameObject.transform.position, ourPosition) < minDistance && !building.BuildingUnitSubject.UnitSpawnManager.IsSpawningUnitForOtherUnitGroup ()) {
					closestCastle = building;
				}
			}
			return closestCastle;
		}
		
		public BuildingSubject GetClosestEnemyBuilding () {
			BuildingSubject closestBuilding = null;
			float minDistance = float.PositiveInfinity;
			Vector3 ourPosition = aiUnitGroup.UnitGroup.MainObject.transform.position;
			
			foreach (PlayerData player in WorldController.Instance.AllPlayers.Values) {
				if (!player.IsEnemyPlayer (this.PlayerData)) {
					continue;
				}
				foreach (BuildingSubject building in player.Buildings) {
					float distance = Vector3.Distance (building.gameObject.transform.position, ourPosition);
					if (distance < minDistance) {
						minDistance = distance;
						closestBuilding = building;
					}
				}
			}
			return closestBuilding;
		}
		
		public bool IsUnitGroupPowerEnoughForFight () {
			int num = GetUnitOffsetToPowerEnoughGroup ();
			return num <= 0;
		}
		
		public int GetUnitOffsetToPowerEnoughGroup () {
			int avarageGroupSize = AiWorldAnalyzer.Instance.GetAiAvarageUnitCount ();
			return avarageGroupSize - AiUnitGroup.UnitGroup.Units.Count;
		}
	}
}

