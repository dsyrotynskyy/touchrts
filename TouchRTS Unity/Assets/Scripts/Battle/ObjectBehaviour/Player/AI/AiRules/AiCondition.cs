using System;
using Entity;

namespace AI
{
	public class AiCondition
	{		
		public static bool PlayerHasOnlyOneCastle (AiUnitGroup aiUnitGroup)
		{
			return aiUnitGroup.AiPlayerMind.PlayerHasOnlyOneCastleAttackedByEnemies ();
		}
		
		public static bool PlayerHasCastleAttackedByEnemies (AiUnitGroup aiUnitGroup) {
			return aiUnitGroup.AiPlayerMind.PlayerHasCastleAttackedByEnemies ();
		}
			
		public static bool HeroIsDead (AiUnitGroup aiUnitGroup)
		{
			if (!aiUnitGroup.IsHeroGroup) {
				return false;
			}
			return aiUnitGroup.UnitGroup.GroupHero.IsDead;
		}
		
		public static bool IsAttackedByMuchBiggerArmy (AiUnitGroup aiUnitGroup) {
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			int good = unitGroup.ClosestFriendUnits.Count + unitGroup.Units.Count;
			int enemyUnits = unitGroup.ClosestEnemies.Count;
			
			return enemyUnits > good * 2;
		}
		
		public static bool IsDefendingCastleFromeEnemies (AiUnitGroup aiUnitGroup) {
			return aiUnitGroup.UnitGroup.UnitGroupDemeanor.IsDefendingCastleFromEnemies ();
		}
		
		public static bool UnitGroupSizeIsLesserAvarage (AiUnitGroup aiUnitGroup) {
			int unitCount = aiUnitGroup.UnitGroup.AliveUnitsCount;
			if (aiUnitGroup.UnitGroup.IsBuilding && unitCount < AiWorldAnalyzer.Instance.GetGarrisonSize ()) {
				return true;
			}
			bool unitGroupIsPowerEnough = aiUnitGroup.AiUnitGroupMemory.IsUnitGroupPowerEnoughForFight ();
			return !unitGroupIsPowerEnough;
		}
		
		public static bool IsNotFightingEnemies (AiUnitGroup aiUnitGroup) {
			return !aiUnitGroup.UnitGroup.UnitGroupDemeanor.IsFightingEnemies ();
		}
		
		public static bool HasPlaceToConstructBuildings (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup) {
				return false;
			}
			
			CastleConstructor castleConstructor = aiUnitGroup.UnitGroup.BuildingSubject.CastleConstructor;
			
			return castleConstructor.HasFreePlaceForOutbuildings () || castleConstructor.HasFreePlaceForTowers ();
		}
		
		public static bool HasBuildingsToUpgrade (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup) {
				return false;
			}
			foreach (UnitSubject unit in aiUnitGroup.UnitGroup.Units) {
				if (unit.IsBuilding && unit.BuildingData.CanUpgrade) {
					return true;
				}
			}
			return false;
		}
	}
}

