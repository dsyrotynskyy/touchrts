using System;
using Entity;
using AI;
using System.Collections.Generic;
using System.Collections;

namespace AI
{
	public class AiRule : IComparable<AiRule>
	{
		public readonly static int LOW_PRIORITY = 10;
		public readonly static int MIDDLE_PRIORITY = 50;
		public readonly static int HIGH_PRIORITY = 100;
		
		private readonly static int HERO_RULE = 54545;
		private readonly static int CASTLE_RULE = 999;		
		
		private readonly static AiRule OurCastleAttackedEveryHeroShouldDefend = new HeroRule (HIGH_PRIORITY, "OurCastleAttackedEveryHeroShouldDefend");
		private readonly static AiRule HeroGroupRetreatAndRestore = new HeroRule (MIDDLE_PRIORITY, "HeroGroupRetreatAndRestore");
		private readonly static AiRule HeroGroupRestoreIfDoingNothing = new HeroRule (MIDDLE_PRIORITY, "HeroGroupRestoreIfDoingNothing");	
		private readonly static AiRule HeroGroupAttackRandomEnemy = new HeroRule (LOW_PRIORITY, "HeroGroupAttackRandomEnemy");
		
		private readonly static AiRule ConstructRandomBuilding = new CastleRule (MIDDLE_PRIORITY, "ConstructRandomBuilding");
		private readonly static AiRule UpgradeRandomBuilding = new CastleRule (LOW_PRIORITY, "UpgradeRandomBuilding");
		
		static AiRule () {
			InitHeroRules ();
			InitCastleRules ();
		}
		
		static void InitHeroRules () {
			OurCastleAttackedEveryHeroShouldDefend.AddConditions (AiCondition.PlayerHasCastleAttackedByEnemies);
			OurCastleAttackedEveryHeroShouldDefend.SetTasks (AiTask.DefendClosestCastleAttackedByEnemies);
		
			HeroGroupRetreatAndRestore.AddConditions (AiCondition.HeroIsDead);
			HeroGroupRetreatAndRestore.AddConditions (AiCondition.UnitGroupSizeIsLesserAvarage);
			HeroGroupRetreatAndRestore.SetTasks (AiTask.RetreatToClosestCastle, AiTask.RestoreUnitGroupWithClosestCastle);
		
			HeroGroupRestoreIfDoingNothing.AddConditions (AiCondition.IsNotFightingEnemies, AiCondition.UnitGroupSizeIsLesserAvarage);
			HeroGroupRestoreIfDoingNothing.SetTasks (AiTask.RetreatToClosestCastle, AiTask.RestoreUnitGroupWithClosestCastle);
			
			HeroGroupAttackRandomEnemy.SetTasks (AiTask.AttackRandomEnemy);
		}
		
		static void InitCastleRules () {
//			RestoreCastleGarrison.AddConditions (AiCondition.UnitGroupSizeIsLesserAvarage);
//			RestoreCastleGarrison.SetTasks (AiTask.TrainGarrison)	;
			
			ConstructRandomBuilding.AddConditions (AiCondition.HasPlaceToConstructBuildings);
			ConstructRandomBuilding.SetTasks (AiTask.ConstructRandomBuilding);
			
			UpgradeRandomBuilding.AddConditions (AiCondition.HasBuildingsToUpgrade);
			UpgradeRandomBuilding.SetTasks (AiTask.UpgradeBuilding);
		}
		
		private int priority;
		private string name;
		private List<Func<AiUnitGroup, bool>[]> conditions = new List<Func<AiUnitGroup, bool>[]> ();
		private AiTask[] tasks;
		
		private int ruleType = HERO_RULE;
		
		public AiRule (int priority, int ruleType, string name)
		{
			this.priority = priority;
			this.ruleType = ruleType;
			this.name = name;
			
			AiWorldAnalyzer.Instance.AddRule (this);
		}	
		
		#region IComparable[AI.AiRule] implementation
		private void AddConditions (params Func<AiUnitGroup, bool>[] conditionArray) {
			this.conditions.Add (conditionArray);
		}
		
		private void SetTasks (params AiTask[] tasks) {
			this.tasks = tasks;
		}
		
		public bool IsActual (AiUnitGroup aiUnitGroup) {
			if (aiUnitGroup.IsHeroGroup && !this.IsHeroRule) {
				return false;
			}
			
			if (aiUnitGroup.IsBuildingGroup && this.IsHeroRule) {
				return false;
			}
			
			if (conditions.Count <= 0) {
				return true;
			}
			
			foreach (Func<AiUnitGroup, bool>[] conditionArray in conditions) {
				bool areConditionsActual = true;
				foreach (Func<AiUnitGroup, bool> isActual in conditionArray) {
					if (!isActual (aiUnitGroup)) {
						areConditionsActual = false;
						break;
					}
				}
				if (areConditionsActual) {
					return true;
				}
			}
						
			return false;
		}
		
		public bool IsHeroRule {
			get {
				return this.ruleType == HERO_RULE;
			}
		}
		
		public int CompareTo (AI.AiRule other)
		{
			if (this.priority < other.priority)
				return 1;
			else if (this.priority > other.priority)
				return -1;
			else
				return 0;
		}
		#endregion

		public AiTask[] AiTasks {
			get {
				return this.tasks;
			}
		}

		public int Priority {
			get {
				return this.priority;
			}
		}
		
		public override string ToString(){return name;}
		
		class HeroRule : AiRule {
			public HeroRule (int priority, string name) : base (priority, HERO_RULE, name) {
			}
		}
		
		class CastleRule : AiRule {
			public CastleRule (int priority, string name) : base (priority, CASTLE_RULE, name) {
			}
		}
	}
}

