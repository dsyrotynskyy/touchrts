using System;
using Entity;
using System.Collections.Generic;

namespace AI
{
	public class AiTask
	{
		
		private Func<AiUnitGroup, bool> taskFunc;
		private Func<AiUnitGroup, bool> isPerformingFunc;
		
		private AiTask (Func<AiUnitGroup, bool> taskFunc, Func<AiUnitGroup, bool> isPerformingFunc)
		{
			this.taskFunc = taskFunc;
			this.isPerformingFunc = isPerformingFunc;
		}
		
		private AiTask (Func<AiUnitGroup, bool> taskFunc)
		{
			this.taskFunc = taskFunc;
		}
		
		/* This function start 
		 * perfoming of task
		 * */
		public bool StartPerforming (AiUnitGroup aiUnitGroup)
		{
			if (taskFunc == null) {
				return false;
			}
			
			return taskFunc (aiUnitGroup);
		}
		
		/* This function using facts
		 * to check is the task performed
		 * cannot command anything to unitGroup 
		 * */
		public bool TryPerform (AiUnitGroup aiUnitGroup)
		{		
			if (isPerformingFunc == null) {
				return true;
			}
			return !this.isPerformingFunc (aiUnitGroup);
		}
		
		#region tasks	
		
		public static AiTask AttackRandomEnemy = new AiTask (AttackClosestEnemyTask, AttackClosestEnemyIsPerforming);
		public static AiTask RetreatToClosestCastle = new AiTask (RetreatToClosestCastleTask, RetreatToClosestCastleIsPerforming);
		public static AiTask RestoreUnitGroupWithClosestCastle = new AiTask (RestoreUnitGroupWithClosestCastleTask, RestoreUnitGroupClosestCastleIsPerforming);		
		public static AiTask DefendClosestCastleAttackedByEnemies = new AiTask (DefendClosestCastleAttackedByEnemiesTask, DefendClosestCastleAttackedByEnemiesIsPerforming);
		
		public static AiTask RestoreUnitGroup = new AiTask (BuildGarrisonTask);	
		public static AiTask ConstructRandomBuilding = new AiTask (ConstructRandomBuildingsTask, ConstructBuildingTaskIsPerforming);	
		public static AiTask UpgradeBuilding = new AiTask (UpgradeRandomBuildingsTask, UpgradeRandomBuildingsTaskIsPerforming);	
		public static AiTask TrainGarrison = new AiTask (BuildGarrisonTask);

		#endregion
		
		#region hero tasks implementation		
		private static bool AttackClosestEnemyTask (AiUnitGroup aiUnitGroup)
		{
			UnitGroupSubject unitGroupTarget = aiUnitGroup.AiUnitGroupMemory.GetClosestEnemyBuilding ().BuildingUnitGroup;
			if (unitGroupTarget == null) {
				return false;
			}
			
			return AiCommand.CapturePositionTask (aiUnitGroup, unitGroupTarget);
		}
		
		private static bool AttackClosestEnemyIsPerforming (AiUnitGroup aiUnitGroup)
		{
			return AiCommand.CapturePositionIsPerforming (aiUnitGroup);
		}
		
		private static bool DefendClosestCastleAttackedByEnemiesTask (AiUnitGroup aiUnitGroup) {
			BuildingSubject buildingSubject = aiUnitGroup.AiUnitGroupMemory.GetOurClosestCastleAttackedByEnemies ();
			
			return AiCommand.CapturePositionTask (aiUnitGroup, buildingSubject.BuildingUnitGroup);
		}
		
		private static bool DefendClosestCastleAttackedByEnemiesIsPerforming (AiUnitGroup aiUnitGroup) {
			return AiCommand.CapturePositionIsPerforming (aiUnitGroup);
		}
		
		private static bool RetreatToClosestCastleTask (AiUnitGroup aiUnitGroup) {
			BuildingSubject buildingSubject = aiUnitGroup.AiUnitGroupMemory.GetOurClosestCastle ();
			if (buildingSubject == null) {
				return false;
			}
				
			return AiCommand.CapturePositionTask (aiUnitGroup, buildingSubject.BuildingUnitGroup);
		}
		
		private static bool RetreatToClosestCastleIsPerforming (AiUnitGroup aiUnitGroup) {
			return AiCommand.CapturePositionIsPerforming (aiUnitGroup);
		}
		
		private static bool RestoreUnitGroupWithClosestCastleTask (AiUnitGroup aiUnitGroup) {
			BuildingSubject castle = aiUnitGroup.AiUnitGroupMemory.GetOurClosestCastle ();
			if (castle == null || !castle.BuildingUnitGroup.IsEnoughCloseForInteraction (aiUnitGroup.UnitGroup)) {
				return false;
			}
			
			bool performing = AiCommand.TryRestoreHeroUnit (aiUnitGroup, castle);
			performing = performing || AiCommand.TryRestoreHeroGroup (aiUnitGroup, castle);

			return performing;
		}
		
		private static bool RestoreUnitGroupClosestCastleIsPerforming (AiUnitGroup aiUnitGroup) {			
			if (!aiUnitGroup.IsHeroGroup) {
				throw new TaskNotSupportedByAiGroupException ();
			}
			
			return RestoreUnitGroupWithClosestCastleTask (aiUnitGroup);
		}	
		
		#endregion
		
		#region castle tasks implementation	
		
		private static bool BuildGarrisonTask (AiUnitGroup aiUnitGroup) {
			UnitGroupSubject unitGroup = aiUnitGroup.UnitGroup;
			if (unitGroup.IsHeroGroup || !unitGroup.BuildingSubject.IsCastle) {
				throw new TaskNotSupportedByAiGroupException ();
			}
			
			int needUnits = 0;
			needUnits = AiWorldAnalyzer.Instance.GetGarrisonSize () - aiUnitGroup.UnitGroup.AliveUnitsCount;
						
			if (needUnits <= 0) {
				return false;
			}
	
			return AiCommand.SpawnRandomUnitsTask (aiUnitGroup, needUnits);
		}
		
		private static bool ConstructRandomBuildingsTask (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup) {
				return false;
			}
			CastleConstructor castleConstructor = aiUnitGroup.UnitGroup.BuildingSubject.CastleConstructor;
			
			foreach (BuildingType buildingType in castleConstructor.OutbuildingsTypes.Keys) {
				if (castleConstructor.CanConstruct (buildingType)) {
					aiUnitGroup.AiUnitGroupMemory.unitType = buildingType;
					return AiCommand.ConstructBuildingTask (aiUnitGroup, buildingType);
				}
			}
			
			return false;
		}
		
		private static bool ConstructBuildingTaskIsPerforming (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup) {
				return false;
			}
			
			foreach (UnitSubject unit in aiUnitGroup.UnitGroup.Units) {
				if (unit.UnitType == aiUnitGroup.AiUnitGroupMemory.unitType) {
					return unit.BuildingData.IsConstructing;
				}
			}
			return false;
		}
		
		private static List<BuildingSubject> buildings = new List<BuildingSubject> ();
		
		private static bool UpgradeRandomBuildingsTask (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup) {
				return false;
			}
			buildings.Clear ();
			foreach (UnitSubject unit in aiUnitGroup.UnitGroup.Units) {
				if (unit.IsBuilding && unit.BuildingData.CanUpgrade) {
					buildings.Add (unit.BuildingData);
				}
			}
			
			if (buildings.Count > 0) {				
				int num = Randomizer.Next (buildings.Count);
				BuildingSubject building = buildings[num];
				buildings.Clear ();
				aiUnitGroup.AiUnitGroupMemory.buildingSubject = building;
				return AiCommand.UpgradeBuildingTask (aiUnitGroup, building);
			}		
			
			return false;
		}
		
		private static bool UpgradeRandomBuildingsTaskIsPerforming (AiUnitGroup aiUnitGroup) {
			if (!aiUnitGroup.IsBuildingGroup || aiUnitGroup.AiUnitGroupMemory.buildingSubject == null) {
				return false;
			}
			return aiUnitGroup.AiUnitGroupMemory.buildingSubject.IsUpgradingBuilding;
		}		
				
		#endregion
		

		
		public class TaskNotSupportedByAiGroupException : Exception
		{
		}
	}
}

