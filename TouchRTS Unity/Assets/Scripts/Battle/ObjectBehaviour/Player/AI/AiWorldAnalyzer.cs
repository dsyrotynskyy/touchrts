using System;
using System.Collections.Generic;

namespace AI
{
	public class AiWorldAnalyzer
	{
		private static readonly AiWorldAnalyzer instance = new AiWorldAnalyzer ();
		private SortedDictionary<int, List<AiRule>> allAiRules = new SortedDictionary<int, List<AiRule>> ();

		private AiWorldAnalyzer ()
		{
		}
		
		public void AddRule (AiRule aiRule)
		{
			int priority = aiRule.Priority;
			List<AiRule> aiRules;
			if (allAiRules.ContainsKey (priority)) {
				aiRules = this.allAiRules [priority];
			} else {
				aiRules = new List<AiRule> ();
				this.allAiRules.Add (priority, aiRules);			
			}
			aiRules.Add (aiRule);
		}

		public AiRule GetBestRule (AiUnitGroup aiUnitGroup)
		{
			AiRule currentAiRule = aiUnitGroup.CurrentRule;
			int priority = 0;
			if (currentAiRule != null) {
				priority = currentAiRule.Priority;
			}
						
			AiRule bestRule;
			if (priority < AiRule.HIGH_PRIORITY) {
				bestRule = GetRandomRuleFromList (aiUnitGroup, AiRule.HIGH_PRIORITY);
				if (bestRule != null) {
					return bestRule;
				}
			}
				
			if (priority < AiRule.MIDDLE_PRIORITY) {
				bestRule = GetRandomRuleFromList (aiUnitGroup, AiRule.MIDDLE_PRIORITY);
				if (bestRule != null) {
					return bestRule;
				}
			}
				
			if (currentAiRule == null) {
				bestRule = GetRandomRuleFromList (aiUnitGroup, AiRule.LOW_PRIORITY);
				if (bestRule != null) {
					return bestRule;
				}
			}		
			
			return currentAiRule;
		}
		
		private AiRule GetRandomRuleFromList (AiUnitGroup aiUnitGroup, int priority)
		{
			if (!allAiRules.ContainsKey (priority)) {
				return null;
			}
			List<AiRule> list = allAiRules [priority];
			list.Reverse ();
			foreach (AiRule rule in list) {
				if (rule.IsActual (aiUnitGroup)) {
					return rule;
				}
			}
			return null;
		}
		
		public int GetGarrisonSize ()
		{
			return 0;
		}
			
		/* Actually not avarage unit count per group=)
		 * */
		public int GetAiAvarageUnitCount ()
		{
//			int unitCount = WorldController.Instance.WorldData.UnitCount;
//			int unitGroupCount = WorldController.Instance.WorldData.UnitGroupCount;
//			return 2 * unitCount / unitGroupCount + 1;
			return 8;
		}
		
		public static AiWorldAnalyzer Instance {
			get {
				return instance; 
			}
		}
	}
}

