using System;
using System.Collections.Generic;

namespace Entity
{
	public class PlayerModificationsData
	{
		public readonly String ALL = "All";
		
		private LinkedList<UnitType> unitModificationsList = new LinkedList<UnitType> ();
		private LinkedList<BuildingType> buildingModificationsList = new LinkedList<BuildingType> ();
		private LinkedList<SpellType> spellModificationList = new LinkedList<SpellType> ();
		
		public PlayerModificationsData ()
		{
		}
		
		public void ApplyModifications (DataTypes types) {
			this.ApplyBuildings (types.Buildings.allData.Values);
			this.ApplySpells (types.Spells.AllSpellTypes.Keys);
			this.ApplyUnits (types.Units.allData.Values);
		}
		
		private void ApplyUnits (IEnumerable<UnitType> units) {
			foreach (UnitType modification in unitModificationsList) {
				foreach (UnitType unit in units) {
					if (unit.IsBuilding) {
						continue;
					}
					if (DoesMatch (unit.Name, modification.Name)) {
						unit.ModifyValues (modification);
					}
				}
			}
		}
		
		private void ApplyBuildings (IEnumerable<BuildingType> buildings) {
			foreach (BuildingType modification in buildingModificationsList) {
				foreach (BuildingType building in buildings) {
					if (DoesMatch (building.Name, modification.Name)) {
						building.ModifyValues (modification);
					}
				}
			}
		}
		
		private void ApplySpells (IEnumerable<SpellType> spells) {
			foreach (SpellType modification in spellModificationList) {
				foreach (SpellType spell in spells) {			
					if (DoesMatch (spell.Name, modification.Name)) {
						spell.ModifyValues (modification);
					}
				}
			}
		}
		
		private bool DoesMatch (String entityName, String modificationData) {
			if (modificationData.Equals (ALL)) {
				return true;
			}
			return modificationData.Contains (entityName);
		}
	}
}

