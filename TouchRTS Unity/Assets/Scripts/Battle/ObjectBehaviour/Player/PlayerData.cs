using System;
using System.Collections.Generic;
using UnityEngine;
using AI;

namespace Entity
{
	public class PlayerData
	{
		public const int MAX_HEROES = 3;
		public static int CONTROL_TYPE_HUMAN = 1231;
		public static int CONTROL_TYPE_COMPUTER = 11;
		public static int CONTROL_TYPE_NETWORK = 13;
		private bool isDefeated = false;
		private int id;
		private String name;
		private int team;
		private int controlType = CONTROL_TYPE_HUMAN;
		private Color playerColor;
		
		public int moneyCount;
		public int allUnitPlaces;
		public int usedUnitPlaces;		
		
		private HeroSubject mainHero = null;
		private List<HeroSubject> heroes = new List<HeroSubject> ();
		private List<UnitGroupSubject> unitsGroups = new List<UnitGroupSubject> ();
		private LinkedList<BuildingSubject> buildings = new LinkedList<BuildingSubject> ();
		private LinkedList<BuildingSubject> castles = new LinkedList<BuildingSubject> ();
		
		private int unitAttackModification;
		private int unitProtectionModification;
		private int spellSinglePowerModification;
		private int spellEffectPowerModification;			
		
		/*List of units availble to spawn for player*/
		private readonly Dictionary<string, RaceData> raceData = new Dictionary<string, RaceData> ();
		private RaceType raceType;
		/* Races Data with modified information about it units
		 * */
		public DataTypes types;
		private AiMind aiPlayerMind;
		
		internal void init (int id, String name, int team, int controlType, Color playerColor)
		{
			this.id = id;
			this.name = name;
			this.team = team;
			this.playerColor = playerColor;
			this.controlType = controlType;
			types = DataTypes.Instance ;
			
			foreach (RaceType race in types.Races.allData.Values) {
				raceData.Add (race.Name, new RaceData ());
			}
		}
		
		internal PlayerData ()
		{
		}
		
		public DataTypes Types {
			get {
				return types;
			}
		}
	
		internal PlayerData (int id, String name, int team, int controlType, Color color)
		{
			this.init (id, name, team, controlType, color);
		}

		public void AddHero (HeroSubject hero)
		{	
			if (this.mainHero == null) {
				this.mainHero = hero;
			}
			
			if (raceType == null) {
				raceType = hero.HeroType.Race;
			}
			
			this.heroes.Add (hero);
		}
		
		public bool CanSpawnHeroes ()
		{
			return this.heroes.Count < MAX_HEROES;
		}
		
		public bool IsHuman ()
		{
			return controlType == CONTROL_TYPE_HUMAN;
		}

		public int MoneyCount {
			get {
				return this.moneyCount;
			}
		}
		
		public void AddMoney (int sum)
		{
			this.moneyCount += sum;
		}
		
		public void DecreaseMoney (int sum)
		{
			this.moneyCount -= sum;
		}

		public int Team {
			get {
				return this.team;
			}
			set {
				team = value;
			}
		}

		public void RemoveUnitGroup (UnitGroupSubject unitGroup)
		{
			unitsGroups.Remove (unitGroup);
			
			if (aiPlayerMind != null) {
				aiPlayerMind.OnUnitGroupRemoved (unitGroup);
			}
		}

		public void AddUnitGroup (UnitGroupSubject unitGroup)
		{
			unitsGroups.Add (unitGroup);
						
			if (aiPlayerMind != null) {
				aiPlayerMind.AddUnitGroup (unitGroup);
			}
		}
		
		public bool IsEnemyPlayer (PlayerData player)
		{
			return player.team != this.team;
		}

		public List<HeroSubject> Heroes {
			get {
				return this.heroes;
			}
		}

		public Color PlayerColor {
			get {
				return this.playerColor;
			}
			set {
				playerColor = value;
			}
		}

		public int Id {
			get {
				return this.id;
			}
		}

		public String Name {
			get {
				return this.name;
			}
		}

		public List<UnitGroupSubject> UnitsGroups {
			get {
				return this.unitsGroups;
			}
		}

		public AiMind AiPlayerMind {
			get {
				return this.aiPlayerMind;
			}
			set {
				aiPlayerMind = value;
				foreach (UnitGroupSubject unitGroup in unitsGroups) {
					aiPlayerMind.AddUnitGroup (unitGroup);
				}
			}
		}
		
		public override string ToString ()
		{
			return "Player " + id;
		}

		public bool CanSpawnUnitTheoretically (UnitType unitType)
		{
			if (unitType.IsHero && !this.CanSpawnHeroes ()) {
				return false;
			}
					
			return true;
		}
		
		public bool HasEnoughFreePlacesToSpawnUnit (UnitType unitType)
		{
			if (unitType.UseUnitPlaces <= 0) {
				return true;
			}
			if (unitType.UseUnitPlaces + usedUnitPlaces <= allUnitPlaces) {
				return true;
			}
			return false;
		}
		
		public interface AiMind
		{
			void OnPlayerStateChanged (int action);

			void OnUnitGroupRemoved (UnitGroupSubject unitGroup);

			void AddUnitGroup (UnitGroupSubject unitGroup);
			
			bool Active {
				get;
				set;
			}
		}


		public LinkedList<BuildingSubject> Buildings {
			get {
				return this.buildings;
			}
		}

		public LinkedList<BuildingSubject> Castles {
			get {
				return this.castles;
			}
		}
		
		public void AddUnit (UnitSubject unit)
		{
			if (unit.IsHero) {
				this.AddHero (unit.UnitGroup.GroupHero);			
			}
			
			
			
			if (unit.IsBuilding) {
				OnBuildingAdded (unit.BuildingData);			
			}
			
			if (!unit.IsGarrisonUnit) {
				this.usedUnitPlaces += unit.UnitType.UseUnitPlaces;
			}	
			
//			if (unit.IsBuilding) {
//				this.buildings.AddLast (unit.BuildingData);
//				if (unit.BuildingData.IsCastle) {
//					this.castles.AddLast (unit.BuildingData);
//				}
//			}
		}
		
		public void RemoveUnit (UnitSubject unit)
		{			
			if (!unit.IsGarrisonUnit) {
				this.usedUnitPlaces -= unit.UnitType.UseUnitPlaces;
			}
			
			if (unit.IsBuilding) {
				OnBuildingRemoved (unit.BuildingData);
			}
		}
		
		private void OnBuildingAdded (BuildingSubject building)
		{
			this.buildings.AddLast (building);
			if (building.IsCastle) {
				this.castles.AddLast (building);
			}
			
			if (building.IsCapital) {
				raceType = building.BuildingType.Race;
			}
			
			foreach (UnitType unit in building.BuildingType.UnitsToSpawn) {
				RaceType race = unit.Race;
				RaceData currentRaceData = this.raceData[race.Name];
				currentRaceData.AddUnitType (unit);
			}
			
			this.OnBuildingReachedLevel (building, building.BuildingLevel);
		}
		
		public void OnBuildingUpgraded (BuildingSubject building) {
			this.OnBuildingReachedLevel (building, building.BuildingLevel);
		}
		
		private void OnBuildingReachedLevel (BuildingSubject building, int level)
		{
			this.allUnitPlaces -= building.PrevLevelOfferedUnitPlaces;
			this.allUnitPlaces += building.OfferedUnitPlaces;	
			
			this.unitAttackModification -= building.PrevPlayerUnitAttackPowerModification;
			this.unitAttackModification += building.PlayerUnitAttackPowerModification;			
			this.unitProtectionModification -= building.PrevPlayerUnitProtectionPowerModification;
			this.unitProtectionModification += building.PlayerUnitProtectionPowerModification;
		
			this.spellEffectPowerModification -= building.PrevSpellEffectPowerModification;
			this.spellEffectPowerModification += building.SpellEffectPowerModification;
			this.spellSinglePowerModification -= building.PrevSpellSingleAttackPowerModification;
			this.spellSinglePowerModification += building.SpellSingleAttackPowerModification;
		}
		
		private void OnBuildingRemoved (BuildingSubject building)
		{
			this.buildings.Remove (building);
			if (building.IsCastle) {
				this.castles.Remove (building);
			}
			
			this.allUnitPlaces -= building.OfferedUnitPlaces;
			
			this.unitAttackModification -= building.PlayerUnitAttackPowerModification;			
			this.unitProtectionModification -= building.PlayerUnitProtectionPowerModification;
		
			this.spellEffectPowerModification -= building.SpellEffectPowerModification;
			this.spellSinglePowerModification -= building.SpellSingleAttackPowerModification;
			
			foreach (UnitType unit in building.UnitsToSpawn) {
				RaceType race = unit.Race;
				RaceData currentRaceData = this.raceData[race.Name];
				currentRaceData.RemoveUnitType (unit);
			}
		}

		public int AllUnitPlaces {
			get {
				return this.allUnitPlaces;
			}
		}
		
		private int powerPoints;
		
		public int PointsOfPower {
			get {
				return powerPoints;
			}
			set {
				powerPoints = value;
			}
		}
		
		public void  AddPowerPoints (int points) {
			this.powerPoints += points;
		}

		public int UsedUnitPlaces {
			get {
				return this.usedUnitPlaces;
			}
		}

		public void UseUnitPlaces (int numUnitPlacesToUse)
		{
			this.usedUnitPlaces += numUnitPlacesToUse;
		}
		
		public void FreeUnitPlaces (int numPlacesToFree)
		{
			this.usedUnitPlaces -= numPlacesToFree;
		}

		public int SpellEffectPowerModification {
			get {
				return this.spellEffectPowerModification;
			}
		}

		public int SpellSinglePowerModification {
			get {
				return this.spellSinglePowerModification;
			}
		}

		public int UnitAttackModification {
			get {
				return this.unitAttackModification;
			}
		}

		public int UnitProtectionModification {
			get {
				return this.unitProtectionModification;
			}
		}
		
		public List<UnitType> UnitsToSpawn {
			get {
				RaceData currentRaceData = this.raceData[raceType.Name];			
				return currentRaceData.UnitsToSpawn;
			}
		}
		
		public bool HasNoCapital {
			get {
				foreach (BuildingSubject building in this.buildings) {
					if (building.IsCapital) {
						return false;
					}
				}
				return true;
			}
		}

		public bool IsDefeated {
			get {
				return this.isDefeated;
			}
			set {
				isDefeated = value;
			}
		}

		public void SetAIActive (bool active)
		{
			aiPlayerMind.Active = active;
		}

		public RaceType RaceType {
			get {
				return this.raceType;
			}
		}
		
		public HeroSubject MainHero {
			get {return this.mainHero;}
		}
	}
	
	class RaceData {
		public readonly Dictionary<UnitType, int> availableUnitTypes = new Dictionary<UnitType, int> ();
		private List<UnitType> unitTypes = new List<UnitType> ();
		
		public void AddUnitType (UnitType unitType) {
			if (availableUnitTypes.ContainsKey (unitType)) {
				availableUnitTypes[unitType] = availableUnitTypes[unitType] + 1;
			} else {
				availableUnitTypes.Add (unitType, 1);
				unitTypes.Add (unitType);
			}
		}
		
		public void RemoveUnitType (UnitType unitType) {
			if (availableUnitTypes.ContainsKey (unitType)) {
				availableUnitTypes[unitType] = availableUnitTypes[unitType] - 1;
				if (availableUnitTypes[unitType] == 0) {
					availableUnitTypes.Remove (unitType);
					unitTypes.Remove (unitType);
				}
			} 
		}
		
		public List<UnitType> UnitsToSpawn {
			get {
				return unitTypes;
			}
		}
		
		
	}
}

