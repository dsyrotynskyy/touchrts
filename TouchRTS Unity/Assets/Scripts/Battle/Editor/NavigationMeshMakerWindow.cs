using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor (typeof(WorldController))]
public class NavigationMeshMakerWindow : Editor
{

	private WorldController worldController;
	
	void Start ()
	{		
	}
	
	void Update ()
	{	
	}
	
	private bool addingObstacleNodes = false;
	private bool addingNodesMode = false;
	private bool addingTriangleMode = false;
	
	public void OnEnable ()
	{		
		worldController = (WorldController)target;
		
		if (worldController.NavigationNodes.Count <= 0) {
			hideDisplay = 0;
			return;
		}
		
		bool displayingNavigation = worldController.NavigationNodes[0].GetComponent<Renderer> ().enabled;
		if (displayingNavigation) {
			hideDisplay = 0;
		} else {
			hideDisplay = 1;
		}
	}
	
	int selectionType = 0;
	int hideDisplay = 0;
	string[] names = new string[] {"None", "Obstacle", "Node", "Triangle"};
	string[] navigationDisplayNames = new string[] {"Display", "Hide"};
	
	public override void OnInspectorGUI ()
	{	     		
		worldController.UseMinimumGraphics = GUILayout.Toggle (worldController.UseMinimumGraphics, "Minimize Graphics", GUILayout.Width (255));
//		worldController.DrawOnGUI = !GUILayout.Toggle (!worldController.DrawOnGUI, "Disable Unity GUI", GUILayout.Width (255));		
		
		selectionType = GUILayout.SelectionGrid (selectionType, names , 4);
		addingTriangleMode = selectionType == 3;
		addingNodesMode = selectionType == 2;
		addingObstacleNodes = selectionType == 1;
		
		int navTrianglesCount = 0;
		if (worldController.NavigationTriangles != null) {
			navTrianglesCount = worldController.NavigationTriangles.Count;
		}
		GUILayout.Label ("Nav triangles count = " + navTrianglesCount, GUILayout.Width (255));
		GUILayout.Label ("Nav nodes count = " + worldController.NavigationNodes.Count, GUILayout.Width (255));
		GUILayout.Label ("Obstacle nodes count = " + worldController.ObstacleNodes.Count, GUILayout.Width (255));	
		
		GUILayout.Label ("Max id = " + worldController.MaxId, GUILayout.Width (255));
		
		if (GUILayout.Button ("Create Navigation Mesh", GUILayout.Width (255))) { 
			selectedNodes.Clear ();
			this.worldController.ResetNavigationMesh ();
//			worldController.CalculateWorldPathTriangles ();
			PrepareWorld ();
			selectionType = 0;
		}
		
		if (GUILayout.Button ("Assign Ids", GUILayout.Width (255))) { 
			worldController.PrepareToGame ();
			PrepareWorld ();
		}
		
		if (GUILayout.Button ("Calculate triangles dijkstra map", GUILayout.Width (255))) { 
			worldController.CalculateWorldPathTrianglesDijkstra ();
			PrepareWorld ();
		}
	
		hideDisplay = GUILayout.SelectionGrid (hideDisplay, navigationDisplayNames , 2);
		if (hideDisplay == 0) {
			this.worldController.DisplayNavigation ();
		} else {
			this.worldController.HideNavigation ();
		}
		
		SceneView.RepaintAll ();
		EditorUtility.SetDirty (worldController);
	}
	
	private void PrepareWorld () {
		worldController.MaxId = 10;
		
//		PlayerSubject[] players = FindObjectsOfType (typeof (PlayerSubject)) as PlayerSubject[];
//		foreach (PlayerSubject player in players) {
//			player.Id = worldController.CreateId ();
//			EditorUtility.SetDirty (player);
//		}
		HeroSubject[] heroes = FindObjectsOfType (typeof (HeroSubject)) as HeroSubject[];
		foreach (HeroSubject hero in heroes) {
			hero.id = worldController.CreateId ();
			EditorUtility.SetDirty (hero);
		}
		
		BuildingSubject[] buildings = FindObjectsOfType (typeof (BuildingSubject)) as BuildingSubject[];
		foreach (BuildingSubject building in buildings) {
			building.id = worldController.CreateId ();
			EditorUtility.SetDirty (building);
		}
		
		UnitSubject[] units = FindObjectsOfType (typeof (UnitSubject)) as UnitSubject[];
		foreach (UnitSubject unit in units) {
			unit.id = worldController.CreateId ();
			EditorUtility.SetDirty (unit);
		}
		
		NavigationTriangle[] triangles = FindObjectsOfType (typeof (NavigationTriangle)) as NavigationTriangle[];
		foreach (NavigationTriangle triangle in triangles) {
			EditorUtility.SetDirty (triangle);
		}
		EditorUtility.SetDirty (worldController);
	}
	
	public void OnSceneGUI ()
	{	
		Event e = Event.current;
	
		Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
		
		RaycastHit hit;
		if ((e.type == EventType.mouseDown) && (Event.current.button != 1 && Event.current.button != 10)) {
			if (Physics.Raycast (ray.origin, ray.direction, out hit)) {	
				if (hit.collider.gameObject.tag == Setup.GameSettings.Tag.GROUND || hit.collider.gameObject.tag == "Untagged") {
					Vector3 point = hit.point;	
					if (addingNodesMode) {
						this.worldController.AddNavigationNode (point, false);	
					}
					if (addingObstacleNodes) {
						this.worldController.AddNavigationNode (point, true);
					}
					if (addingTriangleMode) {
						this.worldController.AddNavigationTriangle (selectedNodes);
						foreach (NavigationNode node in selectedNodes) {
							node.Deselect ();
						}
						selectedNodes.Clear ();
					}
				}
				
				if (hit.collider.gameObject.tag == Setup.GameSettings.Tag.NAVIGATION) {
					NavigationNode node = hit.collider.gameObject.GetComponent<NavigationNode> ();
					if (node != null) {
						node.IsSelected = !node.IsSelected;
						if (node.IsSelected) {
							selectedNodes.AddLast (node);
							
							if (selectedNodes.Count > 3) {
								NavigationNode nodeToRemove = selectedNodes.First.Value;
								nodeToRemove.Deselect ();
								selectedNodes.RemoveFirst ();
							}
							
						} else if (selectedNodes.Contains (node)) {
							node.Deselect ();
							selectedNodes.Remove (node);
						}
					}
				}						
			}
			e.Use ();
			EditorUtility.SetDirty (target);
		}
		
	}
	
	private LinkedList<NavigationNode> selectedNodes = new LinkedList<NavigationNode> ();
}
