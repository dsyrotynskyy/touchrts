using System;

namespace UI
{
	public interface CreationWindow : GameWidget
	{
		void EnableAllButtons (bool enable);
		void EnableCloseButton (bool enable);
		void EnableActionButton (bool enable);
		void SelectElement (string element);
	}
}

