using System;

namespace UI
{
	public interface SpellPanel : GameWidget
	{
		void OnDisableSpellSpawnInput ();
		
		void LoadHeroSpells ();
		void SetEnabled (bool enabled);
	}
}

