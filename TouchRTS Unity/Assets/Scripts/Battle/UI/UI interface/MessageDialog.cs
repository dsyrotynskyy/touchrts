using System;

namespace UI
{
	public interface MessageDialog : GameWidget
	{
		void Display (string message, string id);
		void Display (MessageDialogData messageDialogData);
	}
	
	public struct MessageDialogData {
		public MessageDialogData (string message, string id, OnDialogClosedAction action) {
			this.message = message;
			this.id = id;
			this.action = action;
		}
		
		public string message;
		public string id;
		public OnDialogClosedAction action;
		
		public delegate void OnDialogClosedAction ();
	}
}

