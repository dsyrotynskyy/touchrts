using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
	public interface UIController
	{
		void Init (WorldController worldController);
		void OnUpdate ();
		bool IsDialogShowing ();
		bool CoordCanBeDisplayed (Vector3 coord);
		
		MessageDialog MessageDialog {
			get;
		}
		
		CastleContstructorDialog ConstructionOutbuildingWindow {
			get;
		}
		
		SpellStudyDialog SpellStudyWindow {
			get ;
		}
	
		SpawnUnitDialog SpawnUnitWindow {
			get;
		}
	
		UnitRepartitionDialog UnitRepartitionWindow {
			get ;
		}
		
		SpellPanel SpellPanel {
			get;
		}
		
				/** True if click handled by UI
		 * 
		 * */		
		bool IsUIClick {
			get;
		}
		
		bool SetUIHandle {
			set;
		}
		
		/**
		 * Use this function if You are handling GUI click
		 * */
		void AddUIListener (String guiKey)
		;
		
		/*
		 * Use this function if You stop handling GUI click
		 * */
		void RemoveUIListener (String guiKey)
		;
		
		void CreateUnitControlPanel (UnitSubject unit);
		
		void CreateUnitInfoView (UnitSubject unit);
		
		void DisplayMenu ();
		
		void OnPlayerWin (GameMapScenario.MapScenario mapScenario);
		
		void OnPlayerFail (GameMapScenario.MapScenario mapScenario);
	}
}

