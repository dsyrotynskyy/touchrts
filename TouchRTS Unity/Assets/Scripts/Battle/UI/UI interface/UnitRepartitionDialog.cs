using System;

namespace UI
{
	public interface UnitRepartitionDialog : GameWidget
	{
		void DisplayGroups (UnitGroupSubject group1, UnitGroupSubject group2);
	}
}

