using System;

namespace UI
{
	public interface SpellStudyDialog : CreationWindow
	{
		void DisplaySpellStudy (BuildingSubject building, HeroSubject currentHero);
	}
}

