using System;
namespace UI {
	public interface CastleContstructorDialog : CreationWindow
	{
		void DisplayCastleConstructor (BuildingSubject buildingSubject);
	}
}

