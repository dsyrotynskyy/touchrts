using System;
using UnityEngine;

namespace UI
{
	public interface GameWidget
	{
		Rect GetScreenArea();
		bool IsDisplaying();
		bool IsDialog();
		void OnUpdate ();
	}
}

