using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
	public abstract class AbstractUIController : UIController
	{
		protected SpawnUnitDialog spawnUnitWindow;
		protected CastleContstructorDialog castleConstructionWindow;
		protected UnitRepartitionDialog unitRepartitionWindow;
		protected SpellStudyDialog spellWindow;
		
		protected SpellPanel spellControlPanel;
		protected GameWidget menuButton;
		protected GameWidget miniMap;
		protected GameWidget manaBar;
		protected GameWidget playerMoneyLabel;
		protected GameWidget heroesControlPanel;
		protected readonly List<GameWidget> gameWidgets = new List<GameWidget> ();
		protected readonly List<GameWidget> dialogs = new List<GameWidget> ();
		
		public AbstractUIController ()
		{
		}

		#region UIController implementation

		public void OnUpdate ()
		{
			foreach (GameWidget widget in gameWidgets) {
				widget.OnUpdate ();
			}
		}
		
		public bool IsDialogShowing ()
		{
			return unitRepartitionWindow.IsDisplaying () || spawnUnitWindow.IsDisplaying () || castleConstructionWindow.IsDisplaying () || spellWindow.IsDisplaying ();
		}

		public bool CoordCanBeDisplayed (Vector3 coord)
		{
			if (coord.x > 0 && coord.x < Screen.width && coord.y > 0 && coord.y < Screen.height) {
			} else {
				return false;
			}
		
			Vector3 mapCoord = new Vector3 (coord.x, Mathf.Abs (coord.y - Camera.main.pixelHeight), 0);
		
			Rect miniMap = MiniMap.MiniMapFrameRect;
			if (DoesRectCoversCoord (miniMap, mapCoord)) {
				return false;
			}
		
			foreach (GameWidget widget in gameWidgets) {
				if (widget.IsDisplaying ()) {
					Rect widgetRect = widget.GetScreenArea ();
					if (DoesRectCoversCoord (widgetRect, mapCoord)) {
						return false;
					}
				}
			}				
			return true;
		}
	
		private static bool DoesRectCoversCoord (Rect rect, Vector3 coord)
		{
			bool coversX = rect.x < coord.x && coord.x < rect.x + rect.width;
			bool coversY = rect.y < coord.y && coord.y < rect.y + rect.height;
			return coversX && coversY;
		}

		public CastleContstructorDialog ConstructionOutbuildingWindow {
			get {
				return castleConstructionWindow;
			}
		}

		public SpellStudyDialog SpellWindow {
			get {
				return spellWindow;
			}
		}

		public SpawnUnitDialog SpawnUnitWindow {
			get {
				return spawnUnitWindow;
			}
		}

		public UnitRepartitionDialog UnitRepartitionWindow {
			get {
				return unitRepartitionWindow;
			}
		}
		#endregion

		#region UIController implementation
		public abstract void Init (WorldController worldController)
		;
		#endregion
		
		private static HashSet<string> guis = new HashSet<string> ();
						
		/** True if click handled by UI
		 * 
		 * */		
		public bool IsUIClick {
			get { return guis.Count != 0; }
		}
		
		/**
		 * Use this function if You are handling GUI click
		 * */
		public void AddUIListener (String guiKey)
		{			
			guis.Add (guiKey);
		}
		
		/*
		 * Use this function if You stop handling GUI click
		 * */
		public void RemoveUIListener (String guiKey)
		{
			if (guis.Count == 0) {
				return;
			}
			guis.Remove (guiKey);
		}

		#region UIController implementation
		public bool SetUIClick {
			set {
				throw new NotImplementedException ();
			}
		}
		#endregion

		#region UIController implementation
		public bool SetUIHandle {
			set {
				throw new NotImplementedException ();
			}
		}
		#endregion

		#region UIController implementation
		public void CreateUnitControlPanel (UnitSubject unit)
		{
		}
		#endregion

		#region UIController implementation
		public SpellPanel SpellPanel {
			get {
				return spellControlPanel;
			}
		}
		#endregion

		#region UIController implementation
		public void DisplayMenu ()
		{
		}
		#endregion


		#region UIController implementation
		public void CreateUnitInfoView (UnitSubject unit)
		{
		}
		#endregion


		#region UIController implementation
		public void DisplayMessageDialog (string message, string portraitId)
		{
		}
		#endregion

		#region UIController implementation
		public MessageDialog MessageDialog {
			get {
				return null;
			}
		}
		#endregion

		#region UIController implementation
		public void OnPlayerWin (GameMapScenario.MapScenario mapScenario)
		{
		}

		public void OnPlayerFail (GameMapScenario.MapScenario mapScenario)
		{
		}
		#endregion

		#region UIController implementation
		public SpellStudyDialog SpellStudyWindow {
			get {
				throw new NotImplementedException ();
			}
		}
		#endregion
	}
}

