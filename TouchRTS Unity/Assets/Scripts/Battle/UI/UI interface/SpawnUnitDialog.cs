using System;

namespace UI
{
	public interface SpawnUnitDialog : CreationWindow
	{
		void DisplaySpawnUnit (BuildingSubject building);
		
	}
}

