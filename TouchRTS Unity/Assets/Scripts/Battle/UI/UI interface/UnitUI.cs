using System;

namespace UI
{
	public interface UnitUI
	{
		void Init (UnitSubject unit);
		void OnUpdate ();
	}
}

