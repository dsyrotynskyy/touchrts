using UnityEngine;
using System.Collections;
using UI;

public class MiniMap : MonoBehaviour, GameWidget
{
	public static bool IsMinimapTouch = false;
	public static MiniMap Instance;
	private Camera miniMapCamera;
	private Vector3 clickedPoint = Vector3.zero;
	private Vector2 lastPos = Vector2.zero;
	public float miniMapWidth = 200;
	public float miniMapHeight = 200;
	private float offset = 5;
	public Texture miniMapFrame;
	private Rect miniMapRect = new Rect ();
	private static Rect miniMapFrameRect = new Rect ();
	private GameObject mainCameraMark;
    
	public void Init (WorldController worldController)
	{
		Instance = this;
		
		miniMapHeight = 3 * Screen.height / 10;
		miniMapWidth = miniMapHeight;
		
//		miniMapHeight = 123;
//		miniMapWidth = 123;
		
		miniMapCamera = this.camera;
		miniMapCamera.pixelRect = new Rect (offset, offset, miniMapWidth, miniMapHeight);
		//Loading frame texture for mini-map
		miniMapFrame = (Texture)Resources.Load ("UI/Icons/" + "EmptySquare");
		//Creating rect for mini-map which determines it position on screen
		miniMapRect = new Rect (5, 5, miniMapWidth, miniMapHeight);
		//Rect for frame around mini-map
		miniMapFrameRect = new Rect (offset, Screen.height - offset - miniMapHeight, miniMapWidth + offset, miniMapHeight + offset);
		//////
		float x = WorldController.Instance.WorldData.XSize;
		float z = WorldController.Instance.WorldData.ZSize;

		//Setting mini-map
		miniMapCamera.nearClipPlane = -50;
		miniMapCamera.farClipPlane = 100;
		miniMapCamera.orthographicSize = 50 * z / 108;
		if (WorldController.Instance.WorldData.ZSize < 110) {
			miniMapCamera.transform.position = new Vector3 ((x / 2) + 2.8f, 25, (z / 2) - 1.5f);
			miniMapCamera.orthographicSize = 50 * z / 129;
		} else {
			miniMapCamera.transform.position = new Vector3 (x / 2, 25, z / 2);
			miniMapCamera.orthographicSize = 50 * z / 108;
		}
		CreateMainCameraMark ();
	}

	public void OnUpdate ()
	{
		CheckMiniCameraPosition ();  
		MoveCamera ();		
	}

	private void MoveCamera ()
	{
		if (Input.mousePosition.x < miniMapWidth + offset && Input.mousePosition.y < miniMapHeight + offset && Input.GetMouseButton (0)) {
			IsMinimapTouch = true;
			//			int TerrainLayer = 1 << 10;
//			RaycastHit hitInfo;
//			Ray ray = miniMapCamera.ScreenPointToRay (Input.mousePosition);
//			Physics.Raycast (ray, out hitInfo, Mathf.Infinity, TerrainLayer);
//			Debug.DrawLine (ray.origin, hitInfo.point);
//			
//			clickedPoint = new Vector3 (hitInfo.point.x, mainCamera.transform.position.y, hitInfo.point.z);

			float xPosMouse = Input.mousePosition.x;
			float yPosMouse = Input.mousePosition.y;
			
			float xFloat = xPosMouse * WorldController.Instance.WorldData.XSize / miniMapWidth;
			float yFloat = yPosMouse * WorldController.Instance.WorldData.ZSize / miniMapHeight;
			
			clickedPoint = new Vector3 (xFloat, NavigationEngine.TerrainNormalHeight, yFloat);
			
			float miniMapOfset = Screen.height * 10 / 1000;
			InputController.SetRTSControlType ();
			InputController.RTSCamera.MoveCameraToCoord (new Vector3 (clickedPoint.x, clickedPoint.y, clickedPoint.z - miniMapOfset));
 		} else {
			IsMinimapTouch = false;
		}
	}

	private void CheckMiniCameraPosition ()
	{
		if (miniMapCamera == null)
			return;

		Vector2 currentPos = new Vector2 (Screen.width, Screen.height);
		if ((lastPos - currentPos).magnitude > 2) {
			lastPos = currentPos;
			miniMapCamera.pixelRect = miniMapRect;
		}
	}

	private void CreateMainCameraMark ()
	{
		Transform cameraTransform = Camera.mainCamera.transform;
		mainCameraMark = (GameObject)Instantiate (Resources.Load ("Prefabs/Effects/" + "Mark"),
        Vector3.zero, Quaternion.Euler (0, 0, 0));
//		mainCameraMark.transform.localScale = new Vector3 (25, 25, 25);
		mainCameraMark.layer = 12;
        
		mainCameraMark.transform.parent = cameraTransform;
		mainCameraMark.transform.localPosition = new Vector3 (-25,-15,0);
		mainCameraMark.transform.localRotation = Quaternion.Euler (30, 0, 0);
		Debug.Log ("Mark = " + mainCameraMark.transform.position);
	}
	
	public static Rect MiniMapFrameRect {
		get { return miniMapFrameRect;}
	}

	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		return miniMapFrameRect;
	}

	public bool IsDisplaying ()
	{
		return true;
	}

	public bool IsDialog ()
	{
		return false;
	}
	#endregion
}
