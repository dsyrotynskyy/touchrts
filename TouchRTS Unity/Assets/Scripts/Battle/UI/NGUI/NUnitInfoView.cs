using UnityEngine;
using System.Collections;

public class NUnitInfoView : NUnitUIElement
{
	private UISlider slider;
	private UISprite backgroundSprite;
	
	public GameObject Background;
	
	#region implemented abstract members of NUnitUIElement
	protected override void Prepare ()
	{
		this.slider = gameObject.GetComponent<UISlider> ();	
		this.backgroundSprite = this.Background.GetComponent<UISprite> ();
	}
	#endregion

	protected override void DoUpdateLogic () {
		this.slider.sliderValue = this.UnitOwner.GetLifePercent ();
		this.slider.ForceUpdate ();
		this.backgroundSprite.MarkAsChanged ();
	}
	
	protected override void PositionGameObject () {
		float xScale = 0;
		float yScale = 0;
		
		xScale = 300 * ((2 * UnityEngine.Screen.width + 800f) / 3000);
		yScale = 300 * ((3 * UnityEngine.Screen.height + 1000f) / 2000);
		
		screenPos.Set ((screenPos.x - UnityEngine.Screen.width / 2) / xScale, (screenPos.y - UnityEngine.Screen.height / 1.5f) / yScale, -1f);	
	}

	protected override void SetContentVisible (bool visible)
	{
		slider.gameObject.SetActive (visible);
	}
}
