using UnityEngine;
using System.Collections;

public class AutoTweener
{
	protected UITweener tween;
	protected GameObject gameObject;
	protected bool active;
	// Use this for initialization
	public AutoTweener (GameObject gameObject) {
		this.gameObject = gameObject;
		tween = GetTweener();
		active = this.gameObject.activeSelf;
	}
	
	public virtual UITweener GetTweener () {
		return gameObject.GetComponent<UITweener>();
	}
	
	public void Activate () {	
		gameObject.SetActive (true);	
		
//		tween.Reset();
		tween.Play(true);
		tween.onFinished = null;
	}
	
	private void OnDeactivated (UITweener tweener) {
		gameObject.SetActive (false);
		tween.onFinished = null;
	}
	
	public void Deactivate () {	
//		tween.Reset();
		tween.Play(false);
		
		tween.onFinished = OnDeactivated;
	}
	
	public virtual void SetActive (bool newActive) {
		if (newActive != active) {
			active = newActive;
		} else {
			return;
		}
		if (newActive) {
			Activate ();
		} else {
			Deactivate ();
		}
	}
}

public class PositionTweener : AutoTweener {
	public PositionTweener (GameObject gameObject) : base (gameObject) {
		
	}
	
	public virtual UITweener GetTweener () {
		TweenPosition tween = gameObject.GetComponent<TweenPosition>();
		tween.to = this.gameObject.transform.position;
		return tween;
	}
	
	public virtual void SetActive (bool newActive) {
//		if (newActive == active) {
//			return;
//		}
//		tween.Reset ();
		base.SetActive (newActive);
	}
}

