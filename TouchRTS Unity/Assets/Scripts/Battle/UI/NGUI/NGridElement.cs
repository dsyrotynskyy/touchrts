using UnityEngine;
using System.Collections;

public class NGridElement : MonoBehaviour
{
	private int position;
	protected UISlicedSprite sprite;
	
	public virtual void Init ()
	{
		sprite = this.transform.FindChild ("thumb").GetComponent <UISlicedSprite> ();
	}
	
	public void SetOnClickListener (OnClick OnClick) {
		UIEventListener.Get (this.gameObject).onClick += delegate(GameObject go)
		{
			OnClick ();
		};
	}
	
	public delegate void OnClick ();

	public int Position {
		get {
			return this.position;
		}
		set {
			position = value;
		}
	}
	
	protected void SetSprite (string atlasName, string spriteName) {
		UIAtlas atlas = NGUIController.Instance.AtlasMap[atlasName];
		
		this.sprite.atlas = atlas;
		this.sprite.spriteName = spriteName;
		this.sprite.sprite = atlas.GetSprite (spriteName);
		this.sprite.MarkAsChanged ();
	}
	
	protected void SetSprite (string spriteName) {
		this.sprite.sprite = this.sprite.atlas.GetSprite (spriteName);
		this.sprite.MarkAsChanged ();
	}
	
	public virtual void OnUpdate () {
	}
}

