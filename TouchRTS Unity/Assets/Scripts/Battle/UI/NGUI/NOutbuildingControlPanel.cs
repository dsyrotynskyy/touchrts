using UnityEngine;
using System.Collections;
using UI;

public class NOutbuildingControlPanel : NUnitUIElement
{	
	public static bool UpgradeButtonEnabled = true;
	
	public GameObject UpgradeButton;
	
	private UIButton upgradeButton;
	private BuildingSubject ourBuilding;
	
	public NOutbuildingControlPanel () {
		UpgradeButtonEnabled = true;
	}
	
	protected override void Prepare () {
		ourBuilding = UnitOwner.BuildingData;
		upgradeButton = UpgradeButton.GetComponent<UIButton> ();
		
		UIEventListener.Get (UpgradeButton).onClick += delegate(GameObject go)
		{
			ServerCommandController.Instance.StartUpgradeBuilding (ourBuilding);
		};
				
		CollectSprites (UpgradeButton);		
	}
	
	protected override void DoUpdateLogic () {
		if (ourBuilding.HasUpgradePossibility) {
			UpgradeButton.SetActive (true);
		} else {
			UpgradeButton.SetActive (false); 
			return;
		}
		
		bool enabled = ourBuilding.CanUpgrade;
		upgradeButton.isEnabled = enabled;
	}

	protected override void SetContentVisible (bool visible)
	{
		UpgradeButton.SetActive (visible);
	}
}

