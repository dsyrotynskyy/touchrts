using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;

public class NCastleControlPanel : NUnitUIElement
{
	public static bool ConstructionButtonEnabled = true;
	public static bool UnitTrainButtonEnabled = true;
	public static bool SpellStudyButtonEnabled = true;
	public static bool HeroResurrectionButtonEnabled = true;
	
	public GameObject ConstructionButton;
	public GameObject UnitTrainButton;
	public GameObject SpellStudy;
	public GameObject HeroResurrectionButton;
	
//	private UIButton constructionButton;
//	private UIButton unitTrainButton;
//	private UIButton spellStudyButton;
	private UIButton heroResurrectionButton;
	private BuildingSubject ourBuilding;
	
	private Timer updateTimer = Timer.CreateFrequency (500);
	
	public NCastleControlPanel () {
		updateTimer.ForceTime ();
		
		ConstructionButtonEnabled = true;
		UnitTrainButtonEnabled = true;
		SpellStudyButtonEnabled = true;
		HeroResurrectionButtonEnabled = true;
	}
	
	protected override void Prepare () {
		ourBuilding = UnitOwner.BuildingData;
		
//		constructionButton = ConstructionButton.GetComponent<UIButton> ();
//		unitTrainButton = UnitTrainButton.GetComponent<UIButton> ();
//		spellStudyButton = SpellStudy.GetComponent<UIButton> ();
		heroResurrectionButton = HeroResurrectionButton.GetComponent<UIButton> ();
		
		UIEventListener.Get (ConstructionButton).onClick += delegate(GameObject go)
		{
			this.OnConstructionButtonClick ();
		};
		
		UIEventListener.Get (UnitTrainButton).onClick += delegate(GameObject go)
		{
			this.OnUnitTrainButtonClick ();
		};
		
		UIEventListener.Get (SpellStudy).onClick += delegate(GameObject go)
		{
			this.OnSpellStudyButtonClick ();
		};
		
		UIEventListener.Get (HeroResurrectionButton).onClick += delegate(GameObject go)
		{
			this.OnHeroResurrectionButtonClick ();
		};
		
		CollectSprites (HeroResurrectionButton);
		CollectSprites (SpellStudy);
		CollectSprites (UnitTrainButton);
		CollectSprites (ConstructionButton);		
	}
	
	private void OnConstructionButtonClick () {
		InputController.UI.ConstructionOutbuildingWindow.DisplayCastleConstructor (ourBuilding);
	}
	
	private void OnUnitTrainButtonClick () {
		InputController.UI.SpawnUnitWindow.DisplaySpawnUnit (ourBuilding);
	}
	
	private void OnSpellStudyButtonClick () {
		InputController.UI.SpellStudyWindow.DisplaySpellStudy (ourBuilding, InputController.CurrentHero);	
	}
	
	private void OnHeroResurrectionButtonClick () {
		ServerCommandController.Instance.Resurrect(InputController.CurrentHero);
	}
	
	
	protected override void DoUpdateLogic () {
		UnitTrainButton.SetActive (this.ourBuilding.CanSpawnUnits && UnitTrainButtonEnabled);
		ConstructionButton.SetActive (this.ourBuilding.CanConstructStructures && ConstructionButtonEnabled);	
		SpellStudy.SetActive (this.ourBuilding.HasMageGuild && InputController.CurrentHero != null && SpellStudyButtonEnabled);

		if (updateTimer.EnoughTimeLeft ()) {
			this.UpdateHeroResurrectionButton ();
		}
	}
	
	private void UpdateHeroResurrectionButton () {
		if (ourBuilding.CanResurrect && 
			InputController.CurrentUnitGroup != null &&
			InputController.CurrentUnitGroup.IsHeroGroup &&
			InputController.CurrentUnitGroup.GroupHero.IsDead &&
			HeroResurrectionButtonEnabled) {
			
			HeroResurrectionButton.SetActive (true);
			
			bool isHeroCloseEnough = InputController.CurrentUnitGroup.IsEnoughCloseForInteraction (this.ourBuilding.BuildingUnitGroup);
			heroResurrectionButton.isEnabled = isHeroCloseEnough;
		} else {
			HeroResurrectionButton.SetActive (false);
		}
	}

	protected override void SetContentVisible (bool visible)
	{
		ConstructionButton.SetActive (visible);
		UnitTrainButton.SetActive (visible);
		SpellStudy.SetActive (visible);
		HeroResurrectionButton.SetActive (visible);	
	}
}

