using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;
using System;

public class NGUIController : MonoBehaviour, UIController
{
	private static NGUIController instance;
		
	public GameObject CommonCreationWindow;
	public GameObject HeroIcon;
	public GameObject PauseButton;
	public GameObject SpellPanelWidget;
	public GameObject PlayerInfoLabel;
	public GameObject GamePauseMenu;	
	public GameObject GameMessageDialog;
	public GameObject GemElementDataWindow;
	
	public GameObject AtlasContainer;	
	public GameObject UnitUIOriginal;	
	public GameObject CastleControlPanelOriginal;
	public GameObject OutbuildingUpgradePanelOriginal;
	public GameObject UnitInfoView;
	
	private readonly Dictionary<string, UIAtlas> atlasMap = new Dictionary<string, UIAtlas> ();
	
	private bool isMouseOver;
	
	public void Start ()
	{			
		if (instance != this) {
			instance = this;
		}
	}
	
	#region Initialisation
	public void Init (WorldController worldController) {
		if (instance != this) {
			instance = this;
		}
		
		this.PrepareAtlasMap ();
		
//		this.CreateMiniMap (worldController);
		
		NHeroPanel heroPanel = HeroIcon.GetComponent<NHeroPanel> ();
		heroPanel.Init (worldController);
		gameWidgets.Add (heroPanel);
		
		NCommonCreationWindow nCommonCreationWindow = CommonCreationWindow.GetComponent<NCommonCreationWindow> ();
		nCommonCreationWindow.Init (worldController);
		gameWidgets.Add (nCommonCreationWindow);
		
		NSpellPanel nSpellPanel = SpellPanelWidget.GetComponent<NSpellPanel> ();
		nSpellPanel.Init (worldController);
		gameWidgets.Add (nSpellPanel);
		
		NGamePlayerInfoLabel nPlayerInfoLabel = PlayerInfoLabel.GetComponent<NGamePlayerInfoLabel> ();
		nPlayerInfoLabel.Init (worldController);
		gameWidgets.Add (nPlayerInfoLabel);
		
		NPauseButton nPauseButton = PauseButton.GetComponent <NPauseButton> ();
		nPauseButton.Init (worldController);
		gameWidgets.Add (nPauseButton);
		
		this.gemElementWindow = GemElementDataWindow.GetComponent <NGemElementDataWindow> ();
		this.gemElementWindow.Init (worldController);
		gameWidgets.Add (this.gemElementWindow);
		
		this.gamePauseMenu = GamePauseMenu.GetComponent <NPauseMenu> ();
		this.gamePauseMenu.Init (worldController);
		
		NMessageDialog nMessageDialog = GameMessageDialog.GetComponent<NMessageDialog> ();
		nMessageDialog.Init (worldController);
		
		this.messageDialog = nMessageDialog;
		this.spellPanel = nSpellPanel;
		this.spellWindow = nCommonCreationWindow;
		this.spawnUnitWindow = nCommonCreationWindow;
		this.castleConstructionWindow = nCommonCreationWindow;
	}
	
	private void CreateMiniMap (WorldController worldController) {
		GameObject miniMapObject = GameObject.Find ("MiniMap");
		if (miniMapObject == null) {
			miniMapObject = ObjectController.CreatePrefab ("MiniMap", "Input");			
		}
		miniMap = miniMapObject.GetComponent<MiniMap> ();
		miniMap.Init (worldController);
		gameWidgets.Add (miniMap);
	}
	
	private void PrepareAtlasMap () {
		UIAtlas[] atlasObjects = AtlasContainer.GetComponentsInChildren<UIAtlas> ();
		foreach (UIAtlas atlas in atlasObjects) {
			atlasMap.Add (atlas.name, atlas);
		}
	}
	#endregion
			
	private NGemElementDataWindow gemElementWindow;
	private NPauseMenu gamePauseMenu;
	
	protected SpawnUnitDialog spawnUnitWindow;
	protected CastleContstructorDialog castleConstructionWindow;
	protected UnitRepartitionDialog unitRepartitionWindow;
	protected SpellStudyDialog spellWindow;
	protected SpellPanel spellPanel;
	protected GameWidget spellControlPanel;
	protected GameWidget menuButton;
	protected MiniMap miniMap;
	protected GameWidget manaBar;
	protected GameWidget playerMoneyLabel;
	protected GameWidget heroesControlPanel;
	protected MessageDialog messageDialog;
	protected readonly List<GameWidget> gameWidgets = new List<GameWidget> ();
	protected readonly List<GameWidget> dialogs = new List<GameWidget> ();
	protected readonly List<UnitUI> unitUI = new List<UnitUI> ();

	#region UIController implementation

	public Dictionary<string, UIAtlas> AtlasMap {
		get {
			return this.atlasMap;
		}
	}

	public void OnUpdate ()
	{
		foreach (GameWidget widget in gameWidgets) {
			widget.OnUpdate ();
		}
		
		foreach (UnitUI uiElement in unitUI) {
			uiElement.OnUpdate ();
		}
	}

	public bool IsDialogShowing ()
	{
		return spawnUnitWindow.IsDisplaying () || castleConstructionWindow.IsDisplaying () || spellWindow.IsDisplaying () || gamePauseMenu.gameObject.activeSelf || NCastleGemController.Opened;
	}
	
	public bool DisableInputInteraction () {
		return IsDialogShowing () || gemElementWindow.IsDisplaying (); 
	}

	public bool CoordCanBeDisplayed (Vector3 coord)
	{
		const int OFFSET = 100;
		if (coord.x > -OFFSET && coord.x < Screen.width + OFFSET && coord.y > -OFFSET && coord.y < Screen.height + OFFSET) {
		} else {
			return false;
		}
		
//		Vector3 mapCoord = new Vector3 (coord.x, Mathf.Abs (coord.y - Camera.main.pixelHeight), 0);
		
//		Rect miniMap = MiniMap.MiniMapFrameRect;
//		if (DoesRectCoversCoord (miniMap, mapCoord)) {
//			return false;
//		}
		
//		foreach (GameWidget widget in gameWidgets) {
//			if (widget.IsDisplaying ()) {
//				Rect widgetRect = widget.GetScreenArea ();
//				if (DoesRectCoversCoord (widgetRect, mapCoord)) {
//					return false;
//				}
//			}
//		}				
		return true;
	}
	
	private static bool DoesRectCoversCoord (Rect rect, Vector3 coord)
	{
		bool coversX = rect.x < coord.x && coord.x < rect.x + rect.width;
		bool coversY = rect.y < coord.y && coord.y < rect.y + rect.height;
		return coversX && coversY;
	}

	public CastleContstructorDialog ConstructionOutbuildingWindow {
		get {
			return castleConstructionWindow;
		}
	}

	public SpawnUnitDialog SpawnUnitWindow {
		get {
			return spawnUnitWindow;
		}
	}

	public UnitRepartitionDialog UnitRepartitionWindow {
		get {
			return unitRepartitionWindow;
		}
	}
	#endregion
						
	/** True if click handled by UI
		 * 
		 * */		
	public bool IsUIClick {
		get { return isUIHandle || IsMouseOver || UICamera.touchCount > 0; }
	}
		
	/**
		 * Use this function if You are handling GUI click
		 * */
	public void AddUIListener (String guiKey)
	{			
		throw new System.NotImplementedException ();
	}
		
	/*
		 * Use this function if You stop handling GUI click
		 * */
	public void RemoveUIListener (String guiKey)
	{
		throw new System.NotImplementedException ();
	}
	
	private bool isUIHandle = false;

	public bool SetUIHandle {
		set {
			this.isUIHandle = value;
		}
	}
	
	public void OnDestroy () {
		if (instance == this) {
			instance = null;
		}
	}

	public static NGUIController Instance {
		get {
			return instance;
		}
	}
	
	Vector3 newScale = new Vector3 (1.0f, 1.0f, 1.0f);
	
	#region UIController implementation
	public void CreateUnitControlPanel (UnitSubject unit)
	{
		GameObject parent = UnitUIOriginal;
		GameObject original;
		if (unit.BuildingData.IsCastle) {
			original = CastleControlPanelOriginal;			
		} else {
			return;
		}
		GameObject obj = (GameObject) GameObject.Instantiate (original);
		obj.transform.parent = parent.transform;
		obj.transform.localScale = newScale;
		obj.SetActive (true);
		UnitUI panel = obj.GetComponent<NUnitUIElement> ();
		
		panel.Init (unit);
		this.unitUI.Add (panel);			
	}
	
	public SpellPanel SpellPanel {
		get {
			return this.spellPanel;
		}
	}
	#endregion

	public bool IsMouseOver {
		get {
			
			return this.isMouseOver;
		}
		set {
			isMouseOver = value;
		}
	}

	public void DisplayMenu ()
	{
		if (gamePauseMenu.gameObject.activeInHierarchy) {
			gamePauseMenu.CloseGameMenu ();
		} else {
			gamePauseMenu.OpenGameMenu ();
		}
	}

	#region UIController implementation
	public void CreateUnitInfoView (UnitSubject unit)
	{
//		GameObject parent = UnitUIOriginal;
//		GameObject original = UnitInfoView;
//		string name;
//		
//		GameObject obj = (GameObject) GameObject.Instantiate (original);
//		obj.transform.parent = parent.transform;
//		obj.transform.localScale = newScale;
//		obj.SetActive (true);
//		
//		UnitUI panel = obj.GetComponent<NUnitUIElement> ();
//		
//		panel.Init (unit);
//		this.unitUI.Add (panel);
	}
	#endregion

	#region UIController implementation
	public MessageDialog MessageDialog {
		get {
			return this.messageDialog;
		}
	}
	#endregion

	#region UIController implementation
	public void OnPlayerWin (GameMapScenario.MapScenario mapScenario)
	{
	}

	public void OnPlayerFail (GameMapScenario.MapScenario mapScenario)
	{
	}
	
	public SpellStudyDialog SpellStudyWindow {
		get {
			return spellWindow;
		}
	}
	#endregion

	public NGemElementDataWindow GemElementDescriptionWindow {
		get {
			return this.gemElementWindow;
		}
	}
}

