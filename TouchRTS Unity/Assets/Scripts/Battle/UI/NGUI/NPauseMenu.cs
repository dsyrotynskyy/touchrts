using UnityEngine;
using System.Collections;
using Setup;

public class NPauseMenu : MonoBehaviour
{	
	public GameObject Scene1;
	public GameObject Scene2;
	public GameObject MainMenu;
	public GameObject ExitButton;
	public GameObject CloseButton;
	
	private AutoTweener tween;
	
	public void Init (WorldController controller) {
		tween = new AutoTweener (gameObject);
		
		UIEventListener.Get (CloseButton).onClick += delegate(GameObject go)
		{
			CloseGameMenu ();
		};
		
		UIEventListener.Get (ExitButton).onClick += delegate(GameObject go)
		{
			Application.Quit();
		};
		
		UIEventListener.Get (MainMenu).onClick += delegate(GameObject go)
		{
			ApplicationManager.Instance.LoadScene ("MainMenu");
		};
		
		UIEventListener.Get (Scene1).onClick += delegate(GameObject go)
		{
			ApplicationManager.Instance.LoadScene ("Battle Scene 1");
		};
		
		UIEventListener.Get (Scene2).onClick += delegate(GameObject go)
		{
			ApplicationManager.Instance.LoadScene ("Tutorial Scene 1");
		};
	}
	
	public void OpenGameMenu () {	
		WorldController.Instance.PauseGameUpdate ();		
		
		
		tween.Activate ();
	}

	public void CloseGameMenu () {
		tween.Deactivate ();
		
		WorldController.Instance.ResumeGameUpdate ();
	}
}

