using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;

public abstract class NUnitUIElement : MonoBehaviour, UnitUI
{
	public GameObject ProgressBar;
	private UISlider progressBar;
	
	private UnitSubject unitOwner;
	protected Vector3 screenPos;
	
	private List<UISlicedSprite> sprites = new List<UISlicedSprite> ();
	private int playerId;
	
	private Vector3 prevCoord = Vector3.zero;
	
	public void Init (UnitSubject unit)
	{
		this.unitOwner = unit;
		if (ProgressBar != null) {
			this.progressBar = ProgressBar.GetComponent<UISlider> ();
		}
		this.Prepare ();
		this.SetContentVisible (unitOwner.playerOwner.IsHuman ());
		this.gameObject.SetActive (false);
		this.gameObject.transform.position = screenPos = Camera.main.WorldToScreenPoint (unitOwner.transform.position);	
	}
	
	protected abstract void Prepare () ;
	
	public void OnUpdate ()
	{	
		if (unitOwner.playerOwner.IsHuman ()) {
			this.DoUpdateLogic ();
		} 
		
		if (unitOwner == null || WorldController.IsWorldPaused) {
			gameObject.SetActive (false);
			return;
		} 
		
		screenPos = Camera.main.WorldToScreenPoint (unitOwner.transform.position);	
		bool isVisible = InputController.CoordCanBeDisplayed (screenPos);
		bool becomeVisible = false;
		if (!this.gameObject.activeInHierarchy && isVisible) {
			becomeVisible = true;
		}
		this.gameObject.SetActive (isVisible);		
		
		if (!isVisible) {
			return;
		}
		
		if (playerId != unitOwner.playerOwner.Id) {
			playerId = unitOwner.playerOwner.Id;
			this.SetContentVisible (unitOwner.playerOwner.IsHuman ());
		}		
		
		if (progressBar != null) {
			this.UpdateProgressBar ();
		}
		
		if (prevCoord != Vector3.zero) {
			this.gameObject.transform.position = Vector3.Slerp(this.gameObject.transform.position, prevCoord, 0.3f);		
		} else if (prevCoord == screenPos) {
			return;
		} 
		this.PositionGameObject ();
		
		if (becomeVisible) {
			this.gameObject.transform.position = screenPos;
		} else {
			this.gameObject.transform.position = Vector3.Slerp(this.gameObject.transform.position, screenPos, 0.1f);
		}
		foreach (UISlicedSprite sprite in sprites) {
			sprite.MarkAsChanged ();
		}
		
		prevCoord = screenPos;	
	}
	
	private void UpdateProgressBar ()
	{
		float progress = 0;
		UnitSubject unit = unitOwner;
		if (unitOwner.IsBuilding) {
			bool enabled = unitOwner.playerOwner.IsHuman ();
			progress = unit.BuildingData.ConstructionProgress / 100f;	
			if (progress == 1 || progress == 0) {
				progress = 1 - unit.UnitSpawnManager.Progress / 100f;				
			} 
			
			if (progress >= 1f || progress <= 0f) {				
				progress = 1 - unit.BuildingData.CaptureTimerProgress / 100f;
				if (progress <= 0.1) {
					progress = -123;
				}
				enabled = true;
			}
		
			if (progress >= 1f || progress <= 0.0f) {
				ProgressBar.SetActive (false);
			} else {
				ProgressBar.SetActive (true && enabled);
				progressBar.sliderValue = progress;
			}
		}
	}
		
	protected virtual void PositionGameObject ()
	{
		float xScale = 0;
		float yScale = 0;
		
		xScale = 300 * ((3 * UnityEngine.Screen.width + 1000f) / 4000);
		yScale = 300 * ((3 * UnityEngine.Screen.height + 1000f) / 2000);
		
		screenPos.Set ((screenPos.x - UnityEngine.Screen.width / 2) / xScale, (screenPos.y - UnityEngine.Screen.height / 2) / yScale, -3f);		
	}

	protected virtual void DoUpdateLogic ()
	{
	}
	
	protected abstract void SetContentVisible (bool visible);
	
	protected void CollectSprites (MonoBehaviour obj)
	{
		CollectSprites (obj.gameObject);
	}
	
	protected void CollectSprites (GameObject obj)
	{
		bool activeSelf = obj.activeSelf;
		UISlicedSprite[] sprites = obj.transform.GetComponentsInChildren<UISlicedSprite> () as UISlicedSprite[];
		foreach (UISlicedSprite s in sprites) {
			this.sprites.Add (s);
		}
		obj.SetActive (activeSelf);
		
//		UISlicedSprite sprite = obj.transform.Find ("Background").GetComponent<UISlicedSprite> ();
//		this.sprites.Add (sprite);
	}
	
	public bool IsFree {
		get {
			return this.unitOwner == null; 
		}
	}

	public UnitSubject UnitOwner {
		get {
			return this.unitOwner;
		}
	}
}

