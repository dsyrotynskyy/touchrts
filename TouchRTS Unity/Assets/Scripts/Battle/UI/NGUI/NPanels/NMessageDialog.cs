using UnityEngine;
using UI;
using System.Collections.Generic;

public class NMessageDialog : MonoBehaviour, MessageDialog
{
	private readonly LinkedList<NDialogPhrase> phraseList = new LinkedList<NDialogPhrase> ();
	
	public GameObject MessageLabel;
	
	private UILabel messageLabel;
	private NDialogPhrase currentDialogPhrase = null;
	
	public void Init (WorldController instance) {
		this.messageLabel = MessageLabel.GetComponent<UILabel> ();
	}
	
	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return this.gameObject.activeSelf;
	}

	public bool IsDialog ()
	{
		return true;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{		
	}
	#endregion

	#region MessageDialog implementation
	public void Display (string message, string id) {
		NDialogPhrase phrase = new NDialogPhrase (message, id);
		
		OnAddedPhrase (phrase);
	}
	
	
	public void Display (MessageDialogData messageDialogData)
	{
		NDialogPhrase phrase = new NDialogPhrase (messageDialogData);
		
		OnAddedPhrase (phrase);
	}
	
	private void OnAddedPhrase (NDialogPhrase phrase) {
		if (currentDialogPhrase == null) {
			this.gameObject.SetActive (true);
			WorldController.Instance.PauseGameUpdate ();
			DisplayPhrase (phrase);
		} else {
			this.phraseList.AddLast (phrase);
		}
	}
	#endregion
			
	private void OnClick () {
		currentDialogPhrase.OnHide ();
		
		if (this.phraseList.Count <= 0) {
			this.gameObject.SetActive (false);
			WorldController.Instance.ResumeGameUpdate ();	
			currentDialogPhrase = null;
			return;
		} 
		
		currentDialogPhrase = this.phraseList.First.Value;
		DisplayPhrase (this.phraseList.First.Value);
		this.phraseList.RemoveFirst ();
	}
	
	private void DisplayPhrase (NDialogPhrase phrase) {
		this.messageLabel.text = phrase.Message;
		this.currentDialogPhrase = phrase;
	}
}

class NDialogPhrase {
	private MessageDialogData messageDialogData;
	
	public NDialogPhrase (MessageDialogData messageDialogData) {
		this.messageDialogData = messageDialogData;
	}
	
	public NDialogPhrase (string message, string id) {
		this.messageDialogData.message = message;
		this.messageDialogData.id = id;
	}
	
	public void OnHide ()
	{
		if (messageDialogData.action != null) {
			messageDialogData.action  ();
		}
	}

	public string Id {
		get {
			return this.messageDialogData.id;
		}
	}

	public string Message {
		get {
			return this.messageDialogData.message;
		}
	}
}

