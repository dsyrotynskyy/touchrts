using UnityEngine;
using System.Collections;
using UI;

public class NPauseButton : MonoBehaviour, GameWidget
{
	public void Init (WorldController worldController) {
		
	}
	
	public void OnClick () {
		NGUIController.Instance.DisplayMenu ();
	}
	
	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return true;
	}

	public bool IsDialog ()
	{
		return false;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{
	}
	#endregion
}

