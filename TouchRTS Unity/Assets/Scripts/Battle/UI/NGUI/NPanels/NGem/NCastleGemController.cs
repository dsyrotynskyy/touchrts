using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CastleManagingLogic;

//Main castle gem controller logic
public class NCastleGemController : NUnitUIElement, ICastleManagingWidget
{
	public GameObject CentralGem;
	public GameObject CentralGemButton;
	public GameObject ElementGem1;
	public GameObject ElementGem2;
	public GameObject ElementGem3;
	public GameObject ElementGem4;
	public GameObject ElementGem5;
	public GameObject ElementGem6;
	
	private List<NElementGem> elementGems = new List<NElementGem> ();
	private ControlGemStateLogic currentStateLogic;
	private ControlGemStateLogic basicStateLogic;
	private ControlGemStateLogic unitTrainCastleGemLogic;
	private ControlGemStateLogic buildingConstructionCastleGemLogic;
	private ControlGemStateLogic spellStudyCastleGemLogic;
	
	private UITweener centralGemTweener;
	
	public void FixedUpdate () {
	}

	#region implemented abstract members of NUnitUIElement
	protected override void Prepare ()
	{
		this.BuildingOwner.castleGemController = this;

		centralGemTweener = this.CentralGem.GetComponent<UITweener> ();
		
		UIEventListener.Get (CentralGemButton).onClick += delegate(GameObject go)
		{
			OnCentralGemClicked ();
		};
		
		CollectSprites (CentralGem);
		
		GetElementGemFrom (ElementGem1);
		GetElementGemFrom (ElementGem2);
		GetElementGemFrom (ElementGem3);
		GetElementGemFrom (ElementGem4);
		GetElementGemFrom (ElementGem5);
		GetElementGemFrom (ElementGem6);
		
		basicStateLogic = new BasicControlGemStateLogic (this);	
		unitTrainCastleGemLogic = CastleGemExtendedLogic.CreateUnitTrainCastleGemLogic (this);
		buildingConstructionCastleGemLogic = CastleGemExtendedLogic.CreateBuildingConstructionCastleGemLogic (this);
		spellStudyCastleGemLogic = CastleGemExtendedLogic.CreateSpellStudyCastleGemLogic (this);
		
		SetStateLogic (basicStateLogic);
	}
	
	private void GetElementGemFrom (GameObject obj) {
		NElementGem element = obj.GetComponent<NElementGem> ();
		if (element != null) {
			elementGems.Add (element);
			element.Init (this);
			CollectSprites (element);
		}
	}
	
	protected override void DoUpdateLogic ()
	{
		foreach (NElementGem elementGem in elementGems) {
			elementGem.DoUpdate ();	
		}		
	}

	protected override void SetContentVisible (bool visible)
	{
		CentralGem.SetActive (visible);
		foreach (NElementGem elementGem in elementGems) {
			elementGem.SetActive (visible);
		}
	}
	#endregion
	
	private void SetStateLogic (ControlGemStateLogic logic) {
		this.currentStateLogic = logic;
		
		List<ElementGemLogic> logicList = currentStateLogic.MakeContentObjects ();
		
		for (int i = 0; i < elementGems.Count; i++) {
			NElementGem element = elementGems[i];
			
			ElementGemLogic elementLogic = null;
			if (i < logicList.Count) {
				elementLogic = logicList[i];
			}
			
			element.SetLogic (elementLogic);
		}
		
		logic.OnStateStarted ();
	}

	public static bool Opened = false;

	public void SetClosed (bool closed) {
		Opened = !closed;
		centralGemTweener.Play(!closed);

		if (!closed) {
			CenterCameraPosition ();
		}
		
		foreach (NElementGem elementGem in elementGems) {
			elementGem.Closed = closed;
		}
	}

	private void OnCentralGemClicked ()
	{
		this.currentStateLogic.OnCentralGemClicked ();
	}

	public List<NElementGem> ElementGems {
		get {
			return this.elementGems;
		}
	}
	
	public void SetBasicState () {
		this.SetStateLogic (basicStateLogic);
	}
	
	public void SetUnitTrainState () {
		this.SetStateLogic (unitTrainCastleGemLogic);
	}
	
	public void SetBuildingConstructionState () {
		this.SetStateLogic (buildingConstructionCastleGemLogic);
	}
	
	public void SetSpellStudyState () {
		this.SetStateLogic (spellStudyCastleGemLogic);
	}

	#region IDescriptionWindow implementation
	public BuildingSubject BuildingOwner {
		get {
			return this.UnitOwner.BuildingData;
		}
	}
	
	public HeroSubject HeroTarget {
		get {
			return this.UnitOwner.PlayerOwner.MainHero;
		}
	}
	#endregion

	public void CenterCameraPosition ()
	{
		Vector3 newPosition = this.UnitOwner.transform.position;
		newPosition.Set (newPosition.x + 10, newPosition.y + 10, newPosition.z);
		InputController.RTSCamera.FollowThePointSlow (newPosition);
	}

	protected override void PositionGameObject ()
	{		
		base.PositionGameObject ();
	}

	public ControlGemStateLogic CurrentStateLogic {
		get {
			return this.currentStateLogic;
		}
	}
	
//	private void PlayCentralGemSizeIncreaseAnimation () {
//		
//	}
//	
//	private void PlayCentralGemSizeDecreaseAnimation () {
//	}
}



