using UnityEngine;
using System.Collections;
using CastleManagingLogic;

public class NElementGem : MonoBehaviour
{
	public GameObject BasicIcon;
	public GameObject CategoryIcon;
	public GameObject Button;
	
	private bool closed;
	private NCastleGemController gem;
	private ElementGemLogic elementLogic;

	private UISlicedSprite basicIconSprite;
	private UISlicedSprite categoryIconSprite;
	private UISlicedSprite currentIconSprite;
	private UIButton button;
	
	private PositionTweener positionTweener;
	
	public void Init (NCastleGemController gem) {
		this.gem = gem;
		this.button = Button.GetComponent<UIButton> ();
		this.basicIconSprite = BasicIcon.GetComponent<UISlicedSprite> ();
		this.categoryIconSprite = CategoryIcon.GetComponent<UISlicedSprite> ();
		
		this.positionTweener = new PositionTweener (gameObject);
		
		UIEventListener.Get (Button).onClick += delegate(GameObject go)
		{
			OnElementClick ();
		};
		
		gameObject.SetActive (false);
	}
	
	public bool Closed {
		get {
			return closed;
		}
		set {
			closed = value;
		}
	}
	
	private bool markAsChanged = false;
	
	public void DoUpdate () {
		this.SetActive (elementLogic != null && !closed && !elementLogic.ShouldClose);
		
		if (elementLogic == null || !this.gameObject.activeInHierarchy || closed || elementLogic.ShouldClose) {
			return;
		}
		this.elementLogic.OnUpdate ();
		this.button.enabled = elementLogic.IsEnabled;
		
		if (this.currentIconSprite.sprite.name != elementLogic.iconName || elementLogic.iconName != this.currentIconSprite.sprite.name) {
			UpdateSprite ();
		}
	}

	public void SetLogic (ElementGemLogic elementLogic)
	{
		this.elementLogic = elementLogic;
		if (this.elementLogic == null) {
			return;
		}
		
		if (elementLogic.IsBasicElement) {
			this.BasicIcon.gameObject.SetActive (true);
			this.CategoryIcon.gameObject.SetActive (false);
			this.currentIconSprite = basicIconSprite;
			this.currentIconSprite.MarkAsChanged ();
		} else {
			this.BasicIcon.gameObject.SetActive (false);
			this.CategoryIcon.gameObject.SetActive (true);
			this.currentIconSprite = categoryIconSprite;
			this.currentIconSprite.MarkAsChanged ();
		}
					
		if (this.currentIconSprite.atlas.name != elementLogic.atlasName) {
			UIAtlas atlas = NGUIController.Instance.AtlasMap[elementLogic.atlasName];
			this.currentIconSprite.atlas = atlas;
		}
		this.UpdateSprite ();
		
		this.SetActive (elementLogic != null && !closed && !elementLogic.ShouldClose);
	}
	
	private void UpdateSprite () {
		
		this.currentIconSprite.sprite = currentIconSprite.atlas.GetSprite (elementLogic.iconName);
		this.currentIconSprite.MarkAsChanged ();
	}
	
	public void OnElementClick () {
		if (elementLogic == null) {
			return;
		}
		
		elementLogic.OnClick ();	
	}
	
	public void SetActive(bool visible) {
		this.positionTweener.SetActive (visible);		
	}
}

public abstract class ElementGemLogic {
	protected NCastleGemController gem;
	protected BuildingSubject ourBuilding;
	
	protected bool isEnabled;
	protected bool isBasicElement;
	protected bool shouldClose = false;
	
	public ElementGemLogic (NCastleGemController gem) {		
		this.gem = gem;
		this.ourBuilding = gem.BuildingOwner;
	}
	
	public string atlasName;
	public string iconName;
	
	public bool IsEnabled {
		get {
			return isEnabled;
		}
	}
	
	public bool IsBasicElement {
		get {
			return this.isBasicElement;
		}
	}
	
	public bool ShouldClose {
		get {return shouldClose;}
	}
	
	public abstract void OnClick ();
	
	public abstract void OnUpdate () ;	
}

