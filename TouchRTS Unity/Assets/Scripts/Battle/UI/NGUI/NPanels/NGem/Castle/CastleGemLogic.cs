using System;
using System.Collections.Generic;
using UnityEngine;

namespace CastleManagingLogic
{
	public abstract class ControlGemStateLogic
	{		
		protected NCastleGemController gem;
		
		public ControlGemStateLogic (NCastleGemController gem)
		{
			this.gem = gem;
		}
		
		public abstract void OnStateStarted ();
		
		public abstract void OnCentralGemClicked ();
		
		public abstract List<ElementGemLogic> MakeContentObjects ();
	}

	class BasicControlGemStateLogic : ControlGemStateLogic
	{
		public static bool ConstructionButtonEnabled = true;
		public static bool UnitTrainButtonEnabled = true;
		public static bool SpellStudyButtonEnabled = true;
		public static bool HeroResurrectionButtonEnabled = true;
		public bool isClosed = true;
		private List<ElementGemLogic> elementLogic = new List<ElementGemLogic> ();
	
		public BasicControlGemStateLogic (NCastleGemController gem) : base (gem)
		{
			elementLogic.Add (BasicElementLogic.CreateTrainUnit (gem));
			elementLogic.Add (BasicElementLogic.CreateBuildingConstruction (gem));
			elementLogic.Add (BasicElementLogic.CreateSpellStudy (gem));
			elementLogic.Add (BasicElementLogic.CreateResurrection (gem));
		}
	
		public override void OnCentralGemClicked ()
		{		
			isClosed = !isClosed;
			gem.SetClosed (true);
		}
	
		public override List<ElementGemLogic> MakeContentObjects ()
		{
			return elementLogic;
		}
	
		public override void OnStateStarted ()
		{		
			gem.SetClosed (true);
		}
	
		class BasicElementLogic : ElementGemLogic
		{		
			public ClickLogic clickLogic;
			public UpdateLogic updateLogic;
			
			private BasicElementLogic (NCastleGemController gem) : base (gem)
			{
				this.isBasicElement = true;
			}
			
			public override void OnClick ()
			{
				clickLogic ();
			}
				
			public override void OnUpdate ()
			{
				updateLogic ();
			}
	
			public delegate void ClickLogic ();

			public delegate void UpdateLogic ();
		
			public static BasicElementLogic CreateTrainUnit (NCastleGemController gem)
			{
				BasicElementLogic logic = new BasicElementLogic (gem);
			
				logic.atlasName = "UIClose";
				logic.iconName = "HeroIconNormal";
			
				logic.clickLogic = delegate() { 
					logic.gem.SetUnitTrainState ();
				};		
				logic.updateLogic = delegate() { 				
					logic.isEnabled = logic.ourBuilding.CanSpawnUnits;
					logic.shouldClose = !BasicControlGemStateLogic.UnitTrainButtonEnabled;
				};
			
				return logic;
			}
		
			public static BasicElementLogic CreateBuildingConstruction (NCastleGemController gem)
			{
				BasicElementLogic logic = new BasicElementLogic (gem);
			
				logic.atlasName = "UIClose";
				logic.iconName = "BuildingIcon";
			
				logic.clickLogic = delegate() { 				
					logic.gem.SetBuildingConstructionState ();
				};		
				logic.updateLogic = delegate() { 				
					logic.isEnabled = logic.ourBuilding.CanConstructStructures;
					logic.shouldClose = !BasicControlGemStateLogic.ConstructionButtonEnabled;
				};
			
				return logic;
			}
		
			public static BasicElementLogic CreateSpellStudy (NCastleGemController gem)
			{
				BasicElementLogic logic = new BasicElementLogic (gem);
			
				logic.atlasName = "UIClose";
				logic.iconName = "ItemSkeleton1";
			
				logic.clickLogic = delegate() { 
					gem.SetSpellStudyState ();
				};		
				logic.updateLogic = delegate() { 				
					logic.isEnabled = logic.ourBuilding.HasMageGuild && InputController.CurrentHero != null;
					logic.shouldClose = !BasicControlGemStateLogic.SpellStudyButtonEnabled;
				};
				return logic;
			}
		
			public static BasicElementLogic CreateResurrection (NCastleGemController gem)
			{
				BasicElementLogic logic = new BasicElementLogic (gem);
			
				logic.atlasName = "UIClose";
				logic.iconName = "ItemSkeleton2";
			
				logic.clickLogic = delegate() { 
					Debug.Log ("Resurrection!");
					ServerCommandController.Instance.Resurrect (InputController.CurrentHero);
				};		
				logic.updateLogic = delegate() { 	
					if (gem.BuildingOwner.CanResurrect && 
					InputController.CurrentUnitGroup != null &&
					InputController.CurrentUnitGroup.IsHeroGroup &&
					InputController.CurrentUnitGroup.GroupHero.IsDead &&
					BasicControlGemStateLogic.HeroResurrectionButtonEnabled) {				
						logic.shouldClose = false;
					
						bool isHeroCloseEnough = InputController.CurrentUnitGroup.IsEnoughCloseForInteraction (gem.BuildingOwner.BuildingUnitGroup);
						logic.isEnabled = isHeroCloseEnough;
					} else {
						logic.shouldClose = true;
					
						logic.isEnabled = false;
					}
				
					logic.shouldClose = BasicControlGemStateLogic.HeroResurrectionButtonEnabled;
				};
			
				return logic;
			}
		
		#region implemented abstract members of ElementGemLogic
		
		
		#endregion
		}
	}
	
	public class CastleGemExtendedLogic : ControlGemStateLogic
	{
		protected readonly List<ElementGemLogic> elements = new List<ElementGemLogic> ();
		protected CastleManagingWidgetLogic managingLogic;
		
		private CastleGemExtendedLogic (CastleManagingWidgetLogic managingLogic, NCastleGemController gem) : base (gem)
		{
			this.managingLogic = managingLogic;
			managingLogic.Init (gem);
		}
		
		#region implemented abstract members of ControlGemStateLogic
		public override void OnStateStarted ()
		{			
		}
	
		public override void OnCentralGemClicked ()
		{
			gem.SetBasicState ();
			NGUIController.Instance.GemElementDescriptionWindow.HideWindow ();
		}
		
		public override List<ElementGemLogic> MakeContentObjects ()
		{
			elements.Clear ();
			foreach (ICastleManagingWidgetElement data in managingLogic.MakeContentObjects ()) {
				ElementGemLogic element = new CategoryElementGemLogic (data, managingLogic, gem);
				elements.Add (element);
			}
			
			return elements;
		}
		#endregion
		
		class CategoryElementGemLogic : ElementGemLogic
		{
			ICastleManagingWidgetElement data;
			CastleManagingWidgetLogic logic;
			
			public CategoryElementGemLogic (ICastleManagingWidgetElement data, CastleManagingWidgetLogic logic, NCastleGemController gem) : base (gem)
			{	
				this.isBasicElement = false;
				this.shouldClose = false;
				this.data = data;
				this.logic = logic;
				
				this.atlasName = data.AtlasName;
				this.iconName = data.SpriteName;
			}

			#region implemented abstract members of ElementGemLogic
			public override void OnClick ()
			{
				BuildingSubject building = gem.BuildingOwner;
				NGUIController.Instance.GemElementDescriptionWindow.Display (gem, logic, this.data);
			}

			public override void OnUpdate ()
			{
				this.data.OnUpdate ();
				
				this.isEnabled = this.data.IsEnabled;
				this.shouldClose = !this.data.IsDisplayable;
			}
			#endregion
		}
		
		public static CastleGemExtendedLogic CreateUnitTrainCastleGemLogic (NCastleGemController gem) {
			return new CastleGemExtendedLogic (new UnitTrainWindowLogic (), gem);
		}
		
		public static CastleGemExtendedLogic CreateBuildingConstructionCastleGemLogic (NCastleGemController gem) {
			return new CastleGemExtendedLogic (new BuildingConstructionWindowLogic (), gem);
		}
		
		public static CastleGemExtendedLogic CreateSpellStudyCastleGemLogic (NCastleGemController gem) {
			return new CastleGemExtendedLogic (new SpellTrainWindowLogic (), gem);
		}
	}
}




