using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CastleManagingLogic;

public class NGemElementDataWindow : MonoBehaviour, UI.GameWidget
{
	public GameObject CloseWindowButton;
	public GameObject WindowTitle;
	public GameObject CurrentObjectDescription;
	public GameObject CurrentObjectImage;
	public GameObject CostLabel;
	public GameObject ActionButton;
	
	public Dictionary<UIButton, bool> enablingButtonMap = new Dictionary<UIButton, bool> ();
	
	private UILabel windowTitleLabel;
	private UILabel currentElementDescription;
	private UILabel costLabel;
	private UIButton actionButton;
	
	private UIButton closeWindowButton;
	private UISlicedSprite closeWindowSprite;
		
	private UILabel actionButtonLabel;
	private UISlicedSprite currentElementImage;
	
//	private readonly BuildingConstructionWindowLogic buildingConstructionWindowLogic = new BuildingConstructionWindowLogic ();
//	private readonly SpellTrainWindowLogic spellTrainWindowLogic = new SpellTrainWindowLogic ();
//	private readonly UnitTrainWindowLogic unitTrainWindowLogic = new UnitTrainWindowLogic ();
	private NCastleGemController gem;
	private CastleManagingWidgetLogic currentWindowLogic;
	private ICastleManagingWidgetElement currentElementData;
	private BuildingSubject buildingTarget;
	private HeroSubject heroTarget;
	
	private AutoTweener tween;
	
	public void Init (WorldController worldController) {
		tween = new AutoTweener (gameObject);
		
		UIEventListener.Get (CloseWindowButton).onClick += delegate(GameObject go)
		{
			if (WorldController.IsWorldPaused) {
				return;
			}
			HideWindow ();
		};
		
		UIEventListener.Get (ActionButton).onClick += delegate(GameObject go)
		{
			if (WorldController.IsWorldPaused) {
				return;
			}
			this.currentElementData.OnObjectAction ();
		};
		
		this.windowTitleLabel = WindowTitle.GetComponent<UILabel> ();
		this.currentElementDescription = CurrentObjectDescription.GetComponent<UILabel> ();
		this.actionButton = ActionButton.GetComponent<UIButton> ();
		this.actionButtonLabel = actionButton.transform.FindChild ("Label").GetComponent <UILabel> ();
		this.currentElementImage = CurrentObjectImage.GetComponent<UISlicedSprite> ();
		this.costLabel = CostLabel.GetComponent<UILabel> ();
		this.closeWindowButton = CloseWindowButton.GetComponent<UIButton> ();
		this.closeWindowSprite = closeWindowButton.transform.FindChild ("Background").GetComponent <UISlicedSprite> ();
		
		this.HideWindow ();
	}
	
	public void HideWindow () {
		currentElementData = null;		
		
		tween.Deactivate ();
		this.currentWindowLogic = null;	
		
		this.heroTarget = null;
		this.buildingTarget = null;
		
		this.currentWindowLogic = null;
		this.gem = null;
	}
	
	private void ShowWindow () {
		tween.Activate ();
		
		gem.CenterCameraPosition ();
	}
	
	private void PrepareWindow (CastleManagingWidgetLogic windowLogic) {
		currentElementData = null;
		
		this.currentWindowLogic = windowLogic;	
		this.windowTitleLabel.text = windowLogic.WindowTitle;
		
		this.ShowWindow ();
	}
	
	private void DisplayElementData (ICastleManagingWidgetElement newCurrentElementData) {
		this.currentElementData = newCurrentElementData;
		this.currentElementDescription.text = currentElementData.ElementDescription;
		this.costLabel.text = currentElementData.Cost;
		this.windowTitleLabel.text = currentElementData.ElementName;		
		UIAtlas atlas = NGUIController.Instance.AtlasMap[currentElementData.AtlasName];
		this.currentElementImage.name = currentElementData.SpriteName;
		this.currentElementImage.atlas = atlas;
		this.currentElementImage.sprite = atlas.GetSprite (currentElementData.SpriteName);
		this.currentElementImage.MarkAsChanged ();
		
		this.actionButtonLabel.text = currentElementData.ActionButtonText;
		actionButton.isEnabled = currentElementData.IsEnabled;
	}
	
	#region CastleContstructorDialog implementation
	
	public void Display (NCastleGemController gem, CastleManagingWidgetLogic windowLogic, ICastleManagingWidgetElement data) {
		this.gem = gem;
		this.buildingTarget = gem.BuildingOwner;
		this.heroTarget = gem.HeroTarget;
		this.PrepareWindow (windowLogic);
		this.DisplayElementData (data);
	}
	#endregion
	
	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return this.gameObject.activeSelf;
	}

	public bool IsDialog ()
	{
		return true;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{
		if (this.currentWindowLogic == null) {
			return;
		}
		this.currentWindowLogic.OnUpdate ();

		bool enablingChanged = false;
		if (WorldController.IsWorldPaused) {
			this.actionButton.isEnabled = false;
			
			if (this.closeWindowButton.isEnabled) {
				this.closeWindowButton.isEnabled = false;
			}
			
			if (true) {
				actionButton.UpdateColor (actionButton.isEnabled, false);
			}
		} else {
			bool enabled = currentElementData.IsEnabled;
			enablingChanged = enabled ^ actionButton.isEnabled;
					
			this.actionButton.isEnabled = currentElementData.IsEnabled;
			
			if (!this.closeWindowButton.isEnabled) {
				this.closeWindowButton.isEnabled = true;
				closeWindowSprite.MarkAsChanged ();
				closeWindowSprite.OnUpdate ();
			}			
			
			if (enablingChanged) {
				actionButton.UpdateColor (actionButton.isEnabled, false);
			}
		}
		
		if (this.actionButtonLabel.text != currentElementData.ActionButtonText) {
			this.actionButtonLabel.text = currentElementData.ActionButtonText;
		}
	}
	#endregion

	public BuildingSubject BuildingTarget {
		get {
			return this.buildingTarget;
		}
	}

	public HeroSubject HeroTarget {
		get {
			return this.heroTarget;
		}
	}
	
	
	#region CreationWindow implementation
	public void EnableAllButtons (bool enable)
	{
		foreach (UIButton button in this.enablingButtonMap.Keys) {
			enablingButtonMap[button] = enable;
		}
	}

	public void EnableCloseButton (bool enable)
	{
	}

	public void EnableActionButton (bool enable)
	{
	}


	#endregion

}

