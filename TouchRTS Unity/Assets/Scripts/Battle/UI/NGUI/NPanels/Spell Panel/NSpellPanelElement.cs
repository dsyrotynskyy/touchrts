using UnityEngine;
using System.Collections;

public class NSpellPanelElement : NGridElement
{
	private ISpellPanelElement data;

	public ISpellPanelElement Data {
		get {
			return this.data;
		}		
		set {
			data = value;
			if (data == null) {
				this.gameObject.SetActive (false);
				this.gameObject.name = "null";			
				return;
			}
			this.gameObject.SetActive (true);
			this.gameObject.name = data.ElementName;
			this.SetSprite (data.AtlasName, data.SpriteName);
			
			this.SetOnClickListener (delegate()	{
				if (NGUIController.Instance.DisableInputInteraction () || WorldController.IsWorldPaused) {
					return;
				}			
				this.data.OnObjectAction ();	
				this.SetSprite (data.AtlasName, data.SpriteName);
			});
		}
	}
	
	public override void OnUpdate () {
		if (data == null) {
			return;
		}
		
		if (this.data.SpriteName != this.sprite.spriteName) {
			this.SetSprite (data.SpriteName);
		}
		
		this.sprite.enabled = data.IsEnabled;
	}

	public void OnHover (bool isOver)
	{
		NGUIController.Instance.IsMouseOver = isOver;
	}
}

