using System;
using Entity;

public interface ISpellPanelElement {
		string ElementName {
			get;
		}
	
		void OnObjectAction ();
		
		string AtlasName {
			get;
		}
		
		string SpriteName {
			get;
		}
				
		bool IsEnabled {
			get;
		}
		
		bool IsDisplayable {
			get;
		}
		
		void OnUpdate ();
}

abstract class SpellPanelElement : ISpellPanelElement {
	protected string atlasName;
	protected string spriteNameOff;
	protected string spriteNameOn;
	
	#region ISpellPanelElement implementation
	public abstract void OnObjectAction ();	

	public virtual void OnUpdate ()
	{
	}

	public string AtlasName {
		get {
			return this.atlasName;
		}
	}

	public abstract string SpriteName {
		get ;
	}

	public abstract bool IsEnabled {
		get;
	}
	
	public bool IsDisplayable {
		get {
			return true;
		}
	}
	#endregion

	#region ISpellPanelElement implementation
	public abstract string ElementName {
		get ;
	}
	#endregion
}

class HeroAbilityElement : SpellPanelElement {
	
	private SpellType spellType;
	private HeroSubject ourHero;
	
	public HeroAbilityElement (SpellType spellType, HeroSubject ourHero) {
		this.spellType = spellType;
		this.ourHero = ourHero;
		
		this.atlasName = spellType.AtlasName;
		this.spriteNameOff = spellType.SpriteNameOff;
		this.spriteNameOn = spellType.SpriteNameOn;
	}
	
	private static Timer timeCalculator = Timer.CreateFrequency (100);
	
	#region implemented abstract members of SpellPanelElement
	public override void OnObjectAction ()
	{
		if (!timeCalculator.EnoughTimeLeft ()) {
			return;
		}
		if (SpellSpawnController.SpawnSpellType == spellType) {
			SpellSpawnController.SpawnSpellType = null;
		} else {
			SpellSpawnController.SpawnSpellType = spellType;
		}
	}

	public override bool IsEnabled {
		get {
			return ourHero.CanSpawnSpell (spellType);
		}
	}

	public override string ElementName {
		get {
			return this.spellType.Name;
		}
	}
	
	public override string SpriteName {
		get {
			if (SpellSpawnController.SpawnSpellType == spellType) {
				return this.spriteNameOn;
			} else {
				return this.spriteNameOff;
			}
		}
	}
	#endregion
}

class UnitAbilityElement : SpellPanelElement {
	private AbilityType abilityType;
	private UnitGroupSubject unitGroup;
	
	public UnitAbilityElement (AbilityType abilityType, UnitGroupSubject unitGroup) {
		this.abilityType = abilityType;
		this.unitGroup = unitGroup;
		
		this.atlasName = "rex";
		this.spriteNameOff = "rex5";
		this.spriteNameOn = "rex5";
	}
	
	#region implemented abstract members of SpellPanelElement
	public override void OnObjectAction ()
	{
		CommandController.Instance.CastUnitsSkillAbility (unitGroup, this.abilityType);
		SpellSpawnController.SpawnSpellType = null;
		InputController.SetRTSControlType();
	}

	public override bool IsEnabled {
		get {
			return unitGroup.CanCastUnitsSkill (this.abilityType) ;	
		}
	}

	public override string ElementName {
		get {
			return this.abilityType.Name;
		}
	}
	
	public override string SpriteName {
		get {
			return spriteNameOff;
		}
	}
	
	#endregion
}


