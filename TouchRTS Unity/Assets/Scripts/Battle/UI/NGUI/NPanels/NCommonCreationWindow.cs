using UnityEngine;
using System.Collections;
using CastleManagingLogic;
using UI;
using System.Collections.Generic;

public class NCommonCreationWindow : MonoBehaviour, CastleContstructorDialog, SpawnUnitDialog, SpellStudyDialog, ICastleManagingWidget
{
	public GameObject CloseWindowButton;
	public GameObject WindowTitle;
	public GameObject CurrentObjectDescription;
	public GameObject CurrentObjectImage;
	public GameObject CostLabel;
	public GameObject ObjectsPanel;
	public GameObject UIGrid;
	public GameObject ActionButton;
	
	public Dictionary<UIButton, bool> enablingButtonMap = new Dictionary<UIButton, bool> ();
	
	private UILabel windowTitleLabel;
	private UILabel currentElementDescription;
	private UILabel costLabel;
	private UIButton actionButton;
	
	private UIButton closeWindowButton;
	private UISlicedSprite closeWindowSprite;
		
	private UILabel actionButtonLabel;
	private List<NCreationWindowGridElement> nGridElements = new List<NCreationWindowGridElement> ();
	private UISlicedSprite currentElementImage;
	private UIDraggablePanel uiDraggablePanel;
	
	private readonly BuildingConstructionWindowLogic buildingConstructionWindowLogic = new BuildingConstructionWindowLogic ();
	private readonly SpellTrainWindowLogic spellTrainWindowLogic = new SpellTrainWindowLogic ();
	private readonly UnitTrainWindowLogic unitTrainWindowLogic = new UnitTrainWindowLogic ();
	
	private CastleManagingWidgetLogic currentWindowLogic;
	private NCreationWindowGridElement currentElement;
	private BuildingSubject buildingTarget;
	private HeroSubject heroTarget;
	
	void Start ()
	{	
	}
	
	public void Init (WorldController worldController) {
		UIEventListener.Get (CloseWindowButton).onClick += delegate(GameObject go)
		{
			if (WorldController.IsWorldPaused) {
				return;
			}
			HideWindow ();
		};
		
		UIEventListener.Get (ActionButton).onClick += delegate(GameObject go)
		{
			if (WorldController.IsWorldPaused) {
				return;
			}
			this.currentElement.Data.OnObjectAction ();
		};
		
		this.windowTitleLabel = WindowTitle.GetComponent<UILabel> ();
		this.currentElementDescription = CurrentObjectDescription.GetComponent<UILabel> ();
		this.actionButton = ActionButton.GetComponent<UIButton> ();
		this.actionButtonLabel = actionButton.transform.FindChild ("Label").GetComponent <UILabel> ();
		this.currentElementImage = CurrentObjectImage.GetComponent<UISlicedSprite> ();
		this.costLabel = CostLabel.GetComponent<UILabel> ();
		this.uiDraggablePanel = ObjectsPanel.GetComponent<UIDraggablePanel> ();
		this.closeWindowButton = CloseWindowButton.GetComponent<UIButton> ();
		this.closeWindowSprite = closeWindowButton.transform.FindChild ("Background").GetComponent <UISlicedSprite> ();
		
		buildingConstructionWindowLogic.Init (this);
		spellTrainWindowLogic.Init (this);
		unitTrainWindowLogic.Init (this);
		
		int i = 0;
		foreach (Transform child in UIGrid.transform) {
			NCreationWindowGridElement element = child.gameObject.AddComponent<NCreationWindowGridElement> ();		
			element.Init ();
			element.Position = i;
			nGridElements.Add (element);
			
			this.enablingButtonMap.Add( element.GetComponent<UIButton> (), true);
			i++;
		}
		
		this.HideWindow ();
	}
	
	public void HideWindow () {
		currentElement = null;		
		
		this.gameObject.SetActive (false);	
		this.currentWindowLogic = null;	
		
		this.heroTarget = null;
		this.buildingTarget = null;
		
		foreach (NCreationWindowGridElement element in nGridElements) {
			element.Data = null;			
		}
	}
	
	private void PrepareWindow (CastleManagingWidgetLogic windowLogic) {
		currentElement = null;
		
		this.gameObject.SetActive (true);
		this.currentWindowLogic = windowLogic;	
		this.windowTitleLabel.text = windowLogic.WindowTitle;
		this.uiDraggablePanel.ResetPosition ();
		
		int j = 0;
		foreach (ICastleManagingWidgetElement obj in windowLogic.MakeContentObjects ()) {
			NCreationWindowGridElement element = nGridElements[j];
			element.enabled = true;
			if (j == 0) {
				currentElement = nGridElements [0];
			}
			element.Data = obj;
			element.SetOnClickListener (delegate()	{
				if (InputController.UI.MessageDialog.IsDisplaying () || WorldController.IsWorldPaused) {
					return;
				}
				OnElementSelected (element);				
			});
			j++;
		}
		
		OnElementSelected (currentElement);
		
		this.uiDraggablePanel.ResetPosition ();
		this.uiDraggablePanel.Drag (new Vector2 (-100,1));		
	}
	
	private void OnElementSelected (NCreationWindowGridElement newCurrentElement) {
		this.currentElement = newCurrentElement;
		this.actionButtonLabel.text = newCurrentElement.Data.ActionName;
		
		this.currentElementDescription.text = currentElement.Data.ElementDescription;
		this.costLabel.text = currentElement.Data.Cost;
				
		UIAtlas atlas = NGUIController.Instance.AtlasMap[currentElement.Data.AtlasName];
		this.currentElementImage.name = currentElement.Data.SpriteName;
		this.currentElementImage.atlas = atlas;
		this.currentElementImage.sprite = atlas.GetSprite (currentElement.Data.SpriteName);
		this.currentElementImage.MarkAsChanged ();
		
		actionButton.isEnabled = newCurrentElement.Data.IsEnabled;
	}
	
	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return this.gameObject.activeSelf;
	}

	public bool IsDialog ()
	{
		return true;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{
		if (this.currentWindowLogic == null) {
			return;
		}
		this.currentWindowLogic.OnUpdate ();
		foreach (NCreationWindowGridElement element in nGridElements) {
			if (!currentElement.Data.IsDisplayable && currentElement.Data.ElementName == element.name) {
				this.OnElementSelected (element);
			}
			element.OnUpdate (); 
		}
		bool enablingChanged = false;
		if (WorldController.IsWorldPaused) {
			this.actionButton.isEnabled = false;
			
			if (this.closeWindowButton.isEnabled) {
				this.closeWindowButton.isEnabled = false;
//				closeWindowButton.UpdateColor (closeWindowButton.isEnabled, false);
			}
			
			if (true) {
				actionButton.UpdateColor (actionButton.isEnabled, false);
			}
		} else {
			bool enabled = currentElement.Data.IsEnabled;
			enablingChanged = enabled ^ actionButton.isEnabled;
					
			this.actionButton.isEnabled = currentElement.Data.IsEnabled;
			
			if (!this.closeWindowButton.isEnabled) {
				this.closeWindowButton.isEnabled = true;
				closeWindowSprite.MarkAsChanged ();
				closeWindowSprite.OnUpdate ();
//				closeWindowButton.UpdateColor (closeWindowButton.isEnabled, false);
			}			
			
//			closeWindowButton.UpdateColor (closeWindowButton.isEnabled, false);
			if (enablingChanged) {
				actionButton.UpdateColor (actionButton.isEnabled, false);
			}
		}
	}
	#endregion

	#region CastleContstructorDialog implementation
	public void DisplayCastleConstructor (BuildingSubject buildingSubject)
	{
		this.buildingTarget = buildingSubject;
		this.PrepareWindow (this.buildingConstructionWindowLogic);
	}
	#endregion


	#region SpawnUnitDialog implementation
	public void DisplaySpawnUnit (BuildingSubject buildingSubject)
	{
		this.buildingTarget = buildingSubject;
		this.PrepareWindow (this.unitTrainWindowLogic);
	}
	#endregion


	#region SpellStudyDialog implementation
	public void DisplaySpellStudy (BuildingSubject buildingSubject, HeroSubject currentHero)
	{
		this.buildingTarget = buildingSubject;
		this.heroTarget = currentHero;
		this.PrepareWindow (this.spellTrainWindowLogic);
	}
	#endregion


	public BuildingSubject BuildingOwner {
		get {
			return this.buildingTarget;
		}
	}

	public HeroSubject HeroTarget {
		get {
			return this.heroTarget;
		}
	}
	
	
	#region CreationWindow implementation
	public void EnableAllButtons (bool enable)
	{
		foreach (UIButton button in this.enablingButtonMap.Keys) {
			enablingButtonMap[button] = enable;
		}
	}

	public void EnableCloseButton (bool enable)
	{
		throw new System.NotImplementedException ();
	}

	public void EnableActionButton (bool enable)
	{
		throw new System.NotImplementedException ();
	}

	public void SelectElement (string element)
	{
		throw new System.NotImplementedException ();
	}
	#endregion


}

