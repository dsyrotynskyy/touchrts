using System;
using UI;
using Entity;
using System.Collections.Generic;
using UnityEngine;

namespace CastleManagingLogic
{
	public class SpellTrainWindowLogic : CastleManagingWidgetLogic
	{
		private ICastleManagingWidget window;	
		private BuildingSubject currentBuilding;
		private HeroSubject hero;
		private Dictionary <string, int> maxSpellLevels = new Dictionary<string, int> ();

		private readonly LinkedList<ICastleManagingWidgetElement> dataList = new LinkedList<ICastleManagingWidgetElement> ();
		
		public LinkedList<ICastleManagingWidgetElement> MakeContentObjects ()
		{
			this.Prepare ();			
			foreach (SpellType spellType in currentBuilding.Spells) {
				this.dataList.AddLast (new SpellWindowElement (spellType, currentBuilding, hero, maxSpellLevels));									
			}
			
			return this.dataList;
		}
		
		private void Prepare () {
			this.dataList.Clear ();
			currentBuilding = this.window.BuildingOwner;
			this.hero = this.window.HeroTarget;
			this.CalculateMaxSpellLevels ();
		}
		
		private void CalculateMaxSpellLevels () {
			this.maxSpellLevels.Clear ();
			foreach (SpellType spell in currentBuilding.Spells) {
				if (this.maxSpellLevels.ContainsKey (spell.Name)) {
					int prevLevel = this.maxSpellLevels[spell.Name];
					if (spell.SpellLevel > prevLevel) {
						this.maxSpellLevels[spell.Name] = spell.SpellLevel;
					}
				} else {
					this.maxSpellLevels.Add (spell.Name, spell.SpellLevel);
				}
			}
		}

		public string WindowTitle {
			get {
				return "Spells";
			}
		}

		#region CreationWindowLogic implementation
		public void Init (ICastleManagingWidget window)
		{
			this.window = window;
		}
		#endregion

		#region CreationWindowLogic implementation
		public void OnUpdate ()
		{
			this.CalculateMaxSpellLevels ();
		}
		#endregion
	}
	
	class SpellWindowElement : CreationWindowElement {			
		public Dictionary <string, int> maxSpellLevels;			
		public BuildingSubject currentBuilding;
		public HeroSubject currentHero;
		public SpellType spellType;
		
		public SpellWindowElement (SpellType spellType, BuildingSubject building, HeroSubject currentHero, Dictionary <string, int> maxSpellLevels) {
			this.spellType = spellType;
			this.currentBuilding = building;
			this.currentHero = currentHero;
			this.maxSpellLevels = maxSpellLevels;
			
			this.atlasName = "rex";
			this.spriteName = "rex6";
				
			this.elementName = spellType.Name + spellType.SubTitle;
			this.elementDescription = spellType.Name + spellType.SubTitle;			
			this.cost = spellType.Price + "";
			
			this.actionButtonText = "Study";
		}
	
		#region implemented abstract members of CommonCreationWindow.CreationWindowElement
		public override void OnObjectAction ()
		{
			ServerCommandController.Instance.BuySpell (currentHero, spellType);
			InputController.UI.SpellPanel.LoadHeroSpells ();
		}
		
		public override bool IsEnabled {
			get {
				bool canBuySpell = true;
				
				if (!currentHero.CanTrainSpell (spellType) ) {
					if (currentHero.HasSpell (spellType) && spellType.SpellLevel == maxSpellLevels[spellType.Name]) {
						canBuySpell = false;
					} else {
						return false;
					}
				}				
				
				canBuySpell = canBuySpell && currentHero.HasEnoughMoneyToBuySpell (spellType);
				
				return canBuySpell;	
			}
		}
		
		#region ICreationWindowElement implementation
		public override bool IsDisplayable {
			get {
				if (!currentHero.CanTrainSpell (spellType) ) {
					if (currentHero.HasSpell (spellType) && spellType.SpellLevel == maxSpellLevels[spellType.Name]) {						
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			}
		}
		#endregion

		public override string ActionName {
			get {
				return "Study";
			}
		}
		
		private int prevCount = 0;
		
		public override void OnUpdate ()
		{		
			int currentCount = spellType.SpellLevel;
			if (currentCount != prevCount) {
				if (currentCount == 0) {
					this.count = null;
				} else {
					this.count = currentCount + "";
				}
			}
			this.prevCount = currentCount;
		}
		#endregion
	}
}

