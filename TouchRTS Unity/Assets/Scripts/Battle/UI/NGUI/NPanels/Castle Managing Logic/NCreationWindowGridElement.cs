using UnityEngine;
using System.Collections;
using CastleManagingLogic;

public class NCreationWindowGridElement : NGridElement
{
	protected UILabel label;
	protected UIGrid owner;
	
	public override void Init ()
	{
		base.Init ();
		
		label = this.transform.FindChild ("Label").GetComponent <UILabel> ();
		owner = transform.parent.gameObject.GetComponent <UIGrid> ();
	}
	
	private ICastleManagingWidgetElement data;

	public ICastleManagingWidgetElement Data {
		get {
			return this.data;
		}
		set {
			data = value;
			if (data == null) {
				this.gameObject.SetActive (false);
				this.gameObject.name = "null";			
				return;
			}
			this.gameObject.name = data.ElementName;
			this.SetSprite (data.AtlasName, data.SpriteName);
			this.Count = this.data.Count;
		}
	}
	
	public string Count {
		set {
			label.text = value;
		}
	}
	
	public override void OnUpdate () {
		if (this.Data == null) {
			return;
		}
		this.Data.OnUpdate ();
		this.Count = this.data.Count;
		this.gameObject.SetActive (this.Data.IsDisplayable);
		
		owner.Reposition ();
	}
}

