using System;
using UI;
using Entity;
using System.Collections.Generic;

namespace CastleManagingLogic
{
	public class BuildingConstructionWindowLogic : CastleManagingWidgetLogic
	{
		private ICastleManagingWidget window;
		
		public BuildingConstructionWindowLogic ()
		{
		}
		
		private readonly LinkedList<ICastleManagingWidgetElement> dataList = new LinkedList<ICastleManagingWidgetElement> ();
		public BuildingSubject ourCastleBuilding;
		public CastleConstructor castleConstructor;
		
		#region CreationWindowLogic implementation
		public LinkedList<ICastleManagingWidgetElement> MakeContentObjects ()
		{
			this.Prepare ();			
			
			foreach (BuildingType buildingType in castleConstructor.OutbuildingsTypes.Keys) {
				this.dataList.AddLast (new BuildingWindowElement (buildingType, castleConstructor));
			}
			
			return this.dataList;
		}
		
		private void Prepare () {
			this.dataList.Clear ();
			ourCastleBuilding = this.window.BuildingOwner;
			castleConstructor = this.ourCastleBuilding.CastleConstructor;		
		}

		public string WindowTitle {
			get {
				return "Construction";
			}
		}
		
		public void Init (ICastleManagingWidget window)
		{
			this.window = window;
		}
		#endregion

		#region CreationWindowLogic implementation
		public void OnUpdate ()
		{
		}
		#endregion
	}
	
	class BuildingWindowElement : CreationWindowElement {
		
		public CastleConstructor castleConstructor;
		public BuildingType buildingType;
		
		private BuildingSubject ourBuilding = null;
		
		public BuildingWindowElement (BuildingType buildingType, CastleConstructor castleConstructor) {
			this.atlasName = "rex";
			this.spriteName = "rex8";
				
			this.elementName = buildingType.Name;
			this.elementDescription = buildingType.Description;			
			this.cost = buildingType.Price + "";
			this.count = "";
			
			this.buildingType = buildingType;
			this.castleConstructor = castleConstructor;
			
			this.actionButtonText = "Construct";
		}
		
		#region implemented abstract members of CommonCreationWindow.CreationWindowElement
		public override void OnObjectAction ()
		{
			if (CanConstruct) {
				ServerCommandController.Instance.ConstructBuilding (castleConstructor, buildingType, false);
				return;
			}
			
			if (CanUpgrade) {
				ServerCommandController.Instance.StartUpgradeBuilding (ourBuilding);
			}
		}
		
		private bool CanConstruct {
			get {
				bool isEnabled = castleConstructor.CanConstruct (buildingType);
				return isEnabled;
			}
		}
		
		private bool CanUpgrade {
			get {
				if (!IsBuildingConstructed) {
					return false;
				}
				return ourBuilding.CanUpgrade;
			}
		}
		
		private bool IsBuildingConstructed {
			get {
				if (ourBuilding == null) {
					foreach (BuildingSubject outbuilding in castleConstructor.Outbuildings) {
						if (outbuilding.BuildingType == buildingType) {
							this.actionButtonText = "Upgrade";
							ourBuilding = outbuilding;
							return ourBuilding.CanUpgrade;
						}
					}
					return false;
				} else {
					return ourBuilding.CanUpgrade;
				}
			}
		}
		
		public override bool IsEnabled {
			get {
				return CanConstruct || CanUpgrade;
			}
		}
		
		public override void OnUpdate ()
		{
			
		}

		public override string ActionName {
			get {
				return "Construct";
			}
		}
		#endregion
	}
}

