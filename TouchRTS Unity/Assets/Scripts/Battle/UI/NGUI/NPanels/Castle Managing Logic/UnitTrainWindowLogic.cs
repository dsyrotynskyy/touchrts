using System;
using UI;
using Entity;
using System.Collections.Generic;

namespace CastleManagingLogic
{
	public class UnitTrainWindowLogic : CastleManagingWidgetLogic
	{
		private ICastleManagingWidget window;
		
		private BuildingSubject currentBuilding;
		private UnitType unitType;
	
		public UnitTrainWindowLogic ()
		{
		}
		private readonly LinkedList<ICastleManagingWidgetElement> dataList = new LinkedList<ICastleManagingWidgetElement> ();
		
		public LinkedList<ICastleManagingWidgetElement> MakeContentObjects ()
		{
			this.Prepare ();			
			
			foreach (UnitType unitType in currentBuilding.PlayerOwner.UnitsToSpawn) {
				this.dataList.AddLast (new UnitWindowElement (currentBuilding, unitType));
			}
			
			return this.dataList;
		}
		
		private void Prepare () {
			this.dataList.Clear ();
			currentBuilding = this.window.BuildingOwner;
		}

		public string WindowTitle {
			get {
				return "Units";
			}
		}

		#region CreationWindowLogic implementation
		public void Init (ICastleManagingWidget window)
		{
			this.window = window;
		}
		#endregion

		#region CreationWindowLogic implementation
		public void OnUpdate ()
		{
		}
		#endregion
	}
	
	class UnitWindowElement : CreationWindowElement {		
		public BuildingSubject currentBuilding;
		public UnitType unitType;
	
		public UnitWindowElement (BuildingSubject currentBuilding, UnitType unitType) {
			this.currentBuilding = currentBuilding;
			this.unitType = unitType;
			
			this.atlasName = "rex";
			this.spriteName = "rex7";
				
			this.elementName = unitType.Name;
			this.elementDescription = unitType.Description;			
			this.cost = unitType.Price + "";
			
			this.actionButtonText = "Train";
		}
		
		#region implemented abstract members of CommonCreationWindow.CreationWindowElement
		public override void OnObjectAction ()
		{
			UnitGroupSubject unitGroup = InputController.CurrentUnitGroup;
			if (unitGroup != null) {
				unitGroup = unitGroup.IsEnoughCloseForUnitSpawn (currentBuilding) ? unitGroup : null;	
			}		
			ServerCommandController.Instance.SpawnUnit (currentBuilding, unitType, unitGroup);
		}
		
		public override bool IsEnabled {
			get {
				return currentBuilding.CanSpawn (unitType);	
			}
		}

		public override string ActionName {
			get {
				return "Train";
			}
		}
		
		private int prevCount = 0;
		
		public override void OnUpdate ()
		{
			int currentCount = currentBuilding.BuildingUnitSubject.UnitSpawnManager.GetNumUnits (unitType);
			if (currentCount != prevCount) {
				if (currentCount == 0) {
					this.count = null;
				} else {
					this.count = currentCount + "";
				}
			}
			this.prevCount = currentCount;
		}
		#endregion
	}
}

