using System;
using System.Collections.Generic;

namespace CastleManagingLogic {
	
	public interface ICastleManagingWidget {
		BuildingSubject BuildingOwner {
			get ;
		}

		HeroSubject HeroTarget {
			get ;
		}
	}
	
	public interface CastleManagingWidgetLogic
	{
		string WindowTitle {
			get;
		}

		void Init (ICastleManagingWidget window);
		
		void OnUpdate ();
		
		LinkedList<ICastleManagingWidgetElement> MakeContentObjects ();
	}	
	
	public interface ICastleManagingWidgetElement {
		string Cost {
			get;
		}
		
		string Count {
			get;
		}
		
		string ElementName {
			get;
		}
		
		string ElementDescription {
			get;
		}
		
		void OnObjectAction ();
		
		string AtlasName {
			get;
		}
		
		string SpriteName {
			get;
		}
		
		string ActionName {
			get;
		}
		
		bool IsEnabled {
			get;
		}
		
		bool IsDisplayable {
			get;
		}
		
		string ActionButtonText {
			get;
		}
		
		void OnUpdate ();
	}
	
	public abstract class CreationWindowElement : ICastleManagingWidgetElement {
		
		protected string actionButtonText;
		protected string elementName;
		protected string elementDescription;
		protected string atlasName;
		protected string spriteName;
		protected string cost;
		protected string count;
		
		#region ICreationWindowElement implementation
		public abstract void OnObjectAction ();

		public string ElementDescription {
			get {
				return elementName;
			}
		}

		public string AtlasName {
			get {
				return atlasName;
			}
		}

		public string SpriteName {
			get {
				return spriteName;
			}
		}

		public string ElementName {
			get {
				return elementName;
			}
		}
		
		public abstract bool IsEnabled {
			get;
		}
		
		public abstract string ActionName {
			get ;
		}
		
		public string Cost {
			get {
				return cost;
			}
		}

		public string Count {
			get {
				return count;
			}
		}
		
		public virtual void OnUpdate ()
		{
			
		}
		#endregion

		#region ICreationWindowElement implementation
		public virtual bool IsDisplayable {
			get {
				return true;
			}
		}
		#endregion

		#region ICastleManagingWidgetElement implementation
		public string ActionButtonText {
			get {
				return actionButtonText;
			}
		}
		#endregion
	}
}


