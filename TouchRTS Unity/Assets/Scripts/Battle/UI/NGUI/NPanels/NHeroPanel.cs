using UnityEngine;
using System.Collections;
using UI;

public class NHeroPanel : MonoBehaviour, GameWidget
{
	public GameObject HeroLifeBar;
	public GameObject HeroManaBar;
	
	private UISlider heroLifeSlider;
	private UISprite lifeBackgroundSprite;
	private UISlider heroManaSlider;
	private UISprite manaBackgroundSprite;
	
	void Start ()
	{	
		
	}
	
	public void Init (WorldController controller) {
		UIEventListener.Get (gameObject).onClick += delegate(GameObject go)
		{
			if (NGUIController.Instance.DisableInputInteraction ()) {
				return;
			}
		
			StartCoroutine (OnClickLogic (0.1f));
		};
		
		heroLifeSlider = HeroLifeBar.GetComponent <UISlider> ();
		lifeBackgroundSprite = heroLifeSlider.transform.FindChild ("Foreground").GetComponent <UISprite> ();
		heroManaSlider = HeroManaBar.GetComponent <UISlider> ();
		manaBackgroundSprite = HeroManaBar.transform.FindChild ("Foreground").GetComponent <UISprite> ();	
	}
	
	IEnumerator OnClickLogic (float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		
		if (WorldController.Instance.HumanPlayer != null) {
			HeroSubject hero = InputController.CurrentHero;
		
			if (hero == null) {
				hero = WorldController.Instance.HumanPlayer.Heroes [0];
			}
			
			InputController.CurrentUnitGroup = hero.HeroesGroup;					
		}	
	}

	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return true;
	}

	public bool IsDialog ()
	{
		return false;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{
		HeroSubject hero = InputController.CurrentHero;
		if (hero != null) {
			HeroLifeBar.SetActive (true);
			HeroManaBar.SetActive (true);			
		} else {
			HeroLifeBar.SetActive (false);
			HeroManaBar.SetActive (false);
			return;
		}
		
		this.heroLifeSlider.sliderValue = hero.HeroesUnit.GetLifePercent ();
		this.heroLifeSlider.ForceUpdate ();
		this.lifeBackgroundSprite.MarkAsChanged ();
		
		this.heroManaSlider.sliderValue = hero.HeroesUnit.GetManaPercent ();
		this.heroManaSlider.ForceUpdate ();
		this.manaBackgroundSprite.MarkAsChanged ();	
	}
	#endregion
}

