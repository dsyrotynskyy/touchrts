using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;
using Entity;

public class NSpellPanel : MonoBehaviour, SpellPanel
{
	public GameObject SpellGrid;
	
	private readonly List<NSpellPanelElement> nGridElements = new List<NSpellPanelElement> ();	
	private readonly LinkedList<ISpellPanelElement> dataList = new LinkedList<ISpellPanelElement> ();
		
	public void Init (WorldController worldController) {
		foreach (Transform child in SpellGrid.transform) {
			NSpellPanelElement element = child.gameObject.AddComponent<NSpellPanelElement> ();		
			element.Init ();
			nGridElements.Add (element);
		}
	}

	#region SpellPanel implementation
	public void LoadHeroSpells ()
	{
		currentHero = InputController.CurrentHero; 
		
		foreach (NSpellPanelElement element in nGridElements) {
			element.Data = null;		
		}
		dataList.Clear ();
		
		if (currentHero == null) {
			return;
		}
		
		foreach (SpellType spellType in InputController.CurrentHero.Spells) {
			dataList.AddLast (new HeroAbilityElement (spellType, InputController.CurrentHero));
		}
		
		UnitGroupSubject unitGroup = InputController.CurrentUnitGroup;
		foreach (AbilityType ability in unitGroup.UnitsSkills.Keys) {
			dataList.AddLast (new UnitAbilityElement (ability, unitGroup));
		}
		
		int i = 0;
		foreach (ISpellPanelElement data in dataList) {
			NSpellPanelElement element = nGridElements[i++];
			element.Data = data;
		}
	}
	#endregion

	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		return true;
	}

	public bool IsDialog ()
	{
		return true;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}
	
	private HeroSubject currentHero = null;

	public void OnUpdate ()
	{
		bool res = InputController.CurrentHero != null && !InputController.CurrentHero.IsDead;
		if (!res) {
			InputController.SetRTSControlType ();			
		} else {
			if (!this.gameObject.activeSelf || InputController.CurrentHero != currentHero) {
				LoadHeroSpells ();
			}
		}
		
		if (res) {
			foreach (NSpellPanelElement element in nGridElements) {
				element.OnUpdate ();	
			}
		}
	}
	#endregion

	#region SpellPanel implementation
	public void SetEnabled (bool enabled)
	{
		this.gameObject.SetActive (enabled);
	}
	#endregion


	#region SpellPanel implementation
	public void OnDisableSpellSpawnInput ()
	{
	}
	#endregion


}

