using UnityEngine;
using System.Collections;
using UI;
using Entity;

public class NGamePlayerInfoLabel : MonoBehaviour, GameWidget
{
	public GameObject MoneyLabel;
	public GameObject UnitsLabel;
	public GameObject PowerLabel;
	
	private UILabel moneyLabel;
	private UILabel unitsLabel;
	private UILabel powerLabel;
	
	public void Init (WorldController worldController) {
		this.moneyLabel = MoneyLabel.GetComponent <UILabel> ();
		this.unitsLabel = UnitsLabel.GetComponent <UILabel> ();
		this.powerLabel = PowerLabel.GetComponent <UILabel> ();
	}
	
	#region GameWidget implementation
	public Rect GetScreenArea ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDisplaying ()
	{
		throw new System.NotImplementedException ();
	}

	public bool IsDialog ()
	{
		return false;
	}

	public void DrawGUI ()
	{
		throw new System.NotImplementedException ();
	}

	public void OnUpdate ()
	{
		PlayerData player = WorldController.Instance.HumanPlayer;
		if (player == null) {
			return;
		}
				
		string moneyString = "Money: " 
			+ player.MoneyCount;
		
		this.moneyLabel.text = moneyString;
		
		this.unitsLabel.text = "Units: " + player.UsedUnitPlaces + "/" + player.AllUnitPlaces;
		this.powerLabel.text = "Power: " + player.PointsOfPower;
	}
	#endregion
}

