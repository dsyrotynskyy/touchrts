using UnityEngine;
using System.Collections;

public class RTSGameInputController : MonoBehaviour
{
	private const int CENTERING_COORD = 10;
	private const int COMPUTER_DRAG_SPEED = 180;
	private const int ANDROID_DRAG_SPEED = 15;
	public int ScrollSpeed = 25;
	public int dragSpeed = 5;
	public int ZoomSpeed = 5;
	public int ZoomMax = 100;
	public int PanSpeed = 20;
	public int PanAngleMin = 25;
	public int PanAngleMax = 80;
 
	//Camera bound on X axis
	public float LimitationXMin = 10;
	public float LimitationXMax;
	//Camera bound on Z axis
	public float LimitationZMin;
	public float LimitationZMax;
	public float LimitationYMin;
	public float LimitationYMax = 20;
	private bool isDraggingAllowed = false;
	private int dragStartedTime = 0;
	private	float prevZoomDistance = -1;
	private	int prevTouchCount = 0;
	
	private const int MINIMUM_DRAG_TIME_OFFSET = 1000;
	private const float MAXIMUM_CLICK_DISTANCE = 1f;
		
	private Vector3 dragStartMousePosition;
	private Vector3 touchDownPoint = Vector3.zero;
	private Vector3 postTranslation = Vector3.zero;
	private float postZoom = 0f;
	
	private int currentLeftClickTime;
	private int prevLeftClickTime;
	
	public void Init (WorldController worldController) {		
		this.InitCamera ();
	}
	
	private bool inited = false;
	
	private void InitCamera ()
	{
		inited = true;
		
		Quaternion quat = new Quaternion ();
		quat.eulerAngles = new Vector3 (60, 0, 0);
		transform.rotation = quat;
		
		LimitationYMin = NavigationEngine.TerrainNormalHeight + 10;
		LimitationYMax = NavigationEngine.TerrainNormalHeight + 25;
		
		float yPos = (LimitationYMin) + 10;		
		
		transform.position = new Vector3 (transform.position.x, yPos, transform.position.z);
		
		float size = camera.orthographicSize / camera.aspect;
		MonoBehaviour.print ("Camera offset = " + size);
		
		LimitationXMax = WorldController.Instance.WorldData.XSize - size;
		LimitationZMax = WorldController.Instance.WorldData.ZSize - size;
		
		LimitationXMin = 25;
		LimitationZMin = 0;
		
		this.SetDragSpeed ();
	}

	public void OnUpdate ()
	{		
		this.ProcessPostInput ();
		
		if (MiniMap.IsMinimapTouch || UICamera.touchCount > 0) {
			return;
		}
		
		if (InputController.UI.IsUIClick) {
			isDraggingAllowed = false;
			return;
		}
		
		if (!InputController.IsRTSControl ()) {			
			return;
		}
		
		if (Input.touchCount == 0 && !Input.GetMouseButton (0)) {
			this.ResetTouchData ();
		}
		this.ProcessInput ();
	}
	
		
	private void LateUpdate ()
	{   
		if (cameraMovementCount > 0) {
			transform.position = Vector3.Lerp (transform.position, desiredPos, 1.0f / cameraMovementCount);
			cameraMovementCount--;
		} 
				
		float posX = Mathf.Clamp (transform.position.x, LimitationXMin, LimitationXMax);
		float posY = Mathf.Clamp (transform.position.y, LimitationYMin, LimitationYMax);
		float posZ = Mathf.Clamp (transform.position.z, LimitationZMin, LimitationZMax);
     
		Vector3 inBoundsPosition = new Vector3 (posX, posY, posZ);
       
		transform.position = inBoundsPosition;
	}
	
	private void ProcessPostInput ()
	{
		postTranslation /= 1.5f;
		transform.position += postTranslation;			
		
		this.postZoom /= 1.4f;
		float posY = transform.position.y - postZoom;
		
		transform.position = new Vector3 (transform.position.x, posY, transform.position.z);
	}
	
	private void ResetTouchData ()
	{
		this.prevZoomDistance = -1;
		this.prevTouchCount = 0;
		this.isDraggingAllowed = false;
	}
	
	private void ProcessInput ()
	{
		if (isDraggingAllowed) {
			ProcessDragging ();
		} 		
		
		if (Input.GetMouseButtonDown (0)) {	
			isDraggingAllowed = true;
			touchDownPoint = GetClickCoord ();
			dragStartedTime = System.Environment.TickCount;	
			
			dragStartMousePosition = Input.mousePosition;
		}
		
		int PLATFORM_INPUT_COUNT = 0;
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			PLATFORM_INPUT_COUNT = 1;
		}
		
		if (Input.GetMouseButtonUp (0) && Input.touchCount == PLATFORM_INPUT_COUNT) {
			this.isDraggingAllowed = false;
			currentLeftClickTime = System.Environment.TickCount;
			if (currentLeftClickTime - dragStartedTime < MINIMUM_DRAG_TIME_OFFSET) {
				ProcessClick ();									
			} 
			prevLeftClickTime = currentLeftClickTime;
			touchDownPoint = Vector3.zero;
		}
	}
	
	private void SetDragSpeed ()
	{
		this.dragSpeed = COMPUTER_DRAG_SPEED;
		
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			this.dragSpeed = ANDROID_DRAG_SPEED;
		}
	}
	
	void ProcessDragging ()
	{		
		if (NGUIController.Instance.DisableInputInteraction ()) {
			return;
		}
		
		if (Input.touchCount > 1) {
			prevTouchCount = Input.touchCount;
			ProcessZoom ();
		}
		
		if (prevTouchCount > 1) {
			prevTouchCount = Input.touchCount;
			return;
		}		
		
		prevCommandFollowPoint = false;
		
		Vector3 translation = Vector3.zero;
		translation -= new Vector3 (Input.GetAxis ("Mouse X") * dragSpeed * Time.deltaTime, 0,
        Input.GetAxis ("Mouse Y") * dragSpeed * Time.deltaTime);
		transform.position += translation;
		
		float translationMinValue = 0.5f;
		if (Vector3.Distance(Input.mousePosition , dragStartMousePosition) > 20) {
			if (Mathf.Abs(translation.x) > translationMinValue) {
				this.postTranslation.x = translation.x;		
			}
			
			if (Mathf.Abs(translation.z) > translationMinValue) {
				this.postTranslation.z = translation.z;		
			}			
		}
	}
	
	#region Camera
	
	void ProcessZoom ()
	{
		Vector2 touch1 = Input.touches [0].position;
		Vector2 touch2 = Input.touches [1].position;
		
		float curDistance = Vector2.Distance (touch1, touch2);
		
		if (prevZoomDistance > 0) {
			float distanceOffset = curDistance - prevZoomDistance;			
			RaiseCameraPosition (distanceOffset / 30);		
		}
		this.prevZoomDistance = curDistance;	
	}
	
	private void RaiseCameraPosition (float raiseValue)
	{
		float posY = transform.position.y - raiseValue;
		
		transform.position = new Vector3 (transform.position.x, posY, transform.position.z);
		
		this.postZoom = raiseValue;
	}
	
	private Vector3 desiredPos;
	private int cameraMovementCount = 0;
	
	public void MoveCameraToCoord (Vector3 newDesiredPos)
	{
		prevCommandFollowPoint = false;
		this.desiredPos = new Vector3 (newDesiredPos.x, this.transform.position.y, newDesiredPos.z);
		cameraMovementCount = 5;
	}
	
	#endregion
	
	private void ProcessClick ()
	{
		if (NGUIController.Instance.DisableInputInteraction () || !inputEnabled) {
			return;
		}		
		
		Ray ray = camera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
				
		if (Physics.Raycast (ray.origin, ray.direction, out hit)) {	
			if (Vector3.Distance (hit.point, this.touchDownPoint) < MAXIMUM_CLICK_DISTANCE) {
				HandleObjectClick (hit.collider.gameObject, hit.point);
			}			
		}
	}
	
	private void HandleObjectClick (GameObject gameObject, Vector3 point)
	{
		if (gameObject == null) {
			return;	
		}
		
		if (InputController.IsDialogShowing ()) {
			return;
		}
			
		if (gameObject.tag == "Hero" || gameObject.tag == "Unit") {
			UnitSubject unit = (UnitSubject)gameObject.GetComponent (typeof(UnitSubject));
			if (unit.ProcessClick ()) {				
			} else if (InputController.CurrentHero != null) {
				InputController.CurrentHero.MoveHeroGroupToUnmodifiedPoint (new Vector3 (point.x, NavigationEngine.TerrainNormalHeight, point.z));			
			}
			return;
		}
		
		if ((gameObject.tag == "Ground" || gameObject.tag.Equals (Setup.GameSettings.Tag.NAVIGATION)) && InputController.CurrentHero != null) {
			MoveUnitGroupSingleClick (point);
			return;
		}
		
		if (gameObject.tag == "Building") {
			BuildingSubject beh = (BuildingSubject)gameObject.GetComponent (typeof(BuildingSubject));
//			if (InputController.CurrentHero != null) {										
//				InputController.CurrentHero.MoveHeroGroupToUnmodifiedPoint (beh.BuildingUnitGroup.MovementTarget.position);
//			}
			beh.HandleClick ();
			return;
		}
		
		return;
	}
	
	private Vector3 prevDoubleClickPoint = Vector3.zero;
	
	private void MoveUnitGroupDoubleClick(Vector3 newPoint) {
		int timeOffset = currentLeftClickTime - prevLeftClickTime;
		if (timeOffset < 500 && Vector3.Distance (prevDoubleClickPoint, newPoint) < 1f) {
			InputController.CurrentHero.MoveHeroGroupToUnmodifiedPoint (newPoint);		
		}	
		prevDoubleClickPoint = newPoint;
	}
	
	private void MoveUnitGroupSingleClick(Vector3 newPoint) {
		int timeOffset = System.Environment.TickCount - dragStartedTime;
		if (timeOffset < 200) {
			InputController.CurrentHero.MoveHeroGroupToUnmodifiedPoint (newPoint);		
		}	
	}
	
	private bool prevCommandFollowPoint = false;

	public void FollowCurrentHero ()
	{
		this.FollowThePoint (InputController.CurrentHero.transform.position, true);
	}
	
	public void FollowThePoint (Vector3 point, bool followAnyway = false)
	{
		if (isDraggingAllowed && !followAnyway) {
			return;
		}
		
		this.FollowThePointFast (point);
	}
	
	public void FollowThePointFast (Vector3 point)
	{
		this.FollowThePoint (point, 15);
	}
	
	public void FollowThePointSlow (Vector3 point)
	{
		this.FollowThePoint (point, 85);
	}
	
	public void FollowThePoint (Vector3 point, int speed)
	{
		Vector3 newCameraPos = point;
		newCameraPos.y = transform.position.y;
		newCameraPos.z -= CENTERING_COORD;
		this.desiredPos = newCameraPos;
		
		if (prevCommandFollowPoint) {
			cameraMovementCount = 1;
		} else {
			cameraMovementCount = 15;
			prevCommandFollowPoint = true;
		}
	}
	
	private Vector3 GetClickCoord ()
	{		
		Ray ray = camera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast (ray.origin, ray.direction, out hit)) {	
			return hit.point;
		}
		return Vector3.zero;
	}

	private bool inputEnabled = true;
	
	public void SetInputEnabled (bool enabled)
	{
		this.inputEnabled = enabled;
	}
}


