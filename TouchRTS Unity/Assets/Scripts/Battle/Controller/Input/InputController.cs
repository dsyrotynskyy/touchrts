using UnityEngine;
using System.Collections.Generic;
using UI;

public class InputController : MonoBehaviour
{
	private static bool USE_NGUI = true;
	
	enum InputControlType
	{
		RTSControl,
		SpawnSpell
	};
	
	/*
	 * Selected input handler - spawn spell or rts game camera/unit control
	 * */
	private static InputControlType inputControlType = InputControlType.RTSControl;
	private static UnitGroupSubject currentUnitGroup;
		
	private static UIController UIController;
	private static RTSGameInputController rtsCamera;
	private static SpellSpawnController spellSpawnController;		
	private static bool UITouchHandled;
	
	private static InputController instance;
	
	public void Init(WorldController worldController) {
		instance = this;
		instance.Prepare(worldController);
	}
	
	public void Prepare (WorldController worldController)
	{		
		this.AttachInputScripts (worldController);
		
		if (USE_NGUI) {
			GameObject nguiRoot = GameObject.Find ("NGUI Root");
			if (nguiRoot == null) {
				nguiRoot = ObjectController.CreatePrefab ("NGUI Root", "Input");			
			}
			UIController = nguiRoot.GetComponent<NGUIController> ();
			UIController.Init (worldController);
		} 
	}
	
	/*
	 * Attach input control scripts
	 * */
	private void AttachInputScripts (WorldController worldController)
	{
		spellSpawnController = gameObject.GetComponent<SpellSpawnController> ();
		if (spellSpawnController == null) {
			spellSpawnController = gameObject.AddComponent<SpellSpawnController> ();
		}
		rtsCamera = gameObject.GetComponent<RTSGameInputController> ();
		if (rtsCamera == null) {
			rtsCamera = gameObject.AddComponent<RTSGameInputController> ();
		}
		rtsCamera.Init (worldController);
	}
	
	public static bool MiniMapTouchHappened {
		get {
			return UITouchHandled;
		}
		set {
			UITouchHandled = value;
		}
	}
	
	public void OnUpdate () {
		UITouchHandled = false;
		
		UIController.OnUpdate ();
		
		if (IsRTSControl ()) {
			rtsCamera.OnUpdate ();
		} else if (IsSpawnSpell ()) {
			spellSpawnController.OnUpdate ();
		}
		
		if (InputController.IsSpawnSpell ()) {
			rtsCamera.FollowCurrentHero ();			
		}
	}
	
	public static void SetRTSControlType ()
	{
		inputControlType = InputControlType.RTSControl;
		UI.SpellPanel.OnDisableSpellSpawnInput ();
		SpellSpawnController.SpawnSpellType = null;;
	}
	
	public static void SetSpawnSpellControlType ()
	{
		inputControlType = InputControlType.SpawnSpell;
	}
	
	public static bool IsRTSControl ()
	{
		return inputControlType == InputControlType.RTSControl;
	}
	
	public static bool IsSpawnSpell ()
	{
		if (InputController.CurrentHero == null) {
			return false;
		}
		return inputControlType == InputControlType.SpawnSpell;
	}
	
	public static UnitGroupSubject CurrentUnitGroup {
		get {
			return currentUnitGroup;
		}
		
		set {	
			currentUnitGroup = value;
			SetCurrentGroup (currentUnitGroup, true);
		}
	}

	public static void SetCurrentGroup (UnitGroupSubject group1, bool followPosition)
	{
		currentUnitGroup = group1;
		spellSpawnController.StopSpellCasting ();
		
		if (followPosition) {
			FollowPosition ();
		}
		
		if (currentUnitGroup == InputController.CurrentUnitGroup) {
			UI.SpellPanel.LoadHeroSpells ();
		}
	}

	public static HeroSubject CurrentHero {
		get {
			if (currentUnitGroup == null) {
				return null;
			}
			return currentUnitGroup.GroupHero;
		}
	}
	
	private static void FollowPosition () {
		if (NGUIController.Instance.DisableInputInteraction ()) {
			return;
		}
		
		HeroSubject hero = CurrentUnitGroup.GroupHero;
		
		Vector3 newCameraPos; 
		if (hero == null || hero.HeroesUnit.IsDead ) {
			newCameraPos = CurrentUnitGroup.MovementTarget.position;	
		} else {
			newCameraPos = hero.transform.position;	
		}
		
		rtsCamera.FollowThePoint (newCameraPos);
	}
	
	public static bool IsDialogShowing ()
	{
		return UI.IsDialogShowing ();
	}
	
	public static bool CoordCanBeDisplayed (Vector3 coord)
	{
		return UIController.CoordCanBeDisplayed (coord);
	}

	public static RTSGameInputController RTSCamera {
		get {
			return rtsCamera;
		}
	}
	
	public static UIController UI {
		get {
			return UIController;
		}
	}

	public static InputController Instance {
		get {
			return instance;
		}
	}
	
	public void OnDestroy () {
		if (instance == this) {
			instance = null;
		}
		currentUnitGroup = null;
		
		UIController = null;
		rtsCamera = null;
		spellSpawnController = null;		
		UITouchHandled = false;
	}
}

