using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class SpellSpawnController : MonoBehaviour
{	
	public static readonly int SPELL_STOP_TIME_PERIOD = 5000;
	enum SpellInputType
	{
		SpawnObject,
		MoveObject,
		RaySpell
	};
	
	private static Vector3 nilPoint = Vector3.zero;
	private bool isDragging;
//	private static SpellInputType spellInputType;
	private static SpellType currentSpellType;
	private int groundMask;
	private static Timer stopSpellTimer = Timer.CreateFrequency (SPELL_STOP_TIME_PERIOD);
	
	void Start ()
	{
		int m1 = 1 << LayerMask.NameToLayer ("Ground");
		int m2 = 1 << LayerMask.NameToLayer ("UnitBody");
		groundMask = m1 | m2;
		InvokeRepeating ("ProcessDragging", 0.2f, 0.2f);
	}

	public void OnUpdate ()
	{
		if (InputController.UI.IsUIClick) {
			return;
		}
		
		if (MiniMap.IsMinimapTouch || UICamera.touchCount > 0) {
			return;
		}
		
		if (!InputController.IsSpawnSpell () || currentSpellType == null || InputController.CurrentHero == null || InputController.CurrentHero.IsDead || stopSpellTimer.EnoughTimeLeft ()) {
			isDragging = false;	
			StopSpellMode ();
			return;
		}
		
		if (Input.GetMouseButtonDown (0)) {			
			SpawnObjectClick ();			
			isDragging = true;			
		}
		
		if (Input.GetMouseButtonUp (0)) {
			isDragging = false;	
			StopSpellCasting ();			
		}
	}
	
	private void ProcessDragging ()
	{
		if (MiniMap.IsMinimapTouch || UICamera.touchCount > 0) {
			return;
		}
		
		if (!InputController.IsSpawnSpell () || !isDragging) {			
			return;
		}
		
		if (InputController.UI.IsUIClick) {
			return;
		}
		
		if (currentSpellType == null || !InputController.CurrentHero.CanSpawnSpell (currentSpellType)) {
			isDragging = false;	
			StopSpellMode ();			
			return;
		}
		
		Vector3 point = GetClickCoord ();
		if (point == nilPoint) {
			return;
		} 
		
		if (currentSpellType == null || !InputController.CurrentHero.CanSpawnSpell (currentSpellType)) {
			StopSpellMode ();
			return;
		}
		
		stopSpellTimer.Restart ();
			
		CommandController.Instance.DoSpell (InputController.CurrentHero, SpawnSpellType, point);
//			
//		
//		if (spellInputType == SpellInputType.SpawnObject) {
//			InputController.CurrentHero.DoSpell (SpawnSpellType, point);
//		}
//		
//		if (spellInputType == SpellInputType.MoveObject || spellInputType == SpellInputType.RaySpell) {
//			if (!InputController.CurrentHero.IsSpawningSpell) {
//				CommandController.Instance.DoSpell (InputController.CurrentHero, SpawnSpellType, point);
//				InputController.CurrentHero.DoSpell (SpawnSpellType, point);
//			} else {
//				InputController.CurrentHero.ContinueSpawnSpell (SpawnSpellType, point);
//			}			
//		}
	}
	
	public void StopSpellCasting ()
	{		
		if (InputController.CurrentHero != null && InputController.CurrentHero.IsSpawningSpell) {
			CommandController.Instance.StopSpell (InputController.CurrentHero);
		}		
	}
	
	private void StopSpellMode () {
		SpawnSpellType = null;
		StopSpellCasting ();
		InputController.SetRTSControlType ();
	}
	
	private void SpawnObjectClick ()
	{	
		Vector3 point = GetClickCoord ();
		if (point == nilPoint) {
			return;
		}	
		CommandController.Instance.DoSpell (InputController.CurrentHero, SpawnSpellType, point);
	}
		
	public static SpellType SpawnSpellType { 
		get { return currentSpellType;}
		set {
			if (currentSpellType == value) {
				return;
			}
			currentSpellType = value;
			if (currentSpellType == null) {
				if (InputController.CurrentHero != null) {
					CommandController.Instance.StopSpell (InputController.CurrentHero);
				}			
				return;
			} else {
				InputController.SetSpawnSpellControlType ();
			}
			stopSpellTimer.Restart ();
			
//			if (currentSpellType.IsMovableSpell ()) {
//				spellInputType = SpellInputType.MoveObject;
//			}
//				
//			if (currentSpellType.IsCreatingObjectOnMap ()) {
//				spellInputType = SpellInputType.SpawnObject;
//			}
//				
//			if (currentSpellType.IsRaySpell ()) {
//				spellInputType = SpellInputType.RaySpell;
//			}			
		}
	}
	
	private Vector3 GetClickCoord ()
	{
		if (InputController.UI.IsUIClick) {
			return nilPoint;
		}
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray.origin, ray.direction, out hit, float.PositiveInfinity, groundMask)) {	
			return hit.point;
		}
		return nilPoint;
	}
}
