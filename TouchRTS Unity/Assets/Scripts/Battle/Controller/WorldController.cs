using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;
using Setup;
using GameMapScenario;
using System.Threading;
using MovementEngine;

public class WorldController : MonoBehaviour
{
	public string MapName = "DefaultMap";
	public int WorldXSize = 0;
	public int WorldZSize = 0;
	public bool useMinimumGraphics;
	public bool drawOnGUI;
	public static WorldController instance;
	public static float DEFAULT_TERRAIN_HEIGHT = 30.1f;
	private GameObject heroesCategoryGameObject;
	private GameObject unitsCategoryGameObject;
	private GameObject decorationsCategoryGameObject;
	private GameObject buildingsCategoryGameObject;
	private GameObject spellsCategoryGameObject;
	private GameObject effectsCategoryGameObject;
	private GameObject navigationCategoryGameObject;
	private MapScenario mapScenario;
	private readonly WorldData worldData = new WorldData ();
	private LevelController levelController;
	private bool isDestroyed = false;
	private bool isPathfindingAsyncTaskFinished = true;
	private bool isUnitLogicAsyncTaskFinished = true;
	private readonly NavigationEngine navigationEngine = new NavigationEngine ();
	
	private bool isNetworkGame;
	private bool isNetworkGameClient;
	private bool isNetworkGameServer;
	
	public void Start ()
	{
		instance = this;	
		this.Init ();
	}
	
	private void Init ()
	{	
		ApplicationManager.OnNewWorldSceneStarted ();
		levelController = gameObject.AddComponent<LevelController> ();
		mapScenario = levelController.LoadScenario (this, Application.loadedLevelName);	
		this.PrepareWorld ();
		
		CommandController commandController = ObjectController.CreateCommandController ();
		commandController.Init (this);
		
		isNetworkGameClient = NetworkManager.Instance.IsNetworkGameClient;
		isNetworkGameServer = NetworkManager.Instance.IsNetworkGameServer;
		isNetworkGame = isNetworkGameClient || IsNetworkGameServer;
		
		OrganizeHeroesGameObjects ();
		OrganizeDecorationGameObjects ();
		OrganizeBuildingsGameObjects ();
		OrganizeSpellsGameObjects ();
		OrganizeEffectsGameObjects ();
		OrganizeUnitsGameObjects ();
		
		this.PrepareNavigation ();
		this.PrepareScripts ();						
		
		commandController.PrepareNetwork ();
				
		this.mapScenario.OnStart (this);
		TWorldTime.OnNewSceneStarted ();	
	}
	#region initialisation
	private void PrepareWorld ()
	{
		if (Terrain.activeTerrain != null) {
			Terrain.activeTerrain.gameObject.tag = "Ground";
			Terrain.activeTerrain.gameObject.layer = LayerMask.NameToLayer ("Ground");
			
			Terrain terrain = FindObjectOfType (typeof(Terrain)) as Terrain;
		
			this.WorldData.XSize = (int)terrain.terrainData.size.x;		
			this.WorldData.ZSize = (int)terrain.terrainData.size.z;
		}
	}
	
	private void PrepareNavigation ()
	{
		PrepareNavigationCategoryGameObject (); 
		GameObject[] navigationObjects = GameObject.FindGameObjectsWithTag (GameSettings.Tag.NAVIGATION);
		foreach (GameObject navigationObject in navigationObjects) {
			navigationObject.transform.parent = navigationCategoryGameObject.transform;
		}
		
		if (this.UseNavigationFigure) {
			if (Terrain.activeTerrain != null) {
				Terrain.activeTerrain.gameObject.tag = "Ground";
				Terrain.activeTerrain.gameObject.layer = LayerMask.NameToLayer ("Ground");
			
				Collider collider = Terrain.activeTerrain.GetComponent<Collider> ();
				Destroy (collider);		
				
				if (UseMinimumGraphics) {
					Terrain terrain = Terrain.activeTerrain.GetComponent<Terrain> ();
					terrain.enabled = false;	
				}					
			}
			
			GameObject navigationPlane = GameObject.CreatePrimitive (UnityEngine.PrimitiveType.Plane);
			navigationPlane.transform.position = new Vector3 (0, this.worldData.Height, 0);
			navigationPlane.transform.localScale = new Vector3 (100, 1, 100);
			navigationPlane.tag = GameSettings.Tag.GROUND;
			navigationPlane.layer = GameSettings.Layer.GROUND;
			navigationPlane.name = "Navigation Plane";
			navigationPlane.transform.parent = navigationCategoryGameObject.transform;
			Renderer meshRenderer = navigationPlane.GetComponent<Renderer> ();
			meshRenderer.enabled = false;
			
//			this.CreateMeshFromExistingNodes (true);
//			CalculateWorldPathTrianglesDijkstra ();
			NavigationEngine.Prepare (this, navigationNodes, obstacleNodes);
		
			this.HideNavigation ();
			
			UnitGroupSubject.CalculateClosestUnits (AllUnitGroups);
		} 
	}
	
	private void PrepareNavigationCategoryGameObject ()
	{
		navigationCategoryGameObject = GameObject.Find (GameSettings.Tag.NAVIGATION);
		if (navigationCategoryGameObject == null) {
			navigationCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
			navigationCategoryGameObject.name = GameSettings.Tag.NAVIGATION;			
		} 
	}
	
	private void PrepareScripts ()
	{
		InputController inputController = (InputController)FindObjectOfType (typeof(InputController));		
		inputController.Init (this);
		
		ObjectController objController = (ObjectController)FindObjectOfType (typeof(ObjectController));
		if (objController == null) {
			objController = this.gameObject.AddComponent<ObjectController> ();
		}
		objController.Init (this);
		
		players = FindObjectsOfType (typeof(PlayerSubject)) as PlayerSubject[];
		foreach (PlayerSubject player in players) {
			PlayerInfo info = PlayerInfo.Nill;
			if (this.mapScenario.PlayersInfo.ContainsKey (player.id)) {
				info = this.mapScenario.PlayersInfo [player.id];
			}
			;
			player.Init (this, info);
		}
		
		GameObject[] buildings = GameObject.FindGameObjectsWithTag ("Building");
		foreach (GameObject building in buildings) {
			BuildingSubject beh = (BuildingSubject)building.GetComponent (typeof(BuildingSubject));
			beh.Init (this);
			beh.transform.parent = buildingsCategoryGameObject.transform;
		}
		
		GameObject[] heroes = GameObject.FindGameObjectsWithTag ("Hero");
		foreach (GameObject hero in heroes) {
			HeroSubject beh = (HeroSubject)hero.GetComponent (typeof(HeroSubject));
			beh.Init (this);
			beh.transform.parent = heroesCategoryGameObject.transform;
		}

		this.AddOtherScripts ();
	}
	
	private void AddOtherScripts ()
	{
		if (GameSettings.Display.SHOW_FPS) {
			this.gameObject.AddComponent<FPSWidget> ();
		}
		gameObject.AddComponent<AttackAnimationController> ();
	}

	void OrganizeEffectsGameObjects ()
	{
		effectsCategoryGameObject = GameObject.Find ("Effects");
		if (effectsCategoryGameObject != null) {
			return;
		}
		effectsCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
		effectsCategoryGameObject.name = "Effects";		
	}
	
	private void OrganizeSpellsGameObjects ()
	{
		spellsCategoryGameObject = GameObject.Find ("Spells");
		if (spellsCategoryGameObject != null) {
			return;
		}
		spellsCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
		spellsCategoryGameObject.name = "Spells";		
	}
	
	private void OrganizeHeroesGameObjects ()
	{
		heroesCategoryGameObject = GameObject.Find ("Heroes");
		if (heroesCategoryGameObject == null) {
			heroesCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
			heroesCategoryGameObject.name = "Heroes";
		}
	}
	
	private void OrganizeDecorationGameObjects ()
	{
		decorationsCategoryGameObject = GameObject.Find ("Decorations");
		if (decorationsCategoryGameObject == null) {
			decorationsCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
			decorationsCategoryGameObject.name = "Decorations";
		}
		
		GameObject[] decorations = GameObject.FindGameObjectsWithTag ("Decoration");
		foreach (GameObject decoration in decorations) {
			decoration.transform.parent = decorationsCategoryGameObject.transform;
		}
	}
	
	private void OrganizeBuildingsGameObjects ()
	{
		buildingsCategoryGameObject = GameObject.Find ("Buildings");
		if (buildingsCategoryGameObject == null) {
			buildingsCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
			buildingsCategoryGameObject.name = "Buildings";
		}
		
	}
	
	private void OrganizeUnitsGameObjects ()
	{
		unitsCategoryGameObject = GameObject.Find ("Units");
		if (unitsCategoryGameObject == null) {
			unitsCategoryGameObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("EmptyGameObject"));
			unitsCategoryGameObject.name = "Units";
		}
	}

	public static WorldController Instance {
		get {
			return instance;
		}
	}
		
	int effectNumber = 0;

	public EffectData CreateEffect (EffectType effectType, EffectPerformer effectPerformer, UnitSubject unit)
	{
		EffectData effectData = new EffectData (effectNumber);
		effectData.Init (effectType, effectPerformer, unit);
		return effectData;
	}
		
	public PlayerData CreatePlayer (int id, string name, int team, bool isHuman, Color color)
	{
		if (AllPlayers.ContainsKey (id)) {
			MonoBehaviour.print ("Player with id " + id + " already exist!!!!!!");
			return null;
		}
			
		PlayerData player = new PlayerData ();
		player.init (id, name, team, isHuman ? PlayerData.CONTROL_TYPE_HUMAN : PlayerData.CONTROL_TYPE_COMPUTER, color);
		AllPlayers.Add (id, player);
			
		if (isHuman) {
			worldData.HumanPlayer = player;
		}
			
		return player;
	}
	
	public void RegisterNewUnit (UnitSubject unit)
	{
		this.AllUnits.Add (unit);
	}
	
	public void RegisterNewUnitGroup (UnitGroupSubject unitGroup)
	{
		this.AllUnitGroups.Add (unitGroup);
		WorldController.Instance.WorldData.OnNewUnitGroupCreated ();
	}
	
	#endregion
	
	private static bool DO_ASYNC_PATHFINDING = true;
	private SystemTimer asyncUpdateTimer = SystemTimer.CreateLifePeriod (1000);
	
	private void PushPathfindingAsyncTask ()
	{		
		if (isPathfindingAsyncTaskFinished && TWorldTime.Active && !isNetworkGameClient) {
			isPathfindingAsyncTaskFinished = false;
			Loom.RunAsync (() => {
				try {
					while (asyncUpdateTimer.IsActive) {
						int prevTickCount = System.Environment.TickCount;
						
						DoPathfindingAsyncTask ();	
						
						int currentTickCount = System.Environment.TickCount;
						
						int offset = currentTickCount - prevTickCount;
						
						Thread.Sleep (Mathf.Max (25 - offset, 0));
					}
				} catch (System.Exception e) {
					Debug.Log ("PathThread Error");	
					Debug.LogException (e);
				}
				isPathfindingAsyncTaskFinished = true;
			});
		}
	}
	
	private void DoPathfindingAsyncTask ()
	{				
		if (DO_ASYNC_PATHFINDING) {
			AllUnitGroups.ForEach (delegate(UnitGroupSubject unitGroup)	{
				unitGroup.UpdateUnitPathLogic ();
			});
		} 
	}
	
	private bool DO_CLOSEST_UNITS_CALCULATION_ASYNC = true;
	
	private void FixedUpdateUnitGroups ()
	{
		if (UnitGroupSubject.ShouldCalculateClosestUnits ()) {	
			if (DO_CLOSEST_UNITS_CALCULATION_ASYNC && !isNetworkGameClient) {
				Loom.RunAsync (() => {
					try {
						UnitGroupSubject.CalculateClosestUnits (AllUnitGroups);
					} catch (System.Exception e) {
						Debug.Log ("ClosestUnitsThread Error");	
						Debug.LogException (e);
					}
			});
			} else {
				UnitGroupSubject.CalculateClosestUnits (AllUnitGroups);
			}
		}
		
		AllUnitGroups.ForEach (delegate(UnitGroupSubject unitGroup)
		{
			unitGroup.OnFixedUpdate ();			
			if (!DO_ASYNC_PATHFINDING) {
				unitGroup.UpdateUnitPathLogic ();
			}
		});				
	}	
	
	private bool DO_ASYNC_UNIT_LOGIC_UPDATE = true;
	
	private void FixedUpdateUnits () {
		bool checkDeleting = deleteUnitTimer.EnoughTimeLeft ();	
		int i = 0;
		while (i < AllUnits.Count) {
			UnitSubject currentUnit = AllUnits [i];
			
			if (checkDeleting && !currentUnit.IsInited) {
				lock (AllUnits) {
					AllUnits.RemoveAt (i);
				}
				continue;
			}
			
			currentUnit.OnFixedUpdate ();	
			if (!DO_ASYNC_UNIT_LOGIC_UPDATE) {
				currentUnit.OnAsyncUpdate ();
			}
			i++;
		}
	}
	
	private void PushAsyncUpdateUnits () {
		if (isUnitLogicAsyncTaskFinished && TWorldTime.Active && !isNetworkGameClient) {
			isUnitLogicAsyncTaskFinished = false;
			Loom.RunAsync (() => {
				try {
					while (asyncUpdateTimer.IsActive) {
						int prevTickCount = System.Environment.TickCount;
						
						DoUnitUpdateLogicAsync ();	
						
						int currentTickCount = System.Environment.TickCount;
						
						int offset = currentTickCount - prevTickCount;
						
						Thread.Sleep (Mathf.Max (25 - offset, 0));
					}
				} catch (System.Exception e) {
					Debug.Log ("PathThread Error");	
					Debug.LogException (e);
				}
				isUnitLogicAsyncTaskFinished = true;
			});
		}
	}
	
	private void DoUnitUpdateLogicAsync () {
		int i = 0;
		while (i < AllUnits.Count) {
			UnitSubject currentUnit = AllUnits [i];
			
			currentUnit.OnAsyncUpdate ();
			
			i++;
		}
	}
	
	/* Game Update Method
	 * */
	public void Update ()
	{	
		InputController.Instance.OnUpdate ();
		if (TWorldTime.Active) {
			asyncUpdateTimer.Restart ();
			if (DO_ASYNC_PATHFINDING)	PushPathfindingAsyncTask ();
			if (DO_ASYNC_UNIT_LOGIC_UPDATE) PushAsyncUpdateUnits ();			
		}	
	}
	
	private Timer deleteUnitTimer = Timer.CreateFrequency (1000);
	private bool gameHasEnded = false;
	/* Game FixedUpdate Method
	 * */
	public void FixedUpdate ()
	{	
		if (!TWorldTime.Active || gameHasEnded) {
			StopAllAnimations ();
			return;
		}
		
		TWorldTime.OnUpdate ();
				
		gameHasEnded = !this.levelController.CheckScenarioUpdate ();
		if (gameHasEnded) {
			return;
		}
		
		foreach (SpellSubject spell in this.Spells.Values) {
			spell.OnFixedUpdate ();
		}
			
		this.FixedUpdateUnitGroups ();
		this.FixedUpdateUnits ();		
		
		this.navigationEngine.OnUpdate ();
	}
	
	public void ResumeGameUpdate ()
	{		
		TWorldTime.Active = true;
	}
	
	public void PauseGameUpdate ()
	{
		StopAllAnimations ();
		TWorldTime.Active = false;
		InputController.SetRTSControlType ();
	}
	
	private void StopAllAnimations ()
	{
		int i = 0;
		while (i < AllUnits.Count) {
			UnitSubject currentUnit = AllUnits [i];
			
			if (!currentUnit.IsInited) {
				AllUnits.RemoveAt (i);
				continue;
			}
			
			currentUnit.StopAnimation ();		
			i++;
		}
		
		foreach (SpellSubject spell in this.Spells.Values) {
			spell.StopCasting ();
		}
	}
	
	/* Game GUI method
	 * */

	public PlayerData HumanPlayer {
		get {
			return this.worldData.HumanPlayer;
		}
	}

	public Dictionary<int, PlayerData> AllPlayers {
		get {
			return worldData.AllPlayers;
		}
	}
		
	public IEnumerable<PlayerData> GetAllPlayers ()
	{
		return this.worldData.AllPlayers.Values;
	}
	
	public WorldData WorldData {
		get { return this.worldData;}
	}

	public GameObject BuildingsCategoryGameObject {
		get {
			return this.buildingsCategoryGameObject;
		}
	}

	public GameObject DecorationsCategoryGameObject {
		get {
			return this.decorationsCategoryGameObject;
		}
	}

	public GameObject EffectsCategoryGameObject {
		get {
			return this.effectsCategoryGameObject;
		}
	}

	public GameObject HeroesCategoryGameObject {
		get {
			return this.heroesCategoryGameObject;
		}
	}

	public GameObject SpellsCategoryGameObject {
		get {
			return this.spellsCategoryGameObject;
		}
	}
	
	public GameObject UnitsCategoryGameObject {
		get {
			return this.unitsCategoryGameObject;
		}
	}
	
	public LinkedList<Collider> UnitColliders {
		get {
			return this.worldData.UnitColliders;
		}
	}
	
	public LinkedList<SphereCollider> ObstacleColliders {
		get {
			return this.worldData.ObstacleColliders;
		}
	}

	public List<UnitSubject> AllUnits {
		get {
			return worldData.AllUnits;
		}
	}
	
	public List<UnitGroupSubject> AllUnitGroups {
		get {
			return worldData.AllUnitGroups;
		}
	}
	
	public bool UseNavigationFigure {
		get {
			return GameSettings.Navigation.USE_NAVIGATION_FIGURE;
		}
	}
	
	#region Navigation
	[SerializeField]
	private List<NavigationNode> navigationNodes = new List<NavigationNode> ();
	[SerializeField]
	private List<NavigationNode> sortList = new List<NavigationNode> ();
	[SerializeField]
	private List<NavigationNode> obstacleNodes = new List<NavigationNode> ();
	
	[SerializeField]
	private List<NavigationTriangle> navigationTriangles;
	bool isFinished = false;
	
	public void CreateMeshFromExistingNodes (bool setFinished = false)
	{
		NavigationEngine.PrepareParams ();
		
		
		GameObject[] navigationObjects = GameObject.FindGameObjectsWithTag (GameSettings.Tag.NAVIGATION);
	
		navigationTriangles = new List<NavigationTriangle> ();
		navigationNodes.Clear ();
		sortList.Clear ();	
		obstacleNodes.Clear ();

		foreach (GameObject obj in navigationObjects) {
			NavigationNode node = obj.GetComponent<NavigationNode> ();
						
			if (node != null) {
				node.IsSelected = false;
				if (!node.IsObstacle) {
					sortList.Add (node);
				} else {
					obstacleNodes.Add (node);
					node.Normalize ();
				}				
			} else {
				NavigationTriangle triangle = obj.GetComponent<NavigationTriangle> ();
				if (!this.NavigationTriangles.Contains (triangle)) {
					this.navigationTriangles.Add (triangle);
				}
			}
		}
		
		sortList.Sort ();
		foreach (NavigationNode node in sortList) {
			navigationNodes.Add (node);
			node.Normalize ();
		}	
		
//		if (setFinished) {
//			this.FinishNavigationFigure ();
//		}
		
		foreach (NavigationTriangle triangle in this.navigationTriangles) {
			triangle.PrepareTriangle (this);
		}
		NavigationEngine.Prepare (this, navigationNodes, obstacleNodes);
		
//		CalculateWorldPathTriangles ();
	}
	
	private void CalculateWorldPathTriangles () {
		foreach (NavigationTriangle triangle in navigationTriangles) {
			if (triangle.IsWorldPathTriangle) {
				triangle.CalculateClosestTriangles (this);
			}
		}
	}
	
	public void CalculateWorldPathTrianglesDijkstra () {
		Debug.Log ("Started dijkstra calculation");
		NavigationEngine.Prepare (this, navigationNodes, obstacleNodes);
		int i = 0;
		foreach (NavigationTriangle triangle in navigationTriangles) {
			if (triangle.IsWorldPathTriangle) {
				i++;
				triangle.CalculatePathMap (this);
			}
		}
		Debug.Log ("Ended dijkstra calculation " + i);
	}
	
	private static HashSet<NavigationNode> containedNodes = new HashSet<NavigationNode> ();
	
	private NavigationNode FindHeadNode ()
	{
		containedNodes.Clear ();
		foreach (NavigationNode node in sortList) {
			if (node.PrevNavigationNode != null) {
				containedNodes.Add (node.PrevNavigationNode);
			}
		}
		
		foreach (NavigationNode node in sortList) {
			if (!containedNodes.Contains (node)) {
				return node;
			}
		}
		
		return null;
	}
	
	public NavigationNode AddNavigationNode (Vector3 worldPosition, bool isObstacle)
	{
		this.PrepareNavigationCategoryGameObject ();
		this.ResetNavigationMesh ();

		GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
		NavigationNode node = cube.AddComponent<NavigationNode> ();
		cube.tag = Setup.GameSettings.Tag.NAVIGATION;
		cube.layer = Setup.GameSettings.Layer.NAVIGATION;
		cube.transform.position = worldPosition;
		cube.transform.parent = navigationCategoryGameObject.transform;
		cube.transform.localScale = new Vector3 (3, 6, 3);
		
		int nodeNumber = 0;		
		
		node.isObstacle = isObstacle;
		if (!node.IsObstacle) {
			isFinished = false;
			if (navigationNodes.Count > 0 && navigationNodes[navigationNodes.Count - 1] != null) {
				node.PrevNavigationNode = navigationNodes[navigationNodes.Count - 1];
				nodeNumber = node.PrevNavigationNode.Number + 1;
			}
			navigationNodes.Add(node);
		} else {			
			if (obstacleNodes.Count > 0 && obstacleNodes[obstacleNodes.Count - 1] != null) {
				node.PrevNavigationNode = obstacleNodes[obstacleNodes.Count - 1];	
			}
			
			obstacleNodes.Add (node);
			
			if (node.PrevNavigationNode != null) {
				nodeNumber = node.PrevNavigationNode.Number + 1;		
			}
		}
		
		node.name += nodeNumber;
		node.Number = nodeNumber + 100;
		return node;
	}

	public void ResetNavigationMesh ()
	{
		this.CreateMeshFromExistingNodes ();
	}
	
	public void DisplayNavigation ()
	{
		this.hideNavigation = false;
		foreach (NavigationNode naviNode in navigationNodes) {
			naviNode.GetComponent<Renderer> ().enabled = true;
		}
		
		foreach (NavigationNode naviNode in obstacleNodes) {
			naviNode.GetComponent<Renderer> ().enabled = true;
		}
		
		if (navigationTriangles == null) {
			return;
		}
		
		foreach (NavigationTriangle triangle in this.navigationTriangles) {
			triangle.GetComponent<Renderer> ().enabled = true;
		}
	}
	
	public bool hideNavigation = false;
	
	public void HideNavigation ()
	{
		this.hideNavigation = true;
		foreach (NavigationNode naviNode in navigationNodes) {
			naviNode.GetComponent<Renderer> ().enabled = false;
		}
		
		foreach (NavigationNode naviNode in obstacleNodes) {
			naviNode.GetComponent<Renderer> ().enabled = false;
		}
		
		foreach (NavigationTriangle triangle in this.navigationTriangles) {
			triangle.GetComponent<Renderer> ().enabled = false;
		}
	}
	
	public void OnDrawGizmos ()
	{		
		this.FixNodes ();
		
		if (navigationNodes.Count < 2) {
			return;
		}
		
		if (!hideNavigation) {
			Gizmos.color = Color.gray;
		} else {
			Gizmos.color = Color.green;
		}
		
		
		foreach (NavigationNode node in navigationNodes) {
			if (node.PrevNavigationNode != null) {
				Gizmos.DrawLine (node.PrevNavigationNode.transform.position, node.transform.position);
			}			
		}
		
		Gizmos.color = Color.red;
		
		try {
			foreach (NavigationNode node in obstacleNodes) {
				if (node.PrevNavigationNode != null) {
					Gizmos.DrawLine (node.PrevNavigationNode.transform.position, node.transform.position);
				}			
			}
		} catch (System.Exception e) {
			this.ResetNavigationMesh ();
		}
		if (hideNavigation) {
			return;
		}
		bool resetTriangles = false;
		
		try {
			NavigationTriangle triangle1 = NavigationTriangles[9];
			triangle1.DrawGizmos ();
			NavigationTriangle triangle2 = NavigationTriangles[10];
			triangle2.DrawGizmos ();
			foreach (NavigationTriangle triangle in NavigationTriangles) {			
				triangle.DrawGizmos ();
			}
		} catch (System.Exception e) {
			Debug.Log (e);
			resetTriangles = true;
		}
		
//		if (resetTriangles) {
//			GameObject[] navigationObjects = GameObject.FindGameObjectsWithTag (GameSettings.Tag.NAVIGATION);
//	
//			navigationTriangles.Clear ();
//	
//			foreach (GameObject obj in navigationObjects) {
//				NavigationTriangle triangle = obj.GetComponent<NavigationTriangle> ();			
//				if (triangle != null && !this.navigationTriangles.Contains (triangle)) {					
//					this.navigationTriangles.Add (triangle);
//				}
//			}
//		}
	}
	
	private void FixNodes ()
	{	
		NavigationNode nodeToRemove = null;
		try {
			foreach (NavigationNode node in navigationNodes) {
				if (!node.enabled) {
					nodeToRemove = node;
					break;
				}
				node.transform.position = new Vector3 (node.transform.position.x, DEFAULT_TERRAIN_HEIGHT + 1f, node.transform.position.z);
			}
			
			foreach (NavigationNode node in obstacleNodes) {
				if (!node.enabled) {
					nodeToRemove = node;
					break;
				}
				node.transform.position = new Vector3 (node.transform.position.x, DEFAULT_TERRAIN_HEIGHT + 1f, node.transform.position.z);
			}
			if (nodeToRemove != null) {
				navigationNodes.Remove (nodeToRemove);
			}
		} catch (System.Exception e) {
			print (e.ToString ());
			this.ResetNavigationMesh ();
		}		
	}
	
	public void AddNavigationTriangle (LinkedList<NavigationNode> nodes)
	{
		if (nodes.Count < 3) {
			return;
		}
		
		this.PrepareNavigationCategoryGameObject ();
		
		GameObject triangleObject = GameObject.CreatePrimitive (UnityEngine.PrimitiveType.Cylinder);
		triangleObject.transform.localScale = new Vector3 (3f, 3f, 3f);
		NavigationTriangle triangle = triangleObject.AddComponent<NavigationTriangle> ();		
		triangleObject.tag = GameSettings.Tag.NAVIGATION;
		triangleObject.layer = Setup.GameSettings.Layer.NAVIGATION;
		triangle.Init (this, nodes);
		triangleObject.transform.position = triangle.CalculateCenterPosition ();
		triangleObject.transform.parent = navigationCategoryGameObject.transform;
		
		if (!this.navigationTriangles.Contains (triangle)) {
			this.navigationTriangles.Add (triangle);
		}
		
		foreach (NavigationNode node in navigationNodes) {
			node.IsSelected = false;
		}
		
		foreach (NavigationNode node in obstacleNodes) {
			node.IsSelected = false;
		}
		
	}

	
	public List<NavigationTriangle> NavigationTriangles {
		get {
			return this.navigationTriangles;
		}
	}

	public List<NavigationNode> NavigationNodes {
		get {
			return this.navigationNodes;
		}
	}

	public List<NavigationNode> ObstacleNodes {
		get {
			return this.obstacleNodes;
		}
	}
	#endregion

	public bool UseMinimumGraphics {
		get {
			return this.useMinimumGraphics;
		}
		set {
			useMinimumGraphics = value;
		}
	}

	public void OnDestroy ()
	{
		this.isDestroyed = true;
		
		if (instance == this) {
			instance = null;
		}
		
		NavigationEngine.Clear ();
	}
	
	public static bool IsWorldPaused {
		get {
			return !TWorldTime.Active;
		}
	}
	
	public int MaxId;

	public void PrepareToGame ()
	{
		Terrain terrain = FindObjectOfType (typeof(Terrain)) as Terrain;
		
		WorldXSize = (int)terrain.terrainData.size.x;
		WorldZSize = (int)terrain.terrainData.size.z;
	}
	
	public string CreateId ()
	{
		if (IsNetworkGameClient) {
			throw new System.Exception ();
		}
		return "" + (10 + MaxId++);
	}
	
	public bool IsNetworkGame {
		get {
			return isNetworkGame;
		}
	}
	
	public bool IsNetworkGameServer {
		get {
			return isNetworkGameServer;
		}
	}
	
	public bool IsNetworkGameClient {
		get {
			return isNetworkGameClient;
		}
	}
		
	public UnitSubject FindUnitById (string id)
	{
		foreach (UnitSubject unit in this.AllUnits) {
			if (unit.id.Equals (id)) {
				return unit;
			}
		}
		
		BuildingSubject building = FindBuildingById (id);
		
		if (building != null) {
			return building.BuildingUnitSubject;
		}
		
		return null;
	}
	
	public HeroSubject FindHeroById (string id)
	{
		foreach (UnitGroupSubject unitGroup in this.WorldData.AllUnitGroups) {
			if (unitGroup.IsHeroGroup && unitGroup.GroupHero.id.Equals (id)) {
				return unitGroup.GroupHero;
			}
		}
		return null;
	}
	
	public BuildingSubject FindBuildingById (string id)
	{
		foreach (UnitGroupSubject unitGroup in this.WorldData.AllUnitGroups) {
			if (unitGroup.IsBuilding) {
				BuildingSubject building = unitGroup.BuildingSubject;
				if (building.id.Equals (id)) {
					return unitGroup.BuildingSubject;
				}
				if (building.Outbuildings != null) {					
					foreach (BuildingSubject outbuilding in building.Outbuildings) {
						if (outbuilding.id.Equals (id)) {
							return outbuilding;
						}
					}				
				}
			}			
		}
		return null;
	}
	
	public UnitGroupSubject FindUnitGroupById (string id)
	{
		foreach (UnitGroupSubject unitGroup in this.WorldData.AllUnitGroups) {
			if (unitGroup.IsBuilding && unitGroup.BuildingSubject.id.Equals (id)) {
				return unitGroup;
			}
			if (unitGroup.IsHeroGroup && unitGroup.GroupHero.id.Equals (id)) {
				return unitGroup;
			}
		}
		return null;
	}
	
	private PlayerSubject[] players;
	
	public PlayerSubject FindPlayerById (string id)
	{
		if (players == null) {
			players = FindObjectsOfType (typeof(PlayerSubject)) as PlayerSubject[];		
		}
		
		foreach (PlayerSubject player in this.players) {
			if (player.Id.Equals (id)) {
				return player;
			}
		}
		return null;
	}

	public SpellSubject FindSpellById (string id)
	{
		if (Spells.ContainsKey (id)) {
			return Spells [id];
		}
		return null;
	}

	public MapScenario MapScenario {
		get {
			return this.mapScenario;
		}
	}
	
	public Dictionary<string, SpellSubject> Spells {
		get {
			return this.worldData.Spells;
		}
	}
	
	public void RegisterSpell (SpellSubject spell)
	{
		this.Spells.Add (spell.Id, spell);
	}
	
	public void RemoveSpell (SpellSubject spell)
	{
		this.Spells.Remove (spell.Id);
	}
	
	public EffectPerformer FindEffectPerformerById (string id)
	{
		SpellSubject spell = FindSpellById (id);
		return spell;
	}
	
	public UnitInteractionPerformer FindInteractionPerformerById (string id)
	{
		UnitInteractionPerformer result = FindUnitById (id);
		if (result != null) {
			return result;
		}
		BuildingSubject building = FindBuildingById (id);
		if (building != null) {
			return building.BuildingUnitSubject;
		}
		if (DataTypes.Instance.Abilities.allData.ContainsKey (id)) {
			return DataTypes.Instance.Abilities.allData [id];
		}
		
		return null;
	}

	public NavigationEngine NavigationEngine {
		get {
			return this.navigationEngine;
		}
	}
}




