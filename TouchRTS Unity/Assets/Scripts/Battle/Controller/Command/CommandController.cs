using UnityEngine;
using System.Collections;
using Entity;
using System.Collections.Generic;

public class CommandController : MonoBehaviour
{
	private static CommandController instance;
	
	public virtual void Init (WorldController worldController)
	{
		instance = this;
	}
	
	public void OnDestroy ()
	{
		if (instance == this) {
			instance = null;
		}
	}

	public static CommandController Instance {
		get {
			return instance;
		}
	}
	
	public virtual void PrepareNetwork ()
	{
	}
	
	public virtual bool IsNetworkGame {
		get {
			return false;
		}
	}
	
	public virtual void StartUpgradeBuilding (BuildingSubject building)
	{
		building.StartUpgrade (this);
	}
	
	public virtual void OnBuildingConstructed(BuildingSubject castle, BuildingType constructionType) {
		castle.OnBuildingConstruction (this, constructionType);
	}
	
	public virtual void SpawnUnit (BuildingSubject building, UnitType unitType, UnitGroupSubject unitGroup)
	{
		building.Spawn (this, unitType, unitGroup);
	}

	public virtual void BuySpell (HeroSubject hero, SpellType spellType)
	{
		hero.BuySpell (this, spellType);
	}

	public virtual GameObject ConstructBuilding (CastleConstructor castleConstructor, BuildingType buildingType, bool freeConstruct)
	{
		return castleConstructor.Construct (this, buildingType, freeConstruct);
	}
	
	public virtual void Resurrect (HeroSubject hero)
	{
		hero.Resurrect ();
	}
	
	public virtual GameObject CreateUnit (string unitName, Vector3 position, UnitGroupSubject unitGroup, string supType, bool isGarrison, string id = null)
	{
		if (id == null) {
			id = WorldController.Instance.CreateId ();
		}
		GameObject result = ObjectController.CreateUnit (this, unitName, position, unitGroup, supType, isGarrison, id);
		return result;
	}
	
	public virtual GameObject CreateSpell (SpellType spellType, HeroSubject currentHero, Vector3 touchPosition, PlayerData playerData, string id = null)
	{
		if (id == null) {
			id = WorldController.Instance.CreateId ();
		}
		GameObject spell = ObjectController.CreateSpell (this, spellType, currentHero, touchPosition, playerData, id);
		return spell;
	}
	
	public virtual void DoSpell (HeroSubject currentHero, SpellType spellType, Vector3 point)
	{
		currentHero.DoSpell (this, spellType, point);
	}
	
	public virtual void StopSpell (HeroSubject currentHero)
	{
		currentHero.StopSpellCast (this);
	}
	
	public virtual void CaptureBuilding (BuildingSubject building, PlayerData newOwner) {
		building.CaptureBuildingByPlayer (this, newOwner);
	}

	public virtual void AddEffectToUnit (UnitSubject unit, EffectType effectType, EffectPerformer performer) {
		unit.AddEffect (this, effectType, performer);
	}
	
	public virtual void AffectUnitLife (UnitSubject unit, int power) {
		unit.AffectLife (this, power);
	}
	
	public virtual void AffectUnitMana (UnitSubject unit, int power) {
		unit.AffectMana (this, power);
	}
	
	public virtual void MoveGroupToPoint (UnitGroupSubject unitGroup, Vector3 position, bool moveAnyway = false) {
		unitGroup.MoveGroupToPoint (this, position, moveAnyway);
	}

	public virtual void DoInterraction (UnitSubject unit, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToInterract)
	{
		AttackAnimationController.DoInterraction (this, unit, performer, unitsToInterract);	
	}
	
	public virtual void CastUnitsSkillAbility (UnitGroupSubject unitGroup, AbilityType skill) {
		unitGroup.CastUnitsSkill (this, skill);
	}

	public virtual void OnUpgradeFinished (BuildingSubject building)
	{
		building.OnUpgradeFinished ();
	}
}

