using UnityEngine;
using System.Collections;
using Entity;
using System;
using System.Collections.Generic;
using Setup;

public class ObjectController : MonoBehaviour
{		
	public static bool USE_STUB_MODEL;
	
	public void Init (WorldController worldController)
	{
		USE_STUB_MODEL = worldController.useMinimumGraphics;
		Clear ();
	}

	public static GameObject CreateNetworkController ()
	{
		return CreatePrefab ("NetworkController", "Controller");
	}
	
	public static UnitGroupSubject CreateUnitGroupObject (string name) {
		GameObject obj = new GameObject ();
		obj.name = name + "UnitGroup";
		return obj.AddComponent<UnitGroupSubject> ();
	}
	
	public static CommandController CreateCommandController ()
	{
		GameObject gameObject =  WorldController.Instance.gameObject;
		
		if (NetworkManager.Instance.IsNetworkGameClient) {
			return gameObject.AddComponent<ClientCommandController> ();
		} else if (NetworkManager.Instance.IsNetworkGameServer)  {
			return gameObject.AddComponent<ServerCommandController> ();
		} 
			
		return gameObject.AddComponent<CommandController> ();
	}
	
	public static GameObject CreateUnit (CommandController commandController, string unitName, Vector3 position, UnitGroupSubject unitGroup, string supType, bool isGarrison, string id)
	{		
		GameObject gameObject = CreatePrefab (unitName, "Units", position, supType);
		gameObject.GetComponent<UnitSubject> ().id = id;
		unitGroup.AddAndActivateUnit (gameObject, isGarrison);
		return gameObject;
	}
	
	public static GameObject CreateSpell (CommandController commandController, SpellType spellType, HeroSubject currentHero, Vector3 touchPosition, PlayerData playerData, string id)
	{
		Transform transformParent;
		Quaternion rotation = currentHero.transform.rotation;
		Vector3 position;
		if (spellType.IsRaySpell ()) {
			position = currentHero.transform.position;
			position = new Vector3 (position.x, position.y + 0.5f, position.z);
			transformParent = currentHero.transform;
		} else {
			position = touchPosition;
			transformParent = WorldController.Instance.SpellsCategoryGameObject.transform;
		}
		
		GameObject gameObject = CreatePrefab (spellType.Name, "Spells", position);
		gameObject.transform.rotation = rotation;
		
		gameObject.transform.parent = transformParent;
		gameObject.name = spellType.Name;
		SpellSubject spell = ((SpellSubject)gameObject.GetComponent (typeof(SpellSubject)));
		if (spell == null) {
			spell = gameObject.AddComponent<SpellSubject> ();
		}
		spell.Init (spellType, playerData, currentHero, id);
		
		return gameObject;
	}
	
//	public static GameObject CreateHero (string desiredName, Vector3 position, int playerId, string supType)
//	{
//		GameObject gameObject = CreatePrefab (desiredName, "Units", position, supType);	
//		HeroSubject hero = gameObject.GetComponent<HeroSubject> ();
//		hero.playerId = playerId;
//		hero.Init (WorldController.Instance);
//		return gameObject;
//	}
	
	public static GameObject CreateOutbuilding (string desiredName, BuildingSubject castle, BuildingType.StructurePositionData towerPositionData, string supType, bool createOutbuildingConstruected)
	{
		GameObject gameObject = CreatePrefab (desiredName, "Units", castle.transform.localPosition, supType);

		gameObject.transform.parent = castle.transform;
		BuildingSubject building = gameObject.GetComponent<BuildingSubject> ();
		building.Init (castle, createOutbuildingConstruected);
		
		Vector3 positionOffset = new Vector3 (towerPositionData.xPosition, 0, towerPositionData.zPosition);
		
		gameObject.transform.position += positionOffset;
		gameObject.transform.rotation = castle.transform.rotation;
		gameObject.transform.Rotate (0, towerPositionData.yRotation, 0);
		
		return gameObject;
	}
	
	public static GameObject CreateModelGameObject (string desiredName, Vector3 position, string supType = null)
	{
		GameObject gameObject = CreatePrefab (desiredName, "Models", position, supType);
		gameObject.tag = "UnitBody";
		gameObject.layer = LayerMask.NameToLayer ("UnitBody");
		gameObject.name = desiredName + " model";
		return gameObject;
	}

	public static GameObject CreateShadow ()
	{
		return CreateEffect ("Blob Shadow");
	}
	
	public static GameObject CreateSelectionCircle ()
	{
		return CreateEffect ("Selection Circle");
	}
	
	public static GameObject CreateEffect (string name)
	{
		GameObject gameObject = CreatePrefab (name, "Effects");
		return gameObject;
	}
	
	public static GameObject CreatePrefab (string firstName, string prefabResourceType, Vector3 position, string supType = null)
	{
		string prefabResourceName = CreatePathFromNameSupType (firstName, supType);
		if (prefabResourceType == "Models" && USE_STUB_MODEL) {
			prefabResourceName = "Cube";
		}
		GameObject gameObject = CreatePrefab (prefabResourceName, prefabResourceType);
		gameObject.name = firstName;
		gameObject.transform.position = position;
		gameObject.transform.eulerAngles = new Vector3 (0, Randomizer.Next (360), 0);
		return gameObject;
	}
	
	public static GameObject CreatePrefab (string prefabResourceName, string prefabResourceType)
	{
		string path;
		if (prefabResourceType == null) {
			path = "Prefabs/" + prefabResourceName;	
		} else {
			path = "Prefabs/" + prefabResourceType + "/" + prefabResourceName;	
		}
		UnityEngine.Object obj = Resources.Load (path);
		if (obj == null) {
			int i = 3;
			i++;
		}
		try {
			GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate (obj);
			gameObject.name = prefabResourceName;
			return gameObject;
		} catch (Exception e) {
			MonoBehaviour.print (e);
		}
		return null;
	}
	
	public static void Destroy (GameObject gameObject)
	{
		UnityEngine.Object.Destroy (gameObject);
	}
	
	public static GameObject CreateMiniMapSphere ()
	{
		GameObject mapBounds = GameObject.CreatePrimitive (PrimitiveType.Cube);
		MeshFilter m = mapBounds.GetComponent<MeshFilter> ();
		Destroy (m);
		return mapBounds;
	}
	
	public static GameObject CreateMiniMapSphere2 ()
	{
		GameObject mapBounds = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		MeshFilter m = mapBounds.GetComponent<MeshFilter> ();
		Destroy (m);
		return mapBounds;
	}

	public static GameObject CreateUnitGroupFlag (UnitGroupSubject unitGroup)
	{
		GameObject movementTargetObject;
		movementTargetObject = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("Flag"));
		MonoBehaviour.Destroy (movementTargetObject.collider);
		movementTargetObject.name = "MovementTarget=" + unitGroup.MainObject.name;
		movementTargetObject.transform.parent = WorldController.Instance.EffectsCategoryGameObject.transform;
		
		return movementTargetObject;
	}
	
	private static string CreatePathFromNameSupType (string desiredName, string supType)
	{
		string name = "";
		if (supType != null && supType != RacesTemplate.NEUTRAL_RACE_NAME) {
			name = supType + "/";
		}		
	
		name += desiredName;
		return name;
	}
	
	public static GameObject CreateBulletObject (string bulletName, Vector3 position)
	{
		GameObject gameObject = CreateNewPoolable (bulletName, "MagicAnimations/Bullets", position);		
		return gameObject;
	}
	
	public static GameObject CreateRayObject (string rayName, Vector3 position)
	{
		GameObject gameObject = CreateNewPoolable (rayName, "MagicAnimations/Rays", position);	
		return gameObject;
	}

	public static void DestroyBulletObject (String name, GameObject magicAnimationObject)
	{
		DeleteObject (name, "MagicAnimations/Bullets", magicAnimationObject);
	}

	public static void DestroyRayObject (String name, GameObject magicAnimationObject)
	{
		DeleteObject (name, "MagicAnimations/Rays", magicAnimationObject);
	}
	
	#region ObjectPool
	private static Dictionary <string, Stack<IPoolable>> pool = new Dictionary<string , Stack<IPoolable>> ();

	private static GameObject CreateNewPoolable (string name, string supType, Vector3 position)
	{
		string id = name + supType;
		IPoolable x = null;
            
		if (pool.ContainsKey (id)) {
			Stack<IPoolable> stack = pool [id];
			
			foreach (IPoolable ipoolable in stack) {
			if (!ipoolable.IsUsed) {
					x = ipoolable;
					break;
				}
			}		
		} else {
			pool.Add (id, new Stack<IPoolable> ());
		}

		if (x == null) {
			x = Instanciate (name, supType, position);
			Stack<IPoolable> stack = pool [id];
			stack.Push (x);
		} 
		x.New (position);	

		return x.GameObject;
	}
   	
	private static IPoolable Instanciate (string name, string supType, Vector3 position)
	{
		IPoolable obj = new FlyweightEffect (name, supType, position);;
		return obj;
	}

	private static void DeleteObject (string name, string supType, GameObject targetObject)
	{     
		string id = name + supType;
		if (!pool.ContainsKey (id)) {
			return;
		}
		
		IPoolable victim = null;
		Stack<IPoolable> stack = pool[id];
		foreach (IPoolable obj in stack) {
			if (obj.GameObject == targetObject) {
				victim = obj;
				victim.Reset ();
				break;
			}
		}
	}
    
	private static void Clear ()
	{
		lock (pool) {			
			foreach (Stack<IPoolable> stack in pool.Values) {
				stack.Clear (); 
			}
			pool.Clear ();
		}
	}
	
	public interface IPoolable
	{
		bool IsUsed {
			get;
		}
		
		void New (Vector3 position);
	
		void Reset ();
		
		GameObject GameObject {
			get ;
		}
		
		string Id {
			get;
		}
	}

	class FlyweightEffect : IPoolable
	{	
		private string id;
		private GameObject gameObject;
		private bool isUsed = true;
		
		public FlyweightEffect (string name, string supType, Vector3 position)
		{
			this.isUsed = false;
			this.id = name + supType;
			this.gameObject = ObjectController.CreatePrefab (name, supType, position);
			gameObject.transform.parent = WorldController.Instance.EffectsCategoryGameObject.transform;
		}

		public void New (Vector3 position)
		{
			this.isUsed = true;
			this.gameObject.SetActive (true);
			this.gameObject.transform.position = position;
		}

		public void Reset ()
		{	
			this.isUsed = false;
			this.gameObject.SetActive (false);
			this.gameObject.transform.position = Vector3.zero;
		}

		public string Id {
			get {
				return this.id;
			}
		}

		public GameObject GameObject {
			get {
				return this.gameObject;
			}
		}

		#region IPoolable implementation
		public bool IsUsed {
			get {
				return this.isUsed;
			}
		}
		#endregion


	}
	
	#endregion
}


