using System;
using System.Collections.Generic;
using UnityEngine;
using Entity;

public class WorldData
{
	private int xSize;
	private int zSize;
//	private int height;
	private int unitCount;
	private int unitGroupCount;
	private LinkedList<Collider> unitColliders = new LinkedList<Collider> ();
	private LinkedList<SphereCollider> obstacleColliders = new LinkedList<SphereCollider> ();
	private List<UnitSubject> allUnits = new List<UnitSubject> ();
	private List<UnitGroupSubject> allUnitGroups = new List<UnitGroupSubject> ();
	private Dictionary<string, SpellSubject> spells = new Dictionary<string, SpellSubject> ();
	private Dictionary<int, PlayerData> allPlayers = new Dictionary<int, PlayerData> ();
	private PlayerData humanPlayer;
		
	public WorldData ()
	{
	}
	
	public void OnNewUnitAdded ()
	{
		unitCount++;
	}
	
	public void OnUnitRemoved ()
	{
		unitCount--;
	}
	
	public void OnNewUnitGroupCreated ()
	{
		unitGroupCount++;
	}
	
	public void OnUnitGroupDestroyed ()
	{
		unitGroupCount--;
	}

	public int UnitCount {
		get {
			return this.unitCount;
		}
	}

	public int UnitGroupCount {
		get {
			return this.unitGroupCount;
		}
	}

	public LinkedList<Collider> UnitColliders {
		get {
			return this.unitColliders;
		}
	}

	public LinkedList<SphereCollider> ObstacleColliders {
		get {
			return this.obstacleColliders;
		}
	}

	public List<UnitGroupSubject> AllUnitGroups {
		get {
			return this.allUnitGroups;
		}
	}

	public List<UnitSubject> AllUnits {
		get {
			return this.allUnits;
		}
	}

	public Dictionary<int, PlayerData> AllPlayers {
		get {
			return this.allPlayers;
		}
	}

	public PlayerData HumanPlayer {
		get {
			return this.humanPlayer;
		}
		set {
			this.humanPlayer = value;
		}
	}

	public float Height {
		get {
			return WorldController.DEFAULT_TERRAIN_HEIGHT;
		}
	}

	public int XSize {
		get {
			return this.xSize;
		}
		set {
			xSize = value;
		}
	}

	public int ZSize {
		get {
			return this.zSize;
		}
		set {
			zSize = value;
		}
	}
	
	public Dictionary<string, SpellSubject> Spells {
		get {
			return this.spells;
		}
	}
}


