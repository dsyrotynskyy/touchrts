using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity;

public class AttackAnimationController : MonoBehaviour
{
	private static List<UnitInteractionTask> interactionTaskPool = new List<UnitInteractionTask> ();
	private static HashSet<UnitInteractionTask> usedTasks = new HashSet<UnitInteractionTask> ();
	
	void Start ()
	{
		interactionTaskPool.Clear ();
		usedTasks.Clear ();
		
		for (int i = 0; i < 100; i++) {
			interactionTaskPool.Add (new UnitInteractionTask ());
		}
	}
	
	void FixedUpdate ()
	{	
		usedTasks.RemoveWhere (TaskNotPerforms);
	}
	
	private static bool TaskNotPerforms (UnitInteractionTask task)
	{
		task.Process ();
		return !task.IsUsed;
	}
	
	private static void Display (UnitSubject starter, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToAttack)
	{
		UnitInteractionTask task = GetNextFreeTask ();
		task.Init (starter, performer, unitsToAttack);
		usedTasks.Add (task);
	}

	private static UnitInteractionTask GetNextFreeTask ()
	{
		foreach (UnitInteractionTask task in interactionTaskPool) {
			if (!task.IsUsed) {
				return task;
			}
		}
			
		UnitInteractionTask theTask = new UnitInteractionTask ();
		interactionTaskPool.Add (theTask);
		return theTask;
	}

	public static void PerformInteraction (UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToAttack)
	{
		int val = performer.InteractionPower;
		if (performer.PositiveForTarget) {
			val *= -1;
		}
		
		int percent = performer.InteractionPowerPercentRandomOffset;
		int modifier = Randomizer.Next (performer.InteractionPower * percent / 100);
		if (Randomizer.NextBool ()) {
			modifier *= -1;		
		}
		
		val += modifier;
		foreach (UnitSubject unit in unitsToAttack) {
			CommandController.Instance.AffectUnitLife (unit, val);
		}
	}
	
	public static void DoInterraction (CommandController controller, UnitSubject starter, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToAttack)
	{
		if (performer.MagicAnimationType != null) {
			Display (starter, performer, unitsToAttack);
		} else {
			PerformInteraction (performer, unitsToAttack);		
		}		
	}
}

class UnitInteractionTask
{
	public static int RAY_ACTIVITY_PERIOD = 100;
	
	bool isUsed = false;
	private GameObject magicAnimationObject;
	private UnitSubject starterUnit;
	private UnitSubject targetUnit;
	private UnitInteractionPerformer effectPerformer;
	private readonly LinkedList<UnitSubject> unitsToAttack = new LinkedList<UnitSubject> ();
	
	private BulletType bulletType;
	
	private RayType rayType;
	private LineRenderer rayLineRenderer;
	private Timer rayLifeTimer = Timer.CreateEmpty();

	public void Init (UnitSubject starter, UnitInteractionPerformer performer, LinkedList<UnitSubject> unitsToAttack)
	{
		this.unitsToAttack.Clear ();
		
		foreach (UnitSubject unit in unitsToAttack) {
			this.unitsToAttack.AddLast (unit);
		}
		
		this.starterUnit = starter;
		this.targetUnit = unitsToAttack.First.Value;
		this.effectPerformer = performer;
		 
		if (performer.MagicAnimationType != null) {
			if (performer.MagicAnimationType.IsBullet) {
				this.bulletType =  (BulletType) performer.MagicAnimationType;
				this.magicAnimationObject = ObjectController.CreateBulletObject (bulletType.Name, GetFireStartPosition (starterUnit));				
			} else if (performer.MagicAnimationType.IsRay) {
				this.rayType = (RayType) performer.MagicAnimationType;
				this.magicAnimationObject = ObjectController.CreateRayObject (rayType.Name, GetFireStartPosition (starterUnit));			
				
				this.rayLineRenderer = this.magicAnimationObject.GetComponent<LineRenderer> ();
		
				rayLifeTimer.SetNewLifePeriod (RAY_ACTIVITY_PERIOD);
				AttackAnimationController.PerformInteraction (performer, unitsToAttack);
			}
		}		
		
		isUsed = true;
	}
	
	private Vector3 GetFireStartPosition (UnitSubject starterUnit)
	{
		Vector3 firePosition = starterUnit.TransformPosition;
		if (starterUnit.UnitType.IsBuilding) {
			Vector3 towerPosition = new Vector3 (firePosition.x, firePosition.y + 7, firePosition.z);			
			firePosition = towerPosition;
		} 
		return firePosition;
	}
	
	public void Process ()
	{
		if (targetUnit.IsDead) {
			Reset ();
			return;
		}
		if (this.bulletType != null) {
			this.ProcessBullet ();
		} else if (this.rayType != null) {
			this.ProcessRay ();
		}		
	}
	
	private void ProcessRay ()
	{
		float yModifier = 0.5f;
		Vector3 rayStartPosition = new Vector3(starterUnit.transform.position.x, starterUnit.transform.position.y + yModifier ,starterUnit.transform.position.z);
		rayLineRenderer.SetPosition (0, rayStartPosition);
		Vector3 rayEndPosition = new Vector3 (targetUnit.transform.position.x, targetUnit.transform.position.y + yModifier, targetUnit.transform.position.z);
		rayLineRenderer.SetPosition (1, rayEndPosition);	
		if (!rayLifeTimer.IsActive) {
			Reset ();
			return;
		}		
	}
	
	private void ProcessBullet ()
	{
		Vector3 fwd = targetUnit.transform.position - magicAnimationObject.transform.position;
		
		float distance = Vector3.Distance (magicAnimationObject.transform.position, targetUnit.transform.position);
		
		if (distance < 1f) {
			AttackAnimationController.PerformInteraction (effectPerformer, unitsToAttack);			
			Reset ();
			return;
		}
		
		float speed = bulletType.Speed;
		if (distance < 3f) {
			speed += speed * (3 - distance);
		}
			
		float mult = speed * Time.deltaTime;
		magicAnimationObject.transform.position += fwd * mult;
		magicAnimationObject.transform.rotation = Quaternion.LookRotation (fwd);		
	}
	
	private void Reset ()
	{	
		if (!isUsed) {
			return;
		}
		
		if (effectPerformer.MagicAnimationType.IsBullet) {
			ObjectController.DestroyBulletObject (bulletType.Name, magicAnimationObject);
		} else if (effectPerformer.MagicAnimationType.IsRay) {
			ObjectController.DestroyRayObject (rayType.Name, magicAnimationObject);
		}			
		isUsed = false;		
		magicAnimationObject = null;
		this.rayLineRenderer = null;
		this.bulletType = null;
		this.rayType = null;
		this.targetUnit = null;
	}
			
	public bool IsUsed {
		get {
			return this.isUsed;
		}
	}
}

