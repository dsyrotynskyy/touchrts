using System;
using GameMapScenario;
using Entity;
using UI;
using System.Collections;
using UnityEngine;
using Setup;

public class LevelController : SceneController
{
	public static string predefinedGameScenarioName;
	public static MapScenario predefinedGameScenario;	
		
	public MapScenario LoadScenario (WorldController controller, string currentName) {
		MapScenario scenario;
		if (currentName == predefinedGameScenarioName && predefinedGameScenario != null) {
			MapScenario scenarioToReturn = predefinedGameScenario;
			predefinedGameScenario = null;
			scenario = scenarioToReturn;
		} else {			
			scenario = MonoBehaviour.FindObjectOfType (typeof (ScriptedMapScenario)) as MapScenario;	
		}
		
		if (scenario == null) {
			scenario = new EmptyMapScenario ();
		}
		
		this.mapScenario = scenario;
		this.mapScenario.Init (controller);
		
		return mapScenario;
	}
	
	private MapScenario mapScenario;	
	private String disconnectedLevel = "MainMenu";
	
	public bool CheckScenarioUpdate () {
		mapScenario.OnUpdate ();
		
		bool playerWin = mapScenario.HasWin ();
		bool playerFail = mapScenario.HasFail ();
		
		if (playerWin || playerFail) {
			WorldController.Instance.PauseGameUpdate ();
			
			MessageDialogData data = new MessageDialogData ("Mission Ended", "", StartNextLevel);
			
			InputController.UI.MessageDialog.Display (data);
			return false;
		}
	
		return true;
	}

	public void OnDisconnectedFromServer ()
	{
		ApplicationManager.Instance.LoadScene (disconnectedLevel);
	}
	
	//Cannot be online
	public void StartNextLevel() {
		ApplicationManager.Instance.LoadScene (mapScenario.NextLevelName);
	}
	
	public MapScenario MapScenario {
		get {
			return this.mapScenario;
		}
	}
}


