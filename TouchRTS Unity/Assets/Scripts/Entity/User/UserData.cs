using System;
using Entity;

namespace User
{
	public class UserData
	{
		private static readonly UserData instance = new UserData ();

		public static UserData Instance {
			get {
				return instance;
			}
		}
		
		private UserData ()
		{
		}
		
		private readonly PlayerModificationsData playerModificationsData = new PlayerModificationsData ();
		
		public PlayerModificationsData PlayerModificationsData {
			get {
				return playerModificationsData;
			}
		}
	}
}

