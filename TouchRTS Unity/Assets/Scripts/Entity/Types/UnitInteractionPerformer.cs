using Entity;
using System.Collections.Generic;
/* Represents data of unit interaction
 * Might be spell or unit
 * */
public interface UnitInteractionPerformer : InteractionPerformer {
		bool PositiveForTarget {
			get;
		}
		
		float ActionArea {
			get;
		}
		
		MagicAnimationType MagicAnimationType {
			get;
		}
		
		LinkedList<EffectType> Effects {
			get;
		}
		
		float ManaUsePerInteraction {
			get;
		}	
	
		string Id {
			get;
		}
	}
