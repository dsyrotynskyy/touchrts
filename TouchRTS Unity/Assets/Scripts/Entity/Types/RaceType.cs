using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Entity {
	public class RaceType : AbstractType {
		
		internal LinkedList<HeroType> raceHeroes = new LinkedList<HeroType>();
		
		/* int - class of building
		 * buildingType - buildingType:)
		 */
		internal Dictionary<int, BuildingType> specialRaceBuildings = new Dictionary<int, BuildingType>();
		internal List<UnitType> units = new List<UnitType>();
		internal UnitType garrisonUnitType;
		
		internal RaceType (string name) : base(name)
		{			
		}
		
		public void Store (DataTypes storage) {
			storage.Races.AddToDictionary (this);
		}
		
		internal RaceType() : base (AbstractType.ABSTRACT_DATA_TYPE) {
		}
		
		public void AddSpecialBuilding (BuildingType building) {
			this.specialRaceBuildings.Add(building.BuildingClass, building);			
		}
		
		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			RaceType clone = new RaceType ();
			clone.CloneCommonFieldsFrom (this);
			return clone;
		}
		#endregion

		public UnitType GarrisonUnitType {
			get {
				return garrisonUnitType;
			}
		}
	}
}

