using System;
using System.Collections.Generic;

namespace Entity
{
	public class AbilityType : AbstractType, UnitInteractionPerformer
	{
		internal int effectPowerRandomOffset = 15;
		internal bool positiveForTarget;
		internal float actionArea = 0;
		internal int effectPower;
		internal int timeOfEffectActivity;
		internal MagicAnimationType magicAnimationType;
		internal int manaUsePerInteraction;
		
		internal readonly LinkedList<EffectType> effects = new LinkedList<EffectType>();		
		
		public AbilityType (string name) : base (name)
		{			
		}
		
		public AbilityType () : base (ABSTRACT_DATA_TYPE)
		{
		} 
		
		public void Store (DataTypes storage) {
			storage.Abilities.AddToDictionary (this);
		}
		
		#region EffectPerformer implementation
		public int TimeOfEffectActivity {
			get {
				return timeOfEffectActivity;
			}
		}
		
		public int InteractionPower {
			get {
				return effectPower;
			}
		}
		
//		public int InteractionPower (PlayerData playerPerformer) {
//			return effectPower;		
//		}
		#endregion

		public bool PositiveForTarget {
			get {
				return this.positiveForTarget;
			}
		}

		#region AbilityPerformer implementation
		public float ActionArea {
			get {
				return this.actionArea;
			}
		}

		public MagicAnimationType MagicAnimationType {
			get {
				return this.magicAnimationType;
			}
		}

		public LinkedList<EffectType> Effects {
			get {
				return this.effects;
			}
		}

		public float ManaUsePerInteraction {
			get {
				return manaUsePerInteraction;
			}
		}
		#endregion

		#region EffectPerformer implementation
		public int InteractionPowerPercentRandomOffset {
			get {
				return effectPowerRandomOffset;
			}
		}
		#endregion

		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			AbilityType clone = new AbilityType ();
			clone.CloneCommonFieldsFrom (this);
			
			clone.actionArea = this.actionArea;
			
			clone.effectPowerRandomOffset = effectPowerRandomOffset;
			clone.positiveForTarget = positiveForTarget;
			clone.effectPower = effectPower;
			clone.timeOfEffectActivity = timeOfEffectActivity;
			clone.magicAnimationType = magicAnimationType;
			clone.manaUsePerInteraction = this.manaUsePerInteraction;
			
			foreach (EffectType effect in effects) {
				clone.effects.AddLast (effect);
			}
			
			return clone;
		}
		#endregion

		#region UnitInteractionPerformer implementation
		public string Id {
			get {
				return Name;
			}
		}
		#endregion
	}
	
	class PositiveAbilityType : AbilityType {
		public PositiveAbilityType (string name) : base (name)
		{
			this.positiveForTarget = true;
		}
	}
	
	class OffenseAbilityType : AbilityType {
		public OffenseAbilityType (string name) : base (name)
		{
			this.positiveForTarget = false;
		}
	}
	

}

