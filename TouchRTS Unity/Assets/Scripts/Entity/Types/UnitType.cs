using System;
using System.Collections.Generic;

namespace Entity
{
	public class UnitType : AbstractType
	{	
		private const float MIN_ATACK_DISTANCE = 3.0f;
		
		static UnitType ()
		{
		}
		
		public static readonly int ATACK_TYPE_MAGIC = 123;
		public static readonly int ATACK_TYPE_PHYSICAL = 12345;
		
		internal int protection = 0;
		internal int attackPower;
		internal int attackFrequency;
		internal float attackDistance = MIN_ATACK_DISTANCE;	
		internal float attackArea = 0;
		internal int attackType = ATACK_TYPE_PHYSICAL; //Atack type physical by default
		internal int maxHealth;
		internal int healthRestorationFrequency;
		internal int healthRestorationValue;
		internal int maxMana;
		internal int manaRestorationPeriod;
		internal int manaRestorationValue;
		internal int speed;		
		internal int effectPowerRandomOffset = 15;
		
		internal MagicAnimationType magicAnimationType = null;
		internal int price;
		internal int timeSpawn;
		internal int buildingXRotation;//is used only for buildings	
		internal int useUnitPlaces;	
		
		internal readonly LinkedList<AbilityType> abilities = new LinkedList<AbilityType> ();
		internal readonly LinkedList<EffectType> effects = new LinkedList<EffectType> ();
		
		internal AbilityType skill;
		internal int skillRechargePeriod;
		
		internal RaceType race;
		
		internal UnitType (String id) : base(id)
		{
			this.InitFields ();
		}
		
		public virtual void Store (DataTypes storage) {
			storage.Units.AddToDictionary (this);
			if (this.race == null) {
				this.race = storage.RacesTemplate.Neutral;
			}			
		}
		
		internal UnitType () : base (ABSTRACT_DATA_TYPE) {
			this.InitFields ();
		}
		
		private void InitFields() {
			if (!this.IsBuilding) {
				useUnitPlaces = 1;				
			}
		}
		
		#region work methods
		public float AttackDistance {
			get {
				return this.attackDistance;
			}
		}

		public int Price {
			get {
				return this.price;
			}
		}

		public int TimeSpawn {
			get {
				return this.timeSpawn;
			}
		}
		
		public virtual bool IsHero {
			get { return false;}
		}
		
		public int AttackPower {
			get {
				return this.attackPower;
			}
		}

		public int AttackType {
			get {
				return this.attackType;
			}
		}

		public int MaxHealth {
			get {
				return this.maxHealth;
			}
		}

		public int UnitsProtection {
			get {
				return this.protection;
			}
		}

		public int Speed {
			get {
				return this.speed;
			}
		}	
		#endregion

		public MagicAnimationType MagicAnimationType {
			get {
				return this.magicAnimationType;
			} set {
				this.magicAnimationType = value;
			}
		}
		
		public bool HasMagicAttackAnimation {
			get {
				return this.magicAnimationType != null;
			}
		}
				
		#region work methods
		public virtual bool IsInvulnerable {
			get {
				return false;
			}
		}
		
		public virtual bool IsBuilding {
			get {
				return false;
			}
		}
		
		public bool IsTower {
			get {
				return IsBuilding && attackPower > 0;
			}
		}
		#endregion
		
		public bool IsArcher {
			get {
				return this.attackDistance > MIN_ATACK_DISTANCE;
			}
		}
		
		public int BuildingXRotation {
			get {
				return this.buildingXRotation;
			}
		}

		public int AttackFrequency {
			get {
				return this.attackFrequency;
			}
		}
		
		public void SetRace (RaceType raceType) {
			this.race = raceType;
		}
		
		public RaceType Race {
			get {
				return this.race;
			} 
			set {
				this.SetRace (value);
			}
		}

		public int UseUnitPlaces {
			get {
				return this.useUnitPlaces;
			}
		}
		
		public float AttackArea {
			get {
				return this.attackArea;
			}
		}
		
		public bool DoEffects {
			get {
				return false;
			}
		}

		public LinkedList<AbilityType> Abilities {
			get {
				return this.abilities;
			}
		}	
		
		public void CloneFrom (UnitType unitType) {
			this.protection = unitType.protection;
			this.attackPower = unitType.attackPower;
			this.attackFrequency = unitType.attackFrequency;
			this.attackDistance = unitType.attackDistance;
			this.attackType = unitType.attackType;
			this.maxHealth = unitType.maxHealth;
			this.healthRestorationFrequency = unitType.healthRestorationFrequency;
			this.healthRestorationValue = unitType.healthRestorationValue;
			this.maxMana = unitType.maxMana;
			this.manaRestorationPeriod = unitType.manaRestorationPeriod;
			this.manaRestorationValue = unitType.manaRestorationValue;
			this.speed = unitType.speed;
			this.magicAnimationType = unitType.magicAnimationType;
			this.price = unitType.price;
			this.timeSpawn = unitType.timeSpawn;
			this.buildingXRotation = unitType.buildingXRotation;
			this.race = unitType.race;
//			this.offersUnitPlaces = unitType.offersUnitPlaces;
			this.useUnitPlaces = unitType.useUnitPlaces;
			
			foreach (EffectType effect in unitType.effects) {
				this.effects.AddLast (effect);
			}
			
			foreach (AbilityType effect in unitType.abilities) {
				this.abilities.AddLast (effect);
			}
		}

		#region AbilityPerformer implementation
		public bool PositiveForTarget {
			get {
				return false;
			}
		}

		public float ActionArea {
			get {
				return this.attackArea;
			}
		}

		public LinkedList<EffectType> Effects {
			get {
				return this.effects;
			}
		}

		public float ManaUsePerInteraction {
			get {
				return 0;
			}
		}
		#endregion

		#region EffectPerformer implementation
		public int TimeOfEffectActivity {
			get {
				return 0;
			}
		}
		
		public int InteractionPower {
			get {
				return attackPower;
			}
		}

		#endregion

		#region EffectPerformer implementation
		public int InteractionPowerPercentRandomOffset {
			get {
				return this.effectPowerRandomOffset;
			}
		}
		#endregion
		
		public virtual void ModifyValues (UnitType modificator) {
		}

		public virtual bool IsCapitalGuardian {
			get {
				return false;
			}
		}

		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			UnitType clone = new UnitType ();
			clone.CloneCommonFieldsFrom (this);
			clone.CloneFrom (this);
			return clone;
		}
		#endregion

		public int SkillRechargePeriod {
			get {
				return this.skillRechargePeriod;
			}
		}

		public AbilityType Skill {
			get {
				return this.skill;
			}
		}
	}
	
	class CapitalGuardian : UnitType {
		internal CapitalGuardian (String id) : base(id)
		{
		}
		
		internal CapitalGuardian () : base (ABSTRACT_DATA_TYPE) {
		}
		
		public override bool IsCapitalGuardian {
			get {
				return true;
			}
		}
	}
	
	public interface InteractionPerformer {
		int InteractionPower {
			get;
		}
		
		int InteractionPowerPercentRandomOffset {
			get;
		}
	}
}

