using System;
using System.Collections.Generic;

namespace Entity
{
	public class HeroType : UnitType
	{
		private List<SpellType> spells = new List<SpellType>();
		
		static HeroType() {			
		}
				
		internal HeroType (String id) : base(id)
		{
		}
		
		internal HeroType () : base () {
		}
		
		public override bool IsHero 
		{
			get {return true;}			
		}
		
		public void CloneFrom (HeroType heroType) {
			base.CloneFrom (heroType);
			heroType.spells.ForEach (x => this.spells.Add (x));
		}
		
		
		public List<SpellType> Spells {
			get {
				return this.spells;
			}
		}
		
		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			HeroType clone = new HeroType ();
			clone.CloneFrom (this);
			return clone;
		}
		#endregion
	}
}

