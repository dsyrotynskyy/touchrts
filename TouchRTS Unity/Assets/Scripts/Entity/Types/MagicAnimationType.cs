using System;

namespace Entity
{
	public abstract class MagicAnimationType : AbstractType
	{
		private bool isBullet;
		private bool isRay;
		private bool isAnimatingOnVictimUnit;
		
		private float speed = 8f;
		
		public MagicAnimationType (string name) : base (name)
		{			
		}
		
		public void Store (DataTypes storage) {
			storage.MagicAnimations.AddToDictionary (this);
		}
		
		public virtual bool IsBullet {
			get {return false;}
		}
		
		public virtual bool IsRay {
			get {return false;}
		}
		
		public virtual bool IsAnimatingOnVictimUnit {
			get {return false;}
		}
		
		public float Speed {
			get {return speed;}
		}

		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			return this;
		}
		#endregion
	}
	
	public class BulletType : MagicAnimationType
	{
		public BulletType (string name) : base(name)
		{	
			
		}
		
		public override bool IsBullet {
			get {return true;}
		}
	}
	
	public class RayType : MagicAnimationType
	{
		public RayType (string name) : base(name)
		{
		}
		
		public override bool IsRay {
			get {return true;}
		}
	}
	
	public class MagicAnimatingOnVictimType : MagicAnimationType
	{
		public MagicAnimatingOnVictimType (string name) : base(name)
		{
		}
		
		public override bool IsAnimatingOnVictimUnit {
			get {return true;}
		}
	}
}

