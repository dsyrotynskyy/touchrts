using System;
using System.Collections.Generic;

namespace Entity
{
	public class BuildingType : UnitType
	{
		/* Building types. They will be replaced by analogue buildings of other race,
		 * if they are not built already or race has building of this type
		 * */
		public static int NO_CLASS = 0;
//		public static int SPECIAL_RACE_ABILITY_BUILDING = 326;//Unique for each race
//		public static int FARM = 328;
//		public static int HERO_BUILDING = 329;	

		public static int TOWER = 327;
		public static int BLACKSMITH = 330;
		public static int MAGIC_TOWER = 331;
		public static int CASARM = 332;
		public static int MONEY_BUILDING = 333;
		internal int buildingClass = NO_CLASS;
		internal int moneyBonus;
		internal int moneyBonusFrequency;
		internal UnitType garrisonUnitType;
		internal int garrisonUnitNumber;
		internal int originallyOffersUnitPlaces; // use this if You dont use building level
		internal bool isOutbuilding = false;
		internal readonly LinkedList<SpellType> spells = new LinkedList<SpellType> ();
		internal readonly LinkedList<UnitType> unitsToSpawn = new LinkedList<UnitType> ();
		internal readonly LinkedList<BuildingType> outbuildings = new LinkedList<BuildingType> ();
		internal readonly LinkedList<StructurePositionData> outbuildingPositions = new LinkedList<StructurePositionData> ();
		internal readonly LinkedList<StructurePositionData> towerPositions = new LinkedList<StructurePositionData> ();
		internal readonly Dictionary<int, BuildingLevelData> levelsData = new Dictionary<int, BuildingLevelData> ();
		internal int powerPoints = 0;
//		internal readonly Dictionary<RaceType, UnitType> buildingUnits = new Dictionary<RaceType, UnitType> ();
		
		internal BuildingType (string name) : base(name)
		{	
			this.InitFields ();					
		}
		
		public override void Store (DataTypes storage)
		{
			storage.Buildings.AddToDictionary (this);
			if (this.race == null) {
				this.race = storage.RacesTemplate.Neutral;
			}
		}
		
		internal BuildingType () : base()
		{	
			this.InitFields ();
		}
		
		private void InitFields ()
		{
			this.useUnitPlaces = 0;
			this.originallyOffersUnitPlaces = 5;
			this.attackDistance = UnitGroupSubject.GROUP_INTERACTION_DISTANCE;		
		}
		
		#region work methods
		
		/*
		 * Returns true if it can spawn units
		 * */
		public bool CanSpawnUnits {
			get {
				return unitsToSpawn.Count > 0;
			}
		}
		
		public int MoneyBonus {
			get {
				return this.moneyBonus;
			}
		}

		public int MoneyBonusFrequency {
			get {
				return this.moneyBonusFrequency;
			}
		}

		public LinkedList<UnitType> UnitsToSpawn {
			get {
				return this.unitsToSpawn;
			}
		}
		
		public int GarrisonUnitNumber {
			get {
				return this.garrisonUnitNumber;
			}
		}
		
		public bool HasGarrison ()
		{
			return this.garrisonUnitNumber > 0;
		}

		public UnitType GarrisonUnitType {
			get {
				return this.garrisonUnitType;
			}
		}
		#endregion

		public override bool IsInvulnerable {
			get {
				return maxHealth <= 0;
			}
		}
		
		public override bool IsBuilding {
			get {
				return true;
			}
		}

		public LinkedList<BuildingType> Outbuildings {
			get {
				return this.outbuildings;
			}
		}
		
		public int BuildingClass {
			get {
				return this.buildingClass;
			}
		}
		
		public struct StructurePositionData
		{
			public static StructurePositionData zero;
			public float xPosition;
			public float zPosition;
			public int yRotation;
			
			public StructurePositionData (float xPosition, float zPosition, int yRotation)
			{
				this.xPosition = xPosition;
				this.zPosition = zPosition;
				this.yRotation = yRotation;
			}
			
			public bool IsZeroPosition {
				get { return xPosition == zPosition && zPosition == 0;}
			}
		}

		public LinkedList<StructurePositionData> OutbuildingPositions {
			get {
				return this.outbuildingPositions;
			}
		}

		public LinkedList<StructurePositionData> TowerPositions {
			get {
				return this.towerPositions;
			}
		}
		
		public bool CanConstructStructures {
			get {
				return this.outbuildings.Count > 0;
			}
		}

		public LinkedList<SpellType> Spells {
			get {
				return this.spells;
			}
		}
		
		public void CloneFrom (BuildingType buildingType)
		{
			base.CloneFrom (buildingType);
			this.garrisonUnitNumber = buildingType.garrisonUnitNumber;
			this.powerPoints = buildingType.powerPoints;
			this.buildingClass = buildingType.buildingClass;
			this.moneyBonus = buildingType.moneyBonus;
			this.moneyBonusFrequency = buildingType.moneyBonusFrequency;
			this.garrisonUnitType = buildingType.garrisonUnitType;
			this.isOutbuilding = buildingType.isOutbuilding;
			
			foreach (SpellType spell in buildingType.spells) {
				this.spells.AddLast (spell);
			}
			
			foreach (UnitType unit in buildingType.unitsToSpawn) {
				this.unitsToSpawn.AddLast (unit);
			}
			
			foreach (BuildingType type in buildingType.outbuildings) {
				this.outbuildings.AddLast (type);
			}
			
			foreach (StructurePositionData outbuildingPosition in buildingType.outbuildingPositions) {
				this.outbuildingPositions.AddLast (outbuildingPosition);
			}
			
			foreach (StructurePositionData towerPosition in buildingType.towerPositions) {
				this.towerPositions.AddLast (towerPosition);
			}
			
			foreach (int level in buildingType.levelsData.Keys) {
				this.levelsData.Add (level, buildingType.levelsData [level]);
			}
		}

		public void ModifyValues (BuildingType modification)
		{
		}
		
		public virtual bool IsCapital {
			get {
				return false;
			}
		}
		
		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			BuildingType clone = new BuildingType ();
			clone.CloneCommonFieldsFrom (this);
			clone.CloneFrom (this);
			return clone;
		}
		#endregion
		
		public void AddLevelData (int level, BuildingLevelData newLevelData)
		{
			this.levelsData.Add (level, newLevelData);
		}
		
		public BuildingLevelData GetBuildingDataByLevel (int level)
		{
			if (levelsData.ContainsKey (level)) {
				BuildingLevelData data = levelsData [level];
				return data;
			}
			return null;
		}
				
		public int GetOfferedUnitPlaces (int level)
		{
			BuildingLevelData data = GetBuildingDataByLevel (level);
			if (data == null) {
				return this.originallyOffersUnitPlaces;
			}
			return data.PlacesOffer;
		}
		
		public int GetPlayerProtectionPowerModification (int level)
		{
			BuildingLevelData data = GetBuildingDataByLevel (level);
			if (data == null) {
				return 0;
			}
			return data.PlayerProtectionPower;
		}
		
		public int GetPlayerAttackPowerModification (int level)
		{
			BuildingLevelData data = GetBuildingDataByLevel (level);
			if (data == null) {
				return 0;
			}
			return data.PlayerAttackPower;
		}
		
		public int GetPlayerSpellSinglePowerModification (int level)
		{
			BuildingLevelData data = GetBuildingDataByLevel (level);
			if (data == null) {
				return 0;
			}
			return data.SpellSinglePower;
		}
		
		public int GetPlayerSpellEffectPowerModification (int level)
		{
			BuildingLevelData data = GetBuildingDataByLevel (level);
			if (data == null) {
				return 0;
			}
			return data.SpellEffectPower;
		}
		
		public abstract class BuildingLevelData
		{
			public int cost;
			public int timeUpgrade;
			
			public BuildingLevelData (int cost, int timeUpgrade)
			{
				this.cost = cost;
				this.timeUpgrade = timeUpgrade;
			}
			
			public virtual int PlacesOffer { get { return 0; } }
			
			public virtual int PlayerAttackPower { get { return 0; } }

			public virtual int PlayerProtectionPower { get { return 0; } }
			
			public virtual int SpellSinglePower { get { return 0; } }

			public virtual int SpellEffectPower { get { return 0; } }
			
			public virtual int Cost { get { return cost; } }

			public virtual int TimeUpgrade { get { return timeUpgrade; } }
		}
		
		private static int totalPlacesOffer = 0;
		
		public void AddNewCasarmLevel (int level, int placesOfferModification, int cost, int timeUpgrade) {
			if (level == 1) {
				totalPlacesOffer = 0;
			}
			totalPlacesOffer += placesOfferModification;
			this.AddLevelData (level, new CasarmBuildingLevelData (totalPlacesOffer, cost, timeUpgrade));
		}
		
		private static int totalAttackModif = 0;
		private static int totalProtectionModif = 0;
		
		public void AddNewBlacksmithLevel (int level, int attackModif, int protectionModif, int cost, int timeUpgrade) {
			if (level == 1) {
				totalAttackModif = 0;
				totalProtectionModif = 0;
			}
			totalAttackModif += attackModif;
			totalProtectionModif += protectionModif;
			this.AddLevelData (level, new BlacksmithBuildingLevelData (totalAttackModif, totalProtectionModif, cost, timeUpgrade));
		}
		
		private static int totalSpellSingleAttackModif = 0;
		private static int totalSpellEffectAttackModif = 0;
		
		public void AddNewMageTowerLevel (int level, int spellSingleAttackModif, int spellEffectAttackModif, int cost, int timeUpgrade) {
			if (level == 1) {
				totalSpellSingleAttackModif = 0;
				totalSpellEffectAttackModif = 0;
			}
			totalSpellSingleAttackModif += spellSingleAttackModif;
			totalSpellEffectAttackModif += spellEffectAttackModif;
			this.AddLevelData (level, new MageTowerBuildingLevelData (totalSpellSingleAttackModif, totalSpellEffectAttackModif, cost, timeUpgrade));
		}

		public int PowerPoints {
			get {
				return this.powerPoints;
			}
		}
	}
	
	public class CasarmBuildingLevelData : BuildingType.BuildingLevelData
	{
		public int placesOffer;
			
		public CasarmBuildingLevelData (int placesOffer, int cost, int timeUpgrade) : base (cost, timeUpgrade)
		{
			this.placesOffer = placesOffer;
		}

		public override int PlacesOffer {
			get {
				return this.placesOffer;
			}
		}
	}
		
	public class BlacksmithBuildingLevelData : BuildingType.BuildingLevelData
	{
		public int attackModif;
		public int protectionModif;
			
		public BlacksmithBuildingLevelData (int attackModif, int protectionModif, int cost, int timeUpgrade) : base (cost, timeUpgrade)
		{
			this.attackModif = attackModif;
			this.protectionModif = protectionModif;
		}

		public override int PlayerAttackPower { get { return attackModif; } }

		public override int PlayerProtectionPower { get { return protectionModif; } }			
	}
		
	public class MageTowerBuildingLevelData : BuildingType.BuildingLevelData
	{
		public int spellSingleAttackModif;
		public int spellEffectAttackModif;
			
		public MageTowerBuildingLevelData (int spellSingleAttackModif, int spellEffectAttackModif, int cost, int timeUpgrade) : base (cost, timeUpgrade)
		{
			this.spellSingleAttackModif = spellSingleAttackModif;
			this.spellEffectAttackModif = spellEffectAttackModif;
		}

		public override int SpellSinglePower { get { return spellSingleAttackModif; } }

		public override int SpellEffectPower { get { return spellEffectAttackModif; } }
	}
	
	class CapitalBuildingType : BuildingType
	{
		internal CapitalBuildingType (string name) : base(name)
		{	
		}
		
		internal CapitalBuildingType () : base()
		{	
		}
		
		internal UnitType capitalGuardian;
		
		/* True if there is capital guardian*/
		public override bool IsCapital {
			get {
				return true;
			}
		}

		public UnitType CapitalGuardianUnitType {
			get {
				return this.capitalGuardian;
			}
		}
	}
}

