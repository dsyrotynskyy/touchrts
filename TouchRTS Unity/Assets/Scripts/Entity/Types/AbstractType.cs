using System;
using System.Collections.Generic;

namespace Entity
{
	public abstract class AbstractType
	{
		public static string ABSTRACT_DATA_TYPE = "Abstract";
		private string name;
		private string subTitle;
		internal string description;
		
		public string Name {
			get { return name; }
			set { this.name = value; }
		}
				
		public AbstractType (string name, string subTitle = "", string description = "")
		{
			this.name = name;
			this.subTitle = subTitle;
		}

		public string Description {
			get {
				return this.description;
			}
			set {
				description = value;
			}
		}		

		public string SubTitle {
			get {
				return this.subTitle;
			}
		}
		
		public override string ToString ()
		{
			return string.Format ("Name={0}, Description={1}, SubTitle={2}]", Name, Description, SubTitle);
		}

		protected void CloneCommonFieldsFrom (AbstractType type) {
			this.name = type.name;
			this.subTitle = type.subTitle;
			this.description = type.description;
		}
		
		public abstract AbstractType Clone () ;
	}
}

