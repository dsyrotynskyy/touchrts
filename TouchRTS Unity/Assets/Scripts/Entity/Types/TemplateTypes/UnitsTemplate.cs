using System;
using System.Collections.Generic;

namespace Entity
{
	public class UnitsTemplate 
	{
		public readonly UnitType CapitalGuardianAbstract = new CapitalGuardian ();	
		public readonly UnitType SwordmanAbstract = new UnitType ();
		public readonly HeroType TankHeroAbstract = new HeroType ();
		public readonly UnitType MusketerAbstract = new UnitType ();
		public readonly UnitType HealerAbstract = new UnitType ();
		
		public readonly UnitType Skelet = new UnitType ("Skelet");
		public readonly HeroType SkeletonKing = new HeroType ("SkeletonKing");
		public readonly UnitType SkeletonMage = new UnitType ("SkeletonMage");
		public readonly UnitType UndeadCapitalGuardian = new CapitalGuardian ("UndeadCapitalGuardian");		
		
		public readonly UnitType Swordman = new UnitType ("Swordman");
		public readonly UnitType Musketer = new UnitType ("Musketer");
		public readonly HeroType Captain = new HeroType ("Captain");	
		public readonly UnitType Chaplain = new UnitType ("Chaplain");		
		public readonly UnitType CastleCapitalGuardian = new CapitalGuardian ("CastleCapitalGuardian");		
		
		public UnitsTemplate (DataTypes storage) {
			Init (storage);
			
			Skelet.Store (storage);
			SkeletonKing.Store (storage);
			SkeletonMage.Store (storage);
			UndeadCapitalGuardian.Store (storage);
			
			Swordman.Store (storage);
			Musketer.Store (storage);
			Captain.Store (storage);
			Chaplain.Store (storage);
			CastleCapitalGuardian.Store (storage);
		}
		
		public void Init (DataTypes storage)
		{
			InitAbstract (storage);
			InitUndead (storage);
			InitKingdom (storage);
		}
		
		public void InitAbstract (DataTypes storage) {
			CapitalGuardianAbstract.attackPower = 250;
			CapitalGuardianAbstract.attackFrequency = 500;
			CapitalGuardianAbstract.speed = 1;
			CapitalGuardianAbstract.maxHealth = 15000000;
			CapitalGuardianAbstract.protection = 150;
			CapitalGuardianAbstract.magicAnimationType = storage.MagicAnimationsTemplate.RedRay;
			CapitalGuardianAbstract.attackDistance = 15f;		
			CapitalGuardianAbstract.manaRestorationValue = 2;
			CapitalGuardianAbstract.manaRestorationPeriod = 500;
			CapitalGuardianAbstract.healthRestorationValue = 10;
			CapitalGuardianAbstract.healthRestorationFrequency = 500;
			CapitalGuardianAbstract.timeSpawn = 134;
			
			MusketerAbstract.attackPower = 25;
			MusketerAbstract.attackFrequency = 1000;
			MusketerAbstract.attackDistance = 13f;
			MusketerAbstract.speed = 1;
			MusketerAbstract.magicAnimationType = storage.MagicAnimationsTemplate.FireBolt;
			MusketerAbstract.maxHealth = 12500000;
			MusketerAbstract.protection = 10;
			MusketerAbstract.healthRestorationValue = 10;
			MusketerAbstract.healthRestorationFrequency = 500;
			MusketerAbstract.price = 200;
			MusketerAbstract.timeSpawn = 2000;
			MusketerAbstract.description = "musketer";	
			
			HealerAbstract.attackPower = 15;
			HealerAbstract.attackFrequency = 1000;
			HealerAbstract.attackDistance = 13f;
			HealerAbstract.speed = 1;
			HealerAbstract.magicAnimationType = storage.MagicAnimationsTemplate.YellowRay;
			HealerAbstract.abilities.AddLast (storage.AbilitiesTemplate.HealingAbility);
			HealerAbstract.maxHealth = 1250;
			HealerAbstract.protection = 10;
			HealerAbstract.healthRestorationValue = 10;
			HealerAbstract.healthRestorationFrequency = 500;
			HealerAbstract.price = 200;
			HealerAbstract.timeSpawn = 2000;
			HealerAbstract.description = "Chaplain";	
					
			SwordmanAbstract.attackPower = 40;
			SwordmanAbstract.attackFrequency = 900;
			SwordmanAbstract.speed = 1;
			SwordmanAbstract.maxHealth = 10000000;
			SwordmanAbstract.protection = 10;
			SwordmanAbstract.healthRestorationValue = 10;
			SwordmanAbstract.healthRestorationFrequency = 1000;
			SwordmanAbstract.price = 200;
			SwordmanAbstract.timeSpawn = 1000;
			
			TankHeroAbstract.attackPower = 250;
			TankHeroAbstract.attackFrequency = 900;
			TankHeroAbstract.speed = 1;
			TankHeroAbstract.maxHealth = 52000000;
			TankHeroAbstract.protection = 110;
			TankHeroAbstract.maxMana = 323;
			TankHeroAbstract.manaRestorationValue = 2;
			TankHeroAbstract.manaRestorationPeriod = 500;
			TankHeroAbstract.healthRestorationValue = 10;
			TankHeroAbstract.healthRestorationFrequency = 500;
			TankHeroAbstract.Spells.Add (storage.SpellsTemplate.FireRay);
			TankHeroAbstract.Spells.Add (storage.SpellsTemplate.FireGround1);
			TankHeroAbstract.timeSpawn = 134;
		}  
		
		public void InitUndead (DataTypes storage)
		{
			UndeadCapitalGuardian.CloneFrom (CapitalGuardianAbstract);
			UndeadCapitalGuardian.Race = storage.RacesTemplate.Undead;
			
			SkeletonMage.CloneFrom (MusketerAbstract);
			SkeletonMage.description = "skeleton mage";	
			SkeletonMage.Race = storage.RacesTemplate.Undead;
						
			Skelet.CloneFrom (SwordmanAbstract);
			Skelet.description = "Skelet";
			Skelet.Race = storage.RacesTemplate.Undead;
			
			SkeletonKing.CloneFrom (TankHeroAbstract);
			SkeletonKing.Race = storage.RacesTemplate.Undead;
			SkeletonKing.description = "Skeleton king";			
		}
		
		public void InitKingdom (DataTypes storage)
		{
			CastleCapitalGuardian.CloneFrom (CapitalGuardianAbstract);
			CastleCapitalGuardian.Race = storage.RacesTemplate.Kingdom;			
			
			Musketer.CloneFrom (MusketerAbstract);
			Musketer.Race = storage.RacesTemplate.Kingdom;
			Musketer.description = "musketer";	
			
			Chaplain.CloneFrom (HealerAbstract);
			Chaplain.Race = storage.RacesTemplate.Kingdom;
			Chaplain.description = "Chaplain";	
			Chaplain.skill = storage.AbilitiesTemplate.FireBall;
			Chaplain.skillRechargePeriod = 1000;
					
			Swordman.CloneFrom (SwordmanAbstract);
			Swordman.description = "Swordman";
			Swordman.Race = storage.RacesTemplate.Kingdom;
			
			Captain.CloneFrom (TankHeroAbstract);
			Captain.Spells.Add (storage.SpellsTemplate.FireRay);
			Captain.Spells.Add (storage.SpellsTemplate.FireGround1);
			Captain.attackPower = 340;
			Captain.maxHealth = 24000;
			Captain.attackArea = 5f;
			Captain.Race = storage.RacesTemplate.Kingdom;
			Captain.description = "Kingdom's champion";		
		}
	}
}

