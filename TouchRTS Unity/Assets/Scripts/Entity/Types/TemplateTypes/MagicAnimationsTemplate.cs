using System;
using System.Collections.Generic;

namespace Entity
{
	public class MagicAnimationsTemplate 
	{
		public readonly BulletType FireBolt = new BulletType("FireBolt");
		public readonly BulletType FireBall = new BulletType("FireBall");

		public readonly RayType YellowRay = new RayType("YellowRay");			
		public readonly RayType RedRay = new RayType("RedRay");			
		public readonly RayType BlueRay = new RayType("BlueRay");
		
		public MagicAnimationsTemplate (DataTypes storage) {
			Init ();
			
			YellowRay.Store (storage);
			RedRay.Store (storage);
			BlueRay.Store (storage);
		}
		
		public void Init ()
		{			
		}
	}
}

