using System;
using System.Collections.Generic;

namespace Entity
{
	public class AbilitiesTemplate
	{
		public readonly AbilityType HealingAbility = new PositiveAbilityType ("HealingAbility");
		public readonly AbilityType FireBall = new OffenseAbilityType ("FireBall");
		
		public AbilitiesTemplate (DataTypes types) {
			Init (types);
			
			HealingAbility.Store (types) ;
		}
		
		public void Init (DataTypes types)
		{
			HealingAbility.effectPower = 55;
			HealingAbility.magicAnimationType = types.MagicAnimationsTemplate.BlueRay;
			
			FireBall.effectPower = 550;
			FireBall.magicAnimationType = types.MagicAnimationsTemplate.FireBall;
		}
	}
}

