using System;
using System.Collections.Generic;
namespace Entity
{
	public class RacesTemplate
	{
		public static string NEUTRAL_RACE_NAME = "Neutral";
		public static string KINGDOM_RACE_NAME = "Kingdom";
		
		public readonly RaceType Neutral = new RaceType (NEUTRAL_RACE_NAME);
		public readonly RaceType Kingdom = new RaceType (KINGDOM_RACE_NAME);
		public readonly RaceType Scorp = new RaceType ("Scorp");
		public readonly RaceType Undead = new RaceType ("Undead");
		
		public RacesTemplate (DataTypes types) {
//			Init (types);
			
			Neutral.Store (types);
			Kingdom.Store (types);
			Scorp.Store (types);
			Undead.Store (types);
		}
		
		/* Must be called later
		 * */
		public void Prepare (DataTypes storage) {
			Undead.AddSpecialBuilding (storage.BuildingsTemplate.SkeletonTower);
			Undead.garrisonUnitType = storage.UnitsTemplate.Skelet;
			
			Kingdom.AddSpecialBuilding (storage.BuildingsTemplate.TowerKingdom);
			Kingdom.garrisonUnitType = storage.UnitsTemplate.Swordman;
		}
	}
}

