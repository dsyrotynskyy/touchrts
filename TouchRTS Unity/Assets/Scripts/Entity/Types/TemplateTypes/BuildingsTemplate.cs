using System;
using System.Collections.Generic;

namespace Entity
{
	public class BuildingsTemplate 
	{
		internal readonly CapitalBuildingType CapitalAbstract = new CapitalBuildingType ();
		
		internal readonly BuildingType CastleAbstract = new BuildingType ();
		internal readonly BuildingType FarmAbstract = new BuildingType ();
		internal readonly BuildingType TowerAbstract = new BuildingType ();
		internal readonly BuildingType MageGuildAbstract = new BuildingType ();
		internal readonly BuildingType BarracksAbstract = new BuildingType ();		
		internal readonly BuildingType SanctuaryAbstract = new BuildingType ();		
		
		internal readonly CapitalBuildingType CapitalSkeleton = new CapitalBuildingType ("CapitalSkeleton");
		internal readonly BuildingType CastleSkeleton = new BuildingType ("CastleSkeleton");
		internal readonly BuildingType SkeletonFarm = new BuildingType ("SkeletonFarm");
		internal readonly BuildingType UndeadMageGuild = new BuildingType ("UndeadMageGuild");
		internal readonly BuildingType SkeletonTower = new BuildingType ("SkeletonTower");
		internal readonly BuildingType SanctuaryOfTheDamned = new BuildingType ("SanctuaryOfTheDamned");
		
		internal readonly CapitalBuildingType CapitalKingdom = new CapitalBuildingType ("CapitalKingdom");
		internal readonly BuildingType CastleKingdom = new BuildingType ("CastleKingdom");
		internal readonly BuildingType FarmKingdom = new BuildingType ("FarmKingdom");
		internal readonly BuildingType MageGuildKingdom = new BuildingType ("MageGuildKingdom");
		internal readonly BuildingType TowerKingdom = new BuildingType ("TowerKingdom");
		internal readonly BuildingType SanctuaryKingdom = new BuildingType ("SanctuaryKingdom");
		
		public BuildingsTemplate (DataTypes storage) {
			Init (storage);
			
			CapitalSkeleton.Store (storage);		
			CastleSkeleton.Store (storage);
			SkeletonFarm.Store (storage);
			UndeadMageGuild.Store (storage);
			SkeletonTower.Store (storage);
			SanctuaryOfTheDamned.Store (storage);
			
			CapitalKingdom.Store (storage);
			CastleKingdom.Store (storage);
			FarmKingdom.Store (storage);
			MageGuildKingdom.Store (storage);
			TowerKingdom.Store (storage);
			SanctuaryKingdom.Store (storage);
		}
		
		public void Init (DataTypes storage)
		{	
			InitAbstract (storage);
			InitUndead (storage);
			InitKingdom (storage);
		}
		
		private void InitAbstract (DataTypes storage) {
			CapitalAbstract.moneyBonus = 100;
			CapitalAbstract.powerPoints = 10;
			CapitalAbstract.moneyBonusFrequency = 1000;
			CapitalAbstract.garrisonUnitNumber = 4;
			CapitalAbstract.outbuildingPositions.AddLast (new BuildingType.StructurePositionData (-5f, -0.05f, 0));
			CapitalAbstract.outbuildingPositions.AddLast (new BuildingType.StructurePositionData (5f, -0.05f, 0));			
			CapitalAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (-3.5f, -3.5f, 45));
			CapitalAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (3.5f, -3.5f, 315));
			CapitalAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (-3.5f, 3.5f, 0));
			CapitalAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (3.5f, 3.5f, 0));					
			
			CastleAbstract.moneyBonus = 100;
			CastleAbstract.powerPoints = 50;
			CastleAbstract.moneyBonusFrequency = 1000;
			CastleAbstract.garrisonUnitNumber = 4;
			CastleAbstract.outbuildingPositions.AddLast (new BuildingType.StructurePositionData (-5f, -0.05f, 0));
			CastleAbstract.outbuildingPositions.AddLast (new BuildingType.StructurePositionData (5f, -0.05f, 0));			
			CastleAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (-3.5f, -3.5f, 45));
			CastleAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (3.5f, -3.5f, 315));
			CastleAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (-3.5f, 3.5f, 0));
			CastleAbstract.towerPositions.AddLast (new BuildingType.StructurePositionData (3.5f, 3.5f, 0));					
			CastleAbstract.UnitsToSpawn.AddLast (storage.UnitsTemplate.Musketer);
			CastleAbstract.UnitsToSpawn.AddLast (storage.UnitsTemplate.SkeletonMage);		
			
			TowerAbstract.timeSpawn = 2000;			
			TowerAbstract.attackPower = 80;
			TowerAbstract.attackFrequency = 1000;
			TowerAbstract.magicAnimationType = storage.MagicAnimationsTemplate.FireBolt;
			TowerAbstract.maxHealth = 500;
			TowerAbstract.protection = 10;
			TowerAbstract.healthRestorationValue = 10;
			TowerAbstract.healthRestorationFrequency = 500;
			TowerAbstract.price = 200;
			TowerAbstract.isOutbuilding = true;	
			TowerAbstract.buildingClass = BuildingType.TOWER;
			
			FarmAbstract.moneyBonus = 50;
			FarmAbstract.moneyBonusFrequency = 500;						
			FarmAbstract.timeSpawn = 2000;	
			FarmAbstract.garrisonUnitNumber = 0;
			FarmAbstract.moneyBonus = 50;
			FarmAbstract.moneyBonusFrequency = 200;
			FarmAbstract.price = 300;
			FarmAbstract.SetRace (storage.RacesTemplate.Kingdom);
			FarmAbstract.buildingClass = BuildingType.CASARM;
			
			FarmAbstract.AddNewCasarmLevel (1, 5, 300, 500);
			FarmAbstract.AddNewCasarmLevel (2, 4, 300, 500);
			FarmAbstract.AddNewCasarmLevel (3, 3, 300, 500);
			
			SanctuaryAbstract.timeSpawn = 2000;			
			SanctuaryAbstract.price = 300;
			SanctuaryAbstract.buildingClass = BuildingType.MAGIC_TOWER;

			MageGuildAbstract.spells.AddLast (storage.SpellsTemplate.FireGround2);
			MageGuildAbstract.spells.AddLast (storage.SpellsTemplate.HellHuricane1);		
			MageGuildAbstract.spells.AddLast (storage.SpellsTemplate.HellHuricane2);
			MageGuildAbstract.timeSpawn = 2000;			
			MageGuildAbstract.price = 300;
			MageGuildAbstract.buildingClass = BuildingType.BLACKSMITH;					
		}		
		
		private void InitUndead (DataTypes storage)
		{
			CapitalSkeleton.CloneFrom (CapitalAbstract);
			CapitalSkeleton.capitalGuardian = storage.UnitsTemplate.UndeadCapitalGuardian;
			CapitalSkeleton.UnitsToSpawn.AddFirst (storage.UnitsTemplate.Skelet);			
			CapitalSkeleton.garrisonUnitType = storage.UnitsTemplate.Skelet;	
			CapitalSkeleton.Outbuildings.AddFirst (SkeletonTower);
			CapitalSkeleton.Outbuildings.AddFirst (SkeletonFarm);
			CapitalSkeleton.Outbuildings.AddFirst (UndeadMageGuild);	
			CapitalSkeleton.Outbuildings.AddFirst (SanctuaryOfTheDamned);		
			CapitalSkeleton.SetRace (storage.RacesTemplate.Undead);				
			
			CastleSkeleton.CloneFrom (CastleAbstract);
			CastleSkeleton.garrisonUnitType = storage.UnitsTemplate.Skelet;
			CastleSkeleton.Outbuildings.AddFirst (SkeletonTower);
			CastleSkeleton.SetRace (storage.RacesTemplate.Undead);	
			
			SkeletonTower.CloneFrom (TowerAbstract);
			SkeletonTower.SetRace (storage.RacesTemplate.Undead);		
			SkeletonTower.description = "Skeleton Tower";
			
			SkeletonFarm.CloneFrom (FarmAbstract);
			SkeletonFarm.SetRace (storage.RacesTemplate.Undead);
			SkeletonFarm.description = "Skeleton Farm";	
			SkeletonFarm.buildingClass = BuildingType.CASARM;
			
			SanctuaryOfTheDamned.CloneFrom (SanctuaryAbstract);
			SanctuaryOfTheDamned.SetRace (storage.RacesTemplate.Undead);			
			SanctuaryOfTheDamned.unitsToSpawn.AddLast (storage.UnitsTemplate.SkeletonKing);
			SanctuaryOfTheDamned.description = "Sanctuary of undead";					
			SanctuaryOfTheDamned.buildingClass = BuildingType.MAGIC_TOWER;
			
			UndeadMageGuild.CloneFrom (MageGuildAbstract);
			UndeadMageGuild.UnitsToSpawn.AddFirst (storage.UnitsTemplate.SkeletonMage);
			UndeadMageGuild.SetRace (storage.RacesTemplate.Undead);
			UndeadMageGuild.description = "Undead mage guild";		
			UndeadMageGuild.buildingClass = BuildingType.BLACKSMITH;
		}
		
		private void InitKingdom (DataTypes storage)
		{
			CapitalKingdom.CloneFrom (CapitalAbstract);
			CapitalKingdom.capitalGuardian = storage.UnitsTemplate.CastleCapitalGuardian;
			CapitalKingdom.UnitsToSpawn.AddFirst (storage.UnitsTemplate.Swordman);	
			CapitalKingdom.garrisonUnitType = storage.UnitsTemplate.Swordman;			
			CapitalKingdom.Outbuildings.AddFirst (TowerKingdom);
			CapitalKingdom.Outbuildings.AddFirst (FarmKingdom);
			CapitalKingdom.Outbuildings.AddFirst (MageGuildKingdom);	
			CapitalKingdom.Outbuildings.AddFirst (SanctuaryKingdom);					
			CapitalKingdom.SetRace (storage.RacesTemplate.Kingdom);
			
			CastleKingdom.CloneFrom (CastleAbstract);
			CastleKingdom.garrisonUnitType = storage.UnitsTemplate.Swordman;			
			CastleKingdom.Outbuildings.AddFirst (TowerKingdom);
			CastleKingdom.SetRace (storage.RacesTemplate.Kingdom);	
			
			TowerKingdom.CloneFrom (TowerAbstract);
			TowerKingdom.SetRace (storage.RacesTemplate.Kingdom);		
			TowerKingdom.description = "Tower";
			
			FarmKingdom.CloneFrom (FarmAbstract);
			FarmKingdom.SetRace (storage.RacesTemplate.Kingdom);
			FarmKingdom.description = "Farm";		
			
			SanctuaryKingdom.CloneFrom (SanctuaryAbstract);
			SanctuaryKingdom.SetRace (storage.RacesTemplate.Kingdom);
			SanctuaryKingdom.UnitsToSpawn.AddLast (storage.UnitsTemplate.Chaplain);
			SanctuaryKingdom.description = "Sanctuary";					

			MageGuildKingdom.CloneFrom (MageGuildAbstract);
			MageGuildKingdom.UnitsToSpawn.AddLast (storage.UnitsTemplate.Chaplain);
			MageGuildKingdom.SetRace (storage.RacesTemplate.Kingdom);				
			MageGuildKingdom.description = "Mage guild";								
		}
	}
}

