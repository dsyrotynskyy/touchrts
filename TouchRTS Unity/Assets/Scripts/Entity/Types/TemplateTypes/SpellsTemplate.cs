using System;
using System.Collections.Generic;

namespace Entity
{
	public class SpellsTemplate 
	{
		public readonly SpellType FireGround1 = new SpellType ("FireGround", "Level 1");		
		public readonly SpellType FireGround2 = new SpellType ("FireGround", "Level 2");		
			
		public readonly SpellType HellHuricane1 = new SpellType ("HellHurricane", "Level 1");	
		public readonly SpellType HellHuricane2 = new SpellType ("HellHurricane", "Level 2");			
		
		public readonly SpellType FireRay = new SpellType ("FireRay", "Level 1");
		
		public readonly SpellType MagicHammer = new SpellType ("MagicHammer", "Level 1");		
		
		public SpellsTemplate (DataTypes storage) {
			Init (storage);
			
			FireGround1.Store (storage);
			FireGround2.Store (storage);
			HellHuricane1.Store (storage);
			HellHuricane2.Store (storage);
			FireRay.Store (storage);
			MagicHammer.Store (storage);
		}
		
		public void Init (DataTypes storage)
		{
			FireGround1.spellClass = SpellType.SPELL_BEHAVIOUR_CREATE_OBJECT;
			FireGround1.effectArea = 3;
			FireGround1.manaCost = 8;
			FireGround1.spellObjectLifePeriod = 5000;
			FireGround1.effects.Add(storage.EffectsTemplate.Burn);
			FireGround1.offenseLifeSingle = 40;
			FireGround1.spellEffectPower = 20;	
			FireGround1.timeOfEffectActivity = 10000;
			FireGround1.atlasName = "Tyran Spells";
			FireGround1.spriteNameOff = "fire_spell_off";
			FireGround1.spriteNameOn = "fire_spell_on";
			
			FireGround2.InheritSpell (FireGround1);
			FireGround2.effectArea = 4;
			FireGround2.manaCost = 10;
			FireGround2.spellEffectPower = 20;	
			
			HellHuricane1.spellClass = SpellType.SPELL_BEHAVIOUR_MOVE_BY_INPUT;
			HellHuricane1.effectArea = 5;
			HellHuricane1.manaCost = 8;
			HellHuricane1.manaUseFrequency = 200;
			HellHuricane1.effects.Add(storage.EffectsTemplate.Burn);
			HellHuricane1.offenseLifeSingle = 50;
			HellHuricane1.spellEffectPower = 20;	
			HellHuricane1.timeOfEffectActivity = 10000;
			HellHuricane1.atlasName = "Tyran Spells";
			HellHuricane1.spriteNameOff = "ice_spell_off";
			HellHuricane1.spriteNameOn = "ice_spell_on";
		
			HellHuricane2.InheritSpell (HellHuricane1);
			HellHuricane1.offenseLifeSingle = 40;
			HellHuricane2.spellEffectPower = 20;
			
			FireRay.spellClass = SpellType.SPELL_BEHAVIOUR_RAY_SPELL;
			FireRay.effectArea = 5;
			FireRay.manaCost = 8;
			FireRay.manaUseFrequency = 500;
			FireRay.effects.Add (storage.EffectsTemplate.Burn);
			FireRay.offenseLifeSingle = 55;
			FireRay.spellEffectPower = 45;	
			FireRay.timeOfEffectActivity = 1000;
			FireRay.atlasName = "Tyran Spells";
			FireRay.spriteNameOff = "lightning_spell_off";	
			FireRay.spriteNameOn = "lightning_spell_on";		
			
//			MagicHammer.spellClass = SpellType.SPELL_BEHAVIOUR_MOVE_BY_INPUT;
//			MagicHammer.effectArea = 5;
//			MagicHammer.manaCost = 4;
//			MagicHammer.spellObjectLifePeriod = 350;
//			MagicHammer.effects.Add(Effects.Burn);
		}
		
//		public static SpellType Get(String name) {
//			if (name == null || !AllSpellTypes.ContainsKey(name)) {
//				return null;
//			}
//			return AllSpellTypes[name];
//		}
		
//		public static Dictionary<SpellType, String> allSpellTypes = new Dictionary<SpellType, String>();
//		
//		public static void AddToDictionary(SpellType spellType) {
//			if (allSpellTypes == null) {
//				allSpellTypes = new Dictionary<SpellType, string> ();
//			}
//			allSpellTypes.Add(spellType, spellType.Name);
//		}
	}
}

