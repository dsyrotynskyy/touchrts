using System;
using System.Collections.Generic;


namespace Entity
{
	public class EffectsTemplate 
	{
		public readonly EffectType Burn = new EffectType ("Burn");						
		
		public EffectsTemplate (DataTypes storage) {
			Init ();
			Burn.Store (storage);
		}
		
		public void Init ()
		{
			Burn.activityType = EffectType.ACTIVITY_TYPE_PERIOD;
			Burn.interactionType = EffectType.INTERACTION_TYPE_OFFENSE_LIFE;
			Burn.frequency = 20;	
		}
	}
}

