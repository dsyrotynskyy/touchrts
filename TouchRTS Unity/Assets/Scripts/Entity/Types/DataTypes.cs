using System;
using System.Collections.Generic;

namespace Entity
{
	public class DataTypes
	{	
		private static DataTypes instance = null;
		
		public static DataTypes Instance {
			get {
				if (instance == null) {
					instance = new DataTypes ();
				}
				return instance;
			}
		}
		
		public readonly RacesTemplate RacesTemplate;
		public readonly BuildingsTemplate BuildingsTemplate;
		public readonly AbilitiesTemplate AbilitiesTemplate;
		public readonly EffectsTemplate EffectsTemplate;
		public readonly MagicAnimationsTemplate MagicAnimationsTemplate;
		public readonly SpellsTemplate SpellsTemplate;
		public readonly UnitsTemplate UnitsTemplate;
		
		public readonly Units Units = new Units ();	
		public readonly Heroes Heroes = new Heroes ();
		public readonly Spells Spells = new Spells ();
		public readonly Races Races = new Races ();
		public readonly MagicAnimations MagicAnimations = new MagicAnimations ();
		public readonly Effects Effects = new Effects ();
		public readonly Buildings Buildings = new Buildings ();
		public readonly Abilities Abilities = new Abilities ();
		
		/* Order is very important
		 * */
		public DataTypes () {
			RacesTemplate = new RacesTemplate (this);
			MagicAnimationsTemplate = new MagicAnimationsTemplate (this);
			AbilitiesTemplate = new AbilitiesTemplate (this);
			EffectsTemplate = new EffectsTemplate (this);
			SpellsTemplate = new SpellsTemplate (this);
			UnitsTemplate = new UnitsTemplate (this);
			BuildingsTemplate = new BuildingsTemplate (this);
			
			RacesTemplate.Prepare (this);
			
			Heroes.unit = Units;			
		}
		
		public void CreateFrom (DataTypes source) {
			Units.AddAll (source.Units);
			Spells.AddAll (source.Spells);		
			Races.AddAll (source.Races);			
			MagicAnimations.AddAll (source.MagicAnimations);			
			Effects.AddAll (source.Effects);			
			Buildings.AddAll (source.Buildings);
			Abilities.AddAll (source.Abilities);
			
			this.UpdateReferences ();
		}
		
		private void UpdateReferences () {
			Units.UpdateReferences (this);
			Spells.UpdateReferences (this);		
			Races.UpdateReferences (this);			
			MagicAnimations.UpdateReferences (this);			
			Effects.UpdateReferences (this);			
			Buildings.UpdateReferences (this);
			Abilities.UpdateReferences (this);
		}
	}
	
	public class Spells {
		private Dictionary<SpellType, String> allSpellTypes = new Dictionary<SpellType, String>();
		
		public Dictionary<SpellType, String> AllSpellTypes {
			get {
				return this.allSpellTypes;
			}
		}	
		
		public SpellType GetSpellByName (String name) {
			foreach (SpellType spell in allSpellTypes.Keys) {
				if (spell.Name == name) {
					return spell;
				}
			}
			return null;
		}
		
		public void AddToDictionary(SpellType spellType) {
			if (allSpellTypes == null) {
				allSpellTypes = new Dictionary<SpellType, string> ();
			}
			allSpellTypes.Add(spellType, spellType.Name);
		}
		
		public void AddAll (Spells source) {
			foreach (SpellType spell in source.allSpellTypes.Keys) {
				AddToDictionary (spell);
			}
		}
		
		public void UpdateReferences (DataTypes owner) {
			foreach (SpellType spell in allSpellTypes.Keys) {
				Modify (spell, owner);
			}
		}
		
		private static LinkedList<EffectType> newEffects = new LinkedList<EffectType> ();
				
		public void Modify (SpellType spell, DataTypes source) {
			foreach (EffectType effect in spell.Effects) {
				EffectType newEffect = source.Effects.Get (effect.Name);
				newEffects.AddLast (newEffect);
			}
			spell.Effects.Clear ();
			foreach (EffectType newEffect in newEffects) {
				spell.Effects.Add (newEffect);
			}
			newEffects.Clear ();
		}
		
		public SpellType Get(String name) {
			foreach (SpellType spellType in allSpellTypes.Keys) {
				if (spellType.Name.Equals (name)) {
					return spellType;
				}
			}
			return null;
		}
	}
	
	public class Units : TypeStorage<UnitType> {
		private static LinkedList<AbilityType> newAbilities = new LinkedList<AbilityType> ();
		private static LinkedList<EffectType> newEffects = new LinkedList<EffectType> ();
		private static LinkedList<SpellType> newSpells = new LinkedList<SpellType> ();
		
		public override void Modify (UnitType unitType, DataTypes source)
		{
			foreach (AbilityType ability in unitType.Abilities) {
				AbilityType newAbility = source.Abilities.Get (ability.Name);
				newAbilities.AddLast (newAbility);
			}		
			unitType.Abilities.Clear ();
			foreach (AbilityType newAbility in newAbilities) {
				unitType.Abilities.AddLast (newAbility);
			}
			newAbilities.Clear ();
			
			foreach (EffectType effect in unitType.Effects) {
				EffectType newEffect = source.Effects.Get (effect.Name);
				newEffects.AddLast (newEffect);
			}
			unitType.Effects.Clear ();
			foreach (EffectType newEffect in newEffects) {
				unitType.Effects.AddLast (newEffect);
			}
			newEffects.Clear ();
			
			RaceType newRaceType = source.Races.Get(unitType.Race.Name);
			unitType.Race = newRaceType;
			
			if (unitType.MagicAnimationType != null) {
				MagicAnimationType newAnimationType = source.MagicAnimations.Get(unitType.MagicAnimationType.Name);
				unitType.MagicAnimationType = newAnimationType;
			}
			
			if (unitType.IsHero) {
				HeroType heroType = (HeroType)unitType;
				foreach (SpellType spell in heroType.Spells) {
					SpellType newSpell = source.Spells.Get (spell.Name);
					newSpells.AddLast (newSpell);
				}
				heroType.Spells.Clear ();
				foreach (SpellType newSpell in newSpells) {
					heroType.Spells.Add (newSpell);
				}
				newSpells.Clear ();
			}
		}
	}
	
	public class MagicAnimations : TypeStorage<MagicAnimationType> {
		public override void Modify (MagicAnimationType t, DataTypes source)
		{
		}
	}
	
	public class Races : TypeStorage<RaceType> {
		private static LinkedList<HeroType> newRaceHeroes = new LinkedList<HeroType>();
		
		/* int - class of building
		 * buildingType - buildingType:)
		 */
		private static Dictionary<int, BuildingType> newSpecialRaceBuildings = new Dictionary<int, BuildingType>();
		private static List<UnitType> newRaceUnits = new List<UnitType>();		

		public override void Modify (RaceType raceType, DataTypes source)
		{
			foreach (HeroType hero in raceType.raceHeroes) {
				HeroType newHero = source.Heroes.Get (hero.Name);
				newRaceHeroes.AddLast (newHero);
			}		
			raceType.raceHeroes.Clear ();
			foreach (HeroType newHero in newRaceHeroes) {
				raceType.raceHeroes.AddLast (newHero);
			}
			newRaceHeroes.Clear ();
			
			foreach (UnitType unit in raceType.units) {
				if (unit == null) {
					continue;
				}
				UnitType newUnit = source.Units.Get (unit.Name);
				newRaceUnits.Add (newUnit);
			}		
			raceType.units.Clear ();
			foreach (UnitType newUnit in newRaceUnits) {
				raceType.units.Add (newUnit);
			}
			newRaceUnits.Clear ();
			
			foreach (int key in raceType.specialRaceBuildings.Keys) {
				BuildingType newBuilding = source.Buildings.Get (raceType.specialRaceBuildings[key].Name);
				newSpecialRaceBuildings.Add (key, newBuilding);
			}		
			raceType.specialRaceBuildings.Clear ();
			foreach (int key in newSpecialRaceBuildings.Keys) {
				raceType.specialRaceBuildings.Add (key, newSpecialRaceBuildings[key]);
			}
			newSpecialRaceBuildings.Clear ();
		}
	}
	
	public class Effects : TypeStorage<EffectType> {
		public override void Modify (EffectType t, DataTypes source)
		{
		}
	}
	
	public class Buildings : TypeStorage<BuildingType> {
		private static List<UnitType> newUnits = new List<UnitType>();		
		private static LinkedList<SpellType> newSpells = new LinkedList<SpellType> ();
		private static LinkedList<BuildingType> newBuildings = new LinkedList<BuildingType> ();
		
		#region implemented abstract members of Entity.TypeStorage[BuildingType]
		public override void Modify (BuildingType castle, DataTypes source)
		{
			foreach (UnitType unit in castle.UnitsToSpawn) {
				UnitType newUnit = source.Units.Get (unit.Name);
				newUnits.Add (newUnit);
			}		
			castle.UnitsToSpawn.Clear ();
			foreach (UnitType newUnit in newUnits) {
				castle.UnitsToSpawn.AddLast (newUnit);
			}
			newUnits.Clear ();
			
			foreach (SpellType spell in castle.Spells) {
				SpellType newSpell = source.Spells.Get (spell.Name);
				newSpells.AddLast (newSpell);
			}
			castle.Spells.Clear ();
			foreach (SpellType newSpell in newSpells) {
				castle.Spells.AddLast (newSpell);
			}
			newSpells.Clear ();
			
			foreach (BuildingType building in castle.Outbuildings) {
				newBuildings.AddLast (building);
			}
			castle.Outbuildings.Clear ();
			foreach (BuildingType newBuildingType in newBuildings) {
				castle.Outbuildings.AddLast (newBuildingType);
			}
			newBuildings.Clear ();
		}
		#endregion
	}
	
	public class Abilities : TypeStorage<AbilityType> {
		private static LinkedList<EffectType> newEffects = new LinkedList<EffectType> ();
						
		public override void Modify (AbilityType ability, DataTypes source)
		{
			foreach (EffectType effect in ability.Effects) {
				EffectType newEffect = source.Effects.Get (effect.Name);
				newEffects.AddLast (newEffect);
			}
			ability.Effects.Clear ();
			foreach (EffectType newEffect in newEffects) {
				ability.Effects.AddLast (newEffect);
			}
			newEffects.Clear ();
		}
	}
	
	public class Heroes
	{	
		public Units unit;
		
		public Heroes () {
		}
		
		public HeroType Get (String name)
		{
			return (HeroType)(unit.Get (name));
		}
	}
	
	public abstract class TypeStorage<T> where T : AbstractType {		
		public Dictionary<String, T> allData;
		
		public T Get(String name) {
			if (!allData.ContainsKey(name)) {
				return null;
			}
			return allData[name];
		}

		public void AddAll (TypeStorage<T> source) {
			foreach (T t in source.allData.Values) {
				if (t == null) {
					continue;
				}
				AddToDictionary (t);
			}
		}
		
		public void AddToDictionary(T t) {
			if (allData == null) {
				allData = new Dictionary<string, T> ();
			}
			if (allData.ContainsKey(t.Name)) {
				return;
			}
			allData.Add(t.Name, t);		
		}
		
		public void UpdateReferences (DataTypes owner) {
			foreach (T t in allData.Values) {
				Modify (t, owner);
			}
		}
				
		public abstract void Modify (T t, DataTypes source);
		
		public override string ToString ()
		{
			return this.allData.ToString ();
		}
	}
}

