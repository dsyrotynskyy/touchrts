using System;
using System.Collections.Generic;

namespace Entity
{
	public class EffectType : AbstractType
	{
		/*
		 * Effect is passive. Works all the time
		 * */
		public static readonly int ACTIVITY_TYPE_PASSIVE = 4123;//is active for a time period and is passive, like improve protection
		/**
		 * Works in time period with frequency
		 * */
		public static readonly int ACTIVITY_TYPE_PERIOD = 4125;
		public static readonly int INTERACTION_TYPE_OFFENSE_LIFE = 678;
		public static readonly int INTERACTION_TYPE_HEAL_LIFE = 609;
		
		/*
		 * Should not be used with frequency, becouse protection is static.
		 * May be used only in time period
		 * */
		public static readonly int INTERACTION_TYPE_PROTECTION = 1234;
		
		static EffectType ()
		{		
		}
		
		//Type of activity
		internal int activityType;
		//Type of interaction - healing, damage, protection
		internal int interactionType;
				
		/**
		 * Frequency per function calls(maybe) Ask Danylo for details
		 * */
		internal int frequency;
		
		internal EffectType (string name)  : base(name)
		{
			
		}
		
		public void Store (DataTypes storage) {
			storage.Effects.AddToDictionary (this);
		}
		/*
		 * Return true if is Effect used to effect alied units
		 * otherwise returns false
		 * 
		 * true if power < 0
		 * */
		public bool IsPositiveEffect ()
		{
			return interactionType == INTERACTION_TYPE_HEAL_LIFE;
		}
		
		public int ActivityType {
			get {
				return this.activityType;
			}
		}

		public int Frequency {
			get {
				return this.frequency;
			}
		}

		public int InteractionType {
			get {
				return this.interactionType;
			}
		}

		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			return this;
		}
		#endregion
	}
	
	public interface EffectPerformer {
		int TimeOfEffectActivity {
			get;
		}
		
		int EffectPower {
			get;
		}
		
		bool IsSpell {
			get;
		}
		
		string Id {
			get;
		}
	}
}

