using System;
using System.Collections;
using System.Collections.Generic;

namespace Entity
{
	public class SpellType : AbstractType
	{
		public static readonly int SPELL_BEHAVIOUR_AROUND_HERO = 165;
		/*
		 * Like lightning from hero hammer or who atacks only selected enemy
		 * */
		public static readonly int SPELL_BEHAVIOUR_MOVE_BY_INPUT = 167;
		/*
		 * Like fire flame spawned on territory 
		 * and atacking all enemies around him
		 * */
		public static readonly int SPELL_BEHAVIOUR_CREATE_OBJECT = 1617;
		
		public static readonly int SPELL_BEHAVIOUR_RAY_SPELL = 1618;	
		
		static SpellType() {		
		}
		
		internal int spellLevel = 1;//first level
		
		internal SpellType spellToInherit; //Replace this spell in hero spellbook. Hero must know this spell to study current spell 		
		internal int price = 0;
		
		/**
		 * Effects to affect the unit
		 * */
		internal List<EffectType> effects = new List<EffectType>();
		
		internal int spellClass;
		internal int effectArea;
		internal int manaCost;
		
		internal int offenseLifeSingle;//interact target once on unit effected
		internal int effectPowerRandomOffset = 15;
		internal int spellEffectPower;//interact target every period
				/**
		 * If == 0, only one time
		 * */
		internal int timeOfEffectActivity;// time of period effect activity
	
		internal int spellObjectLifePeriod;
		/*
		 * Used only when SPELL_BEHAVIOUR_MOVE_BY_INPUT
		 * */
		internal int manaUseFrequency;
		
		//For NGUI
		internal string atlasName;
		internal string spriteNameOff;
		internal string spriteNameOn;

		internal SpellType(string name, string subTitle = null) : base(name, subTitle) {
		}
		
		internal SpellType() : base(AbstractType.ABSTRACT_DATA_TYPE) {
		}
		
		public void Store (DataTypes storage) {
			storage.Spells.AddToDictionary (this);
		}
		
		internal void InheritSpell(SpellType spellTypeToInherit) {				
			CloneFrom (spellTypeToInherit);
			this.spellToInherit = spellTypeToInherit;
			this.spellLevel = spellToInherit.spellLevel + 1;
		}
		
		internal void CloneFrom (SpellType spellType) {			
			this.price = spellType.price;
			this.effects.AddRange (spellType.effects);
			this.spellClass = spellType.spellClass;
			this.effectArea = spellType.effectArea;
			this.manaCost = spellType.manaCost;
			
			this.atlasName = spellType.atlasName;
			this.spriteNameOff = spellType.spriteNameOff;
			this.spriteNameOn = spellType.spriteNameOn;
			
			this.offenseLifeSingle = spellType.offenseLifeSingle;
			this.spellEffectPower = spellType.spellEffectPower;
			this.timeOfEffectActivity = spellType.timeOfEffectActivity;
			
			this.spellObjectLifePeriod = spellType.spellObjectLifePeriod;
			
			this.manaUseFrequency = spellType.manaUseFrequency;
		}
		
		#region work methods	
		/*
		 * Spell creates game object on map, targeting units
		 * */
		public bool IsCreatingObjectOnMap() {
			return this.spellClass == SPELL_BEHAVIOUR_CREATE_OBJECT;
		}
		
		public bool IsRaySpell () {
			return this.spellClass == SPELL_BEHAVIOUR_RAY_SPELL;
		}
		
		/*
		 * Spell moves by player touches
		 * */
		public bool IsMovableSpell() {
			return this.spellClass == SPELL_BEHAVIOUR_MOVE_BY_INPUT;
		}

		public List<EffectType> Effects {
			get {
				return this.effects;
			}
		}

		public int EffectArea {
			get {
				return this.effectArea;
			}
		}

		public int ManaCost {
			get {
				return this.manaCost;
			}
		}
		
		public int ManaUseFrequency {
			get {
				return this.manaUseFrequency;
			}
		}
		
		public int SpellObjectLifePeriod {
			get {
				return this.spellObjectLifePeriod;
			}
		}
		
		public int TimeOfEffectActivity {
			get {
				return this.timeOfEffectActivity;
			}
		}	
		#endregion
		public int OffenseLifeSingleValue {
			get {return this.offenseLifeSingle;	}	
		}
//		
//		public int OffenseLifeSingleValue (PlayerData playerPerformer) {
//			return this.offenseLifeSingle + playerPerformer.SpellSinglePowerModification;		
//		}
//		
//		public int InteractionPower (PlayerData playerPerformer) {
//			return this.spellEffectPower + playerPerformer.SpellSinglePowerModification;		
//		}
		
		public int EffectPower {
			get {return spellEffectPower;}
		}
		
		public bool HasPositiveSingleInteraction ()
		{
			return this.offenseLifeSingle < 0;
		}

		public int SpellLevel {
			get {
				return this.spellLevel;
			}
		}

		public int Price {
			get {
				return this.price;
			}
		}

		#region EffectPerformer implementation
		public int InteractionPowerPercentRandomOffset {
			get {
				return effectPowerRandomOffset;
			}
		}
		#endregion
		
		public virtual void ModifyValues (SpellType modificationData) {
		}

		#region implemented abstract members of Entity.AbstractType
		public override AbstractType Clone ()
		{
			SpellType clone = new SpellType ();
			clone.CloneCommonFieldsFrom (this);			
			clone.CloneFrom (this);
			return clone;
		}
		#endregion

		public string AtlasName {
			get {
				return this.atlasName;
			}
		}

		public string SpriteNameOn {
			get {
				return this.spriteNameOn;
			}
		}
		
		public string SpriteNameOff {
			get {
				return this.spriteNameOff;
			}
		}
	}
}

