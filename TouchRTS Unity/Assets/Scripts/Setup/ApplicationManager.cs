using System;
using UnityEngine;
using System.Collections;
using Kajabity.Tools.Java;
using Entity;
using GameMapScenario;
using System.Text;
using System.Collections.Generic;

namespace Setup
{
	public class ApplicationManager
	{
		private static ApplicationManager instance;

//		public static GameManager Instance {
//			get {
//				if (instance == null) {
//					instance = new GameManager ();
//				}
//				return instance;
//			}
//		}
		
		public static bool isGameUpdating = false;
		
		public static void OnNewSceneStarted ()
		{
			if (instance == null) {
				instance = new ApplicationManager ();
			}
		}
		
		public static void OnNewWorldSceneStarted ()
		{
			OnNewSceneStarted ();
			
			isGameUpdating = true;
		}
		
		public Dictionary<string, MapData> battleMaps = new Dictionary<string, MapData> ();
		private TextAsset asset; // Gets assigned through code. Reads the file.
		private string deviceName;
		
		private ApplicationManager ()
		{
			CreateTemplateUnits ();
			DeviceName = CreateRandomString (8);
//			ReadUnitData ();
			InitBattleMaps ();
		}
		
		private void InitBattleMaps () {
			AddMapData ("Battle Scene 1", 2);
			AddMapData ("Network Scene", 2);
		}
		
		private void AddMapData (string name, int id) {
			this.AddMapData (new MapData(name, id));
		}
		
		private void AddMapData (MapData mapData) {
			this.battleMaps.Add (mapData.Name, mapData);
		}
		
		public string CreateRandomString(int length)
		{
		    string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+~";
		    StringBuilder builder = new StringBuilder(length);
		
		    for (int i = 0; i < length; ++i)
		        builder.Append(chars[Randomizer.Next(chars.Length)]);
		
		    return builder.ToString();
		}
		
		private void ReadUnitData ()
		{
			string fileName = "GameData/" + "" + "Units";
			MonoBehaviour.print (fileName);
			asset = Resources.Load (fileName) as TextAsset;
			
			JavaProperties properties = new JavaProperties ();
			
			using (System.IO.MemoryStream ms = new System.IO.MemoryStream(
		    asset.bytes)) {
				properties.Load (ms);
			}
			
			foreach (string str in properties.Keys) {
				MonoBehaviour.print (str + " = " + properties [str]);
			}
		}
		
		private void CreateTemplateUnits ()
		{
		}
		
		public void LoadScene(string name) {			
			LoadScene (name, null);
		}
		
		public void LoadScene (string name, MapScenario scenario) {	
			isGameUpdating = false;
			
			LevelController.predefinedGameScenario = scenario;
			LevelController.predefinedGameScenarioName = name;
			
			Application.LoadLevel (name);
		}
		
//		public static void TestLoadNetworkSceneServer (string name) {			
//			SingleMapScenario scenario = new SingleMapScenario ();
//			scenario.SetHumanPlayer (2, 2);
//			LoadScene (name, scenario);
//		}
//		
//		public static void TestLoadNetworkSceneClient (string name, HostData hostData) {
//			SingleMapScenario scenario = new SingleMapScenario ();
//			scenario.SetHumanPlayer (1, 1);
//			LoadScene (name, scenario);
//		}
		
		public string DeviceName {
			get {
				return this.deviceName;
			}
			set {
				deviceName = value;
			}
		}

		public static ApplicationManager Instance {
			get {
				if (instance == null) {
					instance = new ApplicationManager ();
				}
				return instance;
			}
		}

		public Dictionary<string, MapData> BattleMaps {
			get {
				return this.battleMaps;
			}
		}
	}
	
	public class MapData {		
		private string name;
		private string imageName;
		private string description;
		private int numOfPlayers;
		
		public MapData (string name, int numOfPlayers) {
			this.name = name;
			this.numOfPlayers = numOfPlayers;
		}

		public string Description {
			get {
				return this.description;
			}
			set {
				description = value;
			}
		}

		public string ImageName {
			get {
				return this.imageName;
			}
			set {
				imageName = value;
			}
		}

		public string Name {
			get {
				return this.name;
			}
			set {
				name = value;
			}
		}

		public int NumOfPlayers {
			get {
				return this.numOfPlayers;
			}
			set {
				numOfPlayers = value;
			}
		}
	}
}

