using System;
using UnityEngine;

namespace Setup
{
	public class GameSettings
	{
		public GameSettings ()
		{
		}
		
		public class Display {
			public static readonly bool SHOW_FPS = false;
		}
		
		public class Navigation {
			public static readonly bool USE_NAVIGATION_FIGURE = true;
		}
		
		public class Tag {
			public static readonly string UNIT = "Unit";
			public static readonly string HERO = "Hero";
			public static readonly string BUILDING = "Building";			
			public static readonly string UNIT_BODY = "UnitBody";
			public static readonly string OBSTACLES = "Obstacles";
			public static readonly string MINI_MAP_ELEMENT = "MiniMapElement";
			public static readonly string EFFECT = "Effect";
			public static readonly string GROUND = "Ground";
			public static readonly string NAVIGATION = "Navigation";
		}
		
		public class Layer {
			public static readonly int UNIT = UnityEngine.LayerMask.NameToLayer ("Unit");
			public static readonly int UNIT_BODY = UnityEngine.LayerMask.NameToLayer ("UnitBody");
			public static readonly int OBSTACLES = UnityEngine.LayerMask.NameToLayer ("Obstacles");
			public static readonly int MINI_MAP_ELEMENT = UnityEngine.LayerMask.NameToLayer ("MiniMapElement");
			public static readonly int EFFECT = UnityEngine.LayerMask.NameToLayer ("Effect");
			public static readonly int GROUND = UnityEngine.LayerMask.NameToLayer ("Ground");
			public static readonly int NAVIGATION = UnityEngine.LayerMask.NameToLayer ("Navigation");
		}
	}
}

