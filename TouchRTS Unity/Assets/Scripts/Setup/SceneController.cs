using UnityEngine;
using System.Collections;
using GameMapScenario;
using Setup;

public class SceneController : MonoBehaviour
{
	protected int prevLevelPrefix = 0;
	private bool isNetwork;
	private bool gameStarted;
	protected SingleMapScenario singleMapScenario = new SingleMapScenario ();
	
	protected bool IsNetworkGame {
		get {
			return isNetwork;
		}
		set {
			isNetwork = value;
		}
	}
	
	public void PrepareHumanPlayer (int humanPlayerId, int team = -1) {
		if (team == -1) {
			team = humanPlayerId;
		}
		singleMapScenario.SetNetworkHumanPlayer (humanPlayerId, team, ApplicationManager.Instance.DeviceName);
	}
	
	public void LoadCampaignLevel (string level) {
		ApplicationManager.Instance.LoadScene (level);			
	}
	
	public void LoadLevel (string level, MapScenario mapScenario = null, int levelPrefix = 0) {
		StartCoroutine (RPCLoadLevel (level));
	}
	
	
	
	[RPC]
	public IEnumerator RPCLoadLevel (string level)
	{
		if (IsNetworkGame) {
			Debug.Log("Loading level " + level + " with prefix " + prevLevelPrefix);
		
			// There is no reason to send any more data over the network on the default channel,
			// because we are about to load the level, thus all those objects will get deleted anyway
			Network.SetSendingEnabled(0, false);	
		
			// We need to stop receiving because first the level must be loaded.
			// Once the level is loaded, RPC's and other state update attached to objects in the level are allowed to fire
			Network.isMessageQueueRunning = false;
				
			// All network views loaded from a level will get a prefix into their NetworkViewID.
			// This will prevent old updates from clients leaking into a newly created scene.
			Network.SetLevelPrefix(prevLevelPrefix);
			prevLevelPrefix++;
		}
		gameStarted = true;
		ApplicationManager.Instance.LoadScene (level, singleMapScenario);
//		ApplicationManager.Instance.LoadScene (level, new EmptyMapScenario ());

		yield return 0;
		
		if (IsNetworkGame) {
			// Allow receiving data again
			Network.isMessageQueueRunning = true;
			
			// Now the level has been loaded and we can start sending out data
			Network.SetSendingEnabled(0, true);
		}
		
		// Notify our objects that the level and the network is ready
//		foreach (GameObject go in GameObject.FindGameObjectWithTag("")) {
//			go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);	
//		}
	}
	
	public void OnDisable ()
	{
		if (!gameStarted && IsNetworkGame) {
			NetworkManager.Instance.LeaveNetwork ();
		}		
	}

	public SingleMapScenario SingleMapScenario {
		get {
			return this.singleMapScenario;
		}
	}
}

