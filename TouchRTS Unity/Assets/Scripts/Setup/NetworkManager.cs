using UnityEngine;
using System.Collections;

namespace Setup {
	public class NetworkManager
	{
		private static NetworkManager instance;
	
		public static NetworkManager Instance {
			get {
				if (instance == null) {
					instance = new NetworkManager ();
				}
				return instance;
			}
		}	
		private NetworkManager () {
		}
		
	    private string gameTypeName = "TouchRTSNetwork";
	    private string roomName = "TestRoom";
	
	    private bool isRefreshingHostList = false;
	    private HostData[] hostList;
	
	//    void OnGUI()
	//    {
	//        if (!Network.isClient && !Network.isServer)
	//        {
	//            if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
	//                StartServer();
	//
	//            if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
	//                RefreshHostList();
	//
	//            if (hostList != null)
	//            {
	//                for (int i = 0; i < hostList.Length; i++)
	//                {
	//                    if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName))
	//                        JoinServer(hostList[i]);
	//                }
	//            }
	//        }
	//    }
	
		public void StartServer () {
			if (Network.isServer) {
				return;
			}
			
			const int listenLow = 25000;
			int tries = 0;
			bool useNat = !Network.HavePublicAddress();
			useNat = true;
			while (tries < 5) {
				
				NetworkConnectionError error = Network.InitializeServer(5, listenLow + Randomizer.Next (1000), useNat);
				Network.maxConnections = 10;
				if (error == NetworkConnectionError.NoError) {
					MasterServer.RegisterHost(gameTypeName, roomName);
					Debug.Log ("Server started");
					return;	
				}
				tries++;	
			}
		}
		
		public bool CanJoinServer {
			get {
				isRefreshingHostList = false;
				if (MasterServer.PollHostList().Length > 0)
		        {
		            hostList = MasterServer.PollHostList();
		        } else {
					hostList = null;
				}
				if (hostList == null) {
					return false;
				}
				return this.hostList.Length > 0;
			}
		}
		
		public void JoinRandomServer()
	    {
			foreach (HostData host in hostList) {
				JoinServer(host);
				return;
			}
	        
	    }
		
		bool connected = false;
		
		public void JoinServer(HostData hostData)
	    {
			if (Network.isClient || connected) {
				return;
			}
			connected = true;
			Debug.Log ("Server joined = " + hostData.ToString ());
	        Network.Connect(hostData);
	    }
	
		public void LeaveNetwork ()
		{
			connected = false;
			Debug.Log ("Network left");
			Network.Disconnect();
			MasterServer.UnregisterHost();
		}
	
	    public void RefreshHostList()
	    {
			if (!isRefreshingHostList)
	        {
	            isRefreshingHostList = true;
	            MasterServer.RequestHostList(gameTypeName);
	        }
	    }
		
		public bool IsNetworkGame {
			get {
//				return false;
				return Network.isClient || Network.isServer;
			}
		}
		
		public bool IsNetworkGameServer {
			get {
				if (!IsNetworkGame) {
					return false;
				}
				return Network.isServer;
	//			return false;
			}
		}
	
		public bool IsNetworkGameClient {
			get {
				if (!IsNetworkGame) {
					return false;
				}
				return Network.isClient;
	//			return false;
			}
		}
	}
}
