using UnityEngine;
using System.Collections;
using Setup;

public class GameStarter : MonoBehaviour
{
	public static string MENU_LEVEL_NAME = "MainMenu";
	
	// Use this for initialization
	void Start ()
	{
		ApplicationManager.OnNewSceneStarted ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

