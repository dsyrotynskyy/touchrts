using UnityEngine;
using System;
using System.Collections.Generic;

public class ShakeParameters
{
    public int count;
    public float magnitude;
    public float phaseSpeed;
    public float decreaseTime;
}

// Shake camera with a few movement vectors, generated in random directions
public class CameraShaker : MonoBehaviour
{	
    public int count = 4;
    public float magnitude = 0.15f;
    public float phaseSpeed = 8f;
    public float decreaseTime = 3.0f;	
	
	class ShakeVector
	{
		Vector3 magnitudeVector;
		float intensity;
		
		public float decreaseTime;
		public float phase;
		public float phaseSpeed;
		
		public ShakeVector(float magnitude, float phaseSpeed, float decreaseTime)
		{
			intensity = 1f;
			phase = 0f;
            this.phaseSpeed = (UnityEngine.Random.value + 0.5f) * phaseSpeed;
			this.decreaseTime = decreaseTime;

            magnitudeVector = UnityEngine.Random.insideUnitSphere;
			magnitudeVector.x += Mathf.Sign(magnitudeVector.x) * 0.5f;
			magnitudeVector.y += Mathf.Sign(magnitudeVector.y) * 0.5f;
			magnitudeVector.z += Mathf.Sign(magnitudeVector.z) * 0.5f;
			magnitudeVector *= magnitude;
		}
		
		public Vector3 GetValueAndUpdate(float deltaTime)
		{
			if(intensity < Mathf.Epsilon)
				return Vector3.zero;
			
			var curIntensity = 1f - Mathf.Sqrt(Mathf.Max(1f - intensity * intensity, Mathf.Epsilon));
			
			var sinPhase = Mathf.Sin(phase);
			var offset = new Vector3(sinPhase, sinPhase, sinPhase);
			for(int i = 0; i < 3; ++i)
				offset[i] *= magnitudeVector[i] * curIntensity;
			
			intensity -= 1f / decreaseTime * deltaTime;
			phase += phaseSpeed * 2 * Mathf.PI * deltaTime;
			
			return offset;
		}
		
		public bool IsAlive()
		{
			return intensity > Mathf.Epsilon;
		}
	}
    
	[System.NonSerialized]
	List<ShakeVector> positionVectors = new List<ShakeVector>();
	
	[System.NonSerialized]
	List<ShakeVector> rotationVectors = new List<ShakeVector>();
	
	void Update () 
	{
		if(positionVectors.Count > 0)
		{
			var positionOffset = Vector3.zero;
			for(int i = 0; i < positionVectors.Count; ++i)
			{
				if(!positionVectors[i].IsAlive())
				{
					positionVectors.RemoveAt(i);
					--i;
					continue;
				}
				positionOffset += positionVectors[i].GetValueAndUpdate(Time.deltaTime);
			}
			transform.localPosition += positionOffset;
		}
		
		if(rotationVectors.Count > 0)
		{		
			var rotationOffset = Vector3.zero;
			for(int i = 0; i < rotationVectors.Count; ++i)
			{
				if(!rotationVectors[i].IsAlive())
				{
					rotationVectors.RemoveAt(i);
					--i;
					continue;
				}
				rotationOffset += rotationVectors[i].GetValueAndUpdate(Time.deltaTime);
			}
			transform.localRotation = Quaternion.Euler(rotationOffset) * transform.localRotation;
		}
		
		if(positionVectors.Count == 0 && rotationVectors.Count == 0)
		{
			//transform.localPosition = Vector3.zero;
			//transform.localRotation = Quaternion.identity;;
			//enabled = false;
		}
		
	}
	
	public void AddPositionShake(float magnitude, float phaseSpeed, float decreaseTime)
	{
		positionVectors.Add(new ShakeVector(magnitude, phaseSpeed, decreaseTime));
		//if(!enabled) enabled = true; // Better to check than to set enabled true always
	}		
	
	public void AddPositionShake(int count, float magnitude, float phaseSpeed, float decreaseTime)
	{
		for(int i = 0; i < count; ++i)
			AddPositionShake(magnitude, phaseSpeed, decreaseTime);
	}
	
	public void AddRotationShake(float magnitude, float phaseSpeed, float decreaseTime)
	{
		rotationVectors.Add(new ShakeVector(magnitude, phaseSpeed, decreaseTime));
		//if(!enabled) enabled = true; // Better to check than to set enabled true always		
	}
	
	public void AddRotationShake(int count, float magnitude, float phaseSpeed, float decreaseTime)
	{
		for(int i = 0; i < count; ++i)
			AddRotationShake(magnitude, phaseSpeed, decreaseTime);
	}

    public void AddPositionShake(ShakeParameters parameters)
    {
        if (parameters.count == 0)
        {
            positionVectors.Add(new ShakeVector(parameters.magnitude, parameters.phaseSpeed, parameters.decreaseTime));
            if (!enabled) enabled = true; // Better to check than to set enabled true always
        }
        else
        {
            for (int i = 0; i < parameters.count; ++i)
                AddPositionShake(parameters.magnitude, parameters.phaseSpeed, parameters.decreaseTime);
        }
    }

    public void AddRotationShake(ShakeParameters parameters)
    {
        if (parameters.count == 0)
        {
            rotationVectors.Add(new ShakeVector(parameters.magnitude, parameters.phaseSpeed, parameters.decreaseTime));
            if (!enabled) enabled = true; // Better to check than to set enabled true always	
        }
        else
        {
            for (int i = 0; i < parameters.count; ++i)
                AddRotationShake(parameters.magnitude, parameters.phaseSpeed, parameters.decreaseTime);
        }

    }
	
	void OnGUI()
	{
		if(GUI.Button(new Rect(100, 200, 100, 50), "Shake it!"))
		{
			var sp = new ShakeParameters { count = this.count, magnitude = this.magnitude,
				phaseSpeed = this.phaseSpeed, decreaseTime = this.decreaseTime };
			//AddPositionShake(sp);
			AddRotationShake(sp);
		}
	}

}