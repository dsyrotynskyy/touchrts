using UnityEngine;
using System.Collections;
using GameMapScenario;

public class NetworkTestScenario : ScriptedMapScenario
{
	public string TypeName = "UniqueGameName";
    public string GameName = "RoomName";
	
	public NetworkTestScenario () : base () {
//		NetworkParams nParams = new NetworkParams ();
//		nParams.SetServer (true);
//		this.scenarioData.NetworkParams = nParams;
	}
	
	protected override void OnCreate (WorldController worldController)
	{
		this.scenarioData.SetWinningConditions (new FalseCondition ());
		this.scenarioData.SetFailConditions (new FalseCondition ());
		
		this.SetHumanPlayer (1, 1);
		this.SetAiPlayer (2, 2);
	}
}

//public class NetworkManager : MonoBehaviour
//{
//    private const string typeName = "UniqueGameName";
//    private const string gameName = "RoomName";
//
//    private bool isRefreshingHostList = false;
//    private HostData[] hostList;
//
//    public GameObject playerPrefab;
//
//    void OnGUI()
//    {
//        if (!Network.isClient && !Network.isServer)
//        {
//            if (GUI.Button(new Rect(100, 100, 150, 50), "Start Server"))
//                StartServer();
//
//            if (GUI.Button(new Rect(100, 250, 150, 50), "Refresh Hosts"))
//                RefreshHostList();
//
//            if (hostList != null)
//            {
//                for (int i = 0; i < hostList.Length; i++)
//                {
//                    if (GUI.Button(new Rect(400, 100 + (110 * i), 100, 50), hostList[i].gameName))
//                        JoinServer(hostList[i]);
//                }
//            }
//        }
//    }
//
//	private void StartServer()
//    {
//        Network.InitializeServer(5, 25000, !Network.HavePublicAddress());
//        MasterServer.RegisterHost(typeName, gameName);
//    }
//	
//	private void JoinServer(HostData hostData)
//    {
//        Network.Connect(hostData);
//    }
//	
//	private void RefreshHostList()
//    {
//        if (!isRefreshingHostList)
//        {
//            isRefreshingHostList = true;
//            MasterServer.RequestHostList(typeName);
//        }
//    }
//
//
////    void OnServerInitialized()
////    {
////        SpawnPlayer();
////    }
//
//
//    void Update()
//    {
//        if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
//        {
//            isRefreshingHostList = false;
//            hostList = MasterServer.PollHostList();
//        }
//    }
//
//
////    void OnConnectedToServer()
////    {
////        SpawnPlayer();
////    }
////
////    private void SpawnPlayer()
////    {
////        Network.Instantiate(playerPrefab, Vector3.up * 5, Quaternion.identity, 0);
////    }
//	
//	private void OnPlayerDisconnected (NetworkPlayer player)
//	{
//		Debug.Log("Server destroying player");
//		Network.RemoveRPCs(player, 0);
//		Network.DestroyPlayerObjects(player);
//	}
//
//}
//
