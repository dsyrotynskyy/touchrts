using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMapScenario
{
	public interface MapScenario
	{
		void Init (WorldController worldController);
		void OnStart (WorldController worldController);
		void OnUpdate();
		bool HasWin ();
		bool HasFail ();
		void Destroy ();
		
		string LevelName {
			get;
		}
		
		string NextLevelName {
			get;
		}
		Dictionary<int, PlayerInfo> PlayersInfo {
			get;
		}
		
//		bool IsNetworkGame {
//			get;
//		}
//		
//		NetworkParams NetworkParams {
//			get;
//		}
	}
	
	[System.Serializable]
	public struct PlayerInfo {
		public static PlayerInfo Nill = new PlayerInfo (-1, false, -1, false, 1000);
		
		public PlayerInfo (int id, bool isHuman, int teamNumber, bool isAi, int money, string name = "") {
			this.id = id;
			this.isDefinedAsHuman = isHuman;
			this.teamNumber = teamNumber;
			this.isDefinedAsAi = isAi;
			this.name = name;
			
			this.money = money;
		}
		
		public void Set (int id, bool isHuman, int teamNumber, bool isAi, int money, string name = "") {
			this.id = id;
			this.isDefinedAsHuman = isHuman;
			this.teamNumber = teamNumber;
			this.isDefinedAsAi = isAi;
			this.name = name;
			
			this.money = money;
		}
		
		public void Set (int id, bool isHuman, int teamNumber, bool isAi, string name = "") {
			this.id = id;
			this.isDefinedAsHuman = isHuman;
			this.teamNumber = teamNumber;
			this.isDefinedAsAi = isAi;
			this.name = name;
		}
		
		public int id;
		public bool isDefinedAsHuman;
		public string name;
		public int teamNumber;
		public bool isDefinedAsAi;
		public int money;
		
		public bool IsNill () {
			return this.id == -1;
		}
	}
	
	public class NetworkParams {
		private bool isServer;
		private HostData serverHostData;
		
		public void SetServer (bool server) {
			this.isServer = server;
		}

		public bool IsServer {
			get {
				return this.isServer;
			}
			set {
				isServer = value;
			}
		}		
		public bool IsClient {
			get {
				return serverHostData != null;
			}
		}

		public HostData ServerHostData {
			get {
				return this.serverHostData;
			}
			set {
				serverHostData = value;
			}
		}
	}
}

