using System;
using GameMapScenario;
using UnityEngine;
using Entity;

public class Tutorial1Scenario : ScriptedMapScenario
{
	private ScenarioAction startupAction = new ScenarioAction ();
	private ScenarioAction moveCameraTutorial = new ScenarioAction ();
	private ScenarioAction moveToPointAction = new ScenarioAction ();
	private ScenarioAction goFightEnemyAction = new ScenarioAction ();
	private ScenarioAction spawnSpellAction = new ScenarioAction ();
	private ScenarioAction enemyHeroFightAction = new ScenarioAction ();
	private ScenarioAction goToNecropolis = new ScenarioAction ();
	private ScenarioAction enemyCitadelCaptureAction = new ScenarioAction ();
	
	public HeroSubject ourHero;
	public GameObject movementFlag;
	public HeroSubject enemyHero;
	public BuildingSubject enemyCitadelToCapture;
	
	protected override void OnCreate  (WorldController worldController) {
		this.scenarioActions.AddLast (startupAction);
		this.scenarioActions.AddLast (moveCameraTutorial);
		this.scenarioActions.AddLast (moveToPointAction);
		this.scenarioActions.AddLast (goFightEnemyAction);
		this.scenarioActions.AddLast (spawnSpellAction);
		this.scenarioActions.AddLast (enemyHeroFightAction);
		this.scenarioActions.AddLast (goToNecropolis);
		this.scenarioActions.AddLast (enemyCitadelCaptureAction);
		
		this.SetHumanPlayer (2 ,2);
//		this.SetAiPlayer (1, 1);
	}
	
	public override void OnStart (WorldController worldController)
	{		
		this.scenarioData.SetWinningConditions (new FalseCondition ());
		this.scenarioData.SetFailConditions (new FalseCondition ());
		
		this.startupAction.AddOnActivationAction (delegate() {
			movementFlag.SetActive (false);
			
			ScenarioAction.SetUnitGroupImmortal (ourHero, true);
			ScenarioAction.SetUnitGroupImmortal (enemyHero, true);
			
			ScenarioAction.SetSpellPanelEnabled (false);
			ScenarioAction.SetRTSInputEnabled (false);
			ScenarioAction.DialogDisplay ("Welcome to TouchRTS!\n Tap dialog to start game!");
		});	
		this.startupAction.SetPerformanseConditions (new TrueCondition (300));			
		this.startupAction.AddScenarioActionToActivate (this.moveCameraTutorial);	
		
		this.InitMoveCameraAction ();
		this.InitMoveToPointAction ();
		this.InitGoFightEnemyAction ();
		this.InitSpawnSpellAction ();
		this.InitHeroFightAction ();
		this.InitEnemyCitadelCaptureAction ();
		
		this.SetGameWinAction(delegate() {
			ScenarioAction.DialogDisplay ("You win!");
		});	
	}
	
	private void InitMoveCameraAction () {
		this.moveCameraTutorial.AddOnActivationAction (delegate() {
			ScenarioAction.DialogDisplay ("Drag screen to move camera");
		});	
		this.moveCameraTutorial.SetPerformanseConditions (new CameraPositionChanged (100, 500));
		this.moveCameraTutorial.AddScenarioActionToActivate (moveToPointAction);
	}
	
	private void InitMoveToPointAction () {
		this.moveToPointAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.SetRTSInputEnabled (true);
			movementFlag.SetActive (true);
			ScenarioAction.MoveCameraTo (movementFlag, false);
			ScenarioAction.DialogDisplay ("Tap near the flag to move!");
		});	
		this.moveToPointAction.SetPerformanseConditions (new HeroReachedPosition (ourHero, movementFlag, 1000));		
		this.moveToPointAction.AddOnPerformedAction (delegate() {
			movementFlag.SetActive (false);
			ScenarioAction.DialogDisplay ("Point Reached!");
		});
		this.moveToPointAction.AddScenarioActionToActivate (this.goFightEnemyAction);
	}

	protected void InitGoFightEnemyAction ()
	{
		this.goFightEnemyAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.MoveCameraTo (enemyHero.transform.position, false);
			ScenarioAction.DialogDisplay ("Defeat skeleton king!");
		});	
		this.goFightEnemyAction.SetPerformanseConditions (new HeroReachedPosition (ourHero, enemyHero, 1000));
		this.goFightEnemyAction.AddScenarioActionToActivate (spawnSpellAction);
	}

	protected void InitSpawnSpellAction ()
	{	
		this.spawnSpellAction.AddOnActivationAction ( delegate()
		{
			ScenarioAction.DialogDisplay ("Select spell\n And drag on enemies!");
			ScenarioAction.SetSpellPanelEnabled (true);
		});	
		this.spawnSpellAction.SetPerformanseConditions (new SpellSpawnedCondition (ourHero, 1000));		
		this.spawnSpellAction.AddScenarioActionToActivate (enemyHeroFightAction);
	}

	protected void InitHeroFightAction ()
	{
		this.enemyHeroFightAction.AddOnActivationAction ( delegate()
		{
			ScenarioAction.DialogDisplay ("Great!\nIf you want to deselect - just tap selected spell icon");
			
			ScenarioAction.SetUnitGroupImmortal (ourHero, false);
			ScenarioAction.SetUnitGroupImmortal (enemyHero, false);
		});	
		this.enemyHeroFightAction.SetPerformanseConditions (new AllUnitsDead (enemyHero.HeroesGroup, 500));		
		this.enemyHeroFightAction.AddOnPerformedAction (delegate() {
			ScenarioAction.DialogDisplay ("Undedead returned to Hades!");
		});
		this.enemyHeroFightAction.AddScenarioActionToActivate (goToNecropolis);
		this.enemyHeroFightAction.AddScenarioActionToActivate (enemyCitadelCaptureAction);		
	}

	protected void InitEnemyCitadelCaptureAction ()
	{
		this.goToNecropolis.AddOnActivationAction ( delegate()
		{
			ScenarioAction.MoveCameraTo (enemyCitadelToCapture.transform.position, false);
			ScenarioAction.DialogDisplay ("It is time to capture Necropolis!");
		});	
		
		goToNecropolis.SetPerformanseConditions (new IsFighting (ourHero.HeroesGroup));
		goToNecropolis.AddOnPerformedAction (delegate() {
			ScenarioAction.DialogDisplay ("Oh Shit! They have towers!");
		});
		
		this.enemyCitadelCaptureAction.SetPerformanseConditions (new BuildingCapturedByPlayer (enemyCitadelToCapture, 2));
		this.enemyCitadelCaptureAction.AddOnPerformedAction (delegate() {
			ScenarioAction.DialogDisplay ("Great! Mission has finished succesfully!");
			this.scenarioData.SetWinningConditions (new TrueCondition ());
		});
	}
}


