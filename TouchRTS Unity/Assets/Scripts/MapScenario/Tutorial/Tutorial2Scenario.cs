using System;
using GameMapScenario;
using UnityEngine;
using Entity;

public class Tutorial2Scenario : ScriptedMapScenario
{
	private ScenarioAction startupAction = new ScenarioAction ();
	private ScenarioAction tapConstructionButtonCommand = new ScenarioAction ();
	private ScenarioAction constructBuildingAction = new ScenarioAction ();
	private ScenarioAction closeDialogAction = new ScenarioAction ();	
	private ScenarioAction tapTrainUnitButtonCommand = new ScenarioAction ();
	private ScenarioAction trainUnitAction = new ScenarioAction ();
	private ScenarioAction tapStudySpellButtonCommand = new ScenarioAction ();
	private ScenarioAction trainSpellAction = new ScenarioAction ();
	private ScenarioAction saveFriendsAction = new ScenarioAction ();
	private ScenarioAction friendsSavedAction = new ScenarioAction ();
	
	public HeroSubject ourHero;
	public BuildingSubject ourCapital;
	public BuildingSubject friendCitadel;
	
	protected override void OnCreate  (WorldController worldController) {
		this.scenarioActions.AddLast (startupAction);
		this.scenarioActions.AddLast (tapConstructionButtonCommand);
		this.scenarioActions.AddLast (constructBuildingAction);
		this.scenarioActions.AddLast (closeDialogAction);
		this.scenarioActions.AddLast (tapTrainUnitButtonCommand);
		this.scenarioActions.AddLast (trainSpellAction);
		this.scenarioActions.AddLast (saveFriendsAction);
		this.scenarioActions.AddLast (friendsSavedAction);
		
		this.SetHumanPlayer (2, 2);
		this.SetPlayerInfo (3, false, 2, false);
		this.SetPlayerInfo (1, false, 1, false);
	}
	
	public override void OnStart (WorldController worldController)
	{		
		this.scenarioData.SetWinningConditions (new BuildingCapturedByPlayer (friendCitadel, 2));
		this.scenarioData.SetFailConditions (new FalseCondition ());
		
		this.startupAction.AddOnActivationAction (delegate() {
			ScenarioAction.SetRTSInputEnabled (false);
			ScenarioAction.DialogDisplay ("Welcome back, Hero!");
			
			ScenarioAction.SetCastleControlPanelEnabled (true, false, false, false);
		});	
		this.startupAction.SetPerformanseConditions (new TrueCondition (500));	
		
		this.startupAction.AddScenarioActionToActivate (tapConstructionButtonCommand);
		this.InitTapConstructionButtonCommand ();
		this.InitConstructBuildingDialog ();
		this.InitCloseDialogCommand ();
		this.InitTapTrainUnitButtonCommand ();
		this.InitSpellTrainAction();
		this.InitSaveFriendsAction ();
		
		this.SetGameWinAction(delegate() {
			ScenarioAction.DialogDisplay ("You win!");
		});	
	}
	
	private void InitTapConstructionButtonCommand () {
		this.tapConstructionButtonCommand.AddOnActivationAction (delegate() {
			ScenarioAction.MoveCameraTo (ourCapital.transform.position, false);
			ScenarioAction.DialogDisplay ("Lets build base!");		
		});	
		this.tapConstructionButtonCommand.SetPerformanseConditions (new IsUnitTrainDialogDisplaying ());
		this.tapConstructionButtonCommand.AddScenarioActionToActivate (constructBuildingAction);	
	}
	
	private void InitConstructBuildingDialog () {
		this.constructBuildingAction.AddOnActivationAction (delegate() {
			ScenarioAction.DialogDisplay ("Construct building!");
		});	
		this.constructBuildingAction.SetPerformanseConditions (new HasConstructedBuildings (ourCapital));
		this.constructBuildingAction.AddScenarioActionToActivate (closeDialogAction);
	}
	
	private void InitCloseDialogCommand () {
		this.closeDialogAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.DialogDisplay ("Close dialog");
		});	
		this.closeDialogAction.SetPerformanseConditions (new NoCommonCreationWindowDisplaying ());		
		this.closeDialogAction.AddScenarioActionToActivate (tapTrainUnitButtonCommand);

	}
	
	private void InitTapTrainUnitButtonCommand () {
		this.tapTrainUnitButtonCommand.AddOnActivationAction (delegate()
		{
			ScenarioAction.DialogDisplay ("Now train some warriors!");
			ScenarioAction.SetCastleControlPanelEnabled (false, true, false, false);
		});	
		this.tapTrainUnitButtonCommand.SetPerformanseConditions (new AreAnyUnitsTrained (ourHero.HeroesGroup, 2, 1000));		

		this.tapTrainUnitButtonCommand.AddScenarioActionToActivate (this.trainSpellAction);
	}
	
	protected void InitSpellTrainAction ()
	{
		this.trainSpellAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.DialogDisplay ("Study spell");
			ScenarioAction.SetCastleControlPanelEnabled (false, false, true, false);
		});	
		this.trainSpellAction.SetPerformanseConditions (new AreAnySpellsTrained (ourHero, 1, 1000));		
		this.trainSpellAction.AddScenarioActionToActivate (this.saveFriendsAction);
	}
	
	protected void InitSaveFriendsAction ()
	{
		this.saveFriendsAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.DialogDisplay ("We have to save our alies!");
			ScenarioAction.SetCastleControlPanelEnabled (true, true, true, true);
			ScenarioAction.SetRTSInputEnabled (true);
			ScenarioAction.MoveCameraTo (friendCitadel.transform.position, false);			
		});	
		this.saveFriendsAction.SetPerformanseConditions (new HeroReachedPosition (ourHero, friendCitadel.transform.position, 1000));		
		this.saveFriendsAction.AddScenarioActionToActivate (this.friendsSavedAction);

		this.friendsSavedAction.AddOnActivationAction (delegate()
		{
			ScenarioAction.DialogDisplay ("Hurray, we did it!");
			ScenarioAction.ChangePlayerOwner (friendCitadel.BuildingUnitGroup, 2);
		});	
		this.friendsSavedAction.SetPerformanseConditions (new TrueCondition ());		
	}

//	protected void InitHeroFightAction ()
//	{
//		this.enemyHeroFightAction.AddOnActivationAction ( delegate()
//		{
//			ScenarioAction.DialogDisplay ("Great!\nIf you want to deselect - just tap selected spell icon");
//			
//			ScenarioAction.SetUnitGroupImmortal (ourHero, false);
//			ScenarioAction.SetUnitGroupImmortal (enemyHero, false);
//		});	
//		this.enemyHeroFightAction.SetPerformanseConditions (new AllUnitsDead (enemyHero.HeroesGroup, 500));		
//		this.enemyHeroFightAction.AddOnPerformedAction (delegate() {
//			ScenarioAction.DialogDisplay ("Undedead returned to Hades!");
//		});
//		this.enemyHeroFightAction.AddScenarioActionToActivate (goToNecropolis);
//		this.enemyHeroFightAction.AddScenarioActionToActivate (enemyCitadelCaptureAction);		
//	}
//
//	protected void InitEnemyCitadelCaptureAction ()
//	{
//		this.goToNecropolis.AddOnActivationAction ( delegate()
//		{
//			ScenarioAction.MoveCameraTo (enemyCitadelToCapture.transform.position, false);
//			ScenarioAction.DialogDisplay ("It is time to capture Necropolis!");
//		});	
//		
//		goToNecropolis.SetPerformanseConditions (new IsFighting (ourHero.HeroesGroup));
//		goToNecropolis.AddOnPerformedAction (delegate() {
//			ScenarioAction.DialogDisplay ("Oh Shit! They have towers!");
//		});
//		
//		this.enemyCitadelCaptureAction.SetPerformanseConditions (new BuildingCapturedByPlayer (enemyCitadelToCapture, 2));
//		this.enemyCitadelCaptureAction.AddOnPerformedAction (delegate() {
//			ScenarioAction.DialogDisplay ("Great! Mission has finished succesfully!");
//			this.scenarioData.SetWinningConditions (new TrueCondition ());
//		});
//	}


}


