using System;
using Entity;
using UnityEngine;

namespace GameMapScenario
{
	public class ScenarioAction
	{
		private bool isActivated = true;
		private Timer performsTimer = Timer.CreateFrequency (500);
		private bool wasPerformed;
		private ScenarioCondition[] performanseConditions;
		
		private OnActionActivated onActionActivated;
		private OnActionPerformed onActionPerformed;
		
		private string name;
		
		public ScenarioAction (string name = null) {
			this.name = name;
		}
		
		public void OnUpdate () {
			if (wasPerformed) {
				return;
			}
			if (isActivated) {
				if (this.onActionActivated != null && !InputController.UI.MessageDialog.IsDisplaying ()) {
					foreach (ScenarioCondition condition in performanseConditions) {
						condition.Prepare ();
					}
					this.onActionActivated ();
					this.onActionActivated = null; // to activate it only 1 time
				}
				this.wasPerformed = TryPerform ();
			}
		}

		private bool TryPerform ()
		{
			if (!performsTimer.EnoughTimeLeft ()) {
				return false;
			}
			foreach (ScenarioCondition condition in performanseConditions) {
				if (!condition.IsTrue) {
					return false;
				}
			}
			if (this.onActionPerformed != null)	{
				this.onActionPerformed ();
			}
			return true;
		}
		
//		public void SetStarterAction () {
//			this.Activate ();
//		}
		
		protected void Activate () {
			this.isActivated = true;		
		}
		
		public void SetPerformanseConditions(params ScenarioCondition[] conditions) {
			this.performanseConditions = conditions;
		}
		
		public void AddOnActivationAction (OnActionActivated onActionActivated) {
			if (this.onActionActivated == null) {
				this.onActionActivated = onActionActivated;
			} else {
				this.onActionActivated += onActionActivated;
			}
		}
		
		public void AddOnPerformedAction (OnActionPerformed onActionPerformed) {
			if (this.onActionPerformed == null) {
				this.onActionPerformed = onActionPerformed;
			} else {
				this.onActionPerformed += onActionPerformed;
			}
		}
		
		public void AddScenarioActionToActivate (ScenarioAction otherScenarioAction) {
			otherScenarioAction.isActivated = false;
			OnActionPerformed logic = delegate()
			{
				otherScenarioAction.Activate ();
			};
			AddOnPerformedAction (logic);
		}
		
		public void AddScenarioActionsToActivate (params ScenarioAction[] otherScenarioActions) {
			foreach (ScenarioAction action in otherScenarioActions) {
				AddScenarioActionToActivate (action);
			}
		}
		
		public delegate void OnActionActivated ();
		public delegate void OnActionPerformed ();
		
		public static void DialogDisplay (string message) {
			InputController.UI.MessageDialog.Display (message, "");
		}
		
		public static void DialogDisplay (string message, string id) {
			InputController.UI.MessageDialog.Display (message, id);
		}
		
		public static void ChangePlayerOwner (UnitGroupSubject unitGroup, PlayerData playerOwner) {
			unitGroup.SetNewOwner (playerOwner);
		}
		
		public static void ChangePlayerOwner (UnitGroupSubject unitGroup, int id) {
			PlayerData playerOwner = WorldController.Instance.AllPlayers[id];
			unitGroup.SetNewOwner (playerOwner);
		}
		
		public static void MoveCameraTo (GameObject position, bool fast) {
			MoveCameraTo (position.transform.position, false);			
		}
		
		public static void MoveCameraTo (MonoBehaviour position, bool fast) {
			MoveCameraTo (position.transform.position, false);					
		}
		
		public static void MoveCameraTo (Vector3 position, bool fast) {
			if (fast) {
				InputController.RTSCamera.FollowThePointFast (position);
			} else {
				InputController.RTSCamera.FollowThePointSlow (position);
			}			
		}
		
		public static void SetPlayerAIActive (PlayerData player, bool active) {
			player.SetAIActive (active);
		}
		
		public static void SendUnitGroupTo (UnitGroupSubject unitGroup, Vector3 movementCoord) {
			CommandController.Instance.MoveGroupToPoint (unitGroup, movementCoord);
		}
		
//		public static void MoveCameraTo (Vector3 position) {
//			
//		}
		
		public static void SetAIActive (PlayerData player, bool active) {
			player.SetAIActive (active);
		}
		
		public static void SetUnitGroupImmortal (UnitGroupSubject unitGroup, bool immortal) {
			unitGroup.SetImmortal (immortal);
		}
		
		public static void SetUnitGroupImmortal (HeroSubject hero, bool immortal) {
			hero.HeroesGroup.SetImmortal (immortal);
		}
		
		public static void SetSpellPanelEnabled (bool enabled) {
			InputController.UI.SpellPanel.SetEnabled (enabled);
		}
		
		public static void SetRTSInputEnabled (bool enabled) {
			InputController.RTSCamera.SetInputEnabled (enabled);
		}
		
		public static void SetCastleControlPanelEnabled (
				bool ConstructionButtonEnabled,
				bool UnitTrainButtonEnabled,
				bool SpellStudyButtonEnabled,
	 			bool HeroResurrectionButtonEnabled) {
			NCastleControlPanel.ConstructionButtonEnabled = ConstructionButtonEnabled;
			NCastleControlPanel.UnitTrainButtonEnabled = UnitTrainButtonEnabled;
			NCastleControlPanel.SpellStudyButtonEnabled = SpellStudyButtonEnabled;
			NCastleControlPanel.HeroResurrectionButtonEnabled = HeroResurrectionButtonEnabled;
		}
		
		public static void SetOutbuildingControlPanelEnabled (
				bool enabled) {
			NOutbuildingControlPanel.UpgradeButtonEnabled = enabled;
		}
	}
}

