using System;

namespace GameMapScenario
{
	public class TrueCondition : PerformedScenarioCondition
	{
		public TrueCondition (int time = 0) : base (time) {
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return true;
		}
		#endregion
	}
	
	public class FalseCondition : PerformedScenarioCondition
	{
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return false;
		}
		#endregion
	}
	
	public class MessageDialogIsNotUsed : PerformedScenarioCondition {
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return !InputController.UI.MessageDialog.IsDisplaying ();
		}
		#endregion
	}
	
	public class ScenarioFailCondition : PerformedScenarioCondition {
		
		private MapScenario mapScenario;
		
		public ScenarioFailCondition (MapScenario mapScenario , int time = 0) : base (time) {
			this.mapScenario = mapScenario;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return mapScenario.HasFail ();
		}
		#endregion
	}
	
	public class ScenarioWinCondition : PerformedScenarioCondition {
		
		private MapScenario mapScenario;
		
		public ScenarioWinCondition (MapScenario mapScenario , int time = 0) : base (time) {
			this.mapScenario = mapScenario;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return mapScenario.HasWin ();
		}
		#endregion
	}
	
//	public class TimeLeftCondition : PerformedScenarioCondition
//	{
//		private Timer timer;
//		
//		public TimeLeftCondition (int time)
//		{
//			this.timer = Timer.CreateLifePeriod (time);
//		}
//
//		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
//		protected override bool Performs ()
//		{
//			return !this.timer.IsActive;
//		}
//		#endregion
//
//		public Timer Timer {
//			get {
//				return this.timer;
//			}
//			set {
//				timer = value;
//			}
//		}
//	}
}

