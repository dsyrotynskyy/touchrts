using System;

namespace GameMapScenario
{
	public abstract class PerformedScenarioCondition : ScenarioCondition
	{
		private Timer timer;
		
		public PerformedScenarioCondition () {
		}
		
		public PerformedScenarioCondition (int time) {
			if (time > 0) {
				timer = Timer.CreateLifePeriod (time, false);
			}
		}

		private bool wasPerformedCondition = false;
		private bool performingLogicWasActual = false;

		#region ScenarioCondition implementation
		public bool IsTrue {
			get {
				if (wasPerformedCondition) {
					return true;
				}
				wasPerformedCondition = HavePerformed ();
				if (wasPerformedCondition) {
					Clear ();
				}
				return wasPerformedCondition;
			}
		}
		
		protected bool HavePerformed () {
			if (timer != null) {
				if (performingLogicWasActual) {
					return !timer.IsActive;
				} else if (Performs ()) {
					performingLogicWasActual = true;
					timer.Restart ();				
				}
				return false;	
			} else {
				return Performs ();
			}
		}
		
		protected abstract bool Performs ();
		
		protected virtual void Clear () {
		}

		public virtual void Init (WorldController worldController) {
		}

		public virtual void Prepare ()
		{
		}
		#endregion
	}
}

