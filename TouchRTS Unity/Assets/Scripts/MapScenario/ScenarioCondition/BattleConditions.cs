using System;
using Entity;
using UnityEngine;

namespace GameMapScenario
{
	public class HeroReachedPosition : PerformedScenarioCondition
	{
		private HeroSubject hero;
		private Vector3 position;
		
		public HeroReachedPosition (HeroSubject hero, GameObject position, int time = 0) : base (time)
		{
			this.hero = hero;
			this.position = position.transform.position;
		}
		
		public HeroReachedPosition (HeroSubject hero, Vector3 position, int time = 0) : base (time)
		{
			this.hero = hero;
			this.position = position;
		}
		
		public HeroReachedPosition (HeroSubject hero, MonoBehaviour position, int time = 0) : base (time)
		{		
			this.hero = hero;
			this.position = position.transform.position;
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return Vector3.Distance (hero.transform.position, position) < 25f;
		}
		#endregion
	}
	
	public class BuildingCapturedByPlayer : PerformedScenarioCondition
	{
		private BuildingSubject target;
//		private PlayerData player;
		
		public BuildingCapturedByPlayer (BuildingSubject building, int id)
		{
			this.target = building;
//			this.player = WorldController.Instance.AllPlayers[id];
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return target.PlayerOwner == WorldController.Instance.HumanPlayer;
		}
		#endregion

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override void Clear ()
		{
			target = null;
//			player = null;
		}
		#endregion
	}
	
	public class UnitWasKilled : PerformedScenarioCondition
	{
		private UnitSubject unitTarget;
		
		public UnitWasKilled (UnitSubject unitTarget) {
			this.unitTarget = unitTarget;
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return unitTarget.IsDead;
		}

		#endregion

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override void Clear ()
		{
			unitTarget = null;
		}
		#endregion
	}
	
	public class AllUnitsDead : PerformedScenarioCondition {
		
		private UnitGroupSubject victim;
	
		public AllUnitsDead (UnitGroupSubject victim, int time = 0) : base (time) {
			this.victim = victim;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return victim.AliveUnitsCount == 0;
		}
		#endregion
	}
	
	public class HasConstructedBuildings : PerformedScenarioCondition {
		private BuildingSubject castle;
		
		public HasConstructedBuildings (BuildingSubject castle, int time = 0) : base (time) {
			this.castle = castle;
		}
		
		private int prevUnitNumber = 0;
		
		public override void Prepare () {
			this.prevUnitNumber = 0;
			foreach (BuildingType type in castle.CastleConstructor.OutbuildingsTypes.Keys) {
				if (castle.CastleConstructor.OutbuildingsTypes[type]) {
					this.prevUnitNumber++;
				}
			}
		}
		
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			int total = 0;
			foreach (BuildingType type in castle.CastleConstructor.OutbuildingsTypes.Keys) {
				if (castle.CastleConstructor.OutbuildingsTypes[type]) {
					total++;
				}
			}
			return total > prevUnitNumber;
		}
		#endregion
	}
	
	public class IsBuildingConstructed : PerformedScenarioCondition {
		private BuildingSubject castle;
		private BuildingType typeToConstruct;
		
		public IsBuildingConstructed (BuildingSubject castle, BuildingType typeToConstruct) {
			this.castle = castle;
			this.typeToConstruct = typeToConstruct;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			foreach (BuildingType type in castle.CastleConstructor.OutbuildingsTypes.Keys) {
				if (type != typeToConstruct) {
					continue;
				}
				if (castle.CastleConstructor.OutbuildingsTypes[type]) {
					return true;
				}
			}
			return false;
		}
		#endregion
	}
	
	public class IsSpellTeached : PerformedScenarioCondition {
		private HeroSubject hero;
		private SpellType spellType;
		
		public IsSpellTeached (HeroSubject hero, SpellType spellType) {
			this.hero = hero;
			this.spellType = spellType;
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return hero.Spells.Contains (spellType);
		}
		#endregion
	}
	
		
	public class AreAnySpellsTrained : PerformedScenarioCondition {
		private HeroSubject hero;
		private SpellType spellType;
		int num;
		
		public AreAnySpellsTrained (HeroSubject hero, int num, int time) : base (time) {
			this.hero = hero;
			this.num = num;
		}
		
		private int prevSpellNum = 1;
		
		public override void Prepare () {
			this.prevSpellNum = hero.Spells.Count;
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return this.prevSpellNum + num <= hero.Spells.Count;
		}
		#endregion
	}
	
	public class AreAnyUnitsTrained : PerformedScenarioCondition {
		private UnitGroupSubject unitGroup;
		private UnitType unitType;
		private int numberOfUnitsToTrain = 0;
		
		public AreAnyUnitsTrained (UnitGroupSubject unitGroup, int numberOfUnitsToTrain, int time = 0) : base (time) {
			this.unitGroup = unitGroup;
			this.numberOfUnitsToTrain = numberOfUnitsToTrain;
		}
		
		private int prevUnitNumber = 1;
		
		public override void Prepare () {
			this.prevUnitNumber = unitGroup.AliveUnitsCount;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return unitGroup.AliveUnitsCount - this.prevUnitNumber > numberOfUnitsToTrain;
		}
		#endregion
	}
	
	public class IsPeace : PerformedScenarioCondition {
		
		private UnitGroupSubject unitGroup;
		
		public IsPeace (UnitGroupSubject unitGroup) {
			this.unitGroup = unitGroup;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return !this.unitGroup.AreEnemiesAround ();
		}
		#endregion
	}
	
	
	public class IsFighting : PerformedScenarioCondition {
		
		private UnitGroupSubject unitGroup;
		
		public IsFighting (UnitGroupSubject unitGroup, int time = 0) : base (time) {
			this.unitGroup = unitGroup;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return this.unitGroup.AreEnemiesAround ();
		}
		#endregion
	}
	public class CameraPositionChanged : PerformedScenarioCondition {
		private float distance;
		private Vector3 prevPosition;
		private float totalDistance = 0;
		
		public CameraPositionChanged (float distance, int time = 0) : base (time) {
			this.distance = distance;
		}
		
		public override void Prepare () {
			prevPosition = InputController.RTSCamera.gameObject.transform.position;
		}	

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			totalDistance += Vector3.Distance (prevPosition, InputController.RTSCamera.gameObject.transform.position);
			if (totalDistance > distance) {
				return true;
			}
			prevPosition = InputController.RTSCamera.gameObject.transform.position;
			return false;
		}
		#endregion
	}
	
	public class SpellSpawnedCondition : PerformedScenarioCondition {
		
//		private HeroSubject hero;
		private UnitGroupSubject heroGroupSubject;
		
		public SpellSpawnedCondition (HeroSubject hero, int time = 0) : base (time) {
//			this.hero = hero;
			this.heroGroupSubject = hero.HeroesGroup;
		}
		
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			foreach (UnitSubject unit in heroGroupSubject.ClosestEnemies) {
				foreach (EffectData effect in unit.EffectsOnUnit) {
					if (effect.EffectPerformer.IsSpell) {
						return true;
					}
				}
			}
			return false;
		}
		#endregion
	}
	

	
	
}

