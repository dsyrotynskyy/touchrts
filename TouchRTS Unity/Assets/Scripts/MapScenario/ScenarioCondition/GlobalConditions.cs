using System;
using Entity;

namespace GameMapScenario
{
	public class EnemyPlayersDestroyed : PerformedScenarioCondition
	{
		
		public EnemyPlayersDestroyed () {
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			foreach (PlayerData player in WorldController.Instance.AllPlayers.Values) {
				if (!player.IsDefeated && player.IsEnemyPlayer (WorldController.Instance.HumanPlayer)) {
					return false;
				}
			}
			return true;
		}
		#endregion

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override void Clear ()
		{
		}
		#endregion
	}
	
	public class PlayerLostCapital : PerformedScenarioCondition
	{
		private PlayerData player;
		
		public PlayerLostCapital (int id)
		{
			this.player = WorldController.Instance.AllPlayers[id];
		}

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return player.HasNoCapital;
		}

		public override void Init (WorldController worldController)
		{
			foreach (PlayerData player in worldController.AllPlayers.Values) {
				if (player == this.player) {
					this.player = player;
					return;
				}
			}
		}
		#endregion

		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override void Clear ()
		{
			player = null;
		}
		#endregion
	}
	
	public class HumanPlayerLostCapital : PerformedScenarioCondition {
		protected override bool Performs ()
		{
			return WorldController.Instance.HumanPlayer.HasNoCapital;
		}
	}
}

