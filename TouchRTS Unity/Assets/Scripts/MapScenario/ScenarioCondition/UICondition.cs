using System;
using UI;

namespace GameMapScenario
{
	public class NoCommonCreationWindowDisplaying : PerformedScenarioCondition {
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			UIController ui = InputController.UI;
			return !ui.SpellStudyWindow.IsDisplaying () && !ui.ConstructionOutbuildingWindow.IsDisplaying () && !ui.SpawnUnitWindow.IsDisplaying ();
		}
		#endregion
	}
	
	public class IsUnitTrainDialogDisplaying : PerformedScenarioCondition
	{
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return InputController.UI.SpawnUnitWindow.IsDisplaying ();
		}
		#endregion
	}
	
	public class IsCastleConstructDialogDisplaying : PerformedScenarioCondition
	{
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return InputController.UI.ConstructionOutbuildingWindow.IsDisplaying ();
		}
		#endregion
	}
	
	public class IsSpellSpawnDialogDisplaying : PerformedScenarioCondition
	{
		#region implemented abstract members of GameMapScenario.PerformedScenarioCondition
		protected override bool Performs ()
		{
			return InputController.UI.SpellStudyWindow.IsDisplaying ();
		}
		#endregion
	}
}

