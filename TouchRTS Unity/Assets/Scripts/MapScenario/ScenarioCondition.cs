using System;

namespace GameMapScenario
{
	public interface ScenarioCondition
	{
		void Init (WorldController worldController);
		
		void Prepare ();
		
		bool IsTrue {
			get;
		}
	}
}

