using System;
using Entity;
using Setup;

namespace GameMapScenario
{
	public class SingleMapScenario : CommonMapScenario
	{
		public static string AI_CONTROLLER = "AI";
		public static string OPEN_CONTROLLER = "OPEN";
		public static string CLOSED_CONTROLLER = "CLOSE";
		public static string USED_CONTROLLER = "USED";
		
		public SingleMapScenario (string levelName = "Default") 
		{
			this.levelName = levelName;
			this.nextLevelName = GameStarter.MENU_LEVEL_NAME;
			this.SetWinningConditions (new EnemyPlayersDestroyed ());
			this.SetFailConditions (new HumanPlayerLostCapital ());
		}
		
		public void CreateRandomPlayersData () {
			int numOfPlayers = ApplicationManager.Instance.battleMaps[this.LevelName].NumOfPlayers;
			
			this.SetNetworkAiPlayer (1, 1, 3000);
			this.SetNetworkHumanPlayer (2, 2, 3000, ApplicationManager.Instance.DeviceName);
			
			for (int i = 3; i < numOfPlayers; i++) {
				this.SetNetworkHumanPlayer (i, i, CLOSED_CONTROLLER);
			}
		}
		
		#region MapScenario implementation
		public override void OnUpdate ()
		{
			foreach(PlayerData player in WorldController.Instance.AllPlayers.Values) {
				if (player.IsHuman ()) {
					continue;
				}
				
				if (player.HasNoCapital) {
					player.IsDefeated = true;
				}
			}
		}
		#endregion
	}
}

