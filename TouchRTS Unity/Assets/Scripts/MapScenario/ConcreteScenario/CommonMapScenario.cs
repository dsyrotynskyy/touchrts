using System;
using UnityEngine;
using Entity;
using System.Collections.Generic;
using Setup;

namespace GameMapScenario
{
	public class CommonMapScenario : MapScenario
	{
		protected string levelName = "Default";
		protected string nextLevelName;
		private ScenarioCondition[] winningConditions;
		private ScenarioCondition[] failConditions;
		private readonly Dictionary<int, PlayerInfo> playersInfo = new Dictionary<int, GameMapScenario.PlayerInfo> ();
		
		public CommonMapScenario ()
		{
		}
		
		public void SetWinningConditions (params ScenarioCondition[] winningConditions)
		{
			this.winningConditions = winningConditions;
		}
		
		public void SetFailConditions (params ScenarioCondition[] failConditions)
		{
			this.failConditions = failConditions;
		}		
		
		#region MapScenario implementation
		public virtual void Init (WorldController worldController)
		{
			if (!CorrectScenario) {
				return;
			}
			
			foreach (ScenarioCondition condition in winningConditions) {
				condition.Init (worldController);
			}
			
			foreach (ScenarioCondition condition in failConditions) {
				condition.Init (worldController);
			}
		}

		public virtual bool HasWin ()
		{
			if (!CorrectScenario) {
				return false;
			}
			
			foreach (ScenarioCondition condition in winningConditions) {
				if (!condition.IsTrue) {
					return false;
				}
			}
			
			return true;
		}

		public virtual bool HasFail ()
		{
			if (!CorrectScenario) {
				return false;
			}
			
			foreach (ScenarioCondition condition in failConditions) {
				if (condition.IsTrue) {
					return true;
				}
			}
			
			return false;
		}
		#endregion
		
		private Timer exceptionTimer = Timer.CreateFrequency (1000);
		
		public bool CorrectScenario {
			get {
				bool correct = winningConditions != null && failConditions != null;
				if (!correct && exceptionTimer.EnoughTimeLeft ()) {
					MonoBehaviour.print ("Uncorrect game scenario!");
				}
				return correct;
			}
		}

		#region MapScenario implementation
		public virtual void OnUpdate ()
		{
		}
		#endregion

		public Dictionary<int, GameMapScenario.PlayerInfo> PlayersInfo {
			get {
				return this.playersInfo;
			}
		}
		
		public void SetPlayer (int id, int team, bool isAi, bool isHuman, string name = "") {
			PlayerInfo playerInfo;
			if (this.PlayersInfo.ContainsKey (id)) {
				PlayersInfo.Remove (id);			
			} 
			playerInfo = new PlayerInfo ();
			
			playerInfo.isDefinedAsHuman = isHuman;
			playerInfo.name = name;
			playerInfo.id = id;
			playerInfo.teamNumber = team;
			playerInfo.isDefinedAsAi = isAi;			
			
			this.PlayersInfo.Add (id, playerInfo);
		}
		
		public void SetPlayer (int id, int team, bool isAi, bool isHuman, int money, string name = "") {
			PlayerInfo playerInfo;
			if (this.PlayersInfo.ContainsKey (id)) {
				PlayersInfo.Remove (id);			
			} 
			playerInfo = new PlayerInfo ();
			
			playerInfo.isDefinedAsHuman = isHuman;
			playerInfo.name = name;
			playerInfo.id = id;
			playerInfo.teamNumber = team;
			playerInfo.isDefinedAsAi = isAi;	
			
			playerInfo.money = money;
			
			this.PlayersInfo.Add (id, playerInfo);
		}
		
		public void SetNetworkHumanPlayer (int id, int team, string name) {
			this.SetPlayer (id, team, false, false, name);
		}
		
		public void SetNetworkHumanPlayer (int id, int team, int money, string controllerName) {
			if (controllerName == ApplicationManager.Instance.DeviceName) {
				PlayerInfo victimInfo = new PlayerInfo ();
				foreach (PlayerInfo info in this.PlayersInfo.Values) {
					if (info.name == controllerName) {
						victimInfo = info;
						break;
					}
				}
				victimInfo.name = SingleMapScenario.AI_CONTROLLER;
				this.PlayersInfo[victimInfo.id] = victimInfo;
			}
			this.SetPlayer (id, team, false, false, money, controllerName);			
			
		}
		
		public void SetNetworkAiPlayer (int id, int team) {
			this.SetPlayer (id, team, false, false, SingleMapScenario.AI_CONTROLLER);
		}
		
		public void SetNetworkAiPlayer (int id, int team, int money) {
			this.SetPlayer (id, team, false, false, money, SingleMapScenario.AI_CONTROLLER);
		}
		
		public void SetCampaignAiPlayer (int id, int team) {		
			this.SetPlayer (id, team, true, false, SingleMapScenario.AI_CONTROLLER);
		}
		
		public void SetCampaignHumanPlayer (int id, int team) {
			this.SetPlayer (id, team, false, true, ApplicationManager.Instance.DeviceName);
		}

		#region MapScenario implementation
		public void Destroy ()
		{
			this.failConditions = null;
			this.winningConditions = null;
			this.playersInfo.Clear ();
		}
		#endregion

		#region MapScenario implementation
		public virtual string NextLevelName {
			get {
				return nextLevelName;
			}
		}
		#endregion

		#region MapScenario implementation
		public bool IsNetworkGame {
			get {
				return networkParams != null;
			}
		}
		#endregion
		
		private NetworkParams networkParams;

		#region MapScenario implementation
		public NetworkParams NetworkParams {
			get {
				return networkParams;
			} set {
				networkParams = value;
			}
		}
		
		public string LevelName {
			get {
				return this.levelName;
			}
			set {
				this.levelName = value;
			}
		}
		#endregion

		#region MapScenario implementation
		public void OnStart (WorldController worldController)
		{
		}
		#endregion
	}
}

