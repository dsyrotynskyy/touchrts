using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMapScenario
{
	public class PrimitiveTestScenario : MonoBehaviour, MapScenario
	{
		public PrimitiveTestScenario ()
		{
		}

		#region MapScenario implementation
		public void Init (WorldController worldController)
		{
		}

		public bool HasWin ()
		{
			return false;
		}

		public bool HasFail ()
		{
			return false;
		}
		#endregion

		#region MapScenario implementation
		public void OnUpdate ()
		{
		}
		#endregion

		#region MapScenario implementation
		public Dictionary<int, PlayerInfo> PlayersInfo {
			get {
				Dictionary<int, PlayerInfo> list = new Dictionary<int, PlayerInfo> ();
						
				PlayerInfo player1 = new PlayerInfo ();
				player1.id = 1;
				player1.teamNumber = 1;
				player1.isDefinedAsHuman = true;
				player1.isDefinedAsAi = false;
				list.Add (player1.id, player1);
		
				PlayerInfo player = new PlayerInfo ();
				player.id = 2;
				player.teamNumber = 2;
				player.isDefinedAsHuman = true;
				list.Add (player.id, player);
				
				return list;
			}
		}
		#endregion

		#region MapScenario implementation
		public void Destroy ()
		{
		}
		#endregion
		
		#region MapScenario implementation
		public string NextLevelName {
			get {
				return GameStarter.MENU_LEVEL_NAME;
			}
		}
		#endregion

		#region MapScenario implementation
		public bool IsNetworkGame {
			get {
				return false;
			}
		}
		#endregion
		
		
		#region MapScenario implementation
		public NetworkParams NetworkParams {
			get {
				return null;
			}
		}
		#endregion

		#region MapScenario implementation
		public string LevelName {
			get {
				throw new NotImplementedException ();
			}
		}
		#endregion

		#region MapScenario implementation
		public void OnStart (WorldController worldController)
		{
			throw new NotImplementedException ();
		}
		#endregion
	}
}

