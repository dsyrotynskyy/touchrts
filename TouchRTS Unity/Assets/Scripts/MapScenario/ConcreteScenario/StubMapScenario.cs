using UnityEngine;
using System.Collections;
using GameMapScenario;

public class StubMapScenario : ScriptedMapScenario
{	
	public StubMapScenario () {	
		NextScenarioName = GameStarter.MENU_LEVEL_NAME;
	}
	
	// Use this for initialization
	protected override void OnCreate (WorldController worldController)
	{
		scenarioData.LevelName = "Test Level";
		
		this.SetHumanPlayer (1, 1);
		this.SetAiPlayer (2, 2);
		
		scenarioData.SetWinningConditions (new FalseCondition ());
		scenarioData.SetFailConditions (new FalseCondition ());
	}
}

