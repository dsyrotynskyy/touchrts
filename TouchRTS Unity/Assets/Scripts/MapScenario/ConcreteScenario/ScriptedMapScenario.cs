using UnityEngine;
using System.Collections;
using GameMapScenario;
using System.Collections.Generic;

public abstract class ScriptedMapScenario : MonoBehaviour, MapScenario
{
	public string NextScenarioName = null;
	public List<PlayerInfo> playersInfo = new List<PlayerInfo>() {
		new PlayerInfo (0, false, 0, false, 1000 ),
		new PlayerInfo (1, false, 1, false, 1000),
		new PlayerInfo (2, true, 2, false, 1000),
		new PlayerInfo (3, false, 3, false, 1000),
		new PlayerInfo (4, false, 4, false, 1000)};
	
	protected CommonMapScenario scenarioData = new CommonMapScenario ();
	protected readonly LinkedList<ScenarioAction> scenarioActions = new LinkedList<ScenarioAction> ();
	private Timer updateTimer = Timer.CreateFrequency (150);
	
	protected ScenarioFailCondition scenarioFailCondition;
	private ScenarioAction failAction = new ScenarioAction ("failAction");
	protected ScenarioWinCondition scenarioWinCondition;
	private ScenarioAction winningAction = new ScenarioAction ("winningAction");
	
	private bool hasWin = false;
	private bool hasFail = false;
	
	public ScriptedMapScenario () {
		
	}
	
	#region MapScenario implementation
	public void Init (WorldController worldController)
	{
		this.OnCreate (worldController);
		
		scenarioData.Init (worldController);
		updateTimer.ForceTime ();
		
		scenarioFailCondition = new ScenarioFailCondition (this.scenarioData, 1000);
		scenarioWinCondition = new ScenarioWinCondition (this.scenarioData, 1000);
		
		winningAction.SetPerformanseConditions (new ScenarioWinCondition (this.scenarioData));
		failAction.SetPerformanseConditions (new ScenarioFailCondition (this.scenarioData));
		
		this.scenarioActions.AddLast (failAction);
		this.scenarioActions.AddLast (winningAction);
		
		SetGameFailAction (delegate()
		{
			hasFail = true;
		});
		
		SetGameWinAction (delegate()
		{
			hasWin = true;
		});
	}
	
	protected void SetGameFailAction (ScenarioAction.OnActionPerformed onActionPerformed) {
		this.failAction.AddOnPerformedAction (onActionPerformed);
	}
	
	protected void SetGameWinAction (ScenarioAction.OnActionPerformed onActionPerformed) {
		this.winningAction.AddOnPerformedAction (onActionPerformed);
	}
	
	protected virtual void OnCreate (WorldController worldController) {
	}
		
	public virtual void OnStart (WorldController worldController) {
	}
	
	protected virtual void OnUpdateLogic () {
	}	

	public void OnUpdate ()
	{
		if (!updateTimer.EnoughTimeLeft ()) {
			return;
		}
		
		this.OnUpdateLogic ();
		
		foreach (ScenarioAction scenarioAction in scenarioActions) {
			scenarioAction.OnUpdate ();
		}
		
		scenarioData.OnUpdate ();
	}

	public bool HasWin ()
	{
		return hasWin;
	}

	public bool HasFail ()
	{
		return hasFail;
	}

	public Dictionary<int, PlayerInfo> PlayersInfo {
		get {
			Dictionary<int, PlayerInfo> dictionary = scenarioData.PlayersInfo;
			dictionary.Clear ();
			foreach (PlayerInfo player in playersInfo) {
				dictionary.Add (player.id, player);
			}
			return dictionary;
		}
	}
	#endregion
	
	public void SetPlayerInfo (int id, bool isHuman, int teamNumber, bool isAi, string name = "") {
		PlayerInfo player = this.playersInfo[id];
		player.Set (id, isHuman, teamNumber, isAi, name);
		this.playersInfo[id] = player;
	}
	
	public void SetHumanPlayer (int id,  int teamNumber) {
		PlayerInfo player = this.playersInfo[id];
		player.Set (id, true, teamNumber, false, "");
		this.playersInfo[id] = player;
	}
	
	public void SetAiPlayer (int id, int teamNumber) {
		PlayerInfo player = this.playersInfo[id];
		player.Set (id, false, teamNumber, true, "");
		this.playersInfo[id] = player;
	}

	#region MapScenario implementation
	public void Destroy ()
	{
		this.scenarioData.Destroy ();
		this.scenarioActions.Clear ();
		this.playersInfo.Clear ();
	}
	#endregion
	
	#region MapScenario implementation
		public string NextLevelName {
			get {
				if (NextScenarioName != null) {
					return NextScenarioName;
				}
				return GameStarter.MENU_LEVEL_NAME;
			}
		}
		#endregion

	#region MapScenario implementation
	public bool IsNetworkGame {
		get {
			return this.scenarioData.IsNetworkGame;
		}
	}
	#endregion

	#region MapScenario implementation
	public NetworkParams NetworkParams {
		get {
			return scenarioData.NetworkParams;
		}
	}
	
	public string LevelName {
			get {
				return this.scenarioData.LevelName;
			}
			set {
				this.scenarioData.LevelName = value;
			}
		}
	#endregion
}

