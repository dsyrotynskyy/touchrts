using System;

public class Timer
{
	int firstTimeCall = -1;
	int prevTickCount;
	int currentTick;
	
	/*
	 * if timeOfActivity=-1 equals infinity
	 * */
	int timeOfActivity;
	int frequency;
	
	private Timer ()
	{	
		this.firstTimeCall = TWorldTime.CurrentTime;
	}
	
	public static Timer CreateFrequencyWithLife(int frequency, int timeOfActivity) {
		Timer t = new Timer();
		t.frequency = frequency;
		t.timeOfActivity = timeOfActivity;
		return t;
	}
	
	public static Timer CreateRandomizeFrequency (int frequency, int random) {
		int finalFrequency = frequency;
		int offset = Randomizer.Next (random * frequency / 100 * 2) - random * frequency / 100;
		Timer result = CreateFrequencyWithLife(finalFrequency, -1);
		result.firstTimeCall += offset;
		return result;
	}
	
	public static Timer CreateFrequency(int frequency) {
		return CreateFrequencyWithLife(frequency, -1);
	}
	
	public static Timer CreateLifePeriod(int lifePeriod, bool isActive = true) {
		Timer t = CreateFrequencyWithLife(0, lifePeriod);
		if (!isActive) {
			t.Deactivate ();
		}
		return t;
	}
	
	public static Timer CreateEmpty () {
		return CreateFrequencyWithLife(0, 100);
	}
	
	public void SetNewLifePeriod(int lifePeriod) {
		this.frequency = 0;
		this.timeOfActivity = lifePeriod;
		this.Restart ();
	}
	
	public bool EnoughTimeLeft() {
		currentTick = TWorldTime.CurrentTime;	
		int timeOffset = currentTick - firstTimeCall;
		if (timeOfActivity > 0 && timeOffset > timeOfActivity) {
			return false;	
		}
		
		bool enoughTimeLeft = false;
		
		if (currentTick - prevTickCount > frequency) {	
			enoughTimeLeft = true;
			prevTickCount = currentTick;
		}
		
		return enoughTimeLeft;
	}
	
	public float GetFrequencyProgress () {
		currentTick = TWorldTime.CurrentTime;	
		float offset = currentTick - prevTickCount;
		return offset * 100 / frequency;
	}
	
	public void Restart() {
		this.firstTimeCall = TWorldTime.CurrentTime;
		prevTickCount = this.firstTimeCall;
	}
	
	public bool IsActive {
		get {
			if (timeOfActivity < 0) {
				return true;
			}
			currentTick = TWorldTime.CurrentTime;
			int timeOffset = currentTick - firstTimeCall;			
			return timeOffset < timeOfActivity;
		}
	}
	
	public void Deactivate () {
		firstTimeCall = firstTimeCall - timeOfActivity;
	}
	
	public float GetProgress () {
		if (!IsActive) 
		{
			return 0;
		}
		
		currentTick = TWorldTime.CurrentTime;
		int timeOffset = currentTick - firstTimeCall;
		
		return timeOffset * 100 / timeOfActivity;
	}
	
	public void YieldTime ()
	{
		prevTickCount += 2 * frequency + Randomizer.Next(frequency);
	}

	public void ForceTime ()
	{
		prevTickCount -= frequency;
	}
}

public class SystemTimer
{
	int firstTimeCall = -1;
	int prevTickCount;
	int currentTick;
	
	/*
	 * if timeOfActivity=-1 equals infinity
	 * */
	int timeOfActivity;
	int frequensy;
	
	private SystemTimer ()
	{	
		this.firstTimeCall = System.Environment.TickCount;
	}
	
	public static SystemTimer Create(int frequensy, int timeOfActivity) {
		SystemTimer t = new SystemTimer();
		t.frequensy = frequensy;
		t.timeOfActivity = timeOfActivity;
		return t;
	}
	
	public static SystemTimer CreateFrequency(int frequensy) {
		return Create(frequensy, -1);
	}
	
	public static SystemTimer CreateLifePeriod(int lifePeriod) {
		return Create(0, lifePeriod);
	}
	
	public static SystemTimer CreateEmptyLifePeriod () {
		return Create(0, 100);
	}
	
	public void SetNewLifePeriod(int lifePeriod) {
		this.frequensy = 0;
		this.timeOfActivity = lifePeriod;
		this.ResetFirstTimeCall ();
	}
	
	public bool EnoughTimeLeft() {
		currentTick = System.Environment.TickCount;	
		int timeOffset = currentTick - firstTimeCall;
		if (timeOfActivity > 0 && timeOffset > timeOfActivity) {
			return false;	
		}
		
		bool enoughTimeLeft = false;
		
		if (currentTick - prevTickCount > frequensy) {	
			enoughTimeLeft = true;
			prevTickCount = currentTick;
		}
		
		return enoughTimeLeft;
	}
	
	public void ResetFirstTimeCall() {
		this.firstTimeCall = System.Environment.TickCount;
	}
	
	public bool IsActive {
		get {
			if (timeOfActivity < 0) {
				return true;
			}
			currentTick = System.Environment.TickCount;
			int timeOffset = currentTick - firstTimeCall;			
			return timeOffset < timeOfActivity;
		}
	}
	
	public float GetProgress () {
		if (!IsActive) 
		{
			return 0;
		}
		
		currentTick = System.Environment.TickCount;
		int timeOffset = currentTick - firstTimeCall;
		
		return timeOffset * 100 / timeOfActivity;
	}
	
	public void YieldTime ()
	{
		prevTickCount += 2 * frequensy + Randomizer.Next(frequensy);
	}

	public void Restart ()
	{
		this.firstTimeCall = System.Environment.TickCount;
	}
}


