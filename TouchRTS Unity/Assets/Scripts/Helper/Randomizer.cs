using System;

public class Randomizer
{
	static Random random = new Random();
	
	static Randomizer() {
		
	}
	
	public static int Next(int range) {
		return random.Next(range);
	}
	/*
	 * Use this function if min < 0
	 * */
	public static float Next(float range, float min) {
		float value = (random.Next((int)((range - min) * 100)) + min * 100);
		return value / 100;
	}
	
	public static float RandomBetween (float minValue, float maxValue) {
		float randomVal = Next (maxValue - minValue) + minValue;
		return randomVal;
	}
	
	public static float Next(float range) {
		return random.Next(((int)(range)) * 100) / 100;
	}
	
	public static bool NextBool() {
		return random.Next() % 2 == 0;
	}
	
	public Randomizer ()
	{
	}
}


