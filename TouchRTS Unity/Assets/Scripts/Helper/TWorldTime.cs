using System;
using UnityEngine;

public class TWorldTime
{
	private static int time = 0;
	private static bool active = true;
	public static float deltaTime;
	
	public TWorldTime ()
	{
	}
	
	public static void OnUpdate () {
		if (!active) {
			return;
		}
		deltaTime = Time.deltaTime;
		float timeAccum = 1000 * deltaTime;
		time += (int) timeAccum;
	}	
	
	public static bool Active {
		get {
			return active;
		}
		set {
			active = value;
		}
	}

	public static int CurrentTime {
		get {
			return time;
		}
		set {
			time = value;
		}
	}

	public static void OnNewSceneStarted ()
	{
		Active = true;
	}
}


