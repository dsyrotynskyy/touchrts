using System;
using UnityEngine;

public class ObjectFinder
{
	private ObjectFinder ()
	{
	}
	
	public static Transform FindClosest (Transform ourTransform, GameObject[] victims)
	{
		float closestDistance = Mathf.Infinity;

		int targetNumber = 0;

		for (int i = 0; i < victims.Length; i++) {         
			float distance = Vector3.Distance (ourTransform.position, victims [i].transform.position);

			if (distance < closestDistance) {
				closestDistance = distance;
				targetNumber = i;
			}
		}       
		return victims [targetNumber].transform;
	}
}


